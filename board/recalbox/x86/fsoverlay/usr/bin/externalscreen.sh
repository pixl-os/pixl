#!/bin/bash

# layout screen by Strodown
# Updates done for pixL
# 17/05/2023: add new externalscreen for 2 display layout (stro)
# 07/02/2024: updated to manage default value for primary screen (bozo)
#             manage case of "vertical phone screen" as for steam deck (bozo)
#             to manage more cases when secondary screen is not yet set (bozo)
# 02/03/2024: updated also to be able to manage screen independently (we could enabled primary and/or secondary now) (bozo)
#             screen mode(switch, clone or extended) from device conf (bozo)
# 21/03/2024: add management of specific backlight device for primary screen (bozo)
# 25/09/2024: filter better primary/connected screens to configure initially at first boot (bozo)
# 06/10/2024: force "primary" screen from settings to help to filter output to virtualize (bozo)
# 29/10/2024: add management of wall paper on all screens using "externalscreen" script now (bozo)
# 31/12/2024: add management of "virtual screen" disconnected / remove "listOututResolution/OUTPUT_MAXRES" because not used now

export DISPLAY=:0
xrandrOutput=$(xrandr)

## Kill all wall paper/xdotool instances first
pkill -f feh
pkill -f xdotool

## Get info from xrandr
## Show all output and your state ( connected/disconnected )
allOutputs=$(echo "${xrandrOutput}" | awk '/connected/{print $1,$2}')

## Show only connected monitor 
connectedOutputs=$(echo "${allOutputs}" | awk '$2 == "connected"{print $1}')

## Prefered output first screen and force resolution from recalbox.conf
PRIMARY_SCREEN_ENABLED=$(recalbox_settings -command load -key system.primary.screen.enabled)
PRIMARY_SCREEN=$(recalbox_settings -command load -key system.primary.screen)
PRIMARY_SCREEN_RES=$(recalbox_settings -command load -key system.primary.screen.resolution)
PRIMARY_SCREEN_FREQ=$(recalbox_settings -command load -key system.primary.screen.frequency)
PRIMARY_SCREEN_ROTATION=$(recalbox_settings -command load -key system.primary.screen.rotation)

## Prefered output second screen and force resolution from recalbox.conf
SECONDARY_SCREEN_ENABLED=$(recalbox_settings -command load -key system.secondary.screen.enabled)
SECONDARY_SCREEN=$(recalbox_settings -command load -key system.secondary.screen)
SECONDARY_SCREEN_RES=$(recalbox_settings -command load -key system.secondary.screen.resolution)
SECONDARY_SCREEN_FREQ=$(recalbox_settings -command load -key system.secondary.screen.frequency)
SECONDARY_SCREEN_ROTATION=$(recalbox_settings -command load -key system.secondary.screen.rotation)
SECONDARY_SCREEN_POSITION=$(recalbox_settings -command load -key system.secondary.screen.position)

## Get list of virtual screens
#get conf of virtual screens from recalbox.conf
virtual_screens=$(recalbox_settings -command load -key system.video.screens.virtual)
#echo "system.video.screens.virtual=$virtual_screens"
#replace all | by ,
virtual_screens=$(echo "$virtual_screens" | sed "s/|/,/g")
## Configured virtualized screens
# Get list of existing "disconnected" screens to identified as virtual screens
# Use grep to find lines matching the pattern
grep_output=$(echo "${xrandrOutput}" | awk '$2 == "disconnected"{print $1}')
# Loop through each line in the grep output
for line in $grep_output; do
  # Perform actions on each line here
  if [[ "$virtual_screens" =~ "$line" ]]; then
	#add to list of connected output
	connectedOutputs=$(echo -e "$connectedOutputs\n$line")
  fi
done

echo "virtual_screens : $virtual_screens"
echo "List all ouputs and states:"
echo "$allOutputs"
echo "list of connected monitor(s) (including virtual ones):"
echo "$connectedOutputs"
echo "selected in recalbox.conf:"
echo "Primary Screen: $PRIMARY_SCREEN_ENABLED; $PRIMARY_SCREEN; $PRIMARY_SCREEN_RES; $PRIMARY_SCREEN_FREQ; $PRIMARY_SCREEN_ROTATION"
echo "Secondary Screen: $SECONDARY_SCREEN_ENABLED; $SECONDARY_SCREEN; $SECONDARY_SCREEN_RES; $SECONDARY_SCREEN_FREQ; $SECONDARY_SCREEN_ROTATION; $SECONDARY_SCREEN_POSITION"

#check if empty to get default screen
if [ -z "$PRIMARY_SCREEN" ]; then
  #try first to take the connected/primary one
  PRIMARY_SCREEN=$(echo "${xrandrOutput}" | grep -v disconnected | grep -i connected | grep -i primary | awk '{print $1}')
  if [ -z "$PRIMARY_SCREEN" ]; then
    #finally take the first one found connected as primary if we can't find any "primary" finally
    PRIMARY_SCREEN=$(echo "${xrandrOutput}" | grep -v disconnected | grep -i connected | awk '{print $1}' | head -n 1)
  fi
  recallog -s "Externalscreen.sh" -t "primary screen" "save default value"
  recalbox_settings -command save -key system.primary.screen -value "${PRIMARY_SCREEN}"
  if [ -z "$SECONDARY_SCREEN_ENABLED" ] || [ $SECONDARY_SCREEN_ENABLED eq 0 ]; then
    recallog -s "Externalscreen.sh" -t "primary screen" "enable by default"
    recalbox_settings -command save -key system.primary.screen.enabled -value "1"
  fi
fi

#check if empty to get default resolution and save it for first time (from first screen found and connected)
if [ -z "$PRIMARY_SCREEN_RES" ]; then
  PRIMARY_SCREEN_RES=$(echo "${xrandrOutput}" | grep -i "*" | awk '{print $1}' | head -n 1)
  recallog -s "Externalscreen.sh" -t "primary screen resolution" "save default value"
  recalbox_settings -command save -key system.primary.screen.resolution -value "${PRIMARY_SCREEN_RES}"
fi

#check if empty to get default frequency and save it for first time (from first screen found and connected)
if [ -z "$PRIMARY_SCREEN_FREQ" ]; then
  PRIMARY_SCREEN_FREQ=$(echo "${xrandrOutput}" | grep -i "*" | awk '{print $2}' | cut -d "*" -f 1 | head -n 1)
  recallog -s "Externalscreen.sh" -t "primary screen frequency" "save default value"
  recalbox_settings -command save -key system.primary.screen.frequency -value "${PRIMARY_SCREEN_FREQ}"
fi

#check if empty to get default rotation
if [ -z "$PRIMARY_SCREEN_ROTATION" ]; then
  # Detect 'vertical' screen (as "phone" one used on steam deck in 800x1200) for example
  if [ "$(cut -d, -f1 /sys/class/graphics/fb0/virtual_size)" -le "$(cut -d, -f2 /sys/class/graphics/fb0/virtual_size)" ]; then
    recallog -s "Externalscreen.sh" -t "vertical screen" "clockwize rotation needed for X11 !"
    PRIMARY_SCREEN_ROTATION="right"
  else      
    PRIMARY_SCREEN_ROTATION="normal"
  fi
  recallog -s "Externalscreen.sh" -t "primary screen rotation" "save default value"
  recalbox_settings -command save -key system.primary.screen.rotation -value "${PRIMARY_SCREEN_ROTATION}"
fi

## prepare output arguments for Primary Screen (resolution, freq, rotation)
if [[ -n ${PRIMARY_SCREEN_RES} ]] && [[ -n ${PRIMARY_SCREEN_FREQ} ]] && [[ -n ${PRIMARY_SCREEN_ROTATION} ]]; then
  setResolution="--mode ${PRIMARY_SCREEN_RES} --refresh ${PRIMARY_SCREEN_FREQ} --rotate ${PRIMARY_SCREEN_ROTATION}"

elif [[ -n ${PRIMARY_SCREEN_RES} ]] && [[ -n ${PRIMARY_SCREEN_FREQ} ]]; then
  setResolution="--mode ${PRIMARY_SCREEN_RES} --refresh ${PRIMARY_SCREEN_FREQ}"

elif [[ -n ${PRIMARY_SCREEN_RES} ]]; then 
  setResolution="--mode ${PRIMARY_SCREEN_RES}"
fi

## prepare output arguments for Secondary Screen (resolution, freq, rotation)
if [[ -n ${SECONDARY_SCREEN_RES} ]] && [[ -n ${SECONDARY_SCREEN_FREQ} ]] && [[ -n ${SECONDARY_SCREEN_ROTATION} ]]; then
  setSecondResolution="--mode ${SECONDARY_SCREEN_RES} --refresh ${SECONDARY_SCREEN_FREQ} --rotate ${SECONDARY_SCREEN_ROTATION}"

elif [[ -n ${SECONDARY_SCREEN_RES} ]] && [[ -n ${SECONDARY_SCREEN_FREQ} ]]; then
  setSecondResolution="--mode ${SECONDARY_SCREEN_RES} --refresh ${SECONDARY_SCREEN_FREQ}"

elif [[ -n ${SECONDARY_SCREEN_RES} ]]; then
  setSecondResolution="--mode ${SECONDARY_SCREEN_RES}"
fi

## Check primary screen enabled
if [[ "${PRIMARY_SCREEN_ENABLED}" == "1" ]]; then
  ## Test if primary screen is connected
  if [[ -n "${PRIMARY_SCREEN}" ]]; then
    PRIMARY_SCREEN_OUTPUT=$(echo "${connectedOutputs}" | grep "${PRIMARY_SCREEN}" | awk 'BEGIN {FS=";"} {print $1}')
    if [[ -n "${PRIMARY_SCREEN}" ]] && [[ -n "${PRIMARY_SCREEN_OUTPUT}" ]]; then
      PRIMARY_SCREEN_CONNECTED="${PRIMARY_SCREEN_OUTPUT}"
    fi
  fi
  #restore backlight brightness level from recalbox.conf if needed
  /recalbox/system/hardware/device/pixl-backlight.sh restore_brightness
else
  #set backlight brightness level to 0 (but not saved in recalbox.conf in this case to be able to restore later)
  /recalbox/system/hardware/device/pixl-backlight.sh brightness 0
fi

## Check secondary screen enabled
if [[ "${SECONDARY_SCREEN_ENABLED}" == "1" ]]; then
  ## Test if secondary screen is connected
  if [[ -n "${SECONDARY_SCREEN}" ]]; then
    SECONDARY_SCREEN_OUTPUT=$(echo "${connectedOutputs}" | grep "${SECONDARY_SCREEN}" | awk 'BEGIN {FS=";"} {print $1}')
	if [[ -n "${SECONDARY_SCREEN}" ]] && [[ -n "${SECONDARY_SCREEN_OUTPUT}" ]]; then
      SECONDARY_SCREEN_CONNECTED="${SECONDARY_SCREEN_OUTPUT}"
	fi
  fi
fi

## List all output position
declare -A position=( ["above"]="--above" \
                      ["below"]="--below" \
                      ["left"]="--left-of" \
                      ["right"]="--right-of"
                    )

# initialize variables
xrandr_cmd="xrandr "

# build xrandr_cmd configuration
## set 2 monitors
if [[ -n ${PRIMARY_SCREEN_CONNECTED} ]] && [[ -n ${SECONDARY_SCREEN_CONNECTED} ]]; then
  videomode=$(recalbox_settings -command load -key system.video.screens.mode)
  if [[ $videomode = "clone" ]]; then
    ## we will clone the first one to the second one (doesn't work with all emulator)
	## example : xrandr --output eDP --mode 800x1280 --refresh 60.00 --rotate right --scale 1.5x1.35 --output DisplayPort-0 --same-as eDP --mode 1920x1080 --scale 1x1
    ## need to calculate ratio betweent the 2 resolutions of each screen
    #split resolution to have x and y value
	PRIMARY_SCREEN_RES_SPLIT=(${PRIMARY_SCREEN_RES//x/ })
	SECONDARY_SCREEN_RES_SPLIT=(${SECONDARY_SCREEN_RES//x/ })
	#x_ratio=$(( ${SECONDARY_SCREEN_RES_SPLIT[0]} / ${PRIMARY_SCREEN_RES_SPLIT[0]} ))
	#y_ratio=$(( ${SECONDARY_SCREEN_RES_SPLIT[1]} / ${PRIMARY_SCREEN_RES_SPLIT[1]} ))
	#echo ${SECONDARY_SCREEN_RES_SPLIT[0]} / ${PRIMARY_SCREEN_RES_SPLIT[0]}
	#echo ${SECONDARY_SCREEN_RES_SPLIT[1]} / ${PRIMARY_SCREEN_RES_SPLIT[1]}
	if [[ ${PRIMARY_SCREEN_RES_SPLIT[0]} -gt ${PRIMARY_SCREEN_RES_SPLIT[1]} ]]; then 
		x_ratio=$(echo ${SECONDARY_SCREEN_RES_SPLIT[0]} / ${PRIMARY_SCREEN_RES_SPLIT[0]} | bc -l)
		y_ratio=$(echo ${SECONDARY_SCREEN_RES_SPLIT[1]} / ${PRIMARY_SCREEN_RES_SPLIT[1]} | bc -l)
	else # to manage vertical primary screen as on some "handheld" pc
		x_ratio=$(echo ${SECONDARY_SCREEN_RES_SPLIT[0]} / ${PRIMARY_SCREEN_RES_SPLIT[1]} | bc -l)
		y_ratio=$(echo ${SECONDARY_SCREEN_RES_SPLIT[1]} / ${PRIMARY_SCREEN_RES_SPLIT[0]} | bc -l)
	fi
	## --same-as to clone from a specific one
	## --scale to adapt first screen to second one
	##xrandr_cmd=${xrandr_cmd}"--output ${PRIMARY_SCREEN_CONNECTED} ${setResolution} --scale 1.5x1.35 --output ${SECONDARY_SCREEN_CONNECTED} --same-as ${PRIMARY_SCREEN_CONNECTED} ${setSecondResolution} --scale 1x1"
	##xrandr_cmd=${xrandr_cmd}"--output ${SECONDARY_SCREEN_CONNECTED} ${setSecondResolution} --output ${PRIMARY_SCREEN_CONNECTED} --same-as ${SECONDARY_SCREEN_CONNECTED} ${setResolution} --scale-from ${SECONDARY_SCREEN_RES}"
	xrandr_cmd=${xrandr_cmd}"--output ${SECONDARY_SCREEN_CONNECTED} ${setSecondResolution} --output ${PRIMARY_SCREEN_CONNECTED} ${setResolution} --same-as ${SECONDARY_SCREEN_CONNECTED} --scale ${x_ratio}x${y_ratio}"
  
  else # for video mode "extended" (default mode)
    xrandr_cmd=${xrandr_cmd}"--output ${PRIMARY_SCREEN_CONNECTED} ${setResolution} --primary --output ${SECONDARY_SCREEN_CONNECTED} ${setSecondResolution} ${position[${SECONDARY_SCREEN_POSITION}]} ${PRIMARY_SCREEN_CONNECTED}"
  fi
## set only selected primary screen
## enable first screen and disable second screen
elif [[ -n ${PRIMARY_SCREEN_CONNECTED} ]] && [[ -z ${SECONDARY_SCREEN_CONNECTED} ]] && [[ -n ${SECONDARY_SCREEN} ]]; then
  xrandr_cmd=${xrandr_cmd}"--output ${PRIMARY_SCREEN_CONNECTED} ${setResolution} --primary --transform none --output ${SECONDARY_SCREEN} --off"

## set only selected secondary screen
## enable second screen and disable first screen
elif [[ -n ${SECONDARY_SCREEN_CONNECTED} ]] && [[ -z ${PRIMARY_SCREEN_CONNECTED} ]] && [[ -n ${PRIMARY_SCREEN} ]]; then
  xrandr_cmd=${xrandr_cmd}"--output ${SECONDARY_SCREEN_CONNECTED} ${setSecondResolution} --primary --transform none --output ${PRIMARY_SCREEN} --off"

## enable first screen only but we don't know a secondary one in this case
elif [[ -n ${PRIMARY_SCREEN_CONNECTED} ]] && [[ -z ${SECONDARY_SCREEN_CONNECTED} ]] && [[ -z ${SECONDARY_SCREEN} ]]; then
  xrandr_cmd=${xrandr_cmd}"--output ${PRIMARY_SCREEN_CONNECTED} ${setResolution} --primary --transform none"

# disable all uneeded monitors and set default if no monitor connected
elif [[ -z ${PRIMARY_SCREEN_CONNECTED} ]] && [[ -z ${SECONDARY_SCREEN_CONNECTED} ]]; then
  firstConnected=$(echo "${connectedOutputs}" | head -n 1)
  xrandr_cmd=${xrandr_cmd}"--output ${firstConnected} ${setResolution} --primary"
  recallog -s "Externalscreen.sh" -t "primary screen" "save default value"
  recalbox_settings -command save -key system.primary.screen -value "${firstConnected}"
fi

## display xrandr_cmd content and execute it
echo "Command: $xrandr_cmd"
## Launch cmd 
`$xrandr_cmd`

## Now manage also wall paper here to see if any screen is activated or not
## For primary screen
if [[ -n ${PRIMARY_SCREEN_CONNECTED} ]]; then
  # Launch a wall paper
  feh "/recalbox/system/resources/splash/logo-1-pixl1080p.png" --zoom fill --scale-down &
  #wait one 1/2 second to be sure that is launch
  sleep 0.5
  COORDINATE=$(xrandr | grep -e "${PRIMARY_SCREEN} " | sed 's/primary//' | awk '{print $3}' | awk -F'+' '{print $2, $3}')
  (WINDOWPID=$(xdotool search --sync --class "feh" | head -1 ); sleep 0.5; xdotool windowmove $WINDOWPID $COORDINATE;)&
fi
## Now we manage second one to ahve also a wall paper here to see if a screen is activated or not
## move secondary screen (if connected)
if [[ -n ${SECONDARY_SCREEN_CONNECTED} ]]; then
  # Launch a second wall paper 
  feh "/recalbox/system/resources/splash/logo-1-pixl1080p.png" --zoom fill --scale-down &
  #wait one 1/2 second to be sure that is launch
  sleep 0.5
  COORDINATE=$(xrandr | grep -e "${SECONDARY_SCREEN} " | sed 's/primary//' | awk '{print $3}' | awk -F'+' '{print $2, $3}')
  #take second one
  (WINDOWPID=$(xdotool search --sync --class "feh" | tail -1 ); sleep 0.5; xdotool windowmove $WINDOWPID $COORDINATE;)&
fi

