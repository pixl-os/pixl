#!/bin/sh

NAME=thd
DAEMON=/usr/sbin/$NAME
PIDFILE=/var/run/$NAME.pid
LOCKFILE=/var/run/$NAME.lock
DAEMON_ARGS="--daemon --triggers /etc/triggerhappy/triggers.d --socket /var/run/thd.socket --pidfile $PIDFILE /dev/input/event*"
BASEFILE=$(basename "$0")

# Sanity checks
test -x $DAEMON || exit 0

[ -r /etc/default/triggerhappy ] && . /etc/default/triggerhappy

start() {
        printf "Starting $NAME: "
        start-stop-daemon --start --quiet --pidfile $PIDFILE --exec $DAEMON -- $DAEMON_ARGS \
                && echo "OK" || echo "FAIL"
}

stop() {
        printf "Stopping $NAME: "
        start-stop-daemon --stop --quiet --pidfile $PIDFILE \
                && echo "OK" || echo "FAIL"
}

case "$1" in
        start)
                start
                ;;
        stop)
                stop
                ;;
        restart)
                if (! test -e "${LOCKFILE}" ); then
                    echo "restart locked"
                    echo "restart locked" > $LOCKFILE
                    stop
                    sleep 2
                    THD=$(ps aux | grep -i thd | grep -c /usr/sbin | tr -d '%' | tr -d '[:space:]')
                    if [ ! $THD -eq 0 ] ; then
                        echo "process to kill"
                        pkill thd
                        sleep 1
                    fi
                    start
                    rm $LOCKFILE
                    echo "restart unlocked"
                else
                    echo "restart already locked - aborted"
                fi
                ;;
        *)
                echo "Usage: $0 {start|stop|restart}"
                exit 1
esac
