#!/usr/bin/env python3
import sys
import pygame
import evdev

### Main ###
specific_device_path = ""
print("Number of parameters : " + str(len(sys.argv)-1))
if len(sys.argv) > 3:
    weak_magnitude = float(int(sys.argv[1])/100)
    strong_magnitude = float(int(sys.argv[2])/100)
    duration = int(sys.argv[3])
    if len(sys.argv) > 4:
        specific_device_path = sys.argv[4]

    # Initialize pygame
    pygame.init()

    # Initialize joystick
    pygame.joystick.init()

    # Get count of joysticks.
    joystick_count = pygame.joystick.get_count()
    print(f"Number of joysticks: {joystick_count}")

    # Create a list to store joystick objects
    joysticks = []
    devices = {}
    # Use evdev to get the device path
    devices = [evdev.InputDevice(path) for path in evdev.list_devices()]
    # Iterate through each joystick and create an object
    for i in range(joystick_count):
        # For all joystick
        joysticks.append(pygame.joystick.Joystick(i))
        # Init selected joystick
        joysticks[i].init()
        # Launch rumble (for all or specific device)
        if(specific_device_path == ""):
            joysticks[i].rumble(weak_magnitude, strong_magnitude, duration) # Adjust magnitudes and duration as needed
        else:
            for device in devices:
                print(device.path + " ?= " + specific_device_path)
                print(joysticks[i].get_name() + "?=" + device.name)
                print(f"Device capabilities: {device.capabilities()}")
                if (device.path == specific_device_path) and (joysticks[i].get_name() == device.name):
                    print(f"Device path found : {device.path} for device name: {device.name}")
                    joysticks[i].rumble(weak_magnitude, strong_magnitude, duration) # Adjust magnitudes and duration as needed
                    break
    # Delay to let rumble execution
    pygame.time.delay(duration)

    # Exit pygame
    pygame.quit()

else:
    print("missing parameter(s), need to define :")
    print("    weak_magnitude (0 to 100%)")
    print("    strong_magnitude (0 to 100%)")
    print("    duration (in ms)")
    print("    specific_device_path (optional else all will rumble)")

