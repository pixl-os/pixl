#!/bin/sh

GTMP="/tmp"
DHOME="/recalbox/share/system"

# to be callable by any external tool
if test $# -eq 1
then
    REPORTNAME=$(basename "$1" | sed -e s+'^\([^.]*\)\..*$'+'\1'+)
    OUTPUTFILE=$1
else
    REPORTNAME="recalbox-support-"$(date +%Y%m%d%H%M%S)
    OUTPUTFILE="/recalbox/share/saves/${REPORTNAME}.tar.gz"
fi

TMPDIR="${GTMP}/${REPORTNAME}"

f_cp() {
    test -e "$1" && cp "$1" "$2"
}

d_cp() {
    test -d "$1" && cp -pr "$1" "$2"
}


if mkdir "${TMPDIR}" && mkdir "${TMPDIR}/"{info,boot,recalbox,bios,cheats,overlays,shaders,videos,share_themes,share_init_themes,system,theme_settings,.pegasus-frontend,var,joysticks}
then
    if ! cd "${TMPDIR}"
    then
	echo "Change directory failed" >&2
	exit 1
    fi
else
    echo "Reporting directory creation failed" >&2
    exit 1
fi

export DISPLAY=:0.0

# INFO
DINFO="${TMPDIR}/info"
dmesg                                                                               > "${DINFO}/dmesg.txt"
lsmod                                                                               > "${DINFO}/lsmod.txt"
ps                                                                                  > "${DINFO}/ps.txt"
df -h                                                                               > "${DINFO}/df.txt"
netstat -tuan                                                                       > "${DINFO}/netstat.txt"
lsusb -v                                                                            > "${DINFO}/lsusb.txt" 2>/dev/null
tvservice -m CEA                                                                    > "${DINFO}/tvservice-CEA.txt"
tvservice -m DMT                                                                    > "${DINFO}/tvservice-DMT.txt"
tvservice -s                                                                        > "${DINFO}/tvservice-status.txt"
ifconfig -a                                                                         > "${DINFO}/ifconfig.txt"
ip route get 1.2.3.4 | awk '{print $7}' | tr -d '\n' | tr -d '\r'                   > "${DINFO}/iproute.txt"
lspci                                                                               > "${DINFO}/lspci.txt"
amixer                                                                              > "${DINFO}/amixer.txt"
aplay -l                                                                            > "${DINFO}/aplay-l.txt"
DISPLAY=:0.0 glxinfo                                                                > "${DINFO}/glxinfo.txt"
DISPLAY=:0.0 xrandr                                                                 > "${DINFO}/xrandr.txt"
DISPLAY=:0.0 xrandr -q                                                              > "${DINFO}/xrandr-q.txt"
DISPLAY=:0.0 vulkaninfo                                                             > "${DINFO}/vulkaninfo.txt"
mount                                                                               > "${DINFO}/mount.txt"
blkid                                                                               > "${DINFO}/blkid.txt"
hciconfig                                                                           > "${DINFO}/hciconfig.txt"
wpctl status																		> "${DINFO}/wireplumber.txt"
connmanctl technologies																> "${DINFO}/connman-technologies.txt"
connmanctl services																	> "${DINFO}/connman-services.txt"
pacmd list-sinks  | grep -e 'alsa.card_name =' -e 'alsa.id =' -e 'available:'       > "${DINFO}/pacmd.txt"
for file in /sys/class/drm/card*/edid; do
    echo "$file ->"
    base64 "$file"
    echo -n "status: "
    cat "${file//edid/status}"
    echo -n "enabled?: "
    cat "${file//edid/enabled}"
    echo "modes: "
    cat "${file//edid/modes}"
    echo "----------------------------------------------------------------------------"
done > "${DINFO}/edid.txt"
d_cp /recalbox/share/system/logs                                                    "${DINFO}"
d_cp /recalbox/share/system/.config/lirc                                            "${DINFO}"
d_cp /recalbox/share/system/.config/pegasus-frontend                                "${DINFO}"
d_cp /recalbox/share/system/.pegasus-frontend                                       "${DINFO}"

# /recalbox/share/roms/
find /recalbox/share/roms/ -type f \( ! -iname "*.txt" ! -iname "*.xml" ! -iname "*.png" ! -iname "*.jpg" ! -iname "*.dat" \) ! -path "*/data/*" | wc -l > "${DINFO}/approxnbroms.txt"

# Recalbox intros
ls -l /recalbox/system/resources/splash/ | tail -n +2 | awk '{print $9, "-", $5}'   > "${DINFO}/pixl_intros.txt"

# Update logs
d_cp "${DHOME}/upgrade" "${DINFO}"
find "${DINFO}/upgrade" -type f ! -name "*upgrade*" | xargs rm

# Emulators configs
test -d "${DHOME}/configs" && rsync -a "${DHOME}/configs" "${TMPDIR}" --exclude "ppsspp/PSP/SAVEDATA" --exclude "ppsspp/PSP/PPSSPP_STATE" --exclude "retroarch/overlays"

# pulseaudio
PA_CMDS="list-modules list-sinks list-sources list-clients list-cards list-samples stat dump"
for PA_CMD in $PA_CMDS; do
    echo "=========== $PA_CMD ============" >>"${DINFO}/pulseaudio.txt"
    pacmd "$PA_CMD" >>"${DINFO}/pulseaudio.txt" 2>&1
done

# /boot/
DBOOT="${TMPDIR}/boot"
f_cp /boot/recalbox-boot.conf                                                       "${DBOOT}"
f_cp /boot/hardware.log                                                             "${DBOOT}"
f_cp /boot/config.txt                                                               "${DBOOT}"
f_cp /boot/recalbox-user-config.txt                                                 "${DBOOT}"

# /recalbox/
DRECALBOX="${TMPDIR}/recalbox"
f_cp /recalbox/recalbox.version                                                     "${DRECALBOX}"
f_cp /recalbox/recalbox.arch                                                        "${DRECALBOX}"

# /recalbox/share/bios
DBIOS="${TMPDIR}/bios"
ls -1 -R /recalbox/share/bios                                                       > "${DBIOS}/bios.txt"

# /recalbox/share/cheats
DCHEATS="${TMPDIR}/cheats"
ls -1 -R /recalbox/share/cheats                                                     > "${DCHEATS}/cheats.txt"

# /recalbox/share/overlays
DOVERLAYS="${TMPDIR}/overlays"
ls -1 -R /recalbox/share/overlays                                                   > "${DOVERLAYS}/overlays.txt"

# /recalbox/share/shaders
DSHADERS="${TMPDIR}/shaders"
ls -1 -R /recalbox/share/shaders                                                    > "${DSHADERS}/shaders.txt"

# /recalbox/share/videos
DVIDEOS="${TMPDIR}/videos"
ls -1 -R /recalbox/share/videos                                                     > "${DVIDEOS}/videos.txt"

# /recalbox/share/themes
DSHARETHEME="${TMPDIR}/share_themes"
ls -1 /recalbox/share/themes                                                        > "${DSHARETHEME}/share_themes.txt"

# /recalbox/share_init/themes
DSHAREINITTHEME="${TMPDIR}/share_init_themes"
ls -1 /recalbox/share_init/themes                                                   > "${DSHAREINITTHEME}/share_init_themes.txt"

# /recalbox/share/system/
DSYSTEM="${TMPDIR}/system"
f_cp /recalbox/share/system/recalbox.conf                                           "${DSYSTEM}"

# /recalbox/share/system/.config/pegasus-frontend/theme_settings
DSHARETHEMES="${TMPDIR}/theme_settings"
ls -1 /recalbox/share/system/.config/pegasus-frontend/theme_settings                > "${DSHARETHEMES}/theme_settings.txt"

# /recalbox/share_init/system/.pegasus-frontend/
DSHAREINITSYSTEMPF="${TMPDIR}/.pegasus-frontend"
f_cp /recalbox/share_init/system/.pegasus-frontend/systemlist.xml                   "${DSHAREINITSYSTEMPF}"

# /var/log/
DVAR="${TMPDIR}/var"
d_cp /var/log                                                    					"${DVAR}"

# joysticks
DJOYS="${TMPDIR}/joysticks"
find /dev/input																		> "${DJOYS}/inputs.txt"
for J in /dev/input/event*
do
    N=$(basename ${J})
    evtest --info "${J}"          > "${DJOYS}/evtest.${N}.txt"
    udevadm info -q all -n "${J}" > "${DJOYS}/udevadm.${N}.txt"
done
sdl2-jstest -l > "${DJOYS}/sdl2-jstest.txt"
cp "/var/run/sinden/p"*"/sinden-"*".log" "${DJOYS}" 2>/dev/null

if ! (cd "${GTMP}" && tar cf -  "${REPORTNAME}" | gzip -c > "${OUTPUTFILE}")
then
    echo "Reporting zip creation failed" >&2
    exit 1
fi

rm -rf "${TMPDIR}"
echo "${OUTPUTFILE}"
exit 0
