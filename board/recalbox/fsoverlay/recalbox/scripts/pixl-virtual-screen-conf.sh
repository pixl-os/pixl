#!/bin/bash
# the goal of this script is to configure X11/Xorg from parameters and gpu used

add_modeline() {
	#echo "add_modeline()"
	local modeline name
	modeline="$(gtf "$2" "$3" "$4" | sed -n 's/.*Modeline "\([^" ]\+\)" \(.*\)/\1 \2/p')"
	#echo "modeline = ${modeline}"
	modeline=$(echo "$modeline" | sed 's/_[0-9.]*//')
	#echo "newmodeline = ${modeline}"
	name="$(echo "${modeline}" | sed 's/\([^ ]\+\) .*/\1/')"
	#echo "name = ${name}"
	xrandr --newmode ${modeline} 2>/dev/null
	xrandr --addmode "$1" "${name}" 2>/dev/null
}
add_all_modeline() {
	#we could add modeline to each virtualized screen(s)
	#4K/UHD
	add_modeline $1 3840 2160 60
	#1440p/QHD
	add_modeline $1 2560 1440 60
	#1080p/Full HD
	add_modeline $1 1920 1080 60
	#720p/Full HD
	add_modeline $1 1280 720 60
	#for wii u gamepad ;-)
	add_modeline $1 854 480 60
	#retro resolution (like DOS/WIN GAMES)
	add_modeline $1 1024 768 60
	add_modeline $1 640 480 60
	#retro resolution (like NES, SNES...INTERLACED OR NOT)
	# add_modeline $1 512 448 60
	# add_modeline $1 512 224 60
	# add_modeline $1 320 480 60
	# add_modeline $1 320 448 60
	add_modeline $1 320 240 60
	# add_modeline $1 320 224 60
	# add_modeline $1 320 200 60
	# add_modeline $1 256 480 60
	# add_modeline $1 256 448 60
	# add_modeline $1 256 240 60
	# add_modeline $1 256 239 60
	# add_modeline $1 256 224 60
}

rm_modeline() {
	#echo "rm_modeline()"
	local modeline name
	modeline="$(gtf "$2" "$3" "$4" | sed -n 's/.*Modeline "\([^" ]\+\)" \(.*\)/\1 \2/p')"
	#echo "rm_modeline : modeline = ${modeline}"
	modeline=$(echo "$modeline" | sed 's/_[0-9.]*//')
	#echo "rm_modeline : newmodeline = ${modeline}"
	name="$(echo "${modeline}" | sed 's/\([^ ]\+\) .*/\1/')"
	#echo "rm_modeline : name = ${name}"
	xrandr --delmode "$1" "${name}" 2>/dev/null
	xrandr --rmmode "${name}" 2>/dev/null
}

rm_all_modeline() {
	#we have to remove existing modeline to each "disconnected" screen(s)
	#4K/UHD
	rm_modeline $1 3840 2160 60
	#1440p/QHD
	rm_modeline $1 2560 1440 60
	#1080p/Full HD
	rm_modeline $1 1920 1080 60
	#720p/Full HD
	rm_modeline $1 1280 720 60
	#for wii u gamepad ;-)
	rm_modeline $1 854 480 60
	#retro resolution (like DOS/WIN GAMES)
	rm_modeline $1 1024 768 60
	rm_modeline $1 640 480 60
	#retro resolution (like NES, SNES...INTERLACED OR NOT)
	# rm_modeline $1 512 448 60
	# rm_modeline $1 512 224 60
	# rm_modeline $1 320 480 60
	# rm_modeline $1 320 448 60
	rm_modeline $1 320 240 60
	# rm_modeline $1 320 224 60
	# rm_modeline $1 320 200 60
	# rm_modeline $1 256 480 60
	# rm_modeline $1 256 448 60
	# rm_modeline $1 256 240 60
	# rm_modeline $1 256 224 60
}

populate_virtual_output(){
	## Configured virtualized screens
	# Get list of existing "disconnected" screens to identified as virtual screens (case for amd and intel)
	# Use grep to find lines matching the pattern
	disconnected_outputs=$(cat /tmp/xrandr.tmp | grep " disconnected" | awk '{print $1}')
	if [[ "$1" == "intel" ]]; then
		#for VIRTUAL output only (case for intel for the moment)
		virtual_outputs=$(cat /tmp/xrandr.tmp | grep "VIRTUAL" | awk '{print $1}')
		# Loop through each line in the grep output
		for line in $virtual_outputs; do
		  # Perform actions on each line here
		  if [[ "$virtual_screens" =~ "$line" ]]; then
			if [[ "$disconnected_outputs" =~ "$line" ]]; then
				add_all_modeline $line
			fi
			#do nothing if already "connected"
		  else 
			if ! [[ "$disconnected_outputs" =~ "$line" ]]; then
				rm_all_modeline $line
			fi
		  fi
		done
	else #for amd
		# Loop through each line in the grep output
		for line in $disconnected_outputs; do
		  # Perform actions on each line here
		  if [[ "$virtual_screens" =~ "$line" ]]; then
				add_all_modeline $line
		  else 
				rm_all_modeline $line
		  fi
		done
	fi
}

BASEFILE=$(basename "$0")
systemsetting="recalbox_settings"

# Check if the 'lspci' command is available
if ! command -v lspci &> /dev/null; then
    echo "lspci command not found. Please install it."
    exit 1
fi

#set xrandr tmp file if missing
if ! [ -f /tmp/xrandr.tmp ]; then
	xrandr > /tmp/xrandr.tmp
fi

#get conf of virtual screens from recalbox.conf
virtual_screens="`$systemsetting -command load -key system.video.screens.virtual`"
#echo "system.video.screens.virtual=$virtual_screens"

#replace all | by ,
#virtual_screens=$(echo "$virtual_screens" | sed "s/|/,/g")
#echo "virtual_screens=$virtual_screens (modified)"

# Get the output of lspci and search for GPU-related keywords
# Get the driver used
if ! [ -f /tmp/gpuinfo.tmp ]; then
	lspci | grep -i "VGA" > /tmp/gpuinfo.tmp
fi
gpu_info=$(cat /tmp/gpuinfo.tmp)
echo "gpu_info: $gpu_info"

# Get the driver used
if ! [ -f /tmp/glxinfo.tmp ]; then
	glxinfo -B > /tmp/glxinfo.tmp
fi
video_driver=$(cat /tmp/glxinfo.tmp | grep -i "OpenGL vendor string" | head -n 1 | awk -F":" '{print $2}')
echo "video_driver: $video_driver"

# example to check if nouveau installed

## lsmod | grep nouveau
# nouveau              2871296  4
# drm_ttm_helper         12288  1 nouveau
# ttm                    98304  2 drm_ttm_helper,nouveau
# drm_display_helper    200704  1 nouveau
# drm_kms_helper        253952  4 drm_display_helper,nouveau
# i2c_algo_bit           12288  2 nvidiafb,nouveau
# video                  69632  1 nouveau
# wmi                    36864  2 video,nouveau

# OR nvidia one

## lsmod | grep nouveau
## lsmod | grep nvidia
# nvidia_drm            118784  3
# nvidia_modeset       1605632  6 nvidia_drm
# nvidia              60489728  99 nvidia_modeset
# drm_kms_helper        253952  2 drm_display_helper,nvidia_drm
# nvidiafb               61440  0
# vgastate               16384  1 nvidiafb
# fb_ddc                 12288  1 nvidiafb
# i2c_algo_bit           12288  1 nvidiafb
# video                  69632  1 nvidia_modeset

# Check for NVIDIA
if [[ $gpu_info =~ (nvidia|NVIDIA|Nvidia) ]]; then
    echo "GPU type: NVIDIA"
    #if proprietary driver used
    if [[ $video_driver =~ (nvidia|NVIDIA|Nvidia) ]]; then
		echo "Driver: nvidia"
		#activate access to file system
		mount -o remount,rw /
		if [[ -n "$virtual_screens" ]]; then
			# configuration for NVIDIA case
			#get list of existing connected screens to merge with virtual screens
			# Use grep to find lines matching the pattern
			grep_output=$(cat /tmp/xrandr.tmp | awk '$2 == "connected"{print $1}')
			# Loop through each line in the grep output
			for line in $grep_output; do
			  # Perform actions on each line here
			  if [[ "$virtual_screens" =~ "$line" ]]; then
				echo "$line is present in $virtual_screens."
			  else
				echo "$line is not present in $virtual_screens."
				#check if a screen is really connected to detect that is not already a virtual one
				edid=$(xrandr --prop | grep -m1 -A100 "$line" | grep -m2 -B100000 "connected" | grep EDID:)
				if [[ "$edid" =~ "EDID" ]]; then
					#it seems a real one because EDID is provided
					echo "$line seems a 'real' one connected to a screen"
					#add connected screen to virtual screens
					virtual_screens="$virtual_screens,$line"
				fi
			fi
			done
			echo "virtual_screens=$virtual_screens (modified including connected)"
			#copy template for nvida gpu as new xorg.conf
			cp /etc/X11/xorg.conf.virtual.nvidia.template /etc/X11/xorg.conf
			#replace TAG "#SCREENS#" from template
			#line before: Option "ConnectedMonitor" "#SCREENS#"
			#example of line after: Option "ConnectedMonitor" "HDMI-0,DP-0"
			sed -i "s/#SCREENS#/$virtual_screens/g" /etc/X11/xorg.conf
		else
			#no virtual device requested to add
			#remove configuration file if exist
			rm /etc/X11/xorg.conf
		fi
		#restore file system read only
		mount -o remount,ro /
    else # other as nouveau
		echo "Driver: Nouveau"
    fi
elif [[ $gpu_info =~ (intel|INTEL|Intel) ]]; then
    echo "GPU type: INTEL"
	#if proprietary driver used
    if [[ $video_driver =~ (intel|INTEL|Intel) ]]; then
	    echo "Driver: intel"
		populate_virtual_output intel
	fi
elif [[ $gpu_info =~ (amd|AMD|Amd) ]]; then
    echo "GPU type: AMD"
	#if proprietary driver used
    if [[ $video_driver =~ (amd|AMD|Amd) ]]; then
	    echo "Driver: amd"
		populate_virtual_output amd
	fi
else
    echo "GPU type could not be determined."
    exit 1
fi
exit 0
