#!/bin/bash

source /recalbox/scripts/recalbox-utils.sh
IMAGE_PATH=$(getInstallUpgradeImagePath)
INIT_SCRIPT=$(basename "$0")

UPDATEDIR=/boot/update
SHAREUPDATEDIR=/recalbox/share/system/upgrade

SCREEN_ORIENTATION=""

clean_boot_update() {
  mount -o remount,rw /boot
  rm -rf "$UPDATEDIR"/* .*.
  mount -o remount,ro /boot
}

clean_share_upgrade() {
  mount -o remount,rw /
  rm -rf "$SHAREUPDATEDIR"/* .*.
  mount -o remount,ro /
}

do_update() {

  #Start update...
  python /recalbox/scripts/image_modifier.py "Start update..." $IMAGE_PATH/update-7-pixl.png 1147 830 "/usr/share/fonts/dejavu/DejaVuSans.ttf" 28 "/tmp/output.png" ${SCREEN_ORIENTATION} yes
  sleep 1

  UPDATEFILE="pixl_no_upgrade"

  for file in /recalbox/share/system/upgrade/pixl-*.img; do
    if [[ -f $file ]]; then
      UPDATEFILE=${file}
      break
    fi
  done

  # Mount image...
  echo "Mount image..."
  python /recalbox/scripts/image_modifier.py "Mount image..." $IMAGE_PATH/update-8-pixl.png 1147 830 "/usr/share/fonts/dejavu/DejaVuSans.ttf" 28 "/tmp/output.png" ${SCREEN_ORIENTATION} yes
  sleep 1
  LBA=$(lba-finder "${UPDATEFILE}")
  echo "intiating a losetup for lba ${LBA} (offset $((LBA * 512))s)"
  LOOPFILE=$(losetup -f --show "${UPDATEFILE}" -o $((LBA * 512))) || return 2
  mount "$LOOPFILE" /mnt || return 3

  # Remounting /boot R/W
  echo "Remounting /boot R/W..."
  python /recalbox/scripts/image_modifier.py "Remounting /boot R/W..." $IMAGE_PATH/update-9-pixl.png 1147 830 "/usr/share/fonts/dejavu/DejaVuSans.ttf" 28 "/tmp/output.png" ${SCREEN_ORIENTATION} yes
  sleep 1
  mount -o remount,rw /boot/ || return 4

  # Files copy
  echo "Copying update boot files..."
  python /recalbox/scripts/image_modifier.py "Copying update boot files..." $IMAGE_PATH/update-10-pixl.png 1147 830 "/usr/share/fonts/dejavu/DejaVuSans.ttf" 28 "/tmp/output.png" ${SCREEN_ORIENTATION} yes
  sleep 1
  if [ -f /mnt/boot.lst ]; then
    while read -r file; do
      echo "  processing $file"
      #next line deactivated due to performance impact during update
      #python /recalbox/scripts/image_modifier.py "Processing $file" $IMAGE_PATH/update-10-pixl.png 1147 830 "/usr/share/fonts/dejavu/DejaVuSans.ttf" 28 "/tmp/output.png" ${SCREEN_ORIENTATION} yes
      [[ ! -d /boot/update/$(dirname "$file") ]] && mkdir -p "/boot/update/$(dirname "$file")"
      cp "/mnt/$file" "/boot/update/$file" || return 5
    done < /mnt/boot.lst
    cp /mnt/boot.lst /boot/update/ || return 6
  else
    return 7
  fi
  python /recalbox/scripts/image_modifier.py "Copy update boot files done." $IMAGE_PATH/update-11-pixl.png 1147 830 "/usr/share/fonts/dejavu/DejaVuSans.ttf" 28 "/tmp/output.png" ${SCREEN_ORIENTATION} yes
  sleep 1

  # Umount
  echo "Unmount image..."
  umount /mnt/ || return 8
  python /recalbox/scripts/image_modifier.py "Unmount image..." $IMAGE_PATH/update-12-pixl.png 1147 830 "/usr/share/fonts/dejavu/DejaVuSans.ttf" 28 "/tmp/output.png" ${SCREEN_ORIENTATION} yes
  sleep 1

  return 0

}

log() {
  while read -r line; do
    /usr/bin/recallog -s "${INIT_SCRIPT}" -t "UPGRADE" -f upgrade.log -e "$line"
  done
}

# redirect STDOUT and STDERR to recallog
exec &> >(log)
echo "Starting upgrade..."

# Detect 'vertical' screen (as "phone" one used on steam deck in 800x1200)
SCREEN_ORIENTATION="horizontal"
#test if framebuffer is available else ignored
if test -e /dev/fb0 ; then
  if [ "$(cut -d, -f1 /sys/class/graphics/fb0/virtual_size)" -le "$(cut -d, -f2 /sys/class/graphics/fb0/virtual_size)" ]; then
    SCREEN_ORIENTATION="vertical"
  fi
fi
do_update
RC=$?
if [ $RC -eq 0 ]; then
  if [ -f "/boot/update/pre-upgrade.sh" ]; then
    echo "Running pre-upgrade script..."
    python /recalbox/scripts/image_modifier.py "Running pre-upgrade script..." $IMAGE_PATH/update-12-pixl.png 1147 830 "/usr/share/fonts/dejavu/DejaVuSans.ttf" 28 "/tmp/output.png" ${SCREEN_ORIENTATION} yes
    bash /boot/update/pre-upgrade.sh
    sleep 1
  fi
  # Reboot
  echo "Upgrade successfull, rebooting..."
  python /recalbox/scripts/image_modifier.py "Upgrade successfull, rebooting..." $IMAGE_PATH/update-completed-pixl.png 1147 830 "/usr/share/fonts/dejavu/DejaVuSans.ttf" 28 "/tmp/output.png" ${SCREEN_ORIENTATION} yes
  sleep 3
else
  echo "Upgrade failed - return code $RC" >&2
  python /recalbox/scripts/image_modifier.py "Upgrade failed - return code $RC" $IMAGE_PATH/update-error-pixl.png 1147 830 "/usr/share/fonts/dejavu/DejaVuSans.ttf" 28 "/tmp/output.png" ${SCREEN_ORIENTATION} yes
  sleep 5
fi
sync
shutdown -r now
exit 0
