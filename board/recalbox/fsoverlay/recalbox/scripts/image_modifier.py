from PIL import Image, ImageDraw, ImageFont
import sys
import os
import subprocess

def Log(txt):
    import datetime
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    #print(formatted_time[:-3] + " - " + txt)
    return 0

def is_process_running(process_name):
    """Checks if a process is running using 'pgrep'."""
    try:
        subprocess.run(["pgrep", process_name], check=True, capture_output=True)
        return True
    except subprocess.CalledProcessError:
        return False

def main(argv):
    if len(argv) < 10:
        print("Usage: python image_modifier.py <text_to_write> <png_input_file_path> <x> <y> <font_file_path> <size> <png_output_file_path> <orientation> <display>")
        print("  All parameters are mandatory:")
        print("    text_to_write: text to write on image, could be set as empty using empty string like ''")
        print("    png_input_file_path: full path of picture to use usually from /recalbox/system/resources")
        print("    x/y: coordinate of text position, to set to 0 0 if not text to write")
        print("    font_file_path: full path of the font usually from /usr/share/fonts and could be set as empty using empty string like ''")
        print("    size: size of the font, set 0 if no text to write")
        print("    png_output_file_path: full path of the output file (use /tmp if you don't need to keep it)")
        print("    orientation: horizontal or vertical")
        print("    display: yes or no, to display image immediatelly from this script")
        #exemple for text:
        # python /recalbox/scripts/image_modifier.py 'Download... 1%' '/recalbox/system/resources/update-0-pixl.png' 1147 830 '/usr/share/fonts/dejavu/DejaVuSans.ttf' 28 '/tmp/output.png' horizontal yes
        sys.exit(1)
    text_to_write = argv[1]
    png_input_file_path = argv[2]
    x = argv[3]
    y = argv[4]
    font_file_path = argv[5]
    size = argv[6]
    png_output_file_path = argv[7]
    orientation = argv[8]
    display = argv[9]
    # Process the arguments as needed

    # Load the image
    png_image = Image.open(png_input_file_path)
    image = ImageDraw.Draw(png_image)

    # Define the text properties
    #font = ImageFont.truetype("/usr/share/fonts/dejavu/DejaVuSans-Bold.ttf", 36)
    font = ImageFont.truetype(font_file_path, int(size))
    position = (int(x), int(y))
    # Add text to the image
    image.text(position, text_to_write, font=font)
    if orientation == "vertical":
        # Rotate the image 270 degrees clockwise
        rotated_png_image = png_image.transpose(Image.ROTATE_270)
        # Save rotated modified image
        rotated_png_image.save(png_output_file_path)
    else:
        # Save modified image
        png_image.save(png_output_file_path)

    if display == "yes":
        if os.path.exists("/dev/fb0"):
            # Display image generared/modified using fbv2
            Log("launch fbv2")
            os.system("fbv2 -f -i -r " + png_output_file_path)
        else:
            # Display image generared/modified using mpv
            #check mpv already launched or not
            if is_process_running("mpv"):
                Log("mpv is running, no need to relaunch")
            else:
                Log("launch mpv in background")
                os.system("mpv --vo=drm --fs --no-keepaspect --really-quiet --image-display-duration=5 --loop-playlist=inf " + png_output_file_path + " " + png_output_file_path + " &")

if __name__ == "__main__":
    main(sys.argv)

