#!/bin/bash

source /recalbox/scripts/recalbox-utils.sh
IMAGE_PATH=$(getInstallUpgradeImagePath)
INIT_SCRIPT=$(basename "$0")
ARCH=$(cat /recalbox/recalbox.arch)
UPDATEDIR=/boot/update
UPDATEFILE="${UPDATEDIR}/pixl-$ARCH.img.xz"
UPDATEFILE_SHA1="${UPDATEDIR}/pixl-$ARCH.img.xz.sha1"
UPDATEFILE_UNCOMPRESSED=${UPDATEFILE::-3}
SHAREUPDATEDIR=/recalbox/share/system/upgrade
SHAREUPDATEFILE="${SHAREUPDATEDIR}/pixl-$ARCH.img.xz"
SHAREUPDATEFILE_UNCOMPRESSED=${SHAREUPDATEFILE::-3}
TEST="$2"

if test "$1" != "start"
then
  exit 0
fi

# this removes the content of dir $1
clean_dir() {
  [ ! -d "$1" ] && return
  find "$1" -mindepth 1 -exec rm {} \;
}

clean_boot_update() {
  #if test as second parameter we will not clean update directory
  if test "$TEST" != "test"
  then
    mount -o remount,rw /boot
    clean_dir "$UPDATEDIR"
    mount -o remount,ro /boot
  fi
}

clean_share_upgrade() {
  #if test as second parameter we will not clean update directory
  if test "$TEST" != "test"
  then
    mount -o remount,rw /
    clean_dir "$SHAREUPDATEDIR"
    mount -o remount,ro /
  fi
}

failed_clean_exit() {
  clean_boot_update
  clean_share_upgrade
  touch /tmp/upgradefailed
  sleep 10
  exit 1
}

# Detect 'vertical' screen (as "phone" one used on steam deck in 800x1200)
SCREEN_ORIENTATION="horizontal"
#test if framebuffer is available else ignored
if test -e /dev/fb0 ; then
  if [ "$(cut -d, -f1 /sys/class/graphics/fb0/virtual_size)" -le "$(cut -d, -f2 /sys/class/graphics/fb0/virtual_size)" ]; then
    SCREEN_ORIENTATION="vertical"
  fi
fi

#to check usually for "offline" update from /boot/update
if [[ -f $UPDATEFILE ]]; then
  recallog -s "${INIT_SCRIPT}" -t "UPGRADE-INFO" -f upgrade.log "Offline update found ! moving and decompress ${UPDATEFILE} to SHARE as ${SHAREUPDATEFILE_UNCOMPRESSED}"
  python /recalbox/scripts/image_modifier.py "Offline update found !" ${IMAGE_PATH}/update-0-pixl.png 1147 830 '/usr/share/fonts/dejavu/DejaVuSans.ttf' 28 '/tmp/output.png' ${SCREEN_ORIENTATION} yes
  #we remove potential previous uncompressed file (ignore error)
  rm ${SHAREUPDATEFILE_UNCOMPRESSED} || true
  sleep 1
  
  if [ ! -f "$UPDATEFILE_SHA1" ]; then
    recallog -s "${INIT_SCRIPT}" -t "UPGRADE-WARN" -f upgrade.log "Upgrade SHA1 not found. Skipping upgrade..."
    python /recalbox/scripts/image_modifier.py "Upgrade SHA1 not found. Skipping upgrade..." ${IMAGE_PATH}/update-error-pixl.png 1147 830 '/usr/share/fonts/dejavu/DejaVuSans.ttf' 28 '/tmp/output.png' ${SCREEN_ORIENTATION} yes
    failed_clean_exit
  fi

  recallog -s "${INIT_SCRIPT}" -t "UPGRADE-INFO" -f upgrade.log "checking SHA1"
  python /recalbox/scripts/image_modifier.py "Checking image SHA-1..." ${IMAGE_PATH}/update-1-pixl.png 1147 830 '/usr/share/fonts/dejavu/DejaVuSans.ttf' 28 '/tmp/output.png' ${SCREEN_ORIENTATION} yes
  if ! ( cd "$UPDATEDIR" && sha1sum -c "$UPDATEFILE_SHA1" ); then
    recallog -s "${INIT_SCRIPT}" -t "UPGRADE-ERROR" -f upgrade.log "SHA1 checking failed, removing stale files. Exiting..."
    python /recalbox/scripts/image_modifier.py "SHA1 checking failed, removing stale files. Exiting..." ${IMAGE_PATH}/update-error-pixl.png 1147 830 '/usr/share/fonts/dejavu/DejaVuSans.ttf' 28 '/tmp/output.png' ${SCREEN_ORIENTATION} yes
    failed_clean_exit
  else
    recallog -s "${INIT_SCRIPT}" -t "UPGRADE-INFO" -f upgrade.log "SHA1 check OK. Continue..."
    python /recalbox/scripts/image_modifier.py "SHA1 check OK. Continue..." ${IMAGE_PATH}/update-2-pixl.png 1147 830 '/usr/share/fonts/dejavu/DejaVuSans.ttf' 28 '/tmp/output.png' ${SCREEN_ORIENTATION} yes
    sleep 1
  fi

  clean_dir "$SHAREUPDATEDIR"
  #filename compressed
  source_filename="${UPDATEFILE}"
  echo "source_filename=$source_filename"

#to check now for "online decompressed" update exists from /recalbox/share/system/upgrade/ to clean it asynchronously
elif [[ -f $SHAREUPDATEFILE_UNCOMPRESSED ]]; then
  # asynchronously remove uncompressed file
  # this is to avoid a race condition when doing the upgrade:
  # unmounting the image and removing the file that is still locked
  # creates stale unremovable files that occupy space
  clean_boot_update
  clean_share_upgrade

  # inform we upgraded (may be... not sure finally ;-)
  touch /tmp/upgraded
  exit 0
#to check now for "online" update from /recalbox/share/system/upgrade/
elif [[ -f $SHAREUPDATEFILE ]]; then
  recallog -s "${INIT_SCRIPT}" -t "UPGRADE-INFO" -f upgrade.log "Online update found ! Decompress ${SHAREUPDATEFILE} as ${SHAREUPDATEFILE_UNCOMPRESSED}"
  python /recalbox/scripts/image_modifier.py "Online update found !" ${IMAGE_PATH}/update-0-pixl.png 1147 830 '/usr/share/fonts/dejavu/DejaVuSans.ttf' 28 '/tmp/output.png' ${SCREEN_ORIENTATION} yes
  #we remove potential previous uncompressed file (ignore error)
  rm ${SHAREUPDATEFILE_UNCOMPRESSED} || true
  sleep 1
  #filename compressed
  source_filename="${SHAREUPDATEFILE}"
  echo "source_filename=$source_filename"
else
  #else do nothing ;-)
  exit 0
fi
# common code now for offline and online updates
# NEW METHOD to see better the progress during decoppression
# to decompress and finalize upgrade before reboot
echo "Decompress to 'pixl-x86_64.img' before to update"
sleep 1
#filename decompressed
filename="${SHAREUPDATEFILE_UNCOMPRESSED}"
echo "filename=$filename"
#we get targeted size (in MegaByte) to spy decompression (in MegaBytes and in decimal)
string=$(LC_ALL=en_UK.UTF-8 xz --list $source_filename | grep -i pixl-$ARCH)
#we can't use $1,$2 for awk command that why we use array :-(
read -r -a words <<< "$string"
targetedsize=${words[4]}

#check of remaining size (in MegaByte) from share before to continue
remainingsize=$(df -m /recalbox/share | grep -i /share | awk -F' ' '{print $4}')
echo "remainingsize=$remainingsize"
if (( $(echo "$remainingsize < $targetedsize" |bc -l) )) ; then
  recallog -s "${INIT_SCRIPT}" -t "UPGRADE-ERROR" -f upgrade.log "No enough space on /share, need $targetedsize MB"
  python /recalbox/scripts/image_modifier.py "No enough space on 'share', need $targetedsize MB. Exiting..." $IMAGE_PATH/update-error-pixl.png 1147 830 "/usr/share/fonts/dejavu/DejaVuSans.ttf" 28 "/tmp/output.png" ${SCREEN_ORIENTATION} yes
  failed_clean_exit
fi

#get get number of thread to use 50% of CPU for decompression
#allthreads=$(grep processor /proc/cpuinfo |wc -l |grep '\S')
#usedthreads=$(echo "$allthreads" |awk '{printf "%.0f", $1/2}')
#we use unxz to decompress but in an dedicated independent thread
unxz -T0 -k -f -c $source_filename > $filename &
#to let start before to check
sleep 2
#other examples used during dev:
#cd /recalbox/share/system/upgrade/
#unxz -f -c --threads=$usedthreads /tmp/$componentName/pixl-x86_64.img.xz > $filename &
#unxz -f -v --threads=$usedthreads /recalbox/share/system/upgrade/pixl-x86_64.img.xz &
#unxz -c /recalbox/share/system/upgrade/pixl-x86_64.img.xz > /recalbox/share/system/upgrade/pixl-x86_64.img &
#keep pid to be able to kill it later if needed
string=$(LC_ALL=en_UK.UTF-8 ps | grep -i unxz)
#we can't use $1,$2 for awk command that why we use array :-(
read -r -a words <<< "$string"
pid=${words[0]}
#init variable
filesize=0
value=0
percentage=0
echo "pid=$pid"
echo "targetedsize=$targetedsize"
while (( $(echo "$filesize < $targetedsize" |bc -l) )) ; do
  if [[ -f "$filename" ]]; then
    # Get the file size in bytes
    filesize=$(LC_ALL=en_UK.UTF-8 du -m "$filename" | awk -F ' ' '{print $1}')
    #echo "filesize=$filesize"
    # Calculated value/percentage
    value=$(echo "$filesize" "$targetedsize" |awk '{printf "%.2f", $1/$2}')
    #echo "value=$value"
    percentage=$(echo "$value" |awk '{printf "%.0f", $1*100}')
    #echo "percentage=$percentage"
    sleep 10
    STEP=2
    if (( $percentage < 16 ))
    then
      STEP=2
    elif (( $percentage < 36 ))
    then
      STEP=3
    elif (( $percentage < 48 ))
    then
      STEP=4
    elif (( $percentage < 64 ))
    then
      STEP=5
    elif (( $percentage < 80 ))
    then
      STEP=6
    else
      STEP=7
    fi
    python /recalbox/scripts/image_modifier.py "Decompress image... ${percentage}%" $IMAGE_PATH/update-${STEP}-pixl.png 1147 830 "/usr/share/fonts/dejavu/DejaVuSans.ttf" 28 "/tmp/output.png" ${SCREEN_ORIENTATION} yes
  else
    break
  fi
  
  #check if unxz is working or not ?!
  pid_to_check=$pid
  pgrep_output=$(pgrep unxz)
  if [[ "$pgrep_output" != "$pid_to_check" ]]; then
    echo "The process unxz is not running or has a different PID."
    # Get the file size a last time in bytes
    filesize=$(LC_ALL=en_UK.UTF-8 du -m "$filename" | awk -F ' ' '{print $1}')
    echo "filesize=$filesize"
    # Calculated value/percentage for a last time
    value=$(echo "$filesize" "$targetedsize" |awk '{printf "%.2f", $1/$2}')
    echo "value=$value"
    percentage=$(echo "$value" |awk '{printf "%.0f", $1*100}')
    echo "percentage=$percentage"
    break
  fi

done
#secure to kill unxz in all cases (ignore errors)
kill -9 $pid || true

#check if file is decompressed at 100%
#echo "percentage=$percentage" >> /tmp/$componentName/debug.log
if (( $percentage < 100 ))
then
  recallog -s "${INIT_SCRIPT}" -t "UPGRADE-ERROR" -f upgrade.log "Decompression failed, removing stale files. Exiting..."
  python /recalbox/scripts/image_modifier.py "Decompression failed, removing stale files. Exiting..." $IMAGE_PATH/update-error-pixl.png 1147 830 "/usr/share/fonts/dejavu/DejaVuSans.ttf" 28 "/tmp/output.png" ${SCREEN_ORIENTATION} yes
  failed_clean_exit
fi

#ready to update (clean boot update directory if needed)
#if test as second parameter we will not clean update directory
if test "$2" != "test"
then
  clean_boot_update
fi

recallog -s "${INIT_SCRIPT}" -t "UPGRADE-INFO" -f upgrade.log "proceeding with rest of upgrade process"
python /recalbox/scripts/image_modifier.py "Decompression failed, removing stale files. Exiting..." $IMAGE_PATH/update-${STEP}-pixl.png 1147 830 "/usr/share/fonts/dejavu/DejaVuSans.ttf" 28 "/tmp/output.png" ${SCREEN_ORIENTATION} yes

( exec /recalbox/scripts/upgrade/recalbox-squashfs-upgrade.sh )
exit 0
