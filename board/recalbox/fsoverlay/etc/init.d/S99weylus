#! /bin/sh

EXIT=0
BASEFILE=$(basename "$0")
LOCKFILE="/var/run/${BASEFILE}-service.pid" #lock file contain the pid value
BINFILE="weylus"
BINOPTIONS="--auto-start --no-gui"
LOGFILE="/recalbox/share/system/logs/${BINFILE}.log"
BINDIR="/usr/bin/"
systemsetting="recalbox_settings"
config_file="/recalbox/share/system/recalbox.conf"
remote_display="$($systemsetting -command load -key system.remote.display.enabled -source $config_file)"

checkPID () {
	SPID=$(ps ax | grep -i "${BINDIR}${BINFILE} ${BINOPTIONS}" | grep -v "grep" | awk '{print $1}')
	recallog -s "${BASEFILE}" -t "checkPID" "${BINFILE} Service is running with pid : ${SPID}"
	echo "${SPID}"
}

checkLock () {
	if (test -e "${LOCKFILE}" ); then
		recallog -s "${BASEFILE}" -t "checkLock" "${BINFILE} Service is locked"
		return 0
	else
		recallog -s "${BASEFILE}" -t "checkLock" "${BINFILE} Service is not locked"
		return 1
	fi
}

unlockService() {
	checkLock
	if [ $? -eq 0 ]; then
		rm "${LOCKFILE}"
		if [ $? -eq 0 ]; then
			recallog -s "${BASEFILE}" -t "unlockService" "lock file ${LOCKFILE} removed"
		else
			recallog -s "${BASEFILE}" -t "unlockService" "lock file ${LOCKFILE} removing failed :-(, access right issue ?"
			EXIT=1
		fi
	else
		EXIT=1
	fi
}

killService() {
	SPID=$( checkPID )
	echo "PID found: ${SPID}"
	if [ ! -n "$SPID" ]; then
		recallog -s "${BASEFILE}" -t "killService" "No ${BINFILE} Service found"
		return 1
	else
		#to kill properly
		kill -TERM "${SPID}"
		if [ $? -eq 0 ]; then
			recallog -s "${BASEFILE}" -t "killService" "${BINFILE} Service killed"
			return 0
		else
			recallog -s "${BASEFILE}" -t "killService" "${BINFILE} Service killing failed :-("
			return 1
		fi
	fi
}

checkPIDAndUnlockIfNeeded() {
#this function return 0 if no service launched
#this function return 1 service already launched
	checkLock
	if [ $? -eq 0 ]; then
		LPID=$(cat "${LOCKFILE}")
		recallog -s "${BASEFILE}" -t "checkPIDAndUnlockIfNeeded" "Existing ${BINFILE} Service lock file contains pid : ${LPID}"
		SPID=$( checkPID )
		if [ -n "$SPID" ]; then
			return 1
		else
			recallog -s "${BASEFILE}" -t "checkPIDAndUnlockIfNeeded" "Unlock the ${BINFILE} Service due to pid missing/mistmatch"
			unlockService
			return 0
		fi
	else
		return 0
	fi
}

starting() {
		checkPIDAndUnlockIfNeeded
		if [ $? -eq 0 ]; then
			recallog -s "${BASEFILE}" -t "start" "${BINFILE} Service is launching..."			
		DISPLAY=:0 PATH=/bin:/sbin:/usr/bin:/usr/sbin nohup ${BINDIR}${BINFILE} ${BINOPTIONS} > "${LOGFILE}" &
		#create lockfile
		sleep 1
		SPID=$( checkPID )
		echo ${SPID} > ${LOCKFILE}
		#check lockfile content to confirm that service is well launch      
			checkLock
		if [ $? -eq 0 ]; then
				recallog -s "${BASEFILE}" -t "start" "${BINFILE} Service is launched"
			else
				recallog -s "${BASEFILE}" -t "start" "${BINFILE} Service launching is failed :-("
			fi
		else
			recallog -s "${BASEFILE}" -t "start" "${BINFILE} Service already launched"
		fi
}

stopping() {
		recallog -s "${BASEFILE}" -t "stop" "${BINFILE} Service is stopping..."
		killService
		if [ $? -eq 1 ]; then
			recallog -s "${BASEFILE}" -t "stop" "${BINFILE} Service not stopped finally"
			EXIT=1
		fi
		checkPIDAndUnlockIfNeeded
		if [ $? -eq 0 ]; then
			recallog -s "${BASEFILE}" -t "stop" "${BINFILE} Service stopped"
			EXIT=0
		else
			recallog -s "${BASEFILE}" -t "stop" "${BINFILE} Service not well stopped :-("
			EXIT=1
		fi
}

#Carry out specific functions when asked to by the system
case "$1" in
	start)
		if [[ $remote_display -eq 1 ]]; then
			starting
		else
			recallog -s "${BASEFILE}" -t "start" "${BINFILE} Service not started because system.remote.display.enabled is false"
				EXIT=0
		fi
	;;
	stop)
		stopping
	;;
	status)
		SPID=$( checkPID )
		if [ -n "$SPID" ]; then
			echo "${BINFILE} Service is running with pid : ${SPID}"
			cat "${LOGFILE}"
		else
			echo "${BINFILE} Service is not running for the moment"
		fi
	;;
	restart|reload)
		stopping
		sleep 2
		starting
	;;
	*)
		echo "Usage: $0 {start|stop|status|restart|reload}"
		EXIT=1
	;;
esac

#echo "exit with value: ${EXIT}"
exit ${EXIT}

#examples of command for debugging

#to launch the service for Weylus
#/usr/bin/weylus --auto-start --no-gui

# command to know if weylus is running and the process id associated
#ps ax | grep -i weylus | grep -i /usr/bin/weylus

#to kill process using pid
#kill -15 XXXX

