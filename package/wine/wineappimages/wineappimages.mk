################################################################################
#
# wine appimages
#
################################################################################

WINEAPPIMAGES_LICENSE = MIT license

# Includes appimages
define WINEAPPIMAGES_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/usr/wine
	#install all AppImages available - AppImage name is shorter in repo to avoid problem of data check in git kraken tool for example
	$(INSTALL) -D -m 0555 "$(BR2_EXTERNAL_RECALBOX_PATH)/package/wine/wineappimages/wine-staging-linux-x86-v5.11.AppImage" \
		"$(TARGET_DIR)/usr/wine/wine-staging-linux-x86-v5.11-PlayOnLinux-x86_64.AppImage"
endef

$(eval $(generic-package))
