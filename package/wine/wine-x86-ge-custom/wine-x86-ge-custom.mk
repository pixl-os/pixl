################################################################################
#
# wine-x86-ge-custom
#
################################################################################

# version 
WINE_X86_GE_CUSTOM_VERSION = GE-Proton8-25
WINE_X86_GE_CUSTOM_SOURCE = wine-x86-$(WINE_X86_GE_CUSTOM_VERSION).tar.lzma
WINE_X86_GE_CUSTOM_SITE = https://github.com/pixl-os/wine-x86/releases/download/$(WINE_X86_GE_CUSTOM_VERSION)

#to avoid error to build 32 bits library needed for wine
WINE_X86_GE_CUSTOM_BIN_ARCH_EXCLUDE = /usr/bin32 /usr/wine /usr/lib32 /lib32

define WINE_X86_GE_CUSTOM_EXTRACT_CMDS
	mkdir -p $(@D)/target && cd $(@D)/target && tar xf $(DL_DIR)/$(WINE_X86_GE_CUSTOM_DL_SUBDIR)/$(WINE_X86_GE_CUSTOM_SOURCE)
endef

define WINE_X86_GE_CUSTOM_INSTALL_TARGET_CMDS
	cp -prn $(@D)/target/* $(TARGET_DIR)
endef

$(eval $(generic-package))
