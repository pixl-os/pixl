################################################################################
#
# Arcade Dat Generator
#
################################################################################

ARCADE_DAT_GENERATOR_VERSION = 79a743b8b9241288edaf003cec8181f39b6099b0
ARCADE_DAT_GENERATOR_SITE = $(call gitlab,pixl-os,arcade-dat-generator,$(ARCADE_DAT_GENERATOR_VERSION))
ARCADE_DAT_GENERATOR_DATFILE = "https://github.com/mamedev/mame/releases/download/mame$(LIBRETRO_MAME_VERSION_ROMSET)/mame$(LIBRETRO_MAME_VERSION_ROMSET)lx.zip"
ARCADE_DAT_GENERATOR_LICENSE = GPL2
ARCADE_DAT_GENERATOR_LICENSE_FILES = COPYING
# ARCADE_DAT_GENERATOR_DEPENDENCIES = libretro-mame libretro-flycast supermodel

CONFIG_PACKAGE_ARCADE_DAT_GENERATOR_SHELL_BASH=y

define ARCADE_DAT_GENERATOR_BUILD_CMDS
	#cd $(ARCADE_DAT_GENERATOR_BUILDDIR)
	# download dat file for libretro_mame core
	wget -O $(@D)/mame$(LIBRETRO_MAME_VERSION_ROMSET)lx.zip $(ARCADE_DAT_GENERATOR_DATFILE)
	unzip -n $(@D)/mame$(LIBRETRO_MAME_VERSION_ROMSET)lx.zip -d $(@D)/
	# create dir for output dats and log if needed
	mkdir -p $(@D)/Dat_pixL_Arcade \
				$(@D)/log
	# create dir for dats in recalbox
	mkdir -p $(TARGET_DIR)/recalbox/share_init/bios/dc \
				$(TARGET_DIR)/recalbox/share_init/bios/mame \
				$(TARGET_DIR)/recalbox/share_init/bios/neogeo \
				$(TARGET_DIR)/recalbox/share_init/bios/model3
	# execute script_dat
	bash $(@D)/script_dat.sh
endef

define ARCADE_DAT_GENERATOR_INSTALL_TARGET_CMDS
	cp -R $(@D)/Dat_pixL_Arcade/Atomiswave* $(TARGET_DIR)/recalbox/share_init/bios/dc/
	cp -R $(@D)/Dat_pixL_Arcade/Naomi* $(TARGET_DIR)/recalbox/share_init/bios/dc/
	cp -R $(@D)/Dat_pixL_Arcade/Mame* $(TARGET_DIR)/recalbox/share_init/bios/mame/
	cp -R $(@D)/Dat_pixL_Arcade/Neogeo* $(TARGET_DIR)/recalbox/share_init/bios/neogeo/
	cp -R $(@D)/Dat_pixL_Arcade/Model3* $(TARGET_DIR)/recalbox/share_init/bios/model3/

	# For Create a update online package
	mkdir -p $(BR2_EXTERNAL_RECALBOX_PATH)/package/libretro-mame/update-resources/package
	cp -R $(@D)/Dat_pixL_Arcade/* $(BR2_EXTERNAL_RECALBOX_PATH)/package/libretro-mame/update-resources/package/
endef

$(eval $(generic-package))
