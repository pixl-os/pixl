require("model2")

function Init()

	ScanlinesSurface = Video_CreateSurfaceFromFile("scripts\\scanlines_default.png");
	wide=false
	scanlines=false
	overlay=true
	press=0
	OverlaySurface = Video_CreateSurfaceFromFile("/tmp/model2emu_overlay.png");

end

function Frame()
		if Input_IsKeyPressed(0x3F)==1 and press==0 then wide=not wide press=1
		elseif Input_IsKeyPressed(0x3F)==0 and press==1 then press=0
		end
		
	if wide==true then
		Model2_SetWideScreen(1)
	 	Model2_SetStretchBLow(1)
	else	
		Model2_SetWideScreen(0)	 	
	 	Model2_SetStretchBLow(0)
	end
end

function PostDraw()
-- To display scanlines
	if Options.scanlines.value==1 or scanlines==true then
		Video_DrawSurface(ScanlinesSurface,0,0);
	end
-- To display overlay
	if (Options.overlay.value==1 or overlay==true) then
		Video_DrawSurface(OverlaySurface,0,-1);
	end
end

function timecheatfunc(value)
        I960_WriteWord(RAMBASE+0x19D94,99*60); -- 99 seconds always
end

function firstplacefunc(value)
        I960_WriteWord(RAMBASE+0x573B0,0); -- competitors in front
end

Options =
{
	timecheat={name="Infinite Time",values={"Off","On"},runfunc=timecheatfunc},
	firstplace={name="First Place",values={"Off","On"},runfunc=firstplacefunc},
	scanlines={name="Scanlines (50%)",values={"Off","On"}},
	overlay={name="Overlay",values={"Off","On"}}
}
