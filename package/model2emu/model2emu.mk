################################################################################
#
# model 2 windows standalone emulator
#
################################################################################

# version 1.1a - no source available - windows emulator - need wine appimage
# not developed since 2014
MODEL2EMU_VERSION_CORE = 1.1a
MODEL2EMU_LICENSE = ElSemi

## print version of core in PixL
define MODEL2EMU_PRE_CONFIGURE
	echo "Model 2 emulator;model2emu;$(MODEL2EMU_VERSION_CORE)" > $(TARGET_DIR)/recalbox/share_init/system/configs/model2emu.corenames
endef
MODEL2EMU_PRE_CONFIGURE_HOOKS += MODEL2EMU_PRE_CONFIGURE

# Includes binary, appimage and game configs required to successfully launch and play them.
define MODEL2EMU_INSTALL_TARGET_CMDS
	# copy original windows executables and readme.txt
	mkdir -p $(TARGET_DIR)/usr/bin/model2emu
	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/model2emu/bin/*.exe \
		$(TARGET_DIR)/usr/bin/model2emu
	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/model2emu/bin/*.EXE \
		$(TARGET_DIR)/usr/bin/model2emu
	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/model2emu/bin/README.TXT \
		$(TARGET_DIR)/usr/bin/model2emu
	#prepare saves directory and contents
	mkdir -p $(TARGET_DIR)/recalbox/share_init/saves/model2
	mkdir -p $(TARGET_DIR)/recalbox/share_init/saves/model2/model2emu
	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/model2emu/saves/EMULATOR.INI \
		$(TARGET_DIR)/recalbox/share_init/saves/model2/model2emu
	mkdir -p $(TARGET_DIR)/recalbox/share_init/saves/model2/model2emu/CFG
	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/model2emu/saves/CFG/*.* \
		$(TARGET_DIR)/recalbox/share_init/saves/model2/model2emu/CFG
	mkdir -p $(TARGET_DIR)/recalbox/share_init/saves/model2/model2emu/NVDATA
	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/model2emu/saves/NVDATA/*.* \
		$(TARGET_DIR)/recalbox/share_init/saves/model2/model2emu/NVDATA
	mkdir -p $(TARGET_DIR)/recalbox/share_init/saves/model2/model2emu/scripts
	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/model2emu/saves/scripts/*.* \
		$(TARGET_DIR)/recalbox/share_init/saves/model2/model2emu/scripts
	mkdir -p $(TARGET_DIR)/recalbox/share_init/saves/model2/model2emu/scripts/crosshairs
	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/model2emu/saves/scripts/crosshairs/*.* \
		$(TARGET_DIR)/recalbox/share_init/saves/model2/model2emu/scripts/crosshairs
endef

# Hotkeys using evmapy
define MODEL2EMU_EVMAPY
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs/evmapy
	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/model2emu/model2.model2emu.keys \
		$(TARGET_DIR)/recalbox/share_init/system/configs/evmapy
endef
MODEL2EMU_POST_INSTALL_TARGET_HOOKS = MODEL2EMU_EVMAPY

$(eval $(generic-package))
