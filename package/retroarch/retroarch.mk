################################################################################
#
# RetroArch
#
################################################################################

# Version from pixl-os repo using branch with patches for v1.20.0
RETROARCH_VERSION = 372cb3daeacfcb7e4526c5276e06d087736a685d
RETROARCH_SITE = https://github.com/pixl-os/RetroArch.git

RETROARCH_SITE_METHOD = git
RETROARCH_LICENSE = GPLv3+
RETROARCH_CONF_OPTS += --disable-oss --enable-zlib --disable-opengl1
RETROARCH_DEPENDENCIES = host-pkgconf recalbox-system flac

RETROARCH_COMPILER_COMMONS_CFLAGS = $(COMPILER_COMMONS_CFLAGS_NOLTO)
RETROARCH_COMPILER_COMMONS_CXXFLAGS = $(COMPILER_COMMONS_CXXFLAGS_NOLTO)
RETROARCH_COMPILER_COMMONS_LDFLAGS = $(COMPILER_COMMONS_LDFLAGS_NOLTO)

ifeq ($(BR2_PACKAGE_SDL2),y)
RETROARCH_CONF_OPTS += --enable-sdl2
RETROARCH_DEPENDENCIES += sdl2
else
RETROARCH_CONF_OPTS += --disable-sdl2
ifeq ($(BR2_PACKAGE_SDL),y)
RETROARCH_CONF_OPTS += --enable-sdl
RETROARCH_DEPENDENCIES += sdl
else
RETROARCH_CONF_OPTS += --disable-sdl
endif
endif

ifeq ($(BR2_PACKAGE_LIBDRM),y)
RETROARCH_CONF_OPTS += --enable-kms
endif

# x86 : SSE
ifeq ($(BR2_X86_CPU_HAS_SSE),y)
RETROARCH_CONF_OPTS += --enable-sse
endif

# Common
RETROARCH_CONF_OPTS += --enable-rgui --enable-xmb --enable-ozone --enable-materialui
RETROARCH_CONF_OPTS += --enable-threads --enable-dylib
RETROARCH_CONF_OPTS += --enable-flac --enable-lua
RETROARCH_CONF_OPTS += --enable-networking

# Package dependant

ifeq ($(BR2_PACKAGE_XORG7),y)
RETROARCH_CONF_OPTS += --enable-x11
RETROARCH_DEPENDENCIES += xserver_xorg-server
else
RETROARCH_CONF_OPTS += --disable-x11
endif

ifeq ($(BR2_PACKAGE_ALSA_LIB),y)
RETROARCH_CONF_OPTS += --enable-alsa
RETROARCH_DEPENDENCIES += alsa-lib
else
RETROARCH_CONF_OPTS += --disable-alsa
endif

ifeq ($(BR2_PACKAGE_PULSEAUDIO),y)
RETROARCH_CONF_OPTS += --enable-pulse
RETROARCH_DEPENDENCIES += pulseaudio
else
RETROARCH_CONF_OPTS += --disable-pulse
endif

ifeq ($(BR2_PACKAGE_HAS_LIBGLES),y)
RETROARCH_CONF_OPTS += --enable-opengles
RETROARCH_DEPENDENCIES += libgles
else
RETROARCH_CONF_OPTS += --disable-opengles
endif

ifeq ($(BR2_PACKAGE_HAS_LIBEGL),y)
RETROARCH_CONF_OPTS += --enable-egl
RETROARCH_DEPENDENCIES += libegl
else
RETROARCH_CONF_OPTS += --disable-egl
endif

ifeq ($(BR2_PACKAGE_HAS_LIBOPENVG),y)
RETROARCH_DEPENDENCIES += libopenvg
endif

ifeq ($(BR2_PACKAGE_ZLIB),y)
RETROARCH_CONF_OPTS += --enable-zlib
RETROARCH_DEPENDENCIES += zlib
else
RETROARCH_CONF_OPTS += --disable-zlib
endif

ifeq ($(BR2_PACKAGE_HAS_UDEV),y)
RETROARCH_DEPENDENCIES += udev
RETROARCH_CONF_OPTS += --enable-udev
else
RETROARCH_CONF_OPTS += --disable-udev
endif

ifeq ($(BR2_PACKAGE_FREETYPE),y)
RETROARCH_CONF_OPTS += --enable-freetype
RETROARCH_DEPENDENCIES += freetype
else
RETROARCH_CONF_OPTS += --disable-freetype
endif

#vulkan
ifeq ($(BR2_PACKAGE_VULKAN_LOADER)$(BR2_PACKAGE_VULKAN_HEADERS),yy)
RETROARCH_CONF_OPTS += --enable-vulkan
RETROARCH_DEPENDENCIES += vulkan-headers vulkan-loader
endif

define RETROARCH_CONFIGURE_CMDS
	(cd $(@D); rm -rf config.cache; \
		$(TARGET_CONFIGURE_ARGS) \
		$(TARGET_CONFIGURE_OPTS) \
		CFLAGS="$(TARGET_CFLAGS) $(RETROARCH_COMPILER_COMMONS_CFLAGS)" \
		CXXFLAGS="$(TARGET_CXXFLAGS) $(RETROARCH_COMPILER_COMMONS_CXXFLAGS)" \
		LDFLAGS="$(TARGET_LDFLAGS) $(RETROARCH_COMPILER_COMMONS_LDFLAGS) -lc" \
		CROSS_COMPILE="$(HOST_DIR)/bin/" \
		PKG_CONFIG_PATH="$(STAGING_DIR)/usr/lib/pkgconfig/" \
		./configure \
		--prefix=/usr \
		$(RETROARCH_CONF_OPTS) \
	)
endef

define RETROARCH_FIX_LIBS
	$(SED) "s|-\([IL]\)/usr|-\1$(STAGING_DIR)/usr|g" $(@D)/config.mk
endef

RETROARCH_POST_CONFIGURE_HOOKS += RETROARCH_FIX_LIBS

define RETROARCH_BUILD_CMDS
	$(MAKE) CXX="$(TARGET_CXX)" CC="$(TARGET_CC)" LD="$(TARGET_LD)" -C $(@D) all
endef

define RETROARCH_INSTALL_TARGET_CMDS
	$(MAKE) CXX="$(TARGET_CXX)" -C $(@D) DESTDIR=$(TARGET_DIR) install
endef

# Hotkeys using evmapy
define RETROARCH_EVMAP
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs/evmapy

	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/retroarch/libretro.keys \
		$(TARGET_DIR)/recalbox/share_init/system/configs/evmapy
endef

RETROARCH_POST_INSTALL_TARGET_HOOKS = RETROARCH_EVMAP

$(eval $(generic-package))

# DEFINITION OF LIBRETRO PLATFORM
ifeq ($(BR2_i386),y)
RETROARCH_LIBRETRO_PLATFORM = unix
endif

ifeq ($(BR2_x86_64),y)
RETROARCH_LIBRETRO_PLATFORM = unix
endif

RETROARCH_LIBRETRO_BOARD=$(RETROARCH_LIBRETRO_PLATFORM)
