################################################################################
#
# GLSL_SHADERS
#
################################################################################

# Commits on May 5, 2023
LIBRETRO_GLSL_SHADERS_VERSION = 08af650d7f35f8ad46ee0a9d038fe440144012d2
LIBRETRO_GLSL_SHADERS_SITE = $(call github,libretro,glsl-shaders,$(LIBRETRO_GLSL_SHADERS_VERSION))
LIBRETRO_GLSL_SHADERS_LICENSE = MIT
LIBRETRO_GLSL_SHADERS_LICENSE_FILES = COPYING

define LIBRETRO_GLSL_SHADERS_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/recalbox/share_init/shaders/
	cp -a $(@D)/* \
			$(TARGET_DIR)/recalbox/share_init/shaders/
	rm -f $(TARGET_DIR)/recalbox/share_init/shaders/Makefile \
		$(TARGET_DIR)/recalbox/share_init/shaders/configure
endef

$(eval $(generic-package))
