#!/bin/bash
# Dolphin-emu;dolphin;v5.0.16426
emulatorName="dolphin"
coreName="dolphin"
#echo for debug
#echo "$emulatorName"
#echo "$coreName"

#direct reading of *.corenames to extract and return version string
result=""
result="$(cat /recalbox/share/system/configs/$emulatorName.corenames | grep -i $coreName | awk -v FS='(;)' '{print $3}' | tr -d '\n' | tr -d '\r')"
if test -n "$result"
then
	#not null: we could return value normally
	echo "$result" | tr -d '\n' | tr -d '\r'
else
	#value is not found or null - force to 0.0.0.0 in this case
	echo v0.0.0.0 | tr -d '\n' | tr -d '\r'
fi
exit 0