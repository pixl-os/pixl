################################################################################
#
# DOLPHIN_EMU
#
################################################################################

# https://fr.dolphin-emu.org/download/ for check versioning
# respect vx.x.xxxxx not x.x-xxxxx
DOLPHIN_EMU_VERSION_CORE = 2407.68 # v5.0.21783
# committed on July 16 2024
DOLPHIN_EMU_VERSION = cc3ff347b437fd80f3a2880d1bbc7ba2d5f191f4
DOLPHIN_EMU_SITE = https://github.com/dolphin-emu/dolphin
DOLPHIN_EMU_SITE_METHOD = git
DOLPHIN_EMU_LICENSE = GPL-2.0+
DOLPHIN_EMU_LICENSE_FILES = license.txt
DOLPHIN_EMU_DEPENDENCIES = libevdev ffmpeg zlib libpng lzo libusb libcurl bluez5_utils hidapi xz host-xz \
							xserver_xorg-server qt6base qt6svg sdl2
DOLPHIN_EMU_SUPPORTS_IN_SOURCE_BUILD = NO
DOLPHIN_EMU_GIT_SUBMODULES = YES

## print version of core in PixL
define DOLPHIN_EMU_PRE_CONFIGURE
	echo "Dolphin-emu;dolphin;$(DOLPHIN_EMU_VERSION_CORE)" > $(TARGET_DIR)/recalbox/share_init/system/configs/dolphin.corenames
endef
DOLPHIN_EMU_PRE_CONFIGURE_HOOKS += DOLPHIN_EMU_PRE_CONFIGURE

DOLPHIN_EMU_CONF_OPTS += -DCMAKE_BUILD_TYPE=Release
DOLPHIN_EMU_CONF_OPTS += -DBUILD_SHARED_LIBS=OFF
DOLPHIN_EMU_CONF_OPTS += -DDISTRIBUTOR='pixL-os'
DOLPHIN_EMU_CONF_OPTS += -DUSE_DISCORD_PRESENCE=OFF
DOLPHIN_EMU_CONF_OPTS += -DUSE_MGBA=OFF
DOLPHIN_EMU_CONF_OPTS += -DUSE_UPNP=OFF
DOLPHIN_EMU_CONF_OPTS += -DENABLE_TESTS=OFF
DOLPHIN_EMU_CONF_OPTS += -DENABLE_AUTOUPDATE=OFF
DOLPHIN_EMU_CONF_OPTS += -DENABLE_ANALYTICS=OFF
DOLPHIN_EMU_CONF_OPTS += -DUSE_SYSTEM_LIBS=AUTO
DOLPHIN_EMU_CONF_OPTS += -DENABLE_CLI_TOOL=OFF
DOLPHIN_EMU_CONF_OPTS += -DENABLE_LTO=OFF

DOLPHIN_EMU_MAKE_ENV += LDFLAGS="-Wl,--copy-dt-needed-entries"
DOLPHIN_EMU_CONF_ENV += LDFLAGS="-Wl,--copy-dt-needed-entries"

ifeq ($(BR2_PACKAGE_XORG7),y)
    DOLPHIN_EMU_CONF_OPTS += -DENABLE_X11=ON
else
    DOLPHIN_EMU_CONF_OPTS += -DENABLE_X11=OFF
endif

ifeq ($(BR2_PACKAGE_VULKAN_HEADERS),y)
    DOLPHIN_EMU_CONF_OPTS += -DENABLE_VULKAN=ON
else
    DOLPHIN_EMU_CONF_OPTS += -DENABLE_VULKAN=OFF
endif

# Hotkeys using evmapy
define DOLPHIN_EMU_EVMAP
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs/evmapy

	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/dolphin-emu/dolphin.keys \
		$(TARGET_DIR)/recalbox/share_init/system/configs/evmapy
endef
DOLPHIN_EMU_POST_INSTALL_TARGET_HOOKS = DOLPHIN_EMU_EVMAP

$(eval $(cmake-package))
