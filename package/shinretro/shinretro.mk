################################################################################
#
# pixL themes for Pegasus : https://github.com/pixl-os/shinretro
#
################################################################################

# last commit from pixL-master
# https://github.com/pixl-os/shinretro/tags
SHINRETRO_THEME_VERSION = v0.201.6
SHINRETRO_VERSION = d7c381cbd2c8a203649266d8db6d910af18f680e
SHINRETRO_SITE = $(call github,pixl-os,shinretro,$(SHINRETRO_VERSION))
SHINRETRO_LICENSE = GPL-3.0

define SHINRETRO_INSTALL_TARGET_CMDS
	# create directories if not already done
	mkdir -p $(TARGET_DIR)/recalbox/share_init/themes
	mkdir -p $(TARGET_DIR)/recalbox/share_init/themes/shinretro
	# copy theme files in share_init
	cp -r $(@D)/* $(TARGET_DIR)/recalbox/share_init/themes/shinretro
endef

$(eval $(generic-package))
