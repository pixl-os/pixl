################################################################################
#
# SLANG_SHADERS
#
################################################################################

# Commits on Feb 19, 2025
# and including Mega Bezel V1.17.2_2024-05-18
LIBRETRO_SLANG_SHADERS_VERSION = bcce176b65c90947a7de1d85420d864e665fd184
LIBRETRO_SLANG_SHADERS_SITE = $(call github,libretro,slang-shaders,$(LIBRETRO_SLANG_SHADERS_VERSION))
LIBRETRO_SLANG_SHADERS_LICENSE = MIT
LIBRETRO_SLANG_SHADERS_LICENSE_FILES = COPYING

define LIBRETRO_SLANG_SHADERS_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/recalbox/share_init/shaders/
	cp -a $(@D)/* \
			$(TARGET_DIR)/recalbox/share_init/shaders/
	rm -f $(TARGET_DIR)/recalbox/share_init/shaders/Makefile \
		$(TARGET_DIR)/recalbox/share_init/shaders/configure
	mkdir -p $(TARGET_DIR)/recalbox/share_init/shaders/bezel/Mega_Bezel/Presets
	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/libretro-slang-shaders/pixl-slang-shaders/*.slangp \
		$(TARGET_DIR)/recalbox/share_init/shaders/bezel/Mega_Bezel/Presets/
endef

$(eval $(generic-package))
