################################################################################
#
# rpcs3
#
################################################################################

# Commits on Aug 31, 2024
RPCS3_CORE_VERSION = v0.0.34-$(shell echo $(RPCS3_VERSION) | cut -c1-7)
RPCS3_VERSION = 8dbe88782ce39aeab998079e82b3541e653889c9
RPCS3_SITE = https://github.com/RPCS3/rpcs3.git
RPCS3_SITE_METHOD=git
RPCS3_GIT_SUBMODULES= Y
RPCS3_LICENSE = GPLv2
RPCS3_DEPENDENCIES += alsa-lib llvm ffmpeg libevdev libglew libglu \
						libpng libusb mesa3d ncurses openal qt6base \
						qt6declarative qt6multimedia qt6svg wolfssl \
						libxml2 rtmpdump jack2

## Fix me crash on download prebuilt ffmpeg core 
## Download prebuilt FFMPEG libraries for the target architecture
## Commit Short SHA of a known working FFMPEG prebuilt 
FFMPEG_GIT_SHA = ec6367d3ba9d0d57b9d22d4b87da8144acaf428f

FFMPEG_PREBUILTS_FILE = ffmpeg-linux-x64.zip
FFMPEG_PREBUILTS_URL = https://github.com/RPCS3/ffmpeg-core/releases/download/$(shell echo $(FFMPEG_GIT_SHA) | cut -c1-7)/$(FFMPEG_PREBUILTS_FILE)

define RPCS3_PRE_CONFIGURE_FFMPEG_CORE
	## Download prebuild ffmpeg package
	mkdir -p $(@D)/buildroot-build/3rdparty/
	if [ ! -f $(@D)/buildroot-build/3rdparty/$(FFMPEG_PREBUILTS_FILE) ]; then \
		wget $(FFMPEG_PREBUILTS_URL) -O $(@D)/buildroot-build/3rdparty/ffmpeg.zip; \
	fi
	unzip -o $(@D)/buildroot-build/3rdparty/ffmpeg.zip -d $(@D)/3rdparty/ffmpeg/lib/
	cp $(@D)/3rdparty/ffmpeg/lib/libswresample.a $(@D)/3rdparty/ffmpeg/libswresample.a
endef

define RPCS3_PRE_CONFIGURE
	## print version of core in PixL
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs
	echo "rpcs3;rpcs3;$(RPCS3_CORE_VERSION)" > $(TARGET_DIR)/recalbox/share_init/system/configs/rpcs3.corenames
endef

RPCS3_PRE_CONFIGURE_HOOKS += RPCS3_PRE_CONFIGURE \
							 RPCS3_PRE_CONFIGURE_FFMPEG_CORE

RPCS3_SUPPORTS_IN_SOURCE_BUILD = NO

RPCS3_CONF_OPTS += -DCMAKE_BUILD_TYPE=Release
RPCS3_CONF_OPTS += -DCMAKE_INSTALL_PREFIX=/usr
RPCS3_CONF_OPTS += -DCMAKE_CROSSCOMPILING=ON
RPCS3_CONF_OPTS += -DBUILD_SHARED_LIBS=OFF
RPCS3_CONF_OPTS += -DUSE_NATIVE_INSTRUCTIONS=OFF
RPCS3_CONF_OPTS += -DLLVM_DIR=$(STAGING_DIR)/usr/lib/cmake/llvm/
RPCS3_CONF_OPTS += -DUSE_PRECOMPILED_HEADERS=OFF
RPCS3_CONF_OPTS += -DSTATIC_LINK_LLVM=OFF
RPCS3_CONF_OPTS += -DBUILD_LLVM=OFF
RPCS3_CONF_OPTS += -DUSE_SYSTEM_FFMPEG=OFF
RPCS3_CONF_OPTS += -DUSE_SYSTEM_CURL=ON
RPCS3_CONF_OPTS += -DUSE_SYSTEM_LIBUSB=ON
RPCS3_CONF_OPTS += -DUSE_SDL=ON
RPCS3_CONF_OPTS += -DENABLE_GLSLANG_BINARIES=ON
RPCS3_CONF_OPTS += -DUSE_LIBEVDEV=OFF
RPCS3_CONF_OPTS += -DUSE_SYSTEM_FAUDIO=OFF
RPCS3_CONF_OPTS += -DENABLE_AMD_EXTENSIONS=ON
RPCS3_CONF_OPTS += -DENABLE_NV_EXTENSIONS=ON
RPCS3_CONF_OPTS += -DFFMPEG_LIB_AVCODEC=$(@D)/3rdparty/ffmpeg/lib/libavcodec.a
RPCS3_CONF_OPTS += -DFFMPEG_LIB_AVFORMAT=$(@D)/3rdparty/ffmpeg/lib/libavformat.a
RPCS3_CONF_OPTS += -DFFMPEG_LIB_AVUTIL=$(@D)/3rdparty/ffmpeg/lib/libavutil.a
RPCS3_CONF_OPTS += -DFFMPEG_LIB_SWSCALE=$(@D)/3rdparty/ffmpeg/lib/libswscale.a
RPCS3_CONF_OPTS += -DFFMPEG_LIB_SWRESAMPLE=$(@D)/3rdparty/ffmpeg/libswresample.a
RPCS3_CONF_ENV = LIBS="-ncurses -ltinfo"

define RPCS3_BUILD_CMDS
	mkdir -p $(TARGET_DIR)/recalbox/share_init/bios/ps3
	mkdir -p $(TARGET_DIR)/recalbox/share_init/saves/ps3/rpcs3
	$(TARGET_CONFIGURE_OPTS) \
		$(MAKE) -C $(@D)/buildroot-build
endef

define RPCS3_INSTALL_EVMAPY
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs/evmapy

	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/rpcs3/rpcs3.keys \
		$(TARGET_DIR)/recalbox/share_init/system/configs/evmapy
endef

RPCS3_POST_INSTALL_TARGET_HOOKS = RPCS3_INSTALL_EVMAPY

$(eval $(cmake-package))
