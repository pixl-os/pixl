################################################################################
#
# PIXL_MANAGER
#
################################################################################
# version 2.2.16 - 24/02/2025
PIXL_MANAGER_VERSION = 6308debab5629f6387e72939d57c4f810daf68d2
PIXL_MANAGER_SITE = https://gitlab.com/pixl-os/pixl-manager
PIXL_MANAGER_LICENSE = COPYRIGHT
PIXL_MANAGER_NON_COMMERCIAL = y

PIXL_MANAGER_SITE_METHOD = git

PIXL_MANAGER_DEPENDENCIES = nodejs

define PIXL_MANAGER_BUILD_CMDS
	$(NPM) --prefix $(@D) run installboth
	$(NPM) --prefix $(@D) run buildboth
	rm -rf $(@D)/release
	mkdir -p $(@D)/release/config
	mkdir -p $(@D)/release/client
	mkdir -p $(@D)/release/locales
	cp -R $(@D)/client/build $(@D)/release/client
	find $(@D) -type f -name '*.map' -exec rm {} \;
	cp $(@D)/config/default.js $(@D)/release/config
	cp $(@D)/config/production.js $(@D)/release/config
	cp -R $(@D)/dist $(@D)/release
	cp $(@D)/locales/*.json $(@D)/release/locales
	cp $(@D)/package.json $(@D)/release
	$(NPM) install --production $(@D)/release/ --prefix $(@D)/release/
endef

define PIXL_MANAGER_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/usr/pixl-manager
	cp -r $(@D)/release/* $(TARGET_DIR)/usr/pixl-manager
endef

$(eval $(generic-package))
