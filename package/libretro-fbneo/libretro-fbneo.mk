################################################################################
#
# FBNEO
#
################################################################################

# Version: Commits on Oct 10, 2022 - v1.0.0.03
LIBRETRO_FBNEO_VERSION = 4d0777dc8d07bffa1e89ffe1c9e8921e8a6da3cf
LIBRETRO_FBNEO_SITE = $(call github,libretro,FBNeo,$(LIBRETRO_FBNEO_VERSION))
LIBRETRO_FBNEO_LICENSE = COPYRIGHT
LIBRETRO_FBNEO_LICENSE_FILES = LICENSE.md
LIBRETRO_FBNEO_NON_COMMERCIAL = y

ifeq ($(BR2_ARM_CPU_HAS_NEON),y)
LIBRETRO_FBNEO_OPTIONS += "HAVE_NEON=1"
else
LIBRETRO_FBNEO_OPTIONS += "HAVE_NEON=0"
endif

#set this parameter from board selection in retroarch build
LIBRETRO_FBNEO_PLATFORM = $(RETROARCH_LIBRETRO_BOARD)

ifeq ($(BR2_arm),y)
LIBRETRO_FBNEO_OPTIONS += USE_CYCLONE=1
endif

ifeq ($(BR2_x86_64),y)
#add now profile in options
LIBRETRO_FBNEO_OPTIONS += USE_X64_DRC=1 profile=accuracy
#force to be sure that unix platform is well selected without using board parameter
LIBRETRO_FBNEO_PLATFORM += unix
endif

ifeq ($(BR2_PACKAGE_RECALBOX_TARGET_ODROIDGO2),y)
LIBRETRO_FBNEO_PLATFORM += odroidgo2
endif

define LIBRETRO_FBNEO_BUILD_CMDS
	CFLAGS="$(TARGET_CFLAGS) $(COMPILER_COMMONS_CFLAGS_SO)" \
		CXXFLAGS="$(TARGET_CXXFLAGS) $(COMPILER_COMMONS_CXXFLAGS_SO)" \
		LDFLAGS="$(TARGET_LDFLAGS) $(COMPILER_COMMONS_LDFLAGS_SO)" \
		$(MAKE) CXX="$(TARGET_CXX)" CC="$(TARGET_CC)" -C $(@D)/src/burner/libretro -f Makefile platform="$(LIBRETRO_FBNEO_PLATFORM)" $(LIBRETRO_FBNEO_OPTIONS) \
			GIT_VERSION="$(shell echo $(LIBRETRO_FBNEO_VERSION) | cut -c 1-7)"
endef

define LIBRETRO_FBNEO_INSTALL_TARGET_CMDS
	$(INSTALL) -D $(@D)/src/burner/libretro/fbneo_libretro.so \
		$(TARGET_DIR)/usr/lib/libretro/fbneo_libretro.so
	mkdir -p $(TARGET_DIR)/recalbox/share_upgrade/bios/fbneo/samples
	cp -R $(@D)/dats/* $(TARGET_DIR)/recalbox/share_upgrade/bios/fbneo
	cp -R $(@D)/metadata/* $(TARGET_DIR)/recalbox/share_upgrade/bios/fbneo
endef

$(eval $(generic-package))
