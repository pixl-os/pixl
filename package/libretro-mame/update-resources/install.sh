#!/bin/bash

# To manage mandatory parameters

helpFunction()
{
	echo "Usage: $0 -e existingVersion -n newVersion -c componentName"
	echo "-e Version of the existing installed version as '0.0.1'"
	echo "-n Version of the existing installed version as '0.0.2'"
	echo "-c name of the component as know by Pegasus"
	exit 1 # Exit script after printing help
}

while getopts ":e:n:c:?" opt; do
	case "$opt" in
		e ) existingVersion=${OPTARG} ;;
		n ) newVersion="$OPTARG" ;;
		c ) componentName="$OPTARG" ;;
		? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
	esac
done

#echo for debug
#echo "$existingVersion"
#echo "$newVersion"
#echo "$componentName"

# Initialization of progress and state
echo "0.1" > /tmp/$componentName/progress.log
echo "Installation of $componentName..." > /tmp/$componentName/install.log
echo "0" > /tmp/$componentName/install.err

#authorize file system udpate on root
mount -o remount,rw /

# Print helpFunction in case parameters are empty
if [ -z "$existingVersion" ] || [ -z "$newVersion" ] || [ -z "$componentName" ]
then
	echo "Some or all of the parameters are empty" > /tmp/$componentName/install.log
	echo "1" > /tmp/$componentName/install.err
	helpFunction
fi

# Begin script in case all parameters are correct
# Unzip package in /tmp/$componantName/package directory
if test -n "$(find /tmp/$componentName -maxdepth 1 -name 'package.zip' -print -quit)"
then
	if [ ! -d "/tmp/$componentName/package" ]
	then
		#creation of package directory in component one
		echo "0.2" > /tmp/$componentName/progress.log
		echo "Creation of directory /tmp/$componentName/package" > /tmp/$componentName/install.log
		mkdir /tmp/$componentName/package 
		if [ $? -eq 0 ]
		then
			echo "0.3" > /tmp/$componentName/progress.log
			echo "Directory /tmp/$componentName/package created" > /tmp/$componentName/install.log
		else
			echo "Directory /tmp/$componentName/package creation failed" > /tmp/$componentName/install.log
			echo "1" > /tmp/$componentName/install.err
			exit $?
		fi
	fi
	# Unzip package
	echo "0.4" > /tmp/$componentName/progress.log
	echo "Unzip of package..." > /tmp/$componentName/install.log
	unzip -o /tmp/$componentName/package.zip -d /tmp/$componentName/package
	if [ $? -eq 0 ]
	then
		echo "0.5" > /tmp/$componentName/progress.log
		echo "Unzip of package done" > /tmp/$componentName/install.log
	else
		echo "Unzip of package failed" > /tmp/$componentName/install.log
		echo "1" > /tmp/$componentName/install.err
		exit $?
	fi
	# Do backup of previous version
	#***************************************begin of part to customize*****************************************************
	cp /usr/lib/libretro/mame_libretro.so /usr/lib/libretro/mame_libretro.so_v$existingVersion
	if [ $? -eq 0 ]
	then
		echo "0.6" > /tmp/$componentName/progress.log
		echo "Backup of existing version done" > /tmp/$componentName/install.log
	else
		echo "Backup of existing version failed" > /tmp/$componentName/install.log
		echo "1" > /tmp/$componentName/install.err
		exit $?
	fi
	# Replace mame_libretro.so
	mv -f /tmp/$componentName/package/mame_libretro.so /usr/lib/libretro/mame_libretro.so
	if [ $? -eq 0 ]
	then
		echo "0.7" > /tmp/$componentName/progress.log
		echo "Component move done" > /tmp/$componentName/install.log
		chmod +x /usr/lib/libretro/mame_libretro.so
		if [ $? -eq 0 ]
		then
			echo "0.8" > /tmp/$componentName/progress.log
			echo "Component access right OK" > /tmp/$componentName/install.log
		else
			echo "Component access failed"> /tmp/$componentName/install.log
			echo "1" > /tmp/$componentName/install.err
			exit $?
		fi		
	else
		echo "Component update failed" > /tmp/$componentName/install.log
		echo "1" > /tmp/$componentName/install.err
		exit $?
	fi
	# create folders is not exist !
	mkdir -p /recalbox/share/bios/dc/
	mkdir -p /recalbox/share/bios/mame/
	mkdir -p /recalbox/share/bios/mame/hash
	mkdir -p /recalbox/share/bios/neogeo/
	mkdir -p /recalbox/share/bios/supermodel/
	# Move news dat's files
	mv -f /tmp/$componentName/package/Atomiswave* /recalbox/share/bios/dc/
	mv -f /tmp/$componentName/package/Naomi* /recalbox/share/bios/dc/
	mv -f /tmp/$componentName/package/Mame* /recalbox/share/bios/mame/
	mv -f /tmp/$componentName/package/hash/* /recalbox/share/bios/mame/hash/
	mv -f /tmp/$componentName/package/Neogeo* /recalbox/share/bios/neogeo/
	mv -f /tmp/$componentName/package/Model3* /recalbox/share/bios/supermodel/
	if [ $? -eq 0 ]
	then
		echo "0.9" > /tmp/$componentName/progress.log
		echo "New dat's files move done" > /tmp/$componentName/install.log
	else
		echo "New dat's files move failed"> /tmp/$componentName/install.log
		echo "1" > /tmp/$componentName/install.err
		exit $?
	fi
	echo "1.0" > /tmp/$componentName/progress.log
	echo "Component updated - enjoy !!!" > /tmp/$componentName/install.log
	# Set 0 if end of installation without other action needed, -1 if need restart of Pegasus, -2 if need reboot
	echo "0" > /tmp/$componentName/install.err
	# If OK, we could update the version of core in corresponding .corenames
	emulatorName="retroarch"
	coreName="mame"
	result=""
	result="$(cat /recalbox/share/system/configs/$emulatorName.corenames | grep -i $coreName';' | awk -v FS='(;)' '{print $3}' | tr -d '\n' | tr -d '\r')"
	# Check if file well formatted or not
	if test -n "$result"
	then
		#not null: we could proceed normally
		sed -i 's/'$coreName';'$existingVersion'/'$coreName';'$newVersion'/g' /recalbox/share/system/configs/$emulatorName.corenames
	else
		#value is not found or null - we will write the full line in this case
		if test -n "$(cat /recalbox/share/system/configs/$emulatorName.corenames | grep -i $coreName';')"
		then
			#replace existing line
			sed -i '/'$coreName'/c\MAME;'$coreName';'$newVersion'' /recalbox/share/system/configs/$emulatorName.corenames
		else
			#append a new line
			echo "MAME;$coreName;$newVersion" >> /recalbox/share/system/configs/$emulatorName.corenames
		fi
	fi
	exit 0
	#***************************************end of part to customize*****************************************************
else
	echo "No 'package.zip' available in your sharing in '/tmp/$componentName'" > /tmp/$componentName/install.log
	echo "3" > /tmp/$componentName/install.err
	exit 3
fi	