#!/bin/bash
# pre-requesite: have a ENV variable with authorized github token or "enfile" file in script directory (see enfile.example from the repo)
# example of call of this script for pre-release to stay as draft for manual validation: ./launch_update.sh -v pre-release -s draft -u pixl-os
# example using enfile and time/output for recurrent running: (time ./launch_update.sh) > report_$(date +%Y-%m-%d_%H-%M-%S).txt 2>&1
# example to force "republish" in case of issue: ./launch_update.sh -s republish

if test -f "envfile" ; then
  # Load environment variables from .env file but could be used also to load parameters without adding to command line finally ;-)
  set -o allexport; source envfile; set +o allexport
fi

# Check if required tool is installed
if ! command -v git &> /dev/null; then
  echo "Error: Please install git before running this script."
  exit 1
fi
# Check if required tool is installed
if ! command -v curl &> /dev/null; then
  echo "Error: Please install curl before running this script."
  exit 1
fi
# Check if required tool is installed
if ! command -v gh &> /dev/null; then
  echo "Error: Please install gh before running this script."
  exit 1
fi
# Check if required tool is installed
if ! command -v jq &> /dev/null; then
  echo "Error: Please install jq before running this script."
  exit 1
fi

helpFunction()
{
   echo "Usage: $0 -t update_type -s final_state -u github_user -g github_token"
   echo "-t type of update as 'pre-release' (for pixL Beta) or 'release' (For pixL Release)"
   echo "-s final state as 'draft' or 'publish' or 'republish'(to redeliver files/update info)"
   echo "-u github targeted user"
   echo "-g github token (optional and for test purpose only - environment variable is prefered for security reason)"
   exit 1 # Exit script after printing help
}

while getopts ":t:s:u:?" opt; do
   case "$opt" in
      # Define type of update: could be pre-release/release
      t ) update_type="$OPTARG" ;;
      s ) final_state="$OPTARG" ;;
      u ) github_user="$OPTARG" ;;
      g ) github_token="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

#------------------------------ checks before to build update ---------------------------------

# Print helpFunction in case parameters are empty
if [ -z "$update_type" ] || [ -z "$final_state" ] || [ -z "$github_user" ] || [ -z "$github_token" ]
then
   echo "Some or all of the parameters are empty"
   helpFunction
fi

# Define variables (replace with your details)
GITHUB_USER=${github_user}  
GITHUB_REPO=${github_repo}
GITHUB_TOKEN=${github_token}

#clean files in all cases before to get info (in case of any previous stop of the script on issue)
rm -f version.txt
rm -f commit.txt
rm -f date.txt
rm -f release_notes.md
rm -f release_id.txt
rm -f package.zip

./get_mame_repo_info.sh -u libretro -r mame
# Print success or error message
if [ $? -eq 0 ]; then
  echo "get mame repo informations successful!"
else
  echo "get mame repo informations failed!"
  exit 1
fi

# check if a version if available in file (coming from a previous script executed that get version from original repo/alternbative repo)
if test -f "${PWD}/version.txt" ; then
  VERSION=$(cat ${PWD}/version.txt) #format of version in .txt file should follow the standard format as vX.Y.Z
  # remove everything to have numeric version only for thiks mk file
  NUMERIC_VERSION=${VERSION//v/}
  NUMERIC_VERSION=${NUMERIC_VERSION//./}
  # Clean up deactivated
  #rm -f ${PWD}/version.txt
else  
  echo "version.txt not found!"
  exit 1
fi

../../../scripts/update/create_github_version.sh -t $update_type -u $github_user -r libretro-mame
if [ $? -eq 0 ]; then
  echo "create github version successful!"
else
  echo "create github version failed!"
  exit 1
fi

# check if a commit is available in file (coming from a previous script executed that get commit from original repo/alternbative repo)
if test -f "${PWD}/commit.txt" ; then
  COMMIT=$(cat ${PWD}/commit.txt)
  # Clean up deactivated
  #rm -f ${PWD}/commit.txt
else  
  echo "commit.txt not found!"
  exit 1
fi

# check if a commit date is available in file (coming from a previous script executed that get commit date and commit from original repo/alternbative repo)
if test -f "${PWD}/date.txt" ; then
  DATE=$(cat ${PWD}/date.txt)
  # Clean up deactivated
  #rm -f ${PWD}/date.txt
else  
  echo "date.txt not found!"
  exit 1
fi

# check if a release id is available in file (coming from a previous script executed that get commit date and commit from original repo/alternbative repo)
if test -f "${PWD}/release_id.txt" ; then
  RELEASE_ID=$(cat ${PWD}/release_id.txt)
  # Clean up deactivated
  #rm -f ${PWD}/release_id.txt
else  
  echo "release_id.txt not found!"
  exit 1
fi

# check if mk available
mk_file_path="${PWD}/../libretro-mame.mk"   # Path to the file you want to modify
# Check if the file exists
if [[ ! -f "$mk_file_path" ]]; then
  echo "Error: File '$mk_file_path' does not exist."
  exit 1
fi

# check if not already build finally with the good vesrion
#binary file
binary_file_path="${PWD}/../update-resources/package/mame_libretro.so"   # Path to the file you want to modify
TO_BUILD="true"
if [[ -f "$binary_file_path" ]]; then
  #check now version in mk if it is the same finally
  LAST_BUILD_VERSION=$(cat $mk_file_path | grep -i "LIBRETRO_MAME_VERSION_ROMSET = ")
  echo "LAST_BUILD_VERSION : $LAST_BUILD_VERSION"
  if [[ $LAST_BUILD_VERSION =~ $NUMERIC_VERSION ]]; then
    TO_BUILD="false"
    echo "Warning: This new mame version is already built / we will avoid to rebuild it to win time"
    echo "Remark: Delete $binary_file_path if you want to rebuild"
  fi
fi

#------------------------------ build new version for update ---------------------------------  
if [[ $TO_BUILD == "true" ]] ; then
  #come back to root of repo
  cd ../../..
  # first clean builds impacted
  ARCH="x86_64" PACKAGE=libretro-mame-dirclean scripts/linux/recaldocker-in-background.sh
  ARCH="x86_64" PACKAGE=arcade-dat-generator-dirclean scripts/linux/recaldocker-in-background.sh
  #and remove package directory
  rm -r package/libretro-mame/update-resources/package
  # udpate version/commit in mk file
  # replace version from MK
  value_to_replace="LIBRETRO_MAME_VERSION_ROMSET = "  # Value to search for in each line
  new_line="LIBRETRO_MAME_VERSION_ROMSET = ${NUMERIC_VERSION}"      # New line to insert
  # Use sed to replace lines (safer than overwriting)
  sed -i "/$value_to_replace/c\\$new_line" "$mk_file_path"
  # Confirmation message
  if [ $? -eq 0 ]; then
    echo "Replaced lines with '$value_to_replace' with '$new_line' in '$mk_file_path'"
  else 
    echo "Error to replace lines with '$value_to_replace' with '$new_line' in '$mk_file_path'"
    exit 1
  fi

  # replace comment about TAG from MK
  value_to_replace="#TAG:" # Value to search for in each line
  new_line="#TAG: lrmame${NUMERIC_VERSION}" # New line to insert
  # Use sed to replace lines (safer than overwriting)
  sed -i "/$value_to_replace/c\\$new_line" "$mk_file_path"
  # Confirmation message
  if [ $? -eq 0 ]; then
    echo "Replaced lines with '$value_to_replace' with '$new_line' in '$mk_file_path'"
  else 
    echo "Error to replace lines with '$value_to_replace' with '$new_line' in '$mk_file_path'"
    exit 1
  fi

  # replace commit from MK
  value_to_replace="LIBRETRO_MAME_VERSION = "  # Value to search for in each line
  new_line="LIBRETRO_MAME_VERSION = ${COMMIT}"      # New line to insert
  # Use sed to replace lines (safer than overwriting)
  sed -i "/$value_to_replace/c\\$new_line" "$mk_file_path"
  # Confirmation message
  if [ $? -eq 0 ]; then
    echo "Replaced lines with '$value_to_replace' with '$new_line' in '$mk_file_path'"
  else 
    echo "Error to replace lines with '$value_to_replace' with '$new_line' in '$mk_file_path'"
    exit 1
  fi

  # replace date from MK
  value_to_replace="# Commits on"  # Value to search for in each line
  new_line="# Commits on ${DATE}"      # New line to insert
  # Use sed to replace lines (safer than overwriting)
  sed -i "/$value_to_replace/c\\$new_line" "$mk_file_path"
  # Confirmation message
  if [ $? -eq 0 ]; then
    echo "Replaced lines with '$value_to_replace' with '$new_line' in '$mk_file_path'"
  else 
    echo "Error to replace lines with '$value_to_replace' with '$new_line' in '$mk_file_path'"
    exit 1
  fi

  #display content of mk file udpated for test purpose
  #cat $mk_file_path

  # launch libretro-mame build (and buils cleaned also and associated)
  ARCH="x86_64" PACKAGE=libretro-mame scripts/linux/recaldocker-in-background.sh

  #come back to libretro mame update directory
  cd package/libretro-mame/update
fi

#------------------------------ finish update and upload data --------------------------------
../../../scripts/update/github_upload.sh -f "../update-resources/icon.png" -u $github_user -r libretro-mame
if [ $? -eq 0 ]; then
  echo "github upload icon.png successful!"
else
  echo "github upload icon.png failed!"
  exit 1
fi
../../../scripts/update/github_upload.sh -f "../update-resources/picture.png" -u $github_user -r libretro-mame
if [ $? -eq 0 ]; then
  echo "github upload picture.png successful!"
else
  echo "github upload picture.png failed!"
  exit 1
fi
../../../scripts/update/github_upload.sh -f "../update-resources/install.sh" -u $github_user -r libretro-mame
if [ $? -eq 0 ]; then
  echo "github upload install.sh successful!"
else
  echo "github upload install.sh failed!"
  exit 1
fi
../../../scripts/update/github_upload.sh -f "../update-resources/version.sh" -u $github_user -r libretro-mame
if [ $? -eq 0 ]; then
  echo "github upload version.sh successful!"
else
  echo "github upload version.sh failed!"
  exit 1
fi

# Path to the directory you want to compress
dir_to_zip="../update-resources/package"
# Name for the archive file (without the .zip extension)
archive_name="package"

# Check if the directory exists
if [[ ! -d "$dir_to_zip" ]]; then
  echo "Error: Directory '$dir_to_zip' does not exist."
  exit 1
fi

# Create the archive file with .zip extension
archive_file="${PWD}/$archive_name.zip"

# Use zip command to create the archive
# -r option: recursively compress directories within the specified directory
# -v option (optional): provides verbose output during compression

#go to update-resources directory
cd "$dir_to_zip"
zip -rv "$archive_file" *
if [ $? -eq 0 ]; then
  # Print success message
  echo "Directory '$dir_to_zip' compressed to '$archive_file'"
else
  echo "Directory '$dir_to_zip' compression to '$archive_file' failed!"
  exit 1
fi
#come back to update directory
cd "../../update/"

../../../scripts/update/github_upload.sh -f "package.zip" -u $github_user -r libretro-mame
if [ $? -eq 0 ]; then
  echo "github upload package.zip successful!"
else
  echo "github upload package.zip failed!"
  exit 1
fi

#publish this version if requested
if [[ $final_state == "publish" ]] ; then
  # Update release draft to be published
  RETURN=$(curl -sSL -X PATCH -H "Accept: application/vnd.github+json" -H "Authorization: token $GITHUB_TOKEN" -H "X-GitHub-Api-Version: 2022-11-28" -d "{ \"tag_name\": \"${VERSION}\", \"draft\":false}" https://api.github.com/repos/$GITHUB_USER/libretro-mame/releases/$RELEASE_ID)
  #echo "RETURN : $RETURN"
  if [ $? -eq 0 ]; then
    echo "publish version successful!"
  else
    echo "publishing version failed!"
    exit 1
  fi
fi

# Clean up if no issue only
rm -f version.txt
rm -f commit.txt
rm -f date.txt
rm -f release_notes.md
rm -f release_id.txt
rm -f package.zip
