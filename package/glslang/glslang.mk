################################################################################
#
# glslang
#
################################################################################

GLSLANG_VERSION = 13.1.1
GLSLANG_SITE =  https://github.com/KhronosGroup/glslang
GLSLANG_SITE_METHOD=git
GLSLANG_DEPENDENCIES = vulkan-headers
GLSLANG_INSTALL_STAGING = YES
GLSLANG_SUPPORTS_IN_SOURCE_BUILD = NO
GLSLANG_LICENSE = GPL-2.0

GLSLANG_CONF_OPTS += -DCMAKE_BUILD_TYPE=Release -Wno-dev
GLSLANG_CONF_ENV += LDFLAGS="--lpthread -ldl"
GLSLANG_CONF_OPTS += -DENABLE_OPT=0

HOST_GLSLANG_CONF_OPTS += -DCMAKE_BUILD_TYPE=Release -Wno-dev
HOST_GLSLANG_CONF_OPTS += -DENABLE_OPT=0

ifeq ($(BR2_PACKAGE_MESA3D),y)
GLSLANG_DEPENDENCIES += mesa3d
endif

$(eval $(cmake-package))
$(eval $(host-cmake-package))
