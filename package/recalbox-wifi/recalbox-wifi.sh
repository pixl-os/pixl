#!/bin/bash

global_interface=/var/run/wpa_supplicant_global
config_file=/recalbox/share/system/recalbox.conf
config_file_boot=/boot/recalbox-backup.conf
wpa_file=/overlay/.configs/wpa_supplicant.conf
INIT_SCRIPT=$(basename "$0")
system_setting="recalbox_settings"

# if /recalbox/share is not yet mounted
if ! [ -f "$config_file" ]; then
  # use the boot version of the file
  config_file="$config_file_boot"
fi

mask2cidr() {
    nbits=0
    IFS=.
    for dec in $1 ; do
        case $dec in
            255) let nbits+=8;;
            254) let nbits+=7;;
            252) let nbits+=6;;
            248) let nbits+=5;;
            240) let nbits+=4;;
            224) let nbits+=3;;
            192) let nbits+=2;;
            128) let nbits+=1;;
            0);;
            *) echo "Error: $dec is not recognised"; exit 1
        esac
    done
    echo "$nbits"
}

wpa_supplicant_command() {
  wpa_cli -g "${global_interface}" "$@" >/dev/null
}

# Function to create wifi profiles based on user settings
rb_wifi_configure() {
  [ "$1" = "1" ] && X="" || X="$1"
  local interface="$2"
  local wifi_ssid="$3"
  settings_ssid=$("$system_setting" -command load -key "wifi${X}.ssid" -source "$config_file")
  if [[ "$wifi_ssid" != "" ]] && [[ "$wifi_ssid" != "$settings_ssid" ]] ;then
    # exit because ssid "forced" is not matching with this wifi configuration saved
    return 0
  fi
  settings_key=$("$system_setting" -command load -key "wifi${X}.key" -source "$config_file")
  settings_ip=$("$system_setting" -command load -key "wifi${X}.ip" -source "$config_file")
  settings_gateway=$("$system_setting" -command load -key "wifi${X}.gateway" -source "$config_file")
  settings_netmask=$("$system_setting" -command load -key "wifi${X}.netmask" -source "$config_file")
  settings_nameservers=$("$system_setting" -command load -key "wifi${X}.nameservers" -source "$config_file")

  # setup wpa_supplicant network
  if [[ "$settings_ssid" != "" ]] ;then

    network=$(wpa_cli -i "$interface" add_network)
    recallog -s "${INIT_SCRIPT}" -t "WIFI" "Configuring wifi for SSID: $settings_ssid in index $network"
    wpa_cli -i "$interface" set_network "$network" ssid "\"$settings_ssid\"" >/dev/null || exit 1
    if [ -n "$settings_key" ]; then
        # Connect to protected wifi
        wpa_cli -i "$interface" set_network "$network" psk "\"$settings_key\"" >/dev/null || exit 1
        wpa_cli -i "$interface" set_network "$network" key_mgmt WPA-PSK WPA-EAP WPA-PSK-SHA256 NONE SAE >/dev/null || exit 1
        wpa_cli -i "$interface" set_network "$network" sae_password "\"$settings_key\"" >/dev/null || exit 1
        wpa_cli -i "$interface" set_network "$network" ieee80211w 1 >/dev/null || exit 1
    else
        # Connect to open ssid
        wpa_cli -i "$interface" set_network "$network" key_mgmt NONE >/dev/null || exit 1
    fi
    wpa_cli -i "$interface" set_network "$network" scan_ssid 1 >/dev/null || exit 1
    wpa_cli -i "$interface" enable_network "$network" >/dev/null || exit 1

    # static ip configuration in dhcpcd.conf
    mount -o remount,rw /
    sed -i "/\b\($interface\|static\)\b/d" /etc/dhcpcd.conf
    if [[ "$settings_ip" != "" ]] && \
      [[ "$settings_gateway" != "" ]] && \
      [[ "$settings_netmask" != "" ]] && \
      [[ "$settings_nameservers" != "" ]]; then
      recallog -s "${INIT_SCRIPT}" -t "WIFI" "static ip configuration"
      settings_netmask=$(mask2cidr "$settings_netmask")
      recallog -s "${INIT_SCRIPT}" -t "WIFI" "static ip_address=$settings_ip/$settings_netmask"
      recallog -s "${INIT_SCRIPT}" -t "WIFI" "static routers=$settings_gateway"
      recallog -s "${INIT_SCRIPT}" -t "WIFI" "static domain_name_servers=$settings_nameservers"
      {
        echo "interface $interface"
        echo "static ip_address=$settings_ip/$settings_netmask"
        echo "static routers=$settings_gateway"
        echo "static domain_name_servers=$settings_nameservers"
      } >> /etc/dhcpcd.conf
    fi
    mount -o remount,ro /
  fi
}

# function to drop all of the wpa_suppliment networks
cleanup_interface() {
  # clear wpa_supplicant configuration and rebuild
  local interface="$1"
  # limit any infinite loop to 10 max
  for i in {1..10}; do

    # get the last network id
    netid=$(wpa_cli -i "$interface" list_networks | tail -n1 | cut -d '	' -f 1)
    netid=$(echo "$netid" | tr " " "\n")

    # exit at the header record
    if [[ $netid == network* ]]; then
      break
    fi

    # remove network id
    wpa_cli -i "$interface" remove_network "$netid" >/dev/null
  done
}

configure_interface() {
  local interface="$1"
  local wifi_ssid="$2"
  wpa_cli -i "$interface" set update_config 1 >/dev/null

  settings_region=$("$system_setting" -command load -key "wifi.region" -source "$config_file")
  wpa_cli -i "$interface" set country "$settings_region" >/dev/null

  cleanup_interface "$interface"
  # iterate through all network ...
  for i in {1..3}; do
    rb_wifi_configure "$i" "$interface" "$wifi_ssid"
  done

  # write wpa_supplicant configuration
  wpa_cli -i "$interface" save_config >/dev/null
}

# start wpa_supplicant
enable_wifi() {
  local interface
  pgrep wpa_supplicant >/dev/null || wpa_supplicant -B -M -i wlan* -c "$wpa_file" -Dnl80211,wext -u -f /var/log/wpa_supplicant.log -g "${global_interface}"
  while ! wpa_supplicant_command interface; do
    sleep 0.2
  done
  # enable wifi for plugged interface
  wpa_cli -g /var/run/wpa_supplicant_global interface | sed '1,/^Available interface/d' | while read -r interface; do
    configure_interface "$interface"
  done
}

# start wpa_supplicant but for first one (in priority one)
connect_wifi() {
  local interface
  local wifi_ssid="$1"
  
  pgrep wpa_supplicant >/dev/null || wpa_supplicant -B -M -i wlan* -c "$wpa_file" -Dnl80211,wext -u -f /var/log/wpa_supplicant.log -g "${global_interface}"
  while ! wpa_supplicant_command interface; do
    sleep 0.2
  done
  # enable wifi for plugged interface
  wpa_cli -g /var/run/wpa_supplicant_global interface | sed '1,/^Available interface/d' | while read -r interface; do
    configure_interface "$interface" "$wifi_ssid"
  done
}

# stop wpa_supplicant
disable_wifi() {
  wpa_supplicant_command terminate
  local COUNT=50
  while pgrep wpa_supplicant >/dev/null; do
    sleep 0.2;
    COUNT=$((COUNT-1))
    if [ "$COUNT" -eq 0 ]; then
      pkill -9 wpa_supplicant
      break
    fi
  done
}

set_default_config() {
  [ ! -s "${wpa_file}" ] && echo "# WPA config file" >"${wpa_file}"
  sed -i -e '/^ap_scan=/{h;s/=.*/=1/};${x;/^$/{s//ap_scan=1/;H};x}' "${wpa_file}"
  sed -i -e '/^ctrl_interface=/{h;s#=.*#=/var/run/wpa_supplicant#};${x;/^$/{s##ctrl_interface=/var/run/wpa_supplicant#;H};x}' "${wpa_file}"
}

# #for testing/debugging
# # Check if the function exists (bash specific)
# if declare -f "$1" > /dev/null
# then
  # # call arguments verbatim
  # echo "launched function manually: $@"
  # "$@"
  # #to let update of filesystem during debugging/testing
  # mount -o remount,rw /
  # exit 0
# else
  # # Show a helpful error
  # echo "'$1' is not a known function name" >&2
  # exit 1
# fi
