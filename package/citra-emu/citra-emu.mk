################################################################################
#
# CITRA_EMU
#
################################################################################

CITRA_EMU_VERSION = r4f174f1
CITRA_EMU_SITE = https://github.com/PabloMK7/citra.git
CITRA_EMU_SITE_METHOD = git
CITRA_EMU_GIT_SUBMODULES = YES
CITRA_EMU_LICENSE = GPLv2
CITRA_EMU_DEPENDENCIES += fmt boost ffmpeg sdl2 fdk-aac zlib qt6base qt6tools qt6multimedia
CITRA_EMU_SUPPORTS_IN_SOURCE_BUILD = NO

## print version of core in PixL
define CITRA_EMU_PRE_CONFIGURE
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs
	echo "Citra-emu;citra;$(CITRA_EMU_VERSION)" > $(TARGET_DIR)/recalbox/share_init/system/configs/citra.corenames
endef
CITRA_EMU_PRE_CONFIGURE_HOOKS += CITRA_EMU_PRE_CONFIGURE

CITRA_EMU_CONF_OPTS += -DCMAKE_BUILD_TYPE=Release
CITRA_EMU_CONF_OPTS += -DBUILD_SHARED_LIBS=OFF
CITRA_EMU_CONF_OPTS += -DENABLE_TESTS=OFF
CITRA_EMU_CONF_OPTS += -DENABLE_DEDICATED_ROOM=ON
CITRA_EMU_CONF_OPTS += -DENABLE_WEB_SERVICE=OFF
CITRA_EMU_CONF_OPTS += -DENABLE_OPENAL=OFF
CITRA_EMU_CONF_OPTS += -DUSE_DISCORD_PRESENCE=OFF
CITRA_EMU_CONF_OPTS += -DCITRA_WARNINGS_AS_ERRORS=OFF
CITRA_EMU_CONF_OPTS += -DCITRA_ENABLE_COMPATIBILITY_REPORTING=ON
CITRA_EMU_CONF_OPTS += -DENABLE_COMPATIBILITY_LIST_DOWNLOAD=ON
CITRA_EMU_CONF_OPTS += -DUSE_SYSTEM_BOOST=ON
CITRA_EMU_CONF_OPTS += -DENABLE_SDL2=ON
CITRA_EMU_CONF_OPTS += -DUSE_SYSTEM_SDL2=ON    # important to avoid HIDAPI
CITRA_EMU_CONF_OPTS += -DENABLE_LTO=OFF
CITRA_EMU_CONF_OPTS += -DENABLE_QT=ON # Enable the Qt frontend
CITRA_EMU_CONF_OPTS += -DENABLE_QT_TRANSLATION=ON # Enable translations for the Qt frontend
CITRA_EMU_CONF_OPTS += -DENABLE_QT_UPDATER=OFF
CITRA_EMU_CONF_OPTS += -DCITRA_ENABLE_BUNDLE_TARGET=ON
CITRA_EMU_CONF_OPTS += -DENABLE_OPENAL=OFF
CITRA_EMU_CONF_OPTS += -DCMAKE_C_ARCHIVE_CREATE="<CMAKE_AR> qcs <TARGET> <LINK_FLAGS> <OBJECTS>"
CITRA_EMU_CONF_OPTS += -DCMAKE_C_ARCHIVE_FINISH=true
CITRA_EMU_CONF_OPTS += -DCMAKE_CXX_ARCHIVE_CREATE="<CMAKE_AR> qcs <TARGET> <LINK_FLAGS> <OBJECTS>"
CITRA_EMU_CONF_OPTS += -DCMAKE_CXX_ARCHIVE_FINISH=true
CITRA_EMU_CONF_OPTS += -DCMAKE_AR="$(TARGET_CC)-ar"
CITRA_EMU_CONF_OPTS += -DCMAKE_C_COMPILER="$(TARGET_CC)"
CITRA_EMU_CONF_OPTS += -DCMAKE_CXX_COMPILER="$(TARGET_CXX)"
CITRA_EMU_CONF_OPTS += -DCMAKE_LINKER="$(TARGET_LD)"
CITRA_EMU_CONF_OPTS += -DCMAKE_EXE_LINKER_FLAGS="$(COMPILER_COMMONS_LDFLAGS_EXE)"

CITRA_EMU_CONF_ENV += LDFLAGS=-lpthread

define CITRA_EMU_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/usr/bin
	$(INSTALL) -D $(@D)/buildroot-build/bin/Release/citra-qt \
		$(TARGET_DIR)/usr/bin/citra
	$(INSTALL) -D $(@D)/buildroot-build/bin/Release/citra-room \
		$(TARGET_DIR)/usr/bin/citra-room

	# To create an update online package
	mkdir -p $(BR2_EXTERNAL_RECALBOX_PATH)/package/citra-emu/update-resources/package/
	$(INSTALL) -D $(@D)/buildroot-build/bin/Release/citra-qt \
		$(BR2_EXTERNAL_RECALBOX_PATH)/package/citra-emu/update-resources/package/citra
	$(INSTALL) -D $(@D)/buildroot-build/bin/Release/citra-room \
		$(BR2_EXTERNAL_RECALBOX_PATH)/package/citra/update-resources/package/citra-room
endef

# Hotkeys using evmapy
define CITRA_EMU_EVMAP
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs/evmapy
	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/citra-emu/citra.keys \
		$(TARGET_DIR)/recalbox/share_init/system/configs/evmapy
endef
CITRA_EMU_POST_INSTALL_TARGET_HOOKS = CITRA_EMU_EVMAP

$(eval $(cmake-package))
