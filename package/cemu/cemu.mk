################################################################################
#
# CEMU
#
################################################################################
# if update version CEMU_VERSION, update CEMU_GRAPHIC_PACKS_VERSION
# Commits on Jun 09, 2024
CEMU_VERSION = v2.0-86
CEMU_SITE = https://github.com/cemu-project/Cemu.git
CEMU_SUPPORTS_IN_SOURCE_BUILD = NO
CEMU_GIT_SUBMODULES = YES
CEMU_SITE_METHOD = git
CEMU_LICENSE = GPL-2.0+
CEMU_LICENSE_FILES = license.txt
CEMU_DEPENDENCIES = sdl2 host-libcurl host-pugixml pugixml rapidjson boost libpng \
					libzip host-glslang glslang zlib zstd wxwidgets fmt glm wayland wayland-protocols

CEMU_SUPPORTS_IN_SOURCE_BUILD = NO
## print version of core in PixL
define CEMU_PRE_CONFIGURE
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs
	echo "cemu;Cemu;$(CEMU_VERSION)" > $(TARGET_DIR)/recalbox/share_init/system/configs/cemu.corenames
endef

CEMU_CONF_OPTS += -DCMAKE_BUILD_TYPE=Release
CEMU_PRE_CONFIGURE_HOOKS += CEMU_PRE_CONFIGURE

CEMU_GRAPHIC_PACKS_VERSION = 909
CEMU_GRAPHIC_PACKS_SOURCE = graphicPacks$(CEMU_GRAPHIC_PACKS_VERSION).zip
CEMU_GRAPHIC_PACKS_SITE = https://github.com/cemu-project/cemu_graphic_packs
CEMU_EXTRA_DOWNLOADS = $(CEMU_GRAPHIC_PACKS_SITE)/releases/download/Github$(CEMU_GRAPHIC_PACKS_VERSION)/$(CEMU_GRAPHIC_PACKS_SOURCE)

CEMU_CONF_OPTS += -DCMAKE_BUILD_TYPE=release -Wno-dev
CEMU_CONF_OPTS += -DBUILD_SHARED_LIBS=OFF
CEMU_CONF_OPTS += -DENABLE_DISCORD_RPC=OFF
CEMU_CONF_OPTS += -DENABLE_VCPKG=OFF
CEMU_CONF_OPTS += -DPORTABLE=OFF
CEMU_CONF_OPTS += -DENABLE_SDL=ON
CEMU_CONF_OPTS += -DENABLE_WAYLAND=ON
CEMU_CONF_OPTS += -DCMAKE_CXX_FLAGS="$(TARGET_CXXFLAGS) -I$(STAGING_DIR)/usr/include/glslang"
CEMU_CONF_OPTS += -DENABLE_FERAL_GAMEMODE=OFF
CEMU_CONF_OPTS += -DENABLE_HIDAPI=OFF
CEMU_CONF_OPTS += -Wno-dev

define CEMU_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/usr/bin/cemu/
	#Rename Cemu_release to cemu since v2.0-62
	mv -f $(@D)/bin/Cemu_release $(@D)/bin/cemu
	cp -pr $(@D)/bin/{cemu,gameProfiles,resources} $(TARGET_DIR)/usr/bin/cemu/
	mkdir -p $(TARGET_DIR)/usr/share/recalbox/share/bios/cemu
	$(INSTALL) -m 0755 -D $(BR2_EXTERNAL_RECALBOX_PATH)/package/cemu/get-audio-device \
	$(TARGET_DIR)/usr/bin/cemu/

	# Install default graphic packs
	$(UNZIP) -d $(@D)/graphicPacks$(CEMU_GRAPHIC_PACKS_VERSION) \
		$(CEMU_DL_DIR)/$(CEMU_GRAPHIC_PACKS_SOURCE)
	mkdir -p $(TARGET_DIR)/recalbox/share_init/saves/wiiu/graphicPacks
	cp -prn $(@D)/graphicPacks$(CEMU_GRAPHIC_PACKS_VERSION)/* \
		$(TARGET_DIR)/recalbox/share_init/saves/wiiu/graphicPacks

	# For Create a update online package
	mkdir -p $(BR2_EXTERNAL_RECALBOX_PATH)/package/cemu/update-resources/package/cemu/{gameProfiles,resources}
	cp -pr $(@D)/bin/{cemu,gameProfiles,resources} \
		$(BR2_EXTERNAL_RECALBOX_PATH)/package/cemu/update-resources/package/cemu
endef

# Hotkeys using evmapy
define CEMU_EVMAP
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs/evmapy

	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/cemu/cemu.keys \
		$(TARGET_DIR)/recalbox/share_init/system/configs/evmapy
endef

# Add default settings file
define CEMU_SETTINGS_ORIGIN
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs/cemu

	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/cemu/settings.origin.xml \
		$(TARGET_DIR)/recalbox/share_init/system/configs/cemu
endef

CEMU_POST_INSTALL_TARGET_HOOKS += CEMU_EVMAP
CEMU_POST_INSTALL_TARGET_HOOKS += CEMU_SETTINGS_ORIGIN

$(eval $(cmake-package))
