################################################################################
#
# WEYLUS
#
################################################################################

#VERSION 2024.8.3
WEYLUS_VERSION = 2024.8.3
WEYLUS_SOURCE = weylus_linux.tar.gz
WEYLUS_SITE =  https://github.com/electronstudio/WeylusCommunityEdition/releases/download/$(WEYLUS_VERSION)
WEYLUS_LICENSE = AGPL-3.0-or-later
WEYLUS_LICENSE_FILES = COPYING

define WEYLUS_EXTRACT_CMDS
	mkdir -p $(@D)/target && cd $(@D)/target && tar xf $(DL_DIR)/$(WEYLUS_DL_SUBDIR)/$(WEYLUS_SOURCE)
endef

define WEYLUS_INSTALL_TARGET_CMDS
	mkdir -p $(TARGET_DIR)/usr/bin
	cp -a $(@D)/target/weylus $(TARGET_DIR)/usr/bin
endef

$(eval $(generic-package))
