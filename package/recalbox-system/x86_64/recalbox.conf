# System Variable
# You can configure your recalbox from here
# To set a variable, remove the first ; on the line


# ------------ A - System Options ----------- #
## Splash screen duration
##  0 : Video will be stopped when Pegasus is ready to start.
## -1 : All the video will be played before Pegasus start (default)
## >0 : Time the video will be played before Pegasus start (in seconds)
system.splash.length=-1
## Splash videos selection
## all: select a boot video in recalbox videos and user videos
## recalbox: select a boot video only in recalbox videos
## custom: select a boot video only in user videos
## Any invalid value means "all"
system.splash.select=all
## Splash video resolution
## available resolution retrieved from ssh : mpv --drm-mode=help
## Force slashvideo playback to resolution ex : 1920x1080
system.splash.resolution=
## to manage redshift / desactivated by default
system.video.redshift=0
## Supend mode (if compatible) / empty by default
system.suspendmode=
## Device conf (if necessary) / empty by default
deviceconf=

## Game screen
## screens mode
system.video.screens.mode=extended
## virtual screens
system.video.screens.virtual=
## Primary screen (enabled by default)
system.primary.screen.enabled=1
system.primary.screen=
## Force selected Game screen to resolution ex : 1920x1080
system.primary.screen.resolution=
## Force selected Game screen to frequency ex: 60.00
system.primary.screen.frequency=
## Force selected rotation : normal, inverted, left, right
system.primary.screen.rotation=
## Backlight brightness management (if available)
system.primary.screen.backlight.device=
screen.brightness=
## Secondary screen
system.secondary.screen.enabled=
system.secondary.screen=
## Force selected Secondary screen to resolution ex : 1920x1080
system.secondary.screen.resolution=
## Force selected Secondary screen to frequency ex: 60.00
system.secondary.screen.frequency=
## Force selected rotation : normal, inverted, left, right
system.secondary.screen.rotation=
## Force position : above, below, left, right
system.secondary.screen.position=

## Pegasus Frontend Configuration
## Themes could stay loaded during gaming to avoid reloading after.
## Theme should be compatible
pegasus.theme.keeploaded=0
## Once enabled, you can run emulators in separate windows and keep pegasus/theme activated.
pegasus.multiwindows=0
## Once enabled, only files from gamelist will be take into account.
## Best game file loading ;-)
pegasus.gamelistonly=0
## Once enabled, system gamelist will be seach in priority else game files will be search.
## Intermediate game file loading
pegasus.gamelistfirst=1
## Once enabled, only media from gamelist will be take into account.
## Best loading ;-) / Less Media :-(
pegasus.deactivateskrapermedia=0
## Once enabled, during Skraper media scan a media.xml is generated.
## Quick loading ;-) / All Media :-)
pegasus.usemedialist=1
## Once enabled, media could be loaded dynamically and when it's requested.
## Less memory used :-) / More impact ;-|
pegasus.mediaondemand=0
## To activate debug mode
pegasus.debuglogs=0
## To activate display of monitoring during game session (compatible with few emulators, not all yet)
system.monitoring.enabled=0
## To set display position of monitoring during game session
## Possible values:
##   bottom-left
##   top-left -> default value
##   top-right
##   bottom-right
system.monitoring.position=top-left

## Choose Vulkan for video driver in emulators (retroarch, dolphin, pcsx2, ppsspp)
system.video.driver.vulkan=0


## Recalbox Manager (http manager)
system.manager.enabled=1
## Currently, only version 2 is available
system.manager.version=2

## Recalbox API (REST)
system.api.enabled=0

## Scrapers
### Activate this option to extract region from filename when possible
;scraper.extractregionfromfilename=1
### Select the source of game's names
### 0 = from scrapper result (default)
### 1 = from filename
### 2 = from filename undecorated (i.e: (text) and [text] removed)
;scraper.getnamefrom=0
### ScreenScraper.fr
### Force media region - if not defined, region is taken from system.language. Default: us
;scraper.screenscraper.region=eu
### Force text language - if not defined, region is taken from system.language. Default: en
;scraper.screenscraper.language=fr
### Choose the media to download among:
### screenshot: game screenshot
### title     : game title screenshot
### box2d     : Front case
### box3d     : 3D rendered case
### mixv1     : Recalbox special mix image V1 (default)
### mixv2     : Recalbox special mix image V2
;scraper.screenscraper.media=mixv1
### ScreenScraper account
;scraper.screenscraper.user=
;scraper.screenscraper.password=

## Emulator special keys
## default -> default all special keys
## nomenu -> cannot popup the emulator menu
## none -> no special keys in emulators
system.emulators.specialkeys=default


# ------------ B - Network ------------ #
## Set system hostname
system.hostname=PIXL
## Activate wifi (0,1)
wifi.enabled=0
## Set wifi region
## More info here: https://wiki.recalbox.com/en/tutorials/network/wifi/wifi-country-code
wifi.region=EU
## Wifi SSID (string)
;wifi.ssid=new ssid
## Wifi KEY (string)
## after rebooting the recalbox, the "new key" is replace by a hidden value "enc:xxxxx"
## you can edit the "enc:xxxxx" value to replace by a clear value, it will be updated again at the following reboot
## Escape your special chars (# ; $) with a backslash : $ => \$
;wifi.key=new key

## Wifi - static IP
## if you want a static IP address, you must set all 4 values (ip, netmask, gateway and nameservers)
## if any value is missing or all lines are commented out, it will fall back to the
## default of DHCP. For nameservers, you must set at least 1 nameserver.
;wifi.ip=manual ip address (ex: 192.168.1.2)
;wifi.netmask=network mask (ex: 255.255.255.0)
;wifi.gateway=ip address of gateway (ex: 192.168.1.1)
;wifi.nameservers=ip address of domain name servers, space separated (ex: 192.168.1.1 8.8.8.8)

# secondary wifi (not configurable via the user interface)
;wifi2.ssid=new ssid
;wifi2.key=new key

# third wifi (not configurable via the user interface)
;wifi3.ssid=new ssid
;wifi3.key=new key

## Samba share
system.samba.enabled=1
### Virtual Gamepads
system.virtual-gamepads.enabled=1
### SSH
system.ssh.enabled=1



# ------------ C - Audio ------------ #
## Set the audio device (auto, hdmi, jack)
audio.device=auto
## Set system volume (0..100)
audio.volume=90
## Enable or disable system sounds in ES (0,1)
audio.bgmusic=1
## Mode in Pegasus-Frontend
audio.mode=musicandvideosound

# -------------- D - Controllers ----------------- #
# Enable support for standard bluetooth controllers
controllers.bluetooth.enabled=1
# Enable support of Bluetooth autopairingn / Activated by default
controllers.bluetooth.autopair=1
# Bluetooth methods (adviced methods already set by default)
controllers.bluetooth.scan.methods=2
controllers.bluetooth.pair.methods=0
controllers.bluetooth.unpair.methods=0

# Parameter to filter bluetooth device displayed
controllers.bluetooth.hide.no.name=1
controllers.bluetooth.hide.unknown.vendor=0

# Paremeter to force Bluetooth restart at Pegasus-Frontend launch
controllers.bluetooth.startreset=1

# Enable ERTM
#deactivated by default
controllers.bluetooth.ertm=0


## Please enable only one of these
# -------------- D1 - PS3 Controllers ------------ #
##Enable PS3 controllers support
##deactivated by default
controllers.ps3.enabled=0
## Choose a driver between bluez, official and shanwan
## bluez -> bluez 5 + kernel drivers, support official and shanwan sisaxis
## official -> sixad drivers, support official and gasia sisaxis
## shanwan -> shanwan drivers, support official and shanwan sisaxis
controllers.ps3.driver=bluez


# ------------ D2 - GPIO Controllers ------------ #
## GPIO Controllers
## enable controllers on GPIO with mk_arcarde_joystick_rpi (0,1)
## deactivated by default
controllers.gpio.enabled=0
## mk_gpio arguments, map=1 for one controller, map=1,2 for 2 (map=1,map=1,2)
controllers.gpio.args=map=1,2


## DB9 Controllers
## Enable DB9 drivers for atari, megadrive, amiga controllers (0,1)
## deactivated by default
controllers.db9.enabled=0
## db9 arguments
controllers.db9.args=map=1

## Gamecon controllers
## Enable gamecon controllers, for nes, snes, psx (0,1)
controllers.gamecon.enabled=0
## gamecon_args
controllers.gamecon.args=map=1

## XGaming's XArcade Tankstick and other compatible devices
## deactivated by default
controllers.xarcade.enabled=0

# ------------ D3 - Dolphin Controllers ------------- #
# Wiimote sensor bar position
# set position to 1 for the sensor bar at the top of the screen, to 0 for the sensor bar at the bottom
wii.sensorbar.position=1

# Real wiimotes must not be paired with recalbox system so that they can work with the wii emulator
# set realwiimotes to 1 to use authentics Wiimotes pads on default
wii.realwiimotes=0
wii.emulatedwiimotes.nunchuck=1
wii.emulatedwiimotes.buttons.mapping=1

# Gamecube Pads
# Real gamecube pads work only in dolphin with "Wiiu adapter gamecube controllers" so that they can work with the gamecube emulator
# set realgamcubepads to 1 to use authentics Gamecube pads on default
gamecube.realgamecubepads=0

# ------------ D4 - Joycon Controllers ------------ #
#deactivated by default to be compatible with Nintendo Switch NES Controller
controllers.joycond.enabled=0

# ------------ D5 - Xbox one/Series Controllers ------------- #
#deactivated by default - need Microsoft dongles for that
controllers.xow.enabled=0

# ------------ E - Language and keyboard ------------ #
## Set the language of the system (fr_FR,en_US,en_GB,de_DE,pt_BR,es_ES,it_IT,eu_ES,tr_TR,zh_CN)
system.language=en_US
## set the keyboard layout (fr,en,de,us,es)
;system.kblayout=us
## Set you local time
## Select your timezone from : ls /usr/share/zoneinfo/ (string)
;system.timezone=Europe/Paris



# ------------ F - UPDATES ------------ #
## Automatically check for updates at start (0,1)
updates.enabled=1
# Update type : default to stable
updates.type=stable
# Update format : default to image
# set to "image" format to provide img.xz/sha1 download (if available) and need to decompress, mount and copy files after reboot
# set to "raw" format to provide direct download of files (if available) as linux kernel + os + boot.lst for direct upgrade after reboot (quicker)
updates.format=image
# Update last check time / empty by default
updates.lastchecktime=

# ------------ G - HERE IT IS - GLOBAL EMULATOR CONFIGURATION ------------ #
## The global value will be used for all emulators, except if the value
## is redefined in the emulator

## Set game resolution for emulators
## Please don't modify this setting
## This arch does not support the video mode switch
## (string)
global.videomode=default

## Shader set
## Automatically select shaders for all systems
## (none, retro, scanlines, megabezel_under_overlay, megabezel_above_overlay)
global.shaderset=none
# to set default shader border coverage used for Mega Bezel "above" overlays in retroarch (5% as average by default)
global.shaderbordercoverage=5
# to set few default values by system in retroarch (but not all)
fds.shaderbordercoverage=5
gb.shaderbordercoverage=2
gba.shaderbordercoverage=3
gbc.shaderbordercoverage=0
nes.shaderbordercoverage=5
satellaview.shaderbordercoverage=5
sufami.shaderbordercoverage=5
snes.shaderbordercoverage=5
mame.shaderbordercoverage=10
fbneo.shaderbordercoverage=10
naomi.shaderbordercoverage=10
naomi2.shaderbordercoverage=10
atomiswave.shaderbordercoverage=10
lynx.shaderbordercoverage=2

## Once enabled, your screen will be cropped, and you will have a pixel perfect image (0,1)
global.integerscale=0

## Set gpslp shader for all emulators (prefer shadersets above). Absolute path (string)
global.shaders=

## Set ratio for all emulators (auto,4/3,16/9,16/10,custom)
global.ratio=auto

## Set smooth for all emulators (0,1)
global.smooth=1

## Set rewind for all emulators (0,1)
global.rewind=1

## Set autosave/load savestate for all emulators (0,1)
global.autosave=0

## To defined the color of text and selected and menu 
system.text.color=Original
system.selected.color=Original
system.menu.color=Original

## Enable retroarchievements (0,1)
## Set your www.retroachievements.org username/password
## Escape your special chars (# ; $) with a backslash : $ => \$
global.retroachievements=0
global.retroachievements.hardcore=0
global.retroachievements.username=
global.retroachievements.password=
global.retroachievements.token=
global.retroachievements.unlock.sound=0
global.retroachievements.screenshot=0
global.retroachievements.challenge.indicators=0
global.retroachievements.games.search=0

## Set retroarch input driver (auto, udev, sdl2)
## If you don't have issues with your controllers, let auto
global.inputdriver=auto

## If you do not want recalboxOS to generate the configuration for all emulators (string)
;global.configfile=/path/to/my/configfile.cfg

## Demo screensaver parameters
## Set the system list from which ES will run random games
## Empty list or unexisting key means all available systems
global.demo.systemlist=3do,amigacd32,atari2600,atari5200,atari7800,daphne,fbneo,fds,gamegear,gba,lynx,mame,mastersystem,megadrive,neogeo,nes,ngpc,pcengine,sega32x,sg1000,snes
## Default demo game sessions last 90s. Change this value if you want shorter or longer sessions
;global.demo.duration=90
## Default game info screen duration lasts 6s. Change this value if you want shorter or longer info screens.
;global.demo.infoscreenduration=6

## Retroarch AI Translation service
## Comment out or set to 0 the following key if you don't want the AI service
global.translate=1
## Set the source and the target languages.
## Allowed language list: EN, ES, FR, IT, DE, JP, NL, CS, DA, SV, HR, KO, ZH_CN, ZH_TW, CA, BG, BN, EU, AZ, AR, SQ,
##                        AF, EO, ET, TL, FI, GL, KA, EL, GU, HT, IW, HI, HU, IS, ID, GA, KN, LA, LV, LT, MK, MS,
##                        MT, NO, FA, PL, PT, RO, RU, SR, SK, SL, SW, TA, TE, TH, TR, UK, UR, VI, CY, YI
## Setting the translate.from key to a specified language may speed up or give more accurate results
## If translate.to key is commented, the default value is extracted from system.language or, if system.language is
## undefined, set to auto (=EN).
global.translate.from=auto
global.translate.to=auto
## zTranslate API Key
## go to https://ztranslate.net and create an account.
## validate your account, then log in and go to the settings page
## Look for the API Key at the bottom of the page, then uncomment the following key and paste your API Key:
;global.translate.apikey=RECALBOX
## Other translation service
## If you want to use another translation service or a custom API call, use this key to
## specify the url to call. If the key is not empty, it is used instead of zTranslation's API Key
;global.translate.url=



# ------------ H - EMULATORS CHOICES ----------- #
## You can override the global configurations here
## Here is the snes example
;snes.core=snes9x2010
;snes.shaders=/recalbox/share/shaders/shaders_glsl/mysnesshader.gplsp
;snes.ratio=16/9
;snes.smooth=0
;snes.rewind=1
;snes.autosave=0
;snes.emulator=libretro
;snes.integerscale=0
## If you do not want recalboxOS to generate the configuration for the emulator:
;snes.configfile=/path/to/my/configfile.cfg

## NeoGeo emulator
## You can use pifba or a libretro core (fba2x,libretro)
neogeo.emulator=libretro
## If you set libretro as neogeo.emulator, the line below sets the retroarch core (fbneo, mame2000)
neogeo.core=fbneo

## Demo screensaver parameters
## Include or exclude a particular system from the demo screensaver
## You may change the global.demo.systemlist key or include/exclude every single system
;snes.demo.include=0
## Set the session duration for a particular system
;snes.demo.duration=90

## Megaduck emulator
megaduck.integerscale=0
megaduck.shaders=/recalbox/share/shaders/dot-green.glslp


# ------------ I - NETPLAY PARAMETERS ----------- #
## All these values are handled by Recalbox itself
## Retroarch netplay Settings
global.netplay=1
global.netplay.nickname=
global.netplay.port=55435
global.netplay.relay=
global.netplay.systems=fbneo,mame,mastersystem,megadrive,neogeo,nes,pcengine,sega32x,sg1000,snes,supergrafx,atari2600,pcenginecd,pcfx,fds,tic80,segacd,mrboom,colecovision
global.netplay.lobby=http://lobby.libretro.com/list/

## Citra-emu netplay Settings
## Citra-emu Netplay needs a registration on the following site: https://community.citra-emu.org/
## then once your account creates and connected Recovers your token at this address: https://profile.citra-emu.org/
## set your account username
global.3dsnetplay.nickname=
## set your account token
global.3dsnetplay.token=
## set your birthday "month-day"
global.3dsnetplay.birthday=

# ------------ J - EMULATORS PARAMETERS ----------- #
## Wiu emulator cemu
cemu.upscale.filter=1
cemu.downscale.filter=1
cemu.async.compile=1
cemu.vsync=0
cemu.gamepad.activated=0
cemu.gamepad.at.start=0
cemu.gamepad.on.second.display=0
cemu.use.sdl.button.labels=1
cemu.rumble=0

## 3ds emulator Citra-emu
citra.resolution=1
citra.texture.filter=0
citra.screens.layout=0
citra.bottom.screen.on.second.display=0

## Wii/Gamecube emulator dolphin-emu 
dolphin.resolution=1
dolphin.antialiasing=0x00000001
dolphin.cheats=0
dolphin.vsync=1
dolphin.widescreenhack=0
dolphin.disc.change=0

## Triforce emulator dolphin-triforce
dolphin-triforce.resolution=1
dolphin-triforce.antialiasing=0x00000001
dolphin-triforce.vsync=0
dolphin-triforce.widescreenhack=0

# Psx emulator duckstation 
duckstation.resolution=1 
duckstation.cheats=0
duckstation.vsync=0

## Ps2 emulator Pcsx2
pcsx2.tvshaders=0
pcsx2.resolution=1
pcsx2.anisotropy=0
pcsx2.cheats=0
pcsx2.vsync=0
pcsx2.injectsystemlanguage=1
pcsx2.gui=0
pcsx2.fastboot=0
pcsx2.crosshairs=1
pcsx2.splitscreen.hack=1
pcsx2.splitscreen.fullstretch=0

## Psp emulator PPSSPP
ppsspp.anisotropy.level=0
ppsspp.msaa=0
ppsspp.texture.shader=0
ppsspp.texture.scaling.level=0
ppsspp.texture.filter=0
ppsspp.resolution=1
ppsspp.force.60fps=0
ppsspp.texture.scaling.type=0

## Retroarch Multi platform emulator
retroarch.swap.menu.button=1
retroarch.load.content.animation=0
retroarch.color.theme.menu=1

## Sony PS3 emulator Rpcs3
rpcs3.resolution=100
rpcs3.scanline=Nearest
rpcs3.FidelityFX=50
rpcs3.vsync=0
rpcs3.network=0
rpcs3.upnp=0
rpcs3.rpcn=0
rpcs3.theme=none
rpcs3.rpcn.username=
rpcs3.rpcn.token=
rpcs3.rpcn.password=

## Model2 emulator model2-emu
model2emu.xinput=0
model2emu.fakeGouraud=0
model2emu.bilinearFiltering=0
model2emu.trilinearFiltering=0
model2emu.filterTilemaps=0
model2emu.forceManaged=0
model2emu.enableMIP=0
model2emu.meshTransparency=0
model2emu.fullscreenAA=0
model2emu.scanlines=0
# Default wine conf for model2emu (if empty, we will use default conf from configgen)
model2emu.wine=
model2emu.wineappimage=
model2emu.winearch=
model2emu.winver=
model2emu.winesoftrenderer=0
model2emu.wineaudiodriver=

## Model3 emulator supermodel
supermodel.flip.stereo=0
supermodel.multi.texture=0
supermodel.gpu.threaded=1
supermodel.resolution=auto
supermodel.deadzone=2
supermodel.saturation=100
supermodel.sensitivity=25
supermodel.address.out=127.0.0.1
supermodel.port.in=1970
supermodel.port.out=1971
supermodel.network=0
supermodel.quad.rendering=0
supermodel.new3d.engine=1
supermodel.crosshairs=0
supermodel.supersampling=1
supermodel.crtcolors=0
supermodel.upscalemode=0

## Xbox emulator Xemu
xemu.resolution=1

## To set by default on PC the libretro mame
mame.emulator=libretro
mame.core=mame

## To activate overlay
global.recalboxoverlays=1

## Lightguns parameters
lightgun.sinden.bordersize=thin
lightgun.sinden.crossair.enabled=1
lightgun.sinden.bordercolor=white
lightgun.sinden.recoilmode=none

## Configurations generated by Recalbox
