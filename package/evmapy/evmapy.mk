################################################################################
#
# EVMAPY
#
################################################################################

EVMAPY_VERSION = bd65338c236cd30b4f2d7835733ea5d6b108b75d
EVMAPY_SITE =  $(call github,kempniu,evmapy,$(EVMAPY_VERSION))
EVMAPY_SETUP_TYPE = setuptools
EVMAPY_LICENSE = GPL-2.0

define EVMAPY_FIXCHARS
	$(SED) "s|\xc5\x82|l|g; s|\xc4\x99|e|g; s|\xc5\x84|n|g" $(@D)/*.py $(@D)/evmapy/*.py
endef
EVMAPY_PRE_CONFIGURE_HOOKS += EVMAPY_FIXCHARS

define EVMAPY_INSTALL_SCRIPTS
	mkdir -p $(TARGET_DIR)/usr/bin
	$(INSTALL) -D -m 0755 $(BR2_EXTERNAL_RECALBOX_PATH)/package/evmapy/pixl-evmapy $(TARGET_DIR)/usr/bin
endef

EVMAPY_POST_INSTALL_TARGET_HOOKS = EVMAPY_INSTALL_SCRIPTS

$(eval $(python-package))
