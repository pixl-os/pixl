################################################################################
#
# pixL themes for Pegasus : https://github.com/pixl-os/gameOS
#
################################################################################

# commit from pixL-master
# https://github.com/pixl-os/gameOS/
# version 1.30 - 2025-02-28
GAMEOS_VERSION = 1afe9a8b6e06c8e8ef30017fd4cba5061558932d
GAMEOS_SITE = $(call github,pixl-os,gameOS,$(GAMEOS_VERSION))
GAMEOS_LICENSE = GPL-3.0

define GAMEOS_INSTALL_TARGET_CMDS
	# create directories if not already done
	mkdir -p $(TARGET_DIR)/recalbox/share_init/themes
	mkdir -p $(TARGET_DIR)/recalbox/share_init/themes/gameOS
	# copy theme files in share_init
	cp -R $(@D)/* $(TARGET_DIR)/recalbox/share_init/themes/gameOS
	# remove project files
	find $(TARGET_DIR)/recalbox/share_init/themes/gameOS -name "*.pro" | xargs rm
	find $(TARGET_DIR)/recalbox/share_init/themes/gameOS -name "*.ts" | xargs rm
	find $(TARGET_DIR)/recalbox/share_init/themes/gameOS -name "CODEOWNERS" | xargs rm
endef

$(eval $(generic-package))
