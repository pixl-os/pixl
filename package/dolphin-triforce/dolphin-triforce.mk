################################################################################
#
# DOLPHIN_TRIFORCE
#
################################################################################

# respect vx.x.xxxxx not x.x-xxxxx
# set to v5.0.0 for the moment - first release version of this dolpin fork focus on triforce branch
DOLPHIN_TRIFORCE_VERSION_CORE = v5.0.0
# committed on Feb 3
DOLPHIN_TRIFORCE_VERSION = 95b993587613d6e219eca20d7cdcba28ae7e34dc
DOLPHIN_TRIFORCE_SITE = https://github.com/pixl-os/dolphin
DOLPHIN_TRIFORCE_SITE_METHOD = git
DOLPHIN_TRIFORCE_LICENSE = GPL-2.0+
DOLPHIN_TRIFORCE_LICENSE_FILES = license.txt
DOLPHIN_TRIFORCE_DEPENDENCIES = libevdev ffmpeg zlib libpng lzo libusb libcurl bluez5_utils hidapi xz host-xz
DOLPHIN_TRIFORCE_SUPPORTS_IN_SOURCE_BUILD = NO
DOLPHIN_TRIFORCE_GIT_SUBMODULES = YES

## print version of core in PixL
define DOLPHIN_TRIFORCE_PRE_CONFIGURE
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs
	echo "Dolphin-triforce;dolphin-triforce;$(DOLPHIN_TRIFORCE_VERSION_CORE)" > $(TARGET_DIR)/recalbox/share_init/system/configs/dolphin-triforce.corenames
endef
DOLPHIN_TRIFORCE_PRE_CONFIGURE_HOOKS += DOLPHIN_TRIFORCE_PRE_CONFIGURE

DOLPHIN_TRIFORCE_CONF_OPTS += -DCMAKE_BUILD_TYPE=Release
DOLPHIN_TRIFORCE_CONF_OPTS += -DDISTRIBUTOR='pixL-os'
DOLPHIN_TRIFORCE_CONF_OPTS += -DUSE_MGBA=OFF
DOLPHIN_TRIFORCE_CONF_OPTS += -DUSE_UPNP=ON
DOLPHIN_TRIFORCE_CONF_OPTS += -DENABLE_LTO=OFF
DOLPHIN_TRIFORCE_CONF_OPTS += -DUSE_DISCORD_PRESENCE=OFF
DOLPHIN_TRIFORCE_CONF_OPTS += -DTHREADS_PTHREAD_ARG=OFF
DOLPHIN_TRIFORCE_CONF_OPTS += -DBUILD_SHARED_LIBS=OFF
DOLPHIN_TRIFORCE_CONF_OPTS += -DENABLE_TESTS=OFF
DOLPHIN_TRIFORCE_CONF_OPTS += -DENABLE_AUTOUPDATE=OFF
DOLPHIN_TRIFORCE_CONF_OPTS += -DENABLE_ANALYTICS=OFF
DOLPHIN_TRIFORCE_CONF_OPTS += -DENABLE_CLI_TOOL=OFF

ifeq ($(BR2_PACKAGE_XSERVER_XORG_SERVER),y)
DOLPHIN_TRIFORCE_DEPENDENCIES += xserver_xorg-server qt5base
DOLPHIN_TRIFORCE_CONF_OPTS += -DENABLE_X11=ON
DOLPHIN_TRIFORCE_CONF_OPTS += -DENABLE_NOGUI=OFF
DOLPHIN_TRIFORCE_CONF_OPTS += -DENABLE_EGL=OFF
endif

# Hotkeys using evmapy
define DOLPHIN_TRIFORCE_EVMAP
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs/evmapy

	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/dolphin-triforce/dolphin-triforce.keys \
		$(TARGET_DIR)/recalbox/share_init/system/configs/evmapy
endef
DOLPHIN_TRIFORCE_POST_INSTALL_TARGET_HOOKS = DOLPHIN_TRIFORCE_EVMAP

$(eval $(cmake-package))
