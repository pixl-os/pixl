################################################################################
#
# Supermodel
#
################################################################################

# check version in this page: https://supermodel3.com/Download.html
SUPERMODEL_CORE_VERSION = v0.3a-$(shell echo $(SUPERMODEL_VERSION) | cut -c1-7)
SUPERMODEL_VERSION = 4e7356ab2c077aa3bc3d75fb6e164a1c943fe4c1
SUPERMODEL_SITE = https://github.com/trzy/Supermodel
SUPERMODEL_SITE_METHOD = git
SUPERMODEL_LICENSE = GPL2
SUPERMODEL_LICENSE_FILES = Docs/LICENSE.txt
SUPERMODEL_DEPENDENCIES = zlib libpng libogg libvorbis sdl2_net sdl2 libglu

## print version of core in PixL
define SUPERMODEL_PRE_CONFIGURE
	echo "Supermodel;supermodel;$(SUPERMODEL_CORE_VERSION)" > $(TARGET_DIR)/recalbox/share_init/system/configs/supermodel.corenames
endef
SUPERMODEL_PRE_CONFIGURE_HOOKS += SUPERMODEL_PRE_CONFIGURE

define SUPERMODEL_BUILD_CMDS
	cp $(@D)/Makefiles/Makefile.UNIX $(@D)/Makefile
	$(SED) "s|CC = gcc|CC = $(TARGET_CC) $(COMPILER_COMMONS_CFLAGS_NOLTO)|g" $(@D)/Makefile
	$(SED) "s|CXX = g++|CXX = $(TARGET_CXX) $(COMPILER_COMMONS_CXXFLAGS_NOLTO)|g" $(@D)/Makefile
	$(SED) "s|LD = gcc|LD = $(TARGET_CC) $(COMPILER_COMMONS_LDFLAGS_NOLTO)|g" $(@D)/Makefile
	$(SED) "s|sdl2-config|$(STAGING_DIR)/usr/bin/sdl2-config|g" $(@D)/Makefile
	$(TARGET_CONFIGURE_OPTS) $(MAKE) -C $(@D) -f Makefile \
	NET_BOARD=1 VERBOSE=1 DEBUG=0 ENABLE_DEBUGGER=0
endef

## print version of core in PixL
define SUPERMODEL_INSTALL_TARGET_CMDS
	$(INSTALL) -D -m 0755 $(@D)/bin/supermodel \
		$(TARGET_DIR)/usr/bin/supermodel
	$(INSTALL) -D -m 0644 $(@D)/Config/Games.xml \
		$(TARGET_DIR)/usr/share/supermodel/Games.xml
	mkdir -p $(TARGET_DIR)/usr/share/supermodel/Assets/
	$(INSTALL) -D -m 0644 $(@D)/Assets/* \
		$(TARGET_DIR)/usr/share/supermodel/Assets/
	$(INSTALL) -D -m 0644 $(@D)/Config/Supermodel.ini \
		$(TARGET_DIR)/recalbox/share_init/system/configs/supermodel/Supermodel.ini
	mkdir -p $(TARGET_DIR)/recalbox/share_init/saves/model3/NVRAM

	# For Create a update online package
	$(INSTALL) -D -m 0755 $(@D)/bin/supermodel \
		$(BR2_EXTERNAL_RECALBOX_PATH)/package/supermodel/update-resources/package/supermodel
	$(INSTALL) -D -m 0644 $(@D)/Config/Games.xml \
		$(BR2_EXTERNAL_RECALBOX_PATH)/package/supermodel/update-resources/package/Games.xml
	mkdir -p $(BR2_EXTERNAL_RECALBOX_PATH)/package/supermodel/update-resources/package/Assets
	$(INSTALL) -D -m 0644 $(@D)/Assets/* \
		$(BR2_EXTERNAL_RECALBOX_PATH)/package/supermodel/update-resources/package/Assets
	$(INSTALL) -D -m 0644 $(@D)/Config/Supermodel.ini \
		$(BR2_EXTERNAL_RECALBOX_PATH)/package/supermodel/update-resources/package/Supermodel.ini
endef

# Single `dos2unix` command for line-ending fixup
define SUPERMODEL_LINE_ENDINGS_FIXUP
    find $(@D) -type f \( -name "*.cpp" -o -name "*.h" \) -exec sed -i -e 's/\r$$//g' {} +
endef

# Hotkeys using evmapy
define SUPERMODEL_EVMAP
	mkdir -p $(TARGET_DIR)/recalbox/share_init/system/configs/evmapy

	cp -prn $(BR2_EXTERNAL_RECALBOX_PATH)/package/supermodel/supermodel.keys \
		$(TARGET_DIR)/recalbox/share_init/system/configs/evmapy
endef

SUPERMODEL_PRE_PATCH_HOOKS += SUPERMODEL_LINE_ENDINGS_FIXUP
SUPERMODEL_POST_INSTALL_TARGET_HOOKS = SUPERMODEL_EVMAP

$(eval $(generic-package))
