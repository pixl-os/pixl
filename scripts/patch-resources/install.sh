#!/bin/bash

#to manage mandatory parameters

helpFunction()
{
   echo "Usage: $0 -e existingVersion -n newVersion -c componentName"
   echo "-e Version of the existing installed version as '0.0.1'"
   echo "-n Version of the existing installed version as '0.0.2'"
   echo "-c name of the component as know by Pegasus"
   exit 1 # Exit script after printing help
}

while getopts ":e:n:c:?" opt; do
   case "$opt" in
      e ) existingVersion=${OPTARG} ;;
      n ) newVersion="$OPTARG" ;;
      c ) componentName="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

#echo for debug
#echo "$existingVersion"
#echo "$newVersion"
#echo "$componentName"

#initialization of progress and state
echo "0.1" > /tmp/$componentName/progress.log
echo "Installation of $componentName..." > /tmp/$componentName/install.log
echo "0" > /tmp/$componentName/install.err

#authorize file system udpate on root
mount -o remount,rw /

# Print helpFunction in case parameters are empty
if [ -z "$existingVersion" ] || [ -z "$newVersion" ] || [ -z "$componentName" ]
then
   echo "Some or all of the parameters are empty" > /tmp/$componentName/install.log
   echo "1" > /tmp/$componentName/install.err
   helpFunction
fi

#initialization global variables
RestartOrRebootNeeds="0"

# Begin script in case all parameters are correct
#unzip package in /tmp/$componantName/package directory
if test -n "$(find /tmp/$componentName -maxdepth 1 -name 'package.zip' -print -quit)"
then
    if [ ! -d "/tmp/$componentName/package" ]
    then
		#creation of package directory in component one
		echo "0.2" > /tmp/$componentName/progress.log
        echo "Creation of directory /tmp/$componentName/package" > /tmp/$componentName/install.log
        mkdir /tmp/$componentName/package 
		if [ $? -eq 0 ]
		then
			echo "0.3" > /tmp/$componentName/progress.log
			echo "Directory /tmp/$componentName/package created" > /tmp/$componentName/install.log
		else
			echo "Directory /tmp/$componentName/package creation failed" > /tmp/$componentName/install.log
			echo "1" > /tmp/$componentName/install.err
			exit $?
		fi
    fi
    #unzip package
	echo "0.4" > /tmp/$componentName/progress.log
	echo "Unzip of package..." > /tmp/$componentName/install.log
    unzip -o /tmp/$componentName/package.zip -d /tmp/$componentName/package
	if [ $? -eq 0 ]
    then
		echo "0.5" > /tmp/$componentName/progress.log
		echo "Unzip of package done" > /tmp/$componentName/install.log
    else
		echo "Unzip of package failed" > /tmp/$componentName/install.log
		echo "1" > /tmp/$componentName/install.err
		exit $?
	fi
    #***************************************begin of part to customize*****************************************************
	#Patches preparation - to match between repo and os file system
	echo "0.6" > /tmp/$componentName/progress.log
	echo "Patchs preparation.." > /tmp/$componentName/install.log
	RestartOrRebootNeeds=$(
	    find /tmp/$componentName/package/. -name "*.patch" -print0 | while IFS= read -r -d $'\0' file; do
			#echo "Prepare" ${file}
			# IMPORTANT REMARK: in this part, if we add new replacement depending type of files (configgen, script, services, etc..)
			# we have to respect order of change to start by change without restart/reboot, with restart and withreboot
			#set 0 if end of installation without other action needed, -1 if need restart of Pegasus, -2 if need reboot
			# ------ part for patch without restart/reboot -------- 
			#For configgen files case
			#echo "sed 's#/projects/configgen/#/usr/lib/python3.11/site-packages/#g' '${file}' > '${file}.prepared'"
			sed 's#/projects/configgen/#/usr/lib/python3.11/site-packages/#g' "${file}" > "${file}.prepared"
			#echo "cmp -s '${file}' '${file}.prepared'"
			if ! cmp -s "${file}" "${file}.prepared" && [ -z "$RestartOrRebootNeeds" ]; then
				RestartOrRebootNeeds="0"
			fi

			# ------ part for patch with restart -------- 
			#RFU
			# sed 's#/XXXX/XXXX/#/XXXX/XXXX/XXXX/XXXXX/#g' "${file}" > "${file}.prepared"
			# if ! cmp -s "${file}" "${file}.prepared" && [ "$RestartOrRebootNeeds" -ne -2 ]; then
				# RestartOrRebootNeeds="-1"
			# fi
			
			# ------ part for patch with reboot -------- 
			#RFU
			# sed 's#/XXXX/XXXX/#/XXXX/XXXX/XXXX/XXXXX/#g' "${file}" > "${file}.prepared"
			# if ! cmp -s "${file}" "${file}.prepared"; then
				# RestartOrRebootNeeds="-2"
			# fi
			echo "$RestartOrRebootNeeds"
		done | tail -n 1
	)
	#echo "RestartOrRebootNeeds : $RestartOrRebootNeeds" 
	echo "0.7" > /tmp/$componentName/progress.log
	echo "Patchs applications.." > /tmp/$componentName/install.log
	find /tmp/$componentName/package/. -name "*.patch.prepared" -print0 | while IFS= read -r -d $'\0' file; do
		#echo "Apply" ${file}
		#start from root
		cd /
		# to check first if patch has been already applied
		#with -p1 to remove a/ and b/
		#with --dry-run to test patch before to apply it fully
		#with -R to know if we could reverse or not
		patch -p1 -R --dry-run < ${file}
		if [ $? -eq 0 ]
		then
			echo "0.8" > /tmp/$componentName/progress.log
			echo "${file} already applied"  > /tmp/$componentName/install.log
			# nothing to do, certainly done from a previous patch
		else
			# to check now if patch could be applied completely (to avoid partial patch !!!)
			#with -p1 to remove a/ and b/
			#with --dry-run to test patch before to apply it fully
			patch -p1 --dry-run < ${file}
			if [ $? -eq 0 ]
			then
				echo "0.8" > /tmp/$componentName/progress.log
				#patch applied definitively (only if no 'FAILED' found)
				patch -p1 < ${file}
				echo "${file} applied" > /tmp/$componentName/install.log
			else
				echo "${file} failed" > /tmp/$componentName/install.log
				echo "2" > /tmp/$componentName/install.err
				exit 1
			fi
		fi
	done
	if [ $? -eq 0 ]
	then
		echo "1.0" > /tmp/$componentName/progress.log
		if [ $RestartOrRebootNeeds -eq 0 ]
		then
			echo "pixL patched" > /tmp/$componentName/install.log
		elif [ $RestartOrRebootNeeds -eq -1 ]
		then
			echo "pixL patched - need restart" > /tmp/$componentName/install.log
		else
			echo "pixL patched - need reboot" > /tmp/$componentName/install.log
		fi
		#set 0 if end of installation without other action needed, -1 if need restart of Pegasus, -2 if need reboot
		echo $RestartOrRebootNeeds > /tmp/$componentName/install.err
		#update embedded pixL version
		result=""
		result=$(grep -i "${newVersion}" /recalbox/recalbox.version)
		if [ -z "$result" ]; then
			sed -i "s/${existingVersion}/${newVersion}/g" /recalbox/recalbox.version
		fi
		exit 0
	else
		echo "pixL patching failed" > /tmp/$componentName/install.log
		echo "3" > /tmp/$componentName/install.err
		exit 1
	fi
    #***************************************end of part to customize*****************************************************
else
    echo "No 'package.zip' available in your sharing in '/tmp/$componentName'" > /tmp/$componentName/install.log
    echo "3" > /tmp/$componentName/install.err
    exit 3
fi

