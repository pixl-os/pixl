#!/bin/bash

#direct reading of /recalbox/recalbox.version to extract numbers and . to take a version as string
result=""
result="$(grep -Eo '[0-9,.]{1,}' /recalbox/recalbox.version | tr -d '\n' | tr -d '\r')"
if test -n "$result"
then
	#not null: we could return value normally
	echo "v$result" | tr -d '\n' | tr -d '\r'
else
	#value is not found or null - force to 0.0.0.0 in this case
	echo v0.0.0.0 | tr -d '\n' | tr -d '\r'
fi
exit 0