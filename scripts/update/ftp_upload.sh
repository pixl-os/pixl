#!/bin/bash

# need to install lftp component on your linux (ex ubuntu: sudo apt install lftp)
# Check if required tool is installed
if ! command -v lftp ; then
  echo "Error: Please install lftp before running this script."
  exit 1
fi

# to trust the ftp server first time on your linux, you have to launch ssh command using your HOST or uncomment/comment line 47 for the first time
# Check if required tool is installed
if ! command -v ssh ; then
  echo "Error: Please install ssh before running this script."
  exit 1
fi

# if you do it yourself manually, as for example:
# ssh ftp.toto.net
# The authenticity of host 'ftp.toto.net (123.123.123.123)' can't be established.
# ED25519 key fingerprint is SHA256:xhlnoEvl7+m8sqk8wkLkCkh/bvkOQkvkFIVefK2og.
# This key is not known by any other names.
# Are you sure you want to continue connecting (yes/no/[fingerprint])? yes

#to manage mandatory parameters
helpFunction()
{
   echo "Usage: $0 -t update_type -f update_format -h host -l login -p password"
   echo "-t type of update as 'dev', 'beta' or 'release'"
   echo "-f format of update as 'image' (default) or 'raw'"
   echo "-h host of ftp server"
   echo "-l login of user to access FTP"
   echo "-p password of user to access FTP (optional, could be enter manually)"
   exit 1 # Exit script after printing help
}

while getopts ":t:f:h:l:p:?" opt; do
   case "$opt" in
      # Define type of update: could be dev/beta/release
      t ) update_type="$OPTARG" ;;
      # Define format of update: could be image or raw(decompressed version for update only)
      f ) update_format="$OPTARG" ;;
      # FTP HOST
      h ) HOST="$OPTARG" ;;
      # login/password of ftp
      l ) LOGIN="$OPTARG" ;;
      p ) PASSWORD="$OPTARG" ;;      
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

# Print helpFunction in case parameters are empty
if [ -z "$update_type" ] || [ -z "$update_format" ] || [ -z "$HOST" ] || [ -z "$LOGIN" ]
then
   echo "Some or all of the parameters are empty"
   helpFunction
fi

# line to uncomment after if you want to trust quickly your ftp server for the first time
#ssh ${HOST}; exit 0

# if PASSWORD is empty from command line parameters
if [ -z "$PASSWORD" ]
then
  # please enter manually the password
  read -p "Please enter your password for $LOGIN : " PASSWORD
fi

# Targeted files/directories/urls from update format
if [[ "$update_format" != "raw" ]]; then
  json_file="${update_type}-pixl-os.json"
  ressource_path="../update-resources/image_update"
  update_content_path="../../output/images/recalbox"
  # Base URL for assets
  assets_remote_path="${update_type}/x86_64"
else
  json_file="${update_type}-${update_format}-pixl-os.json"
  ressource_path="../update-resources/raw_update"
  update_content_path="../../output/images/pc-boot"
  # Base URL for assets
  assets_remote_path="${update_type}/x86_64_${update_format}"
fi

if [[ "$update_type" != "dev" ]]; then
  # Base URL using prefix "latest" for assets release or beta
  assets_remote_path="latest_${assets_remote_path}"
fi

# lftp password management
export LFTP_PASSWORD=${PASSWORD}

# parameters
JSON_FILE="updates/${json_file}"
ASSETS_DIR="updates/${assets_remote_path}"
BACKUP_DATE=$(LC_ALL=en_UK.UTF-8 date +%Y-%m-%dT%H:%M:%SZ)
JSON_BACKUP_FILE="updates/${json_file}_${BACKUP_DATE}"
BACKUP_DIR="updates/${assets_remote_path}_${BACKUP_DATE}"

# Strategy...
# 0) backup json file by rename existing one with date/time
lftp --env-password sftp://${LOGIN}@${HOST} -e "mv '${JSON_FILE}' '${JSON_BACKUP_FILE}'; bye"
# Print success or error message
if [ $? -eq 0 ]; then
  echo "rename json for backup successful!"
else
  echo "rename json for backup failed (already backuped ?!)"
fi

# 0 bis) backup existing assets by rename existing assets directory with date/time
lftp --env-password sftp://${LOGIN}@${HOST} -e "mv '${ASSETS_DIR}' '${BACKUP_DIR}'; bye"
# Print success or error message
if [ $? -eq 0 ]; then
  echo "rename for backup successful!"
else
  echo "rename for backup failed (already backupec ?!)"
fi

# 1) recreate assets directory
lftp --env-password sftp://${LOGIN}@${HOST} -e "mkdir '${ASSETS_DIR}'; bye"
# Print success or error message
if [ $? -eq 0 ]; then
  echo "new assets directory created successfully!"
else
  echo "new assets directory failed (already done ?!)"
fi

# 2) upload new assets
# Upload directory contents recursively (using mirror)
lftp --env-password sftp://${LOGIN}@${HOST} -e "mirror -R '${update_content_path}/' '${ASSETS_DIR}/'; bye"
# Print success or error message
if [ $? -eq 0 ]; then
  echo "upload assets successfully!"
else
  echo "upload assets failed!"
  exit 1
fi

# 3) upload install/version scripts and other ressources
# Upload directory contents recursively (using mirror)
lftp --env-password sftp://${LOGIN}@${HOST} -e "mirror -R '${ressource_path}/' '${ASSETS_DIR}/'; bye"
# Print success or error message
if [ $? -eq 0 ]; then
  echo "upload install/version scripts and ressources successfully!"
else
  echo "upload install/version scripts and ressources failed!"
  exit 1
fi

# 4) upload json file
lftp --env-password sftp://${LOGIN}@${HOST} -e "cd /home/${LOGIN}/updates; put '${json_file}'; bye"
# Print success or error message
if [ $? -eq 0 ]; then
  echo "upload json file successful!"
else
  echo "upload json file failed"
  exit 1
fi

exit 0
