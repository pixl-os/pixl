#!/bin/bash

# example of call of this script for pre-release: ./create_github_version.sh -t pre-release -v v0.266 -u bozothegeek -r mame -g qsdlkqjdflsqfdmldsfks45df4sd5f4s5fsd4f

# Check if required tool is installed
if ! command -v git &> /dev/null; then
  echo "Error: Please install git before running this script."
  exit 1
fi
# Check if required tool is installed
if ! command -v jq &> /dev/null; then
  echo "Error: Please install jq before running this script."
  exit 1
fi
# Check if required tool is installed
if ! command -v curl &> /dev/null; then
  echo "Error: Please install curl before running this script."
  exit 1
fi

#to manage mandatory parameters
helpFunction()
{
   echo "Usage: $0 -t update_type -v version -u github_user -r github_repo -g github_token"
   echo "-t type of update as 'pre-release' (for pixL Beta) or 'release' (For pixL Release)"
   echo "-v version of update (as vX.Y.Z) examples : 'v0.255' or 'v1.2' (optional if version.txt exists)"
   echo "-u github targeted user"
   echo "-r github targeted repo"
   echo "-g github token (optional and for test purpose only - environment variable is prefered for security reason)"
   echo "-s final state as 'draft' or 'publish' or 'republish' (to republish in case of issue)"
   exit 1 # Exit script after printing help
}

while getopts ":t:v:u:r:g:s:?" opt; do
   case "$opt" in
      # Define type of update: could be pre-release/release
      t ) update_type="$OPTARG" ;;
      v ) update_version="$OPTARG" ;;
      u ) github_user="$OPTARG" ;;
      r ) github_repo="$OPTARG" ;;
      g ) github_token="$OPTARG" ;;
      s ) final_state="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

# Print helpFunction in case parameters are empty
if [ -z "$update_type" ] || [ -z "$final_state" ] || ([ -z "$update_version" ]  && [[ ! -f "${PWD}/version.txt" ]]) || [ -z "$github_user" ] || [ -z "$github_repo" ] || ([ -z "$github_token" ] && [ -z "$GITHUB_TOKEN" ])
then
   echo "Some or all of the parameters/files/env variables are empty/missing"
   helpFunction
fi

# Define variables (replace with your details)
GITHUB_USER=${github_user}  
GITHUB_REPO=${github_repo}
if [ -z "$GITHUB_TOKEN" ]
then
  GITHUB_TOKEN=${github_token}  # Personal access token with repo:releases permission
fi

# check if a version if available in file (coming from a previous script executed that get version from original repo/alternbative repo)
if test -f "${PWD}/version.txt" ; then
  VERSION=$(cat ${PWD}/version.txt) #format of version in .txt file should follow the standard format as vX.Y.Z
  # Clean up deactivated
  #rm -f ${PWD}/version.txt
else  
  VERSION=${update_version}  # Update with your desired standard version format as vX.Y.Z
fi

# Check if a file named "release_notes.md" exists and prepare it to be added in description
body=""
if test -f "${PWD}/release_notes.md"; then
  echo "release notes file available !"
  # Read the file content of release_notes.md
  # Remove CR
  sed -i 's/\r//g' "${PWD}/release_notes.md"
  version_content=$(cat ${PWD}/release_notes.md) 
  #echo "$version_content"
  # Escape any special characters in the content
  body_content=$(printf '%s' "$version_content" | sed 's/\\/\\\\/g; s/"/\\"/g' | awk '{gsub(/\t/, "\\t"); print}')
  # convert end of line by "\n" string
  while IFS= read -r line; do
    body="${body}${line}\n"
  done <<< "$body_content"
  # Clean up deactivated
  #rm -f ${PWD}/release_notes.md
else
  echo "release notes file not available !"
  body="No release notes generated or available from original repo"
fi

# Authenticate with GitHub using personal access token
AUTH_HEADER="Authorization: token $GITHUB_TOKEN"

# Get latest release tag for comparisons
#ALL_JSON=$(curl -sSL -H "Accept: application/vnd.github+json" -H "$AUTH_HEADER" -H "X-GitHub-Api-Version: 2022-11-28" https://api.github.com/repos/$GITHUB_USER/$GITHUB_REPO/releases)
#echo "ALL_JSON: $ALL_JSON"

# we could have this warning here : "curl: (23) Failure writing output to destination"
# it's normal because we don't save results in file
LATEST_TAG=$(curl -sSL -H "Accept: application/vnd.github+json" -H "$AUTH_HEADER" -H "X-GitHub-Api-Version: 2022-11-28" https://api.github.com/repos/$GITHUB_USER/$GITHUB_REPO/releases | grep -i -m 1 '"name":' | awk -F'"' '{print $4}')
echo "LATEST_TAG: $LATEST_TAG"
LATEST_ID=$(curl -sSL -H "Accept: application/vnd.github+json" -H "$AUTH_HEADER" -H "X-GitHub-Api-Version: 2022-11-28" https://api.github.com/repos/$GITHUB_USER/$GITHUB_REPO/releases | grep -i -m 1 '"id":' | awk -F': ' '{print $2}' | awk -F',' '{print $1}')
echo "LATEST_ID: $LATEST_ID"

# Check if the provided version already exists as a tag
if [[ "$VERSION" == "$LATEST_TAG" ]]; then
  echo "Version '$VERSION' already exists as a release tag."
  RELEASE_ID=${LATEST_ID}
  #check if it's a draft or not, only draft will be updated to avoid mistake
  LATEST_IS_DRAFT=$(curl -sSL -H "Accept: application/vnd.github+json" -H "$AUTH_HEADER" -H "X-GitHub-Api-Version: 2022-11-28" https://api.github.com/repos/$GITHUB_USER/$GITHUB_REPO/releases | grep -i -m 1 '"draft":' | awk -F': ' '{print $2}' | awk -F',' '{print $1}')
  echo "LATEST_IS_DRAFT: $LATEST_IS_DRAFT"
  if [[ "$LATEST_IS_DRAFT" == "false" ]] && [[ $final_state != "republish" ]]; then
    echo "can't update last release because already publish - need to delete this version manually in repo if needed"
    exit 1
  fi  
else
  # Create a release draft
  RELEASE_DATA=$(curl -sSL -X POST -H "Accept: application/vnd.github+json" -H "$AUTH_HEADER" -H "X-GitHub-Api-Version: 2022-11-28" https://api.github.com/repos/$GITHUB_USER/$GITHUB_REPO/releases -d '{ "tag_name": "'${VERSION}'", "name":"'${VERSION}'","body":"Description of the pre-release", "draft":true, "prerelease":true, "generate_release_notes":false}')
  #echo $RELEASE_DATA

  # Extract release ID from response
  RELEASE_ID=$(echo "$RELEASE_DATA" | jq -r '.id')
fi
echo "RELEASE_ID: $RELEASE_ID"
echo  "$RELEASE_ID"> release_id.txt

# Update release with release notes URL
RETURN=$(curl -sSL -X PATCH -H "Accept: application/vnd.github+json" -H "Authorization: token $GITHUB_TOKEN" -H "X-GitHub-Api-Version: 2022-11-28" -d "{ \"tag_name\": \"${VERSION}\", \"body\":\"${body}\" }" https://api.github.com/repos/$GITHUB_USER/$GITHUB_REPO/releases/$RELEASE_ID)
#echo "RETURN : $RETURN"

echo "Pre-release version '$VERSION' created or updated successfully!"
