#!/bin/bash

# example of call of this script to upload: ./github_upload.sh -f version.txt -v v0.266 -u bozothegeek -r mame -g qsdlkqjdflsqfdmldsfks45df4sd5f4s5fsd4f

# Check if required tool is installed
if ! command -v git &> /dev/null; then
  echo "Error: Please install git before running this script."
  exit 1
fi
# Check if required tool is installed
if ! command -v gh &> /dev/null; then
  echo "Error: Please install gh before running this script."
  exit 1
fi

#to manage mandatory parameters
helpFunction()
{
   echo "Usage: $0 -f file -v version -u github_user -r github_repo -g github_token"
   echo '-f file to upload in dedicated version'
   echo "-v version of update (as vX.Y.Z) examples : 'v0.255' or 'v1.2' (optional if version.txt exists)"
   echo "-u github targeted user"
   echo "-u github targeted repo"
   echo "-g github token (optional and for test purpose only - environment variable is prefered for security reason)"
   exit 1 # Exit script after printing help
}

while getopts ":f:v:u:r:g:?" opt; do
   case "$opt" in
      # Define type of update: could be pre-release/release
      f ) file_to_upload="$OPTARG" ;;
      v ) update_version="$OPTARG" ;;
      u ) github_user="$OPTARG" ;;
      r ) github_repo="$OPTARG" ;;
      g ) github_token="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done
echo "file_to_upload : ${PWD}/$file_to_upload"
echo "version.txt : ${PWD}/version.txt"
# Print helpFunction in case parameters are empty
if [ -z "${PWD}/$file_to_upload" ] || ([ -z "$update_version" ]  && [[ ! -f "${PWD}/version.txt" ]]) || [ -z "$github_user" ] || [ -z "$github_repo" ] || ([ -z "$github_token" ] && [ -z "$GITHUB_TOKEN" ])
then
   echo "Some or all of the parameters/files/env variables are empty/missing"
   helpFunction
fi

# check if a version if available in file (coming from a previous script executed that get version from original repo/alternbative repo)
if test -f "${PWD}/version.txt" ; then
  VERSION=$(cat ${PWD}/version.txt) #format of version in .txt file should follow the standard format as vX.Y.Z
  # Clean up deactivated
  #rm -f ${PWD}/version.txt
else  
  VERSION=${update_version}  # Update with your desired standard version format as vX.Y.Z
fi

# Define variables (replace with your details)
GITHUB_USER=${github_user}  
GITHUB_REPO=${github_repo}
if [ -z "$GITHUB_TOKEN" ]
then
  GITHUB_TOKEN=${github_token}  # Personal access token with repo:releases permission
fi

# check if file to upload is available
if [[ ! -f "${PWD}/${file_to_upload}" ]] ; then
   echo "file to upload not available : ${PWD}/${file_to_upload}"
   exit 1
fi

# Replace with the tag name of the release
RELEASE_TAG="$VERSION"
echo "RELEASE_TAG: $RELEASE_TAG"
# Replace with the filename you want to upload
FILE_TO_UPLOAD="${PWD}/${file_to_upload}"
echo "FILE_TO_UPLOAD: $FILE_TO_UPLOAD"

# Set authentication for gh with personal access token
export GH_TOKEN="$GITHUB_TOKEN"

# Upload the file as an asset
gh release upload "$RELEASE_TAG" "$FILE_TO_UPLOAD" -R "$GITHUB_USER/$GITHUB_REPO" --clobber
if [ $? -eq 0 ]; then
  # Print success message
  echo "File '$FILE_TO_UPLOAD' uploaded successfully!"
else
  echo "File '$FILE_TO_UPLOAD' upload failed!"
  exit 1
fi

