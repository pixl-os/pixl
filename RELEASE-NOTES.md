# Release notes

This file will be processed and will be distributed with PixL Releases.
The text, located between the first `## Version XXX` and the second one, will appear
as release notes for end user on a Recalbox upgrade.

## Version 1.37

### News

- First public release
- For PC x86 64 only (LCD Steamdeck included)
- Using Pegasus-Frontend (pixL version)
- gameOS/Shinretro themes embedded (pixL versions)
- Based on recalbox 8.X, features added :
    + Lightgun support: support of Wiimote/Dolphin + Sinden Lightgun
    + CD-ROM support: read CD/GD-ROM for Sony PS1, Sega Dreamcast, Sega Saturn, Sega CD, Panasonic 3DO & Nec PC Engine CD
    + Cartridge support : read cartridge using USB-NES & Retrode for NES/FAMICOM, Super Nintendo/Famicom, Sega Genesis/Megadrive,
      Nintendo 64, Sega Master System, Nintendo GameBoy (classic, advance, color), and more... 
    + New emulation: Sega Model 2 (Wine support), Sony PS3, Nintendo Wii U, Microsoft Xbox, Sega Chihiro (Beta), Sega Namco Nintendo Triforce
    + Advanced management of data directories (ROMS,BIOS,SAVES,THEMES,...) from USB/Internal storages
    + Retroachievements display from gameOS Theme
    + Full media support for themes (media gallery and game manual display from gameOS Theme)
    + New Recognition of controllers and layout display in Pegasus-Frontend
    + Full Nintendo 64 controller management and adapters
    + Redshift included to ajusts color temperature of your screen according to your surrounding
    + rEFInd Boot Manager
    + Vulkan support
    + New managemlent of Bluetooth/Network/Wifi throught Pegasus-Frontend
    + New Retroarch Netplay management / Session filtering by "Friend" in Pegasus-Frontend
    + Grouping of system / sorting by name/date/constructor in gameOS theme
    + Collections management in gameOS theme
    + Dual-screen management (clone, extended or 'switch' mode)
    + Advanced settings for emulators
    + Support of overlays for some standalone emulators as Dolphin, Dolphin-Triforce, PCSX2
    + Multi-windows support (Beta) / Theme/Frontend in background (Beta)
    + Mouse/Keyboard support from Pegasus-Frontend with full Change of Controls (Usefull to improve accessibility)
    + Color configuration of Pegasus-Frontend menu
    + Advanced management of gamelist/medialist (Beta) from Pegasus-Frontend
    + Theme designer embedded in gameOS theme

