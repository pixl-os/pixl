# Testing

This file is in french as all our beta testers are french.


A chaque modification du système, les développeurs ajoutent une ou plusieurs lignes dans ce fichier.

Les beta testeurs cochent les cases lors des sessions de tests.

Toutes les cases doivent être cochées avant la release stable.

Pour les pending features, elles ne seront activées que si les beta testeurs les valident selon les critères de qualité de pixL.

Pour rappel le log principal est ici : /recalbox/share/system/logs/recalbox.log

## [pixL-master] - Beta33 2023-08-04
	- TO DO

## [RETRO-X] - Beta32 2023-05-26
- Controllers:
	- [ ] connecter manette xbox 360 filaire ou avec le dongle, vérifier le mapping proposé, est ce le bon ?
	- [ ] connecter manette xbox one S en bluetooth, connecté ? vérifier le mapping proposé, est ce le bon ?
	- [ ] connecter manette xbox one S en filaire, vérifier le mapping proposé, est ce le bon ?
	- [ ] connecter manette xbox one/series avec dongle v1, connecté ? vérifier le mapping proposé, est ce le bon ?
	- [ ] connecter manette xbox one/series avec dongle v2, connecté ? vérifier le mapping proposé, est ce le bon ?
	- [ ] connecter manette xbox one/series en filaire, vérifier le mapping proposé, est ce le bon ?
	- [ ] connecter manette xbox series en bluetooth (risque d'être difficile), connecté ? vérifier le mapping proposé, est ce le bon ?
	- [ ] vérifier pour chaque manette xbox connectée que les icones et designs proposés correspondent bien 
	aux types de manettes qui ont des icones/visuels disponibles. Visible dans le menu du bluetooth et des contrôlleurs.
	- [ ] vérifier que les niveaux de batteries sont bien disponibles en "bluetooth" et pour les manettes xbox one, one s et series. 
	ils peuvent mettre quelques secondes à apparaitre.
	
## [RETRO-X] - Beta31 2023-01-21
- OS:
	- [ ] Jouer en utilisant un GPU intel
	- [ ] Jouer en utilisant un APU inclut dans un AMD Ryzen
	- [ ] Jouer en utilisant un GPU Radeon
	- [ ] Jouer en utilisant un GPU Nvidia
	
- Pegasus-Frontend:
	- [ ] Tester la mise à jour online à partir d'une Beta 30 (d'autres versions ne sont pas capable de le faire)
	- [ ] Tester les nouveaux paramètres de chargement de la liste des jeux
		- "Gamelist First" : 
			- [ ] En activant cet option, vérifier que si on a un système sans gamelist, il est bien détecté (il faudra désactiver "gamelist only")
			- [ ] En activant cet option, Vérifier que les autres systèmes se charge bien en utilisant le gamelist donc rapidement
			- [ ] Noter le nombre de jeux/systèmes et le temps constatés dans les logs* lors du chargement (pas besoin d'activer le mode debug pour cela)
		- "Medialist" : 
			- [ ] En activant cet option, vérifier que le fichier media.xml est bien créé lors de l'activation de ce paramètre
			- [ ] En activant cet option, vérifier que le chargement se fera plus rapidement pour les jeux avec ce fichier présent
			- [ ] Noter le nombre de jeux/systèmes et le temps constatés dans les logs* lors du chargement (pas besoin d'activer le mode debug pour cela)
		- "Media 'on Demand'" : 
			- [ ] En activant cet option et en utilisant "Désactiver Skraper", les médias doivent cependant apparaitre comme les logos, manuel, etc... 
			- [ ] Noter le nombre de jeux/systèmes et le temps constatés dans les logs* lors du chargement (pas besoin d'activer le mode debug pour cela)
	*: la ligne suivante dans le recalbox.conf est disponible pour avoir le temps de chargement des jeux: 
		[i] Stats - Global Timing: Gamelists/Assets parsing/searching took XXXXXms
- Theme gameOS-pixL:
	Gestions des groupes/sysytèmes:
		- [ ] Tester l'affichage/navigation des groupes/systèmes utilisant un emplacement/slot
		- [ ] Tester l'affichage/navigation des groupes/systèmes utilisant 2 emplacements/slots
		- [ ] Tester l'affichage/navigation des systèmes sans les groupes (par dédaut)
		- [ ] Changer la façon de trier les systèmes par nom, date de sortie et/ou fabricant
	Améliorations de la navigation:
		- [ ] Tester globalement la navigation dans le thème avant de lancer un jeu
		- [ ] Tester globalement la navigation dans le thème après retour d'un jeu
	Chargement du thème:
		- [ ] Tester le rechargement/affichage de la fenêtre de dialog lors de modification de paramètre du thème
	