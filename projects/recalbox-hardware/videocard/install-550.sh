#!/bin/bash

#to manage mandatory parameters

helpFunction()
{
   echo "Usage: $0 -e existingVersion -n newVersion -c componentName"
   echo "-e Version of the existing installed version as '0.0.0'"
   echo "-n Version of the new version to install as '550.78'"
   echo "-c name of the component as know by Pegasus"
   exit 1 # Exit script after printing help
}

while getopts ":e:n:c:?" opt; do
   case "$opt" in
      e ) existingVersion=${OPTARG} ;;
      n ) newVersion="$OPTARG" ;;
      c ) componentName="$OPTARG" ;;
      ? ) helpFunction ;; # Print helpFunction in case parameter is non-existent
   esac
done

#echo for debug
#echo "$existingVersion"
#echo "$newVersion"
#echo "$componentName"

#initialization of progress and state
echo "0.1" > /tmp/$componentName/progress.log
echo "Installation of $componentName..." > /tmp/$componentName/install.log
echo "0" > /tmp/$componentName/install.err

#authorize file system udpate on root
mount -o remount,rw /

# Print helpFunction in case parameters are empty
if [ -z "$existingVersion" ] || [ -z "$newVersion" ] || [ -z "$componentName" ]
then
   echo "Some or all of the parameters are empty" > /tmp/$componentName/install.log
   echo "1" > /tmp/$componentName/install.err
   helpFunction
fi

# Begin script in case all parameters are correct
#no package in this case
echo "0.3" > /tmp/$componentName/progress.log
echo "no package necessary" > /tmp/$componentName/install.log

#***************************************begin of part to customize*****************************************************
## nvidia-driver installation script ##
# get nvidia gpu model
echo "0.5" > /tmp/$componentName/progress.log
echo "Get gpu model(s)" > /tmp/$componentName/install.log
# "lspci -d::0300" only list vga class devices 
gpu="$(lspci -vmm -d10de::0300 | sed -E '/^Device/!d;s/.*\[([^]]+)\].*/\1/' | head -n 1)"
# "lspci -nd10de::0300" only list nvidia vendor and vga class devices
gpuid="$(lspci -nd10de::0300 | egrep -o "[[:xdigit:]]{4}:[[:xdigit:]]{4}" | cut -d ":" -f 2 | sed '/^$/d' | head -n 1)"
# get Intel igpu
# if has intel igpu and nvidia gpu serie M you need optimus technologie is not compatible for a moment in pixL
# and if battery detected to say that is potentially a mobile device
if ls /sys/class/power_supply/BAT* 1> /dev/null 2>&1; then
  igpu="$(lspci -d8086::0300)"
else
  igpu=""
fi

#if nvidia gpu detected
if [ "${gpu}" ]; then
  #authorize file system udpate on boot
  mount -o remount,rw /boot
  #activate nvidia driver installation for next reboot
  #remove ';'
  sed -i '/;nvidia-driver/s/;nvidia-driver/nvidia-driver/' /boot/recalbox-boot.conf
  #set to true in all cases
  sed -i '/nvidia-driver/s/=.*/=true/' /boot/recalbox-boot.conf
  if [ $? -eq 0 ]
  then
  	echo "0.7" > /tmp/$componentName/progress.log
    echo "Setting of Nvidia driver done" > /tmp/$componentName/install.log
  else
    echo "Setting of Nvidia driver failed" > /tmp/$componentName/install.log
    echo "1" > /tmp/$componentName/install.err
 	  exit $?
  fi
  #if nvidia gpu detected with igpu
  if [ "${igpu}" ]; then
    #activate optimus for xorg conf 
    #remove ';'
    sed -i '/;nvidia-optimus/s/;nvidia-optimus/nvidia-optimus/' /boot/recalbox-boot.conf
    #set to true in all cases
    sed -i '/nvidia-optimus/s/=.*/=true/' /boot/recalbox-boot.conf
    if [ $? -eq 0 ]
    then
    	echo "0.8" > /tmp/$componentName/progress.log
      echo "Setting Nvidia optimus done" > /tmp/$componentName/install.log
    else
    	echo "Setting Nvidia optimus failed" > /tmp/$componentName/install.log
    	echo "1" > /tmp/$componentName/install.err
    	exit $?
    fi
  fi
fi
echo "1.0" > /tmp/$componentName/progress.log
echo "Component updated - need reboot" > /tmp/$componentName/install.log
#set 0 if end of installation without other action needed, -1 if need restart of Pegasus, -2 if need reboot
echo "-2" > /tmp/$componentName/install.err
exit 0
#***************************************end of part to customize*****************************************************
