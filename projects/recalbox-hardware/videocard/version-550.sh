#!/bin/bash

## nvidia-driver version script ##

# get nvidia gpu model
# "lspci -d::0300" only list vga class devices 
gpu="$(lspci -vmm -d10de::0300 | sed -E '/^Device/!d;s/.*\[([^]]+)\].*/\1/' | head -n 1)"
# "lspci -nd10de::0300" only list nvidia vendor and vga class devices
gpuid="$(lspci -nd10de::0300 | egrep -o "[[:xdigit:]]{4}:[[:xdigit:]]{4}" | cut -d ":" -f 2 | sed '/^$/d' | head -n 1)"
# get Intel igpu
# if has intel igpu and nvidia gpu serie M you need optimus technologie is not compatible for a moment in pixL
# and if battery detected to say that is potentially a mobile device
if ls /sys/class/power_supply/BAT* 1> /dev/null 2>&1; then
  igpu="$(lspci -d8086::0300)"
else
  igpu=""
fi

# check the compatibility of the graphics card with the driver version "550.X"
supportList550=$(grep -i -E "$(printf '\t')${gpuid}" /recalbox/system/hardware/videocard/nvidiacheckcompatibility-550.txt)

# check the version
nvidia550="$(echo /usr/lib/extra/nvidia-5* |cut -d'-' -f 2)"

# detect/configure nvidia driver version to install
if [ -n "${gpuid}" ] && [ -n "${supportList550}" ]; then
      nvver="${nvidia550}"
      #echo "${supportList550}"
fi

# set paths/vars
libs="/usr/lib"
xorg="${libs}/xorg/modules"
# path change in linux kernel 6.3.6
# nvkern="/lib/modules/`uname -r`/extra"
nvkern="/lib/modules/`uname -r`/updates"
nvlibs="${libs}/extra/nvidia-${nvver}"
nvxorg="${nvlibs}/xorg"
nvko="${nvkern}/nvidia.ko"

# is gpu compatible ?
if [ -n "${nvver}" ]; then
	#get a version if exists
	# path change in linux kernel 6.3.6
	version="$(modinfo /lib/modules/$(uname -r)/updates/nvidia.ko | grep ^version | awk '{print $2}' | tr -d '\n' | tr -d '\r')"
	if [ -n "${version}" ]; then
		echo v"${version}" | tr -d '\n' | tr -d '\r'
	else #as no previous version found/installed
		echo v0.0.0 | tr -d '\n' | tr -d '\r'
	fi
else
	#exit with higher version to avoid update
	echo v999.99.99 | tr -d '\n' | tr -d '\r'
fi
exit 0
