#!/bin/sh

EXIT=0
VOLUME=""
BASEFILE=$(basename "$0")
config_file="/recalbox/share/system/recalbox.conf"
LOCKFILE=/var/run/$BASEFILE.lock
systemsetting="recalbox_settings"

getVolume(){
	VOLUME=$( pactl get-sink-volume @DEFAULT_SINK@ | awk -F"/" '{print $2}' | tr -d '%' | tr -d '[:space:]' )
}

increaseVolume(){
	#recallog -s "${BASEFILE}" -t "volume+" "increase volume for 5% (max 100%)"
	getVolume
	if [[ ${VOLUME} -gt 95 ]] ; then
		pactl set-sink-volume @DEFAULT_SINK@ "100%"
	else
		pactl set-sink-volume @DEFAULT_SINK@ "+5%"
	fi
	getVolume
	#recallog -s "${BASEFILE}" -t "volume" "is set now at: ${VOLUME}"
}

decreaseVolume(){
	#recallog -s "${BASEFILE}" -t "volume-" "decrease volume for 5%"
	getVolume
	if [[ ${VOLUME} -lt 5 ]] ; then
		pactl set-sink-volume @DEFAULT_SINK@ "0%"
	else
		pactl set-sink-volume @DEFAULT_SINK@ "-5%"
	fi
	getVolume
	#recallog -s "${BASEFILE}" -t "volume" "is set now at: ${VOLUME}"
}

saveVolume(){
	#recallog -s "${BASEFILE}" -t "saveVolume" "in ${config_file}"
	recalbox_settings -command save -key audio.volume -value "${VOLUME}"
}

alertPegasusOnVolumeChange(){
	#recallog -s "${BASEFILE}" -t "alertPegasusOnVolumeChange" "for volume : ${VOLUME}"
	timeout 0.2 curl "http://localhost:1234/api?conf=reload&parameter=audio.volume"
}

alertPegasusOnAudioDeviceChange(){
	#recallog -s "${BASEFILE}" -t "alertPegasusOnAudioChange()" "for audio selected/switched"
	timeout 0.2 curl "http://localhost:1234/api?conf=reload&parameter=audio.device"
}

case "$1" in
	set-default-sink)
		#recallog -s "${BASEFILE}" -t "set-default-sink" "from recalbox.conf audio.device"
		audiodevice="$($systemsetting -command load -key audio.device)"
		IFS=':'
		read -ra arr <<< "${audiodevice}"
		card=${arr[0]}
		echo "card : ${card}"
		port=$( echo ${arr[1]} | sed 's/\[/\\[/g' | sed 's/\]/\\]/g' )
		echo "port : ${port}"
		SINKINDEX=$( pacmd list-sinks | grep -e "index:" -e "${card}" -e "${port}" | grep -e "active port:" -B3 | grep -i "index:" | awk -F: '{print $2}' | tr -d '%' | tr -d '[:space:]' )
		echo "SINKINDEX : ${SINKINDEX}"
		#echo "pacmd set-default-sink ${SINKINDEX}"
		pacmd set-default-sink "${SINKINDEX}"
		alertPegasusOnAudioDeviceChange
	;;
	mute-toggle)
		#recallog -s "${BASEFILE}" -t "mute-toggle" "mute/unmute device"
		pactl set-sink-mute @DEFAULT_SINK@ toggle
	;;
	volume)
		getVolume
		echo "${VOLUME}"
	;;
	volume+)
		if (! test -e "${LOCKFILE}" ); then
			echo "volume locked"
			echo "volume locked" > $LOCKFILE
			sleep 0.2
			increaseVolume
			saveVolume
			alertPegasusOnVolumeChange
			rm $LOCKFILE
			echo "volume unlocked"
		else
			echo "volume already locked - aborted"
		fi
	;;
	volume-)
		if (! test -e "${LOCKFILE}" ); then
			echo "volume locked"
			echo "volume locked" > $LOCKFILE
			sleep 0.2
			decreaseVolume
			saveVolume
			alertPegasusOnVolumeChange
			rm $LOCKFILE
			echo "volume unlocked"
		else
			echo "volume already locked - aborted"
		fi
	;;
	*)
		echo "Usage: $0 {set-default-sink|mute-toggle|volume|volume+|volume-}"
		EXIT=1
	;;
esac

#echo "exit with value: ${EXIT}"
exit ${EXIT}
