#!/bin/bash

BATT=""
EXIT=0
BASEFILE=$(basename "$0")
config_file="/recalbox/share/system/recalbox.conf"

# Function to get the battery percentage
getBattery() {
    BATT=$(cat /sys/class/power_supply/{BAT,bat}*/uevent 2>/dev/null | grep -E "^POWER_SUPPLY_CAPACITY=" | sed -e 's/^POWER_SUPPLY_CAPACITY=//' | sort -rn | head -1)
    if [ ! -n "${BATT}" ] ; then
        NOW=$(cat /sys/class/power_supply/{FUEL,fuel}*/uevent 2>/dev/null | grep -E "^POWER_SUPPLY_CHARGE_NOW=" | sed -e 's/^POWER_SUPPLY_CHARGE_NOW=//' | sort -rn | head -1)
        MAX=$(cat /sys/class/power_supply/{FUEL,fuel}*/uevent 2>/null | grep -E "^POWER_SUPPLY_CHARGE_FULL=" | sed -e 's/^POWER_SUPPLY_CHARGE_FULL=//' | sort -rn | head -1)
        if [ ! -z "$NOW" ] && [ ! -z "$MAX" ] && [ "$MAX" != 0 ]; then
            BATT=$((200 * $NOW / $MAX % 2 + 100 * $NOW / $MAX))
        fi
    fi
    echo ${BATT}
}

# Function to check if the ayn power supply is connected
is_power_connected() {
    PLUGGED=$(cat /sys/class/power_supply/*/online 2>/dev/null | grep -E "^1")
    if [ -n "${PLUGGED}" ]; then
        echo "1"  # Power supply is connected
    else
        echo "0"  # Power supply is not connecte
    fi
}

case "$1" in
	battery)
		NEW_BATTERY=$(getBattery)
		echo "${NEW_BATTERY}" | tr -d '%' | tr -d '[:space:]'
	;;
	power_connected)
		CONNECTED=$(is_power_connected)
		echo "${CONNECTED}" | tr -d '%' | tr -d '[:space:]'
	;;
	*)
		echo "Usage: $0 {battery|power_connected}"
		EXIT=1
	;;
esac

#echo "exit with value: ${EXIT}"
exit ${EXIT}
