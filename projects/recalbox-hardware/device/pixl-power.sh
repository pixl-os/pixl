#!/bin/sh

EXIT=0
VOLUME=""
BASEFILE=$(basename "$0")
config_file="/recalbox/share/system/recalbox.conf"
SUSPEND_MODE=$(recalbox_settings -command load -key system.suspendmode)
SHUTDOWNDELAY="0" #not used for the moment

stopping() {
	recallog -s "${BASEFILE}" -t "shutdown" "system will be shutdown"
	/sbin/shutdown -hP now
}

suspending() {
	recallog -s "${BASEFILE}" -t "suspend" "system will be suspend"
	# shutdown / suspend
	case "${SUSPEND_MODE}" in
		"suspend")
			pm-is-supported --suspend && pm-suspend
		;;
		"hybrid")
			# pm-hibernate could be not supported on some devices
			pm-is-supported --suspend-hybrid && pm-suspend-hybrid
		;;
	esac
}

alertPegasusToShutdown(){
	recallog -s "${BASEFILE}" -t "alertPegasus" "to display 'shutdown dialog box'"
	timeout 0.2 curl "http://localhost:1234/api?action=shutdown"
}

case "$1" in
	press)
		recallog -s "${BASEFILE}" -t "press" "power button"
		recallog -s "${BASEFILE}" -t "press" "check if we should shutdown or display menu"
		#system.suspendmode=suspend or empty / depends of device initialized
		if [ "${SUSPEND_MODE}" = "suspend" ]; then
			touch "/var/run/shutdown-delay.flag"
			SLEEPTIME=$(expr "${SHUTDOWNDELAY}" + 2)
			sleep ${SLEEPTIME}
			if test -e "/var/run/shutdown-delay.flag"
			then
				# the one who manage to remove the file can do the action
				if rm "/var/run/shutdown-delay.flag" 2>/dev/null
				then
					touch "/var/run/shutdown-delay.last"
					alertPegasusToShutdown
				fi
			fi
		else
			stopping
		fi
	;;
	release)
		recallog -s "${BASEFILE}" -t "release" "power button"
		recallog -s "${BASEFILE}" -t "release" "check if we should suspend or not"
		#system.suspendmode=suspend or empty / depends of device initialized
		# the one who manage to remove the file can do the action
		FLAG_DELAY=$(expr $(($(date +%s) - $(date +%s -r "/var/run/shutdown-delay.flag"))))
		if rm "/var/run/shutdown-delay.flag" 2>/dev/null
		then
			# if there is a delay, check it first
			if test "${SHUTDOWNDELAY}" != ""
			then
				if test "${FLAG_DELAY}" -lt "${SHUTDOWNDELAY}"
				then
					# no action if the delay didn't elapsed
					exit 0
				fi
			fi

			### callable only once every 7 seconds
			### because some boards (steamdeck) have 2 events reacting to the KEY_POWER
			### this is mainly for suspend to not call it 2 times
			if test -e "/var/run/shutdown-delay.last"
			then
				DELAY=$(expr $(($(date +%s) - $(date +%s -r "/var/run/shutdown-delay.last"))))
				if test "${DELAY}" -lt 5
				then
					echo "wait between 2 calls" >2&
					exit 0
				fi
			fi
			touch "/var/run/shutdown-delay.last"
			########
			suspending
		fi
	;;
	suspend)
		suspending
	;;
	shutdown)
		stopping
	;;
	*)
		echo "Usage: $0 {press|release|suspend|shutdown}"
		EXIT=1
	;;
esac

#echo "exit with value: ${EXIT}"
exit ${EXIT}
