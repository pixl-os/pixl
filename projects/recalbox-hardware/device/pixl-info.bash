#!/bin/bash

EXIT=0
BASEFILE=$(basename "$0")
config_file="/recalbox/share/system/recalbox.conf"

# Function to check if the ayn power supply is connected
is_power_connected() {
    PLUGGED=$(cat /sys/class/power_supply/*/online 2>/dev/null | grep -E "^1")
    if [ -n "${PLUGGED}" ]; then
        echo "1"  # Power supply is connected
    else
        echo "0"  # Power supply is not connecte
    fi
}

getTime(){
    DT=$(date +%H:%M)
  	echo "Time: ${DT}"
}

getCpu() {
  test -e "${1}/cpufreq/scaling_max_freq" || return 0

  if test -e "${1}/online"
  then
	  OL=$(cat "${CPU}/online")
	  test "${OL}" = 1 || return 0
  fi
  cat "${CPU}/cpufreq/scaling_max_freq"
}

getBatteryInfo() {
  # Detect battery
  BATT=""
  BATT=$(cat /sys/class/power_supply/{BAT,bat}*/uevent 2>/dev/null | grep -E "^POWER_SUPPLY_CAPACITY=" | sed -e s+'^POWER_SUPPLY_CAPACITY='++ | sort -rn | head -1)
  if ! test -n "${BATT}"
  then
      NOW=$(cat /sys/class/power_supply/{FUEL,fuel}*/uevent 2>/dev/null | grep -E "^POWER_SUPPLY_CHARGE_NOW=" | sed -e s+'^POWER_SUPPLY_CHARGE_NOW='++ | sort -rn | head -1)
      MAX=$(cat /sys/class/power_supply/{FUEL,fuel}*/uevent 2>/dev/null | grep -E "^POWER_SUPPLY_CHARGE_FULL=" | sed -e s+'^POWER_SUPPLY_CHARGE_FULL='++ | sort -rn | head -1)
      if [ ! -z "$NOW" ] && [ ! -z "$MAX" ] && [ "$MAX" != 0 ]
      then
          BATT=$((200*$NOW/$MAX % 2 + 100*$NOW/$MAX))
      fi
  fi
  
  BATTREMAINING=""
  BATTREMAINING_CURRENT=$(cat /sys/class/power_supply/{BAT,bat}*/current_now 2>/dev/null)
  BATTREMAINING_CHARGE=$(cat /sys/class/power_supply/{BAT,bat}*/charge_now 2>/dev/null)
  if test -n "${BATTREMAINING_CURRENT}" -a -n "${BATTREMAINING_CHARGE}"
  then
      if test "${BATTREMAINING_CURRENT}" -gt 0
      then
  	BATTREMAINING_HOURS=$(expr ${BATTREMAINING_CHARGE} / ${BATTREMAINING_CURRENT})
  	BATTREMAINING_MINS=$(expr ${BATTREMAINING_CHARGE} '*' 60 / ${BATTREMAINING_CURRENT} - 60 '*' ${BATTREMAINING_HOURS})
  	test ${BATTREMAINING_MINS} -lt 10 && BATTREMAINING_MINS=0${BATTREMAINING_MINS}
  	BATTREMAINING=" (${BATTREMAINING_HOURS}:${BATTREMAINING_MINS})"
      fi
  fi
  # battery
  if test -n "${BATT}"
  then
	CONNECTED=$(is_power_connected)
	if [ $CONNECTED -eq 0 ] ; then
		echo "Battery: ${BATT}%${BATTREMAINING}"
	else
		echo "Battery: ${BATT}%"
	fi
  fi
}

getBoardInfo() {
  V_BOARD=$(cat /recalbox/recalbox.arch)
  V_CPUNB=$(grep -i 'cpu cores' /proc/cpuinfo | head -n 1 | cut -d ': ' -f2 | tr -d '%' | tr -d '[:space:]')
  V_CPUMAXNB=$(grep processor /proc/cpuinfo | wc -l)
  V_CPUMODEL1=$(grep -E $'^model name\t:' /proc/cpuinfo | head -1 | sed -e s+'^model name\t: '++)
  V_SYSTEM=$(uname -rs)
  # min freq : minimum freq among the max cpus ; consider only online cpus
  V_CPUMINFREQ=$(for CPU in /sys/devices/system/cpu/cpu*; do getCpu "${CPU}"; done | sort -n  | head -1)
  V_CPUMAXFREQ=$(for CPU in /sys/devices/system/cpu/cpu*; do getCpu "${CPU}"; done | sort -rn | head -1)
  let V_CPUMINFREQ=${V_CPUMINFREQ}/1000
  let V_CPUMAXFREQ=${V_CPUMAXFREQ}/1000
  
  V_BOARD_MODEL=$(cat /sys/firmware/devicetree/base/model 2>/dev/null | tr -d '\0' | sed -e s+"[^A-Za-z0-9]"+"_"+g)
  if test -z "${V_BOARD_MODEL}"
  then
      # give an other chance with dmi
      V_BOARD_MODEL=$(cat /sys/devices/virtual/dmi/id/board_name 2>/dev/null | tr -d '\0' | sed -e s+"[^A-Za-z0-9]"+"_"+g)
  fi
  # 3rd time lucky
  if test -z "${V_BOARD_MODEL}" || test "${V_BOARD_MODEL}" == "Default_string"
  then
      V_BOARD_MODEL=$(cat /sys/devices/virtual/dmi/id/product_name 2>/dev/null | tr -d '\0' | sed -e s+"[^A-Za-z0-9]"+"_"+g)
  fi
  if test -n "${V_BOARD_MODEL}"
  then
      echo "Model: ${V_BOARD_MODEL}"
  fi
  echo "System: ${V_SYSTEM}"
  V_ARCH=$(uname -m)
  echo "Architecture: ${V_ARCH}"
  if [ "$V_BOARD" != "$V_ARCH" ]; then
      echo "Board: ${V_BOARD}"
  fi
  
  if echo "${V_BOARD}" | grep -qE "^rpi[0-9]$"
  then
      REVISION=$(grep -E $'^Revision\t:' /proc/cpuinfo | head -1 | sed -e s+'^Revision\t: '++)
      test -n "${REVISION}" && echo "Revision: ${REVISION}"
  fi
  
  [[ -z ${V_CPUMODEL1} ]] || echo "CPU Model: ${V_CPUMODEL1}"
  
  echo "CPU Cores: ${V_CPUNB}"
  echo "CPU Threads: ${V_CPUMAXNB}"
  
  if test "${V_CPUMINFREQ}" != "${V_CPUMAXFREQ}"
  then
      echo "CPU Freq: ${V_CPUMINFREQ}/${V_CPUMAXFREQ} MHz"
  else
      echo "CPU Max Freq: ${V_CPUMAXFREQ} MHz"
  fi
  
  declare -a check_cpu_features=("avx2" "sse4_1")
  declare -a cpu_features=()
  for feature in "${check_cpu_features[@]}"
  do
      if grep -q " ${feature}" /proc/cpuinfo
      then
          cpu_features+=("${feature}")
      fi
  done
  if [ "${#cpu_features[@]}" -gt 0 ]
  then
      echo "CPU Features: ${cpu_features[@]}"
  fi
}

getFreqInfo(){
  V_CPUMINFREQ=$(for CPU in /sys/devices/system/cpu/cpu*; do getCpu "${CPU}"; done | sort -n  | head -1)
  V_CPUMAXFREQ=$(for CPU in /sys/devices/system/cpu/cpu*; do getCpu "${CPU}"; done | sort -rn | head -1)
  let V_CPUMINFREQ=${V_CPUMINFREQ}/1000
  let V_CPUMAXFREQ=${V_CPUMAXFREQ}/1000
  if test "${V_CPUMINFREQ}" != "${V_CPUMAXFREQ}"
  then
      echo "CPU Freq: ${V_CPUMINFREQ}/${V_CPUMAXFREQ} MHz"
  else
      echo "CPU Max Freq: ${V_CPUMAXFREQ} MHz"
  fi
}
getTempInfo(){
  # temperature
  # Unit: millidegree Celsius
  TEMPE=$(cat /sys/devices/virtual/thermal/thermal_zone*/temp /sys/class/hwmon/hwmon*/temp*_input 2>/dev/null | sort -rn | head -1 | sed -e s+"[0-9][0-9][0-9]$"++)
  if test -n "${TEMPE}"
  then
      echo "Temp: ${TEMPE}�C"
  fi
}

getMemoryInfo(){
  MEM_TOTAL_KB=$(head /proc/meminfo | grep -E "^MemTotal:" | sed -e s+"^MemTotal:[ ]*\([0-9]*\) kB$"+"\\1"+)
  MEM_AVAILABLE_KB=$(head /proc/meminfo | grep -E "^MemAvailable:" | sed -e s+"^MemAvailable:[ ]*\([0-9]*\) kB$"+"\\1"+)
  MEM_TOTAL_MB=$(expr "${MEM_TOTAL_KB}" / 1024)
  MEM_AVAILABLE_MB=$(expr "${MEM_AVAILABLE_KB}" / 1024)
  echo "Memory: ${MEM_AVAILABLE_MB}/${MEM_TOTAL_MB} MB"
}

getDisplayInfo(){
  # Current Resolution
  if test -z "${DISPLAY}"; then
      export DISPLAY=:0.0
  fi
  
  if [[ "${FULL_DISPLAY}" != 0 ]]; then
  	# OPENGL
  	if test "${V_BOARD}" = "x86" -o "${V_BOARD}" = "x86_64"
  	then
  	    V_OPENGLVERSION=$(DISPLAY=:0.0 glxinfo 2>/dev/null | grep -E '^OpenGL core profile version string:' | sed -e s+'^OpenGL core profile version string:[ ]*'++)
  	    if test -z "${V_OPENGLVERSION}"
  	    then
  		V_OPENGLVERSION=$(DISPLAY=:0.0 glxinfo 2>/dev/null | grep -E '^OpenGL version string:' | sed -e s+'^OpenGL version string:[ ]*'++)
  	    fi
  	    echo "OpenGL Driver Version: ${V_OPENGLVERSION}"
  	fi
  
  	# VULKAN
  	if test -e /tmp/vulkaninfo.tmp
  	then
  		APIVERSION=$(grep 'Vulkan Instance Version:' /tmp/vulkaninfo.tmp | cut -d ':' -f2)
  	    if test -n "${APIVERSION}"; then
  		    echo "Vulkan Driver Version: ${APIVERSION}"
  	    else
  		    echo "Vulkan Driver Version: none"
  	    fi
  	fi
  fi
}

getPadBattery(){
  # PAD Battery
  for PADBAT in /sys/class/power_supply/*/device/uevent
  do
      if test -e "${PADBAT}" # when nothing is found, the expression is return
      then
  	# HID devices only
  	PADNAME=$(sed -nE 's/^HID_NAME=(.+)/\1/p' "${PADBAT}")
  	if test -n "${PADNAME}"
  	then
  	    # parent of parent / uevent
  	    BATTUEVENT=$(dirname "${PADBAT}")
  	    BATTUEVENT=$(dirname "${BATTUEVENT}")/uevent
  	    BATT="$(batocera-padsinfo "${BATTUEVENT}")"
  	    echo "${PADNAME}: ${BATT}%"
  	fi
      fi
  done
}

case "$1" in
  battery)
    getBatteryInfo
  ;;
  board)
    getBoardInfo
  ;;
	display)
    FULL_DISPLAY=1
    getDisplayInfo
	;;
  frequence)
     getFreqInfo
  ;;
	memory)
    getMemoryInfo
	;;
	pad_battery)
    getPadBattery
	;;
  popup)
    INFO1=$(getBatteryInfo)
    INFO2=$(getMemoryInfo)
    INFO3=$(getTempInfo)
    INFO4=$(getFreqInfo)
    INFO5=$(getTime)
    #Frame buffer could return wrong size :-(
    #WIDTH=$(cut -d, -f1 /sys/class/graphics/fb0/virtual_size)
    #HEIGHT=$(cut -d, -f2 /sys/class/graphics/fb0/virtual_size)
    WIDTH=$(DISPLAY=:0 xrandr |grep \* |awk '{print $1}' | cut -dx -f1 | tr -d '%' | tr -d '[:space:]')
    HEIGHT=$(DISPLAY=:0 xrandr |grep \* |awk '{print $1}' | cut -dx -f2 | tr -d '%' | tr -d '[:space:]')
    if [ $WIDTH -le $HEIGHT ] ; then
		echo "for vertical screen"
		#switch between WIDTH and HEIGHT
		NEW_HEIGHT=${WIDTH}
		WIDTH=${HEIGHT}
		HEIGHT=${NEW_HEIGHT}
	else
		echo "for horizontal screen"
		#do nothing
	fi
	fontSize=$(expr ${WIDTH} '/' 100 '*' 8 '/' 5 ) # for 1.6% of the screen for the font size
	shiftFromBorder=$(expr ${WIDTH} '/' 100 '*' 0) # for 0% of shift as margin
	popupWidth=$(expr ${WIDTH} '-' ${shiftFromBorder} '*' 2)
	popupHeight=$(expr ${fontSize} '*' 2) # 1,8 x size of the font
	delta=$(expr ${HEIGHT} '-' ${popupHeight})
	echo -e "${INFO1} - ${INFO2} - ${INFO3} - ${INFO4} - ${INFO5}" | xmessage -timeout 2 -buttons "" -font "-*-*-*-*-*-*-${fontSize}-*-*-*-*-*-iso8859-1" -bw 0 -fg white -bg black -bd black -geometry ${popupWidth}x${popupHeight}+${shiftFromBorder}-${delta} -file - &
  ;;
  temp)
    getTempInfo
  ;;
  time)
    getTime
  ;;
	*)
		echo "Usage: $0 {battery|board|display|frequence|pad_battery|popup|temp|time}"
		EXIT=1
	;;
esac

#echo "exit with value: ${EXIT}"
exit ${EXIT}