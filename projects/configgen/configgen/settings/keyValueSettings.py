#!/usr/bin/env python
from __future__ import annotations

from typing import Dict, Union

import os
import re

def Log(txt):
    import datetime
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    #print(formatted_time[:-3] + " - " + txt)
    return 0

class keyValueSettings:
    def __init__(self, settingsFile: str, extraSpaces = False):
        self.__settingsFile: str = settingsFile
        self.__settings: Dict[str, Union[str, 'Comment', 'EmptyLine']] = {}
        self.__extraSpaces = extraSpaces # Allow 'key = value' instead of 'key=value' on writing.
        self.__true = '1'
        self.__false = '0'

    def __getitem__(self, item: str):
        if item in self.__settings:
            return self.__settings[item]
        raise KeyError

    def defineBool(self, true: str, false: str) -> keyValueSettings:
        self.__true = true
        self.__false = false
        return self

    def clear(self) -> keyValueSettings:
        self.__settings.clear()
        return self

    def changeSettingsFile(self, newfilename) -> keyValueSettings:
        self.__settingsFile = newfilename
        return self

    @property
    def SettingsFile(self) -> str: return self.__settingsFile

    def getString(self, key: str, default: str) -> str:
        if key in self.__settings:
            val = self.__settings[key]
            if isinstance(val, str):
                return val
        return default

    def getInt(self, key: str, default: int) -> int:
        if key in self.__settings:
            val = self.__settings[key]
            if isinstance(val, str):
                return int(val.strip('"'))
        return default

    def getBool(self, key: str, default: bool) -> bool:
        if key in self.__settings:
            val = self.__settings[key]
            if isinstance(val, str):
                return val.strip('"') == self.__true
        return default

    def hasOption(self, key: str) -> bool:
        if key in self.__settings:
            return True
        return False

    def setString(self, key: str, value: str) -> keyValueSettings:
        self.__settings[key] = value
        return self

    def setInt(self, key: str, value: int) -> keyValueSettings:
        self.__settings[key] = str(value)
        return self

    def setBool(self, key: str, value: bool) -> keyValueSettings:
        self.__settings[key] = self.__true if value else self.__false
        return self

    def setDefaultString(self, key: str, defaultvalue: str) -> keyValueSettings:
        if key not in self.__settings:
            self.__settings[key] = defaultvalue
        return self

    def setDefaultInt(self, key: str, defaultvalue: int) -> keyValueSettings:
        if key not in self.__settings:
            self.__settings[key] = str(defaultvalue)
        return self

    def setDefaultBool(self, key: str, defaultvalue: bool) -> keyValueSettings:
        if key not in self.__settings:
            self.__settings[key] = self.__true if defaultvalue else self.__false
        return self

    def removeOption(self, key: str) -> keyValueSettings:
        self.__settings.pop(key, None)
        return self

    def removeOptionStartingWith(self, pattern) -> keyValueSettings:
        keysToRemove = []
        for key in self.__settings.keys():
            if key.startswith(pattern):
                keysToRemove.append(key)
        for key in keysToRemove:
            self.__settings.pop(key, None)
        return self

    def getOptionSubset(self, startWith) -> Dict[str, str]:
        result: Dict[str, str] = {}
        swl = len(startWith)
        for key, value in self.__settings.items():
            if isinstance(value, str) and key.startswith(startWith):
                result[key[swl:]] = value
        return result

    def saveFile(self) -> keyValueSettings:
        folder = os.path.dirname(self.__settingsFile)
        if not os.path.exists(folder):
            os.makedirs(folder)

        with open(self.__settingsFile, 'w', encoding="utf-8") as sf:  # Use 'w' mode and specify encoding
            for line in self._get_lines_to_write():
                sf.write(line)
        return self

    def loadFile(self, clear=False) -> keyValueSettings:
        if clear:
            self.__settings.clear()
        try:
            with open(self.__settingsFile, 'r', encoding="utf-8") as lines: # Specify encoding
                for line in lines:
                    self._process_line(line)
                    Log("for" + self.__settingsFile + " line process : " + line)
        except FileNotFoundError:
            print(f"Warning: File not found: {self.__settingsFile}") # Handle file not found error
        return self

    def _get_lines_to_write(self):
        lines = []
        for key, value in self.__settings.items():
            if isinstance(value, Comment) or isinstance(value, EmptyLine):
                lines.append(str(value) + '\n')
                Log("for" + self.__settingsFile + " line append as comment/empty: " + str(value))
            elif isinstance(value,str):
                line = f"{key}{' = ' if self.__extraSpaces else '='}{value}\n"
                lines.append(line)
                Log("for" + self.__settingsFile + " line append as key/value: " + line)
        return lines

    def _process_line(self, line):
        line = line.rstrip('\n')

        if line.startswith('#') or line.startswith('//'):
            key = f"__comment_{len(self.__settings)}"
            self.__settings[key] = Comment(line)
        elif not line.strip():
            key = f"__empty_{len(self.__settings)}"
            self.__settings[key] = EmptyLine()
        elif line.startswith('['):
            return
        else:
            m = re.match(r'^([^#;].*?)\s?=\s?(.+)$', line)
            if m:
                key = m.group(1).strip()
                value = m.group(2).strip()
                self.__settings[key] = value
                Log("for" + self.__settingsFile + " line 'really' process : " + key + " = " + value)

    def add_comment(self, comment_text):
        key = f"__comment_{len(self.__settings)}"
        self.__settings[key] = Comment(comment_text)

    def __str__(self):
        return str(self.__settings)

class Comment: # Class to represent comments
    def __init__(self, text):
        self.text = text

    def __str__(self):
        return self.text

class EmptyLine:
    def __str__(self):
        return ""