#!/usr/bin/env python
HOME_INIT = '/recalbox/share_init/system/'
HOME = '/recalbox/share/system'
CONF = HOME + '/configs'
CACHE = HOME + "/.cache"
SAVES = '/recalbox/share/saves'
SAVES_INIT = '/recalbox/share_init/saves'
SCREENSHOTS = '/recalbox/share/screenshots'
BIOS = '/recalbox/share/bios'
BIOS_INIT = '/recalbox/share_init/bios'
OVERLAYS = '/recalbox/share/overlays'
RECALBOX_OVERLAYS = '/recalbox/share_init/overlays'
RECALBOX_240P_OVERLAYS = '/recalbox/share_init/240poverlays'
ROMS = '/recalbox/share/roms'
CHEATS = '/recalbox/share/cheats'

pegasusInputs = HOME + '/.pegasus-frontend/input.cfg'
recalboxConf = HOME + '/recalbox.conf'
logdir = HOME + '/logs/'

pegasusLightGun = HOME_INIT + '/.pegasus-frontend/lightgun.cfg'
pegasusLightGunFromShare = HOME + '/.pegasus-frontend/lightgun.cfg'
GameListFileName = "gamelist.xml"

# This dict is indexed on the emulator name, not on the system
recalboxBins =\
{
    'advancemame'       : '/usr/bin/advmame',
    'amiberry'          : '/usr/bin/amiberry',
    'beebem'            : '/usr/bin/Beebem',
    'citra'             : '/usr/bin/citra',
    'daphne'            : '/usr/bin/hypseus',
    'dolphin'           : '/usr/bin/dolphin-emu',
    'dolphin-triforce'  : '/usr/bin/dolphin-triforce',
    'dosbox'            : '/usr/bin/dosbox',
    'duckstation'       : '/usr/bin/duckstation/duckstation',
    'fba2x'             : '/usr/bin/fba2x',
    'frotz'             : '/usr/bin/sfrotz',
    'gsplus'            : '/usr/bin/GSplus',
    'hatari'            : '/usr/bin/hatari',
    'libretro'          : '/usr/bin/retroarch',
    'linapple'          : '/usr/bin/linapple',
    'mame'              : '/usr/bin/mame/mame',
    'mupen64plus'       : '/usr/bin/mupen64plus',
    'openbor'           : '/usr/bin/OpenBOR',
    'oricutron'         : '/usr/bin/oricutron/oricutron',
    'pcsx_rearmed'      : '/usr/bin/pcsx_rearmed',
    'pcsx2'             : '/usr/bin/PCSX2/pcsx2',
    'pisnes'            : '/usr/bin/pisnes',
    'ppsspp'            : '/usr/bin/PPSSPPSDL',
    'rb5000'            : '/usr/bin/rb5000',
    'reicast'           : '/usr/bin/reicast.elf',
    'rpcs3'             : '/usr/bin/rpcs3',
    'scummvm'           : '/usr/bin/scummvm',
    'simcoupe'          : '/usr/bin/simcoupe',
    'solarus'           : '/usr/bin/solarus-run',
    'supermodel'        : '/usr/bin/supermodel',
    'model2emu'         : '/usr/bin/model2emu/emulator_multicpu.exe',
    'ti99sim'           : '/usr/bin/ti99sim/ti99sim-sdl',
    'vice'              : '/usr/bin/x64',
    'xroar'             : '/usr/bin/xroar',
    'xemu'              : '/usr/bin/xemu',
    'cemu'              : '/usr/bin/cemu/cemu',
}


retroarchRoot = CONF + '/retroarch'
retroarchCustom = retroarchRoot + '/retroarchcustom.cfg'
retroarchCustomOrigin = retroarchRoot + "/retroarchcustom.cfg.origin"
retroarchCoreCustom = retroarchRoot + "/cores/retroarch-core-options.cfg"
retroarchInitCustomOrigin = HOME_INIT + "configs/retroarch/retroarchcustom.cfg.origin"

retroarchCores = "/usr/lib/libretro/"
shadersRoot = "/recalbox/share/shaders/"
shadersExt = '.gplsp'
libretroExt = '_libretro.so'
screenshotsDir = "/recalbox/share/screenshots/"
savesDir = "/recalbox/share/saves/"

fbaRoot = CONF + '/fba/'
fbaCustom = fbaRoot + 'fba2x.cfg'
fbaCustomOrigin = fbaRoot + 'fba2x.cfg.origin'


mupenConf = CONF + '/mupen64/'
mupenCustom = mupenConf + "mupen64plus.cfg"
mupenInput = mupenConf + "InputAutoCfg.ini"
mupenSaves = SAVES + "/n64"
mupenMappingUser    = mupenConf + 'input.xml'
mupenMappingSystem  = '/recalbox/share_init/system/configs/mupen64/input.xml'

shaderPresetRoot = "/recalbox/share_init/system/configs/shadersets/"

reicastCustom = CONF + '/reicast'
reicastConfig = reicastCustom + '/emu.cfg'
reicastConfigInit = HOME_INIT + 'configs/reicast/emu.cfg'
reicastSaves = SAVES + '/dreamcast'
reicastBios = BIOS

dolphinConfig       = CONF + "/dolphin-emu"
dolphinData         = SAVES + "/dolphin-emu"
dolphinIni          = dolphinConfig + '/Dolphin.ini'
dolphinLoggerIni    = dolphinConfig + '/Logger.ini'
dolphinHKeys        = dolphinConfig + '/Hotkeys.ini'
dolphinGFX          = dolphinConfig + '/GFX.ini'
dolphinGameSettings = dolphinConfig + "/GameSettings"
RetroAchievementsIni = dolphinConfig + '/RetroAchievements.ini'
dolphinSYSCONF      = dolphinData + '/Wii/shared2/sys/SYSCONF'

duckstationRootFolder = HOME + "/configs"
duckstationDefaultConfigIni = HOME_INIT + "configs/duckstation/settings.ini"
duckstationConfigIni = duckstationRootFolder + "/duckstation/settings.ini"

ppssppConf = CONF + '/ppsspp/PSP/SYSTEM'
ppssppControlsIni = ppssppConf + '/controls.ini'
ppssppControls = CONF + '/ppsspp/gamecontrollerdb.txt'
ppssppControlsInit = HOME_INIT + 'configs/ppsspp/PSP/SYSTEM/controls.ini'
ppssppConfig = ppssppConf + '/ppsspp.ini'
ppssppSaves = SAVES + '/psp'

pcsx2RootFolder = HOME + '/configs'
pcsx2ConfigFile = pcsx2RootFolder + '/PCSX2/inis/PCSX2.ini'
pcsx2DefaultConfigFile = HOME_INIT + 'configs/PCSX2/inis/PCSX2.ini'

dosboxCustom = CONF + '/dosbox'
dosboxConfig = dosboxCustom + '/dosbox.conf'

scummvmSaves = SAVES + '/scummvm'

simcoupeConfig = HOME + '/.simcoupe/SimCoupe.cfg'

viceConfig = CONF + "/vice/vice.conf"

advancemameConfig = CONF + '/advancemame/advmame.rc'
advancemameConfigOrigin = CONF + '/advancemame/advmame.rc.origin'

amiberryMountPoint = '/tmp/amiga'
amiberrySaves      = SAVES

daphneInputIni = CONF + '/daphne/dapinput.ini'
daphneHomedir = ROMS + '/daphne'
daphneDatadir = '/usr/share/daphne'

oricutronConfig = HOME + '/.config/oricutron.cfg'

openborConfig = HOME + '/configs/openbor/default.cfg'
openborConfigOrigin = HOME + '/configs/openbor/default.cfg.origin'

gsplusConfig = HOME + '/.config/gsplus.cfg'

atari800CustomConfig = HOME + '/.atari800.cfg'

hatariCustomConfig = HOME + '/.hatari/hatari.cfg'

pcsxRootFolder = '/recalbox/share/system/configs/pcsx'
pcsxConfigFile = pcsxRootFolder + '/pcsx.cfg'

pisnesRootFolder = '/recalbox/share/system/configs/pisnes'
pisnesConfigFile = pisnesRootFolder + '/snes9x.cfg'

supermodelRooFolder = CONF + '/supermodel'
supermodelSaves = SAVES + '/model3'
supermodelSavestates = supermodelSaves + '/savestate'
supermodelConfig = supermodelRooFolder + '/Supermodel.ini'
supermodelConfigInit = HOME_INIT + 'configs/supermodel/Supermodel.ini'

model2emuConfigFile = SAVES + '/model2/model2emu/EMULATOR.INI'
model2emuPWD = SAVES + '/model2/model2emu'
model2emuCFG = SAVES + '/model2/model2emu/CFG'
model2emuLUA = SAVES + '/model2/model2emu/scripts'
model2emuDAT = SAVES + '/model2/model2emu/NVDATA'

model2emuConfigFileInit = SAVES_INIT + '/model2/model2emu/EMULATOR.INI'
model2emuPWDInit = SAVES_INIT + '/model2/model2emu'
model2emuCFGInit = SAVES_INIT + '/model2/model2emu/CFG'
model2emuLUAInit = SAVES_INIT + '/model2/model2emu/scripts'
model2emuDATInit = SAVES_INIT + '/model2/model2emu/NVDATA'

crtFilesRootFolder = '/recalbox/system/configs/crt/'
crtUserFilesRootFolder = '/recalbox/share/system/configs/crt/'

citraConfig = CONF + "/citra-emu"
citraData   = SAVES + "/3ds"
citraIni    = citraConfig + '/qt-config.ini'
citraSysConf = citraData + '/nand/data/00000000000000000000000000000000/sysdata/00010017/00000000/config'

xemuConfig = CONF + "/xemu"
xemuIni = xemuConfig + "/xemu.ini"

minivmacRomFile = '/tmp/minivmac.cmd'
minivmacOsFile = BIOS + '/macintosh/MinivMacBootv2.dsk'

frotzConfig = HOME + '/.config/frotz/frotz.conf'

cemuConfig  = CONF + '/cemu'
cemuSettingsOrigin = HOME_INIT + "/configs/cemu/settings.origin.xml"
cemuSettings = cemuConfig + "/settings.xml"
cemuRomdir = ROMS + '/wiiu'
cemuSaves = SAVES + '/wiiu'
cemuDatadir = '/usr/bin/cemu'

rpcs3Config = CONF + '/rpcs3'
rpcs3Homedir = ROMS + '/ps3'
rpcs3Saves = SAVES + '/ps3'
rpcs3CurrentConfig = CONF + '/rpcs3/GuiConfigs/CurrentSettings.ini'
rpcs3YmlConfig = CONF + '/rpcs3/config.yml'
rpcs3VfsConfig = CONF + '/rpcs3/vfs.yml'
rpcnConfig = CONF + '/rpcs3/rpcn.yml'
rpcs3configInput = CONF + '/rpcs3/config_input.yml'
rpcs3configevdev = CONF + '/rpcs3/InputConfigs/Evdev/Default Profile.yml'

libretroMameCFG = SAVES + '/mame/mame/cfg'