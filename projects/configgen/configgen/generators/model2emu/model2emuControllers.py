#!/usr/bin/env python

#Modules
import configgen.recalboxFiles as recalboxFiles
import os
import time
import subprocess
import configgen.utils.popup as popup

#Classes
from typing import Dict
from configgen.controllers.controller import Controller, InputItem
from configgen.controllers.controller import ControllerPerPlayer
from configgen.generators.model2emu.model2emuLightGuns import model2emuLightGun
from configgen.Emulator import Emulator
from configgen.settings.keyValueSettings import keyValueSettings
from configgen.utils.wine import wine

#CONSTANTS
#tip to manage Axis or Button from the same inputItem depending of the usage
AS_AXIS = 100
AS_BUTTON = 1
    
def Log(txt):
    import datetime
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    #print(formatted_time[:-3] + " - " + txt)
    return 0
    
class model2emuControllers:

    #################### DAYTONA (and clones)  ###############################
    DaytonaInputs: Dict[str, int] = \
    {
        "Up":                      InputItem.ItemUp,
        "Down":                    InputItem.ItemDown,
        "Left":                    InputItem.ItemLeft,
        "Right":                   InputItem.ItemRight,
        "Steering":                InputItem.ItemJoy1Left, # X AXIS by default from Joy1
        "Accelerate":              InputItem.ItemR2, # R2 by default else Y AXIS inversed from Joy1 will be used
        "Brake":                   InputItem.ItemL2, # L2 by default else Y AXIS from Joy1 will be used
        "Shift1":                  InputItem.ItemJoy2Up, #keyboard key Q by deault
        "Shift2":                  InputItem.ItemJoy2Left, #keyboard key S by deault
        "Shift3":                  InputItem.ItemJoy2Down, #keyboard key D by deault
        "Shift4":                  InputItem.ItemJoy2Right, #keyboard key F by deault
        "ShiftNeutral":            InputItem.ItemR3, #keyboard key G by deault
        "VR1":                     InputItem.ItemY,
        "VR2":                     InputItem.ItemX,
        "VR3":                     InputItem.ItemA,
        "VR4":                     InputItem.ItemB,
        "Start":                   InputItem.ItemStart,
        "Coin1":                   InputItem.ItemSelect,
        "Coin2":                   -1, #keyboard key 6 by deault
        "Service":                 -1, #keyboard key F1 by deault
        "Test":                    -1, #keyboard key F2 by deault
        "ShowRenderStats":         -1, #keyboard key F8 by deault
        "ToggleTitlemapRender":    -1, #keyboard key F7 by deault
        "CyleTitlemapUpdateRate":  -1, #keyboard key F6 by deault
    }

    #################### Desert Tank  ###############################
    DesertTankInputs: Dict[str, int] = \
    {
        "SteerLeft":               InputItem.ItemLeft,
        "SteerRight":              InputItem.ItemRight,
        "TurretUp":                InputItem.ItemUp,
        "TurretDown":              InputItem.ItemDown,
        "Accel+":                  InputItem.ItemR1,
        "Accel-":                  InputItem.ItemL1,
        "Steering":                InputItem.ItemJoy1Left, # X AXIS by default from Joy1
        "Turret":                  -1, #finally axis is not well, we keep dpad up/down in this case
        "Accelerator":             InputItem.ItemR2, # R2 by default else R1 will be used
        "Shift":                   InputItem.ItemL2, # L2 by default else L1 will be used
        "MachineGun":              InputItem.ItemB,
        "Cannon":                  InputItem.ItemA,
        "VR1":                     InputItem.ItemJoy2Left,
        "VR2":                     InputItem.ItemJoy2Down,
        "VR3":                     InputItem.ItemJoy2Right,
        "Start":                   InputItem.ItemStart,
        "Coin":                    InputItem.ItemSelect,
        "Coin2":                   -1, #keyboard key 6 by deault
        "Service":                 -1, #keyboard key F1 by deault
        "Test":                    -1, #keyboard key F2 by deault
        "ShowRenderStats":         -1, #keyboard key F8 by deault
        "ToggleTitlemapRender":    -1, #keyboard key F7 by deault
        "CyleTitlemapUpdateRate":  -1, #keyboard key F6 by deault
    }

    #################### Dynamite Baseball 97 ###############################
    DynamiteBaseballInputs: Dict[str, int] = \
    {
        "Up":                      InputItem.ItemUp,
        "Down":                    InputItem.ItemDown,
        "Left":                    InputItem.ItemLeft,
        "Right":                   InputItem.ItemRight,
        "Bat":                     InputItem.ItemJoy1Left,
        "Button1":                 InputItem.ItemB,
        "Button2":                 InputItem.ItemA,
        "Bat+":                    InputItem.ItemX,
        "Bat-":                    InputItem.ItemY,
        "Start":                   InputItem.ItemStart,
        "Coin":                    InputItem.ItemSelect,
        "Player2Up":               InputItem.ItemUp,
        "Player2Down":             InputItem.ItemDown,
        "Player2Left":             InputItem.ItemLeft,
        "Player2Right":            InputItem.ItemRight,
        "PLayer2Bat":              InputItem.ItemJoy1Left,
        "Player2Button1":          InputItem.ItemB,
        "Player2Button2":          InputItem.ItemA,
        "Player2Bat+":             InputItem.ItemX,
        "Player2Bat-":             InputItem.ItemY,
        "Player2Start":            InputItem.ItemStart,
        "Player2Coin":             InputItem.ItemSelect,
        "Service":                 -1, #keyboard key F1 by deault
        "Test":                    -1, #keyboard key F2 by deault
        "ShowRenderStats":         -1, #keyboard key F8 by deault
        "ToggleTitlemapRender":    -1, #keyboard key F7 by deault
        "CyleTitlemapUpdateRate":  -1, #keyboard key F6 by deault
    }

    #################### Indianapolis 500/Sega Touring Car Championship (and clones)  ###############################
    IndianapolisInputs: Dict[str, int] = \
    {
        "Up":                      InputItem.ItemUp,
        "Down":                    InputItem.ItemDown,
        "Left":                    InputItem.ItemLeft,
        "Right":                   InputItem.ItemRight,
        "Steering":                InputItem.ItemJoy1Left, # X AXIS by default from Joy1
        "Accelerate":              InputItem.ItemR2, # R2 by default else Y AXIS inversed from Joy1 will be used
        "Brake":                   InputItem.ItemL2, # L2 by default else Y AXIS from Joy1 will be used
        "Button1":                 InputItem.ItemA,  # zoom in 
        "Button2":                 InputItem.ItemL1, # gears shift (decrease)
        "Button3":                 InputItem.ItemR1, # gears shift (increase)
        "Button4":                 InputItem.ItemX,  # zoom out
        "Start":                   InputItem.ItemStart,
        "Coin":                    InputItem.ItemSelect,
        "Service":                 -1, #keyboard key F1 by deault
        "Test":                    -1, #keyboard key F2 by deault
        "ShowRenderStats":         -1, #keyboard key F8 by deault
        "ToggleTitlemapRender":    -1, #keyboard key F7 by deault
        "CyleTitlemapUpdateRate":  -1, #keyboard key F6 by deault
    }

    #################### Over Rev (and clones)  ###############################
    OverRevInputs: Dict[str, int] = \
    {
        "Up":                      InputItem.ItemUp,
        "Down":                    InputItem.ItemDown,
        "Left":                    InputItem.ItemLeft,
        "Right":                   InputItem.ItemRight,
        "Steering":                InputItem.ItemJoy1Left, # X AXIS by default from Joy1
        "AccelerateINV":           InputItem.ItemR2, # R2 by default else Y AXIS inversed from Joy1 will be used but to invert for this game
        "BrakeINV":                InputItem.ItemL2, # L2 by default else Y AXIS from Joy1 will be used but to invert for this game
        "Button1":                 InputItem.ItemX,  # view change
        "Button2":                 InputItem.ItemL1, # gears shift (decrease)
        "Button3":                 InputItem.ItemR1, # gears shift (increase)
        "Button4":                 InputItem.ItemA,  # not used
        "Start":                   InputItem.ItemStart,
        "Coin":                    InputItem.ItemSelect,
        "Service":                 -1, #keyboard key F1 by deault
        "Test":                    -1, #keyboard key F2 by deault
        "ShowRenderStats":         -1, #keyboard key F8 by deault
        "ToggleTitlemapRender":    -1, #keyboard key F7 by deault
        "CyleTitlemapUpdateRate":  -1, #keyboard key F6 by deault
    }
    
    #################### Motor Raid  ###############################
    ManxTTInputs: Dict[str, int] = \
    {
        "Up":                      InputItem.ItemUp,
        "Down":                    InputItem.ItemDown,
        "Left":                    InputItem.ItemLeft,
        "Right":                   InputItem.ItemRight,
        "Steering":                InputItem.ItemJoy1Left, # X AXIS by default from Joy1
        "Accelerate":              InputItem.ItemR2, # R2 by default else dpad up will be used
        "Brake":                   InputItem.ItemL2, # L2 by default else dpad down will be used
        "ShiftUp":                 InputItem.ItemR1,
        "ShiftDown":               InputItem.ItemL1,        
        "Start":                   InputItem.ItemStart,
        "Coin1":                   InputItem.ItemSelect,
        "Coin2":                   -1, #keyboard key 6 by deault
        "Service":                 -1, #keyboard key F1 by deault
        "Test":                    -1, #keyboard key F2 by deault
        "ShowRenderStats":         -1, #keyboard key F8 by deault
        "ToggleTitlemapRender":    -1, #keyboard key F7 by deault
        "CyleTitlemapUpdateRate":  -1, #keyboard key F6 by deault
    }

    #################### Motor Raid  ###############################
    MotorRaidInputs: Dict[str, int] = \
    {
        "Up":                      InputItem.ItemUp,
        "Down":                    InputItem.ItemDown,
        "Left":                    InputItem.ItemLeft,
        "Right":                   InputItem.ItemRight,
        "Bank":                    InputItem.ItemJoy1Left, # X AXIS by default from Joy1
        "Accelerate":              InputItem.ItemR2, # R2 by default else Y AXIS inversed from Joy1 will be used
        "Brake":                   InputItem.ItemL2, # L2 by default else Y AXIS from Joy1 will be used
        "Kick":                    InputItem.ItemA,
        "Punch":                   InputItem.ItemB,        
        "Start":                   InputItem.ItemStart,
        "Coin1":                   InputItem.ItemSelect,
        "Coin2":                   -1, #keyboard key 6 by deault
        "Service":                 -1, #keyboard key F1 by deault
        "Test":                    -1, #keyboard key F2 by deault
        "ShowRenderStats":         -1, #keyboard key F8 by deault
        "ToggleTitlemapRender":    -1, #keyboard key F7 by deault
        "CyleTitlemapUpdateRate":  -1, #keyboard key F6 by deault
    }

    #################### Sega Rally Championship (and clones)  ###############################
    SegaRallyInputs: Dict[str, int] = \
    {
        "Up":                      InputItem.ItemUp,
        "Down":                    InputItem.ItemDown,
        "Left":                    InputItem.ItemLeft,
        "Right":                   InputItem.ItemRight,
        "Steering":                InputItem.ItemJoy1Left, # X AXIS by default from Joy1
        "Accelerate":              InputItem.ItemR2, # R2 by default else Y AXIS inversed from Joy1 will be used
        "Brake":                   InputItem.ItemL2, # L2 by default else Y AXIS from Joy1 will be used
        "Shift1":                  InputItem.ItemJoy2Up, #keyboard key Q by deault
        "Shift2":                  InputItem.ItemJoy2Left, #keyboard key S by deault
        "Shift3":                  InputItem.ItemJoy2Down, #keyboard key D by deault
        "Shift4":                  InputItem.ItemJoy2Right, #keyboard key F by deault
        "ShiftNeutral":            InputItem.ItemR3, #keyboard key G by deault
        "VR":                      InputItem.ItemY,
        "Start":                   InputItem.ItemStart,
        "Coin1":                   InputItem.ItemSelect,
        "Coin2":                   -1, #keyboard key 6 by deault
        "Service":                 -1, #keyboard key F1 by deault
        "Test":                    -1, #keyboard key F2 by deault
        "ShowRenderStats":         -1, #keyboard key F8 by deault
        "ToggleTitlemapRender":    -1, #keyboard key F7 by deault
        "CyleTitlemapUpdateRate":  -1, #keyboard key F6 by deault
    }

    #################### Sega Ski Super G ###############################
    SegaSkiSuperGInputs: Dict[str, int] = \
    {
        "SwingRight":              InputItem.ItemRight,
        "SwingLeft":               InputItem.ItemLeft,
        "InclLeft":                InputItem.ItemL1,
        "InclRight":               InputItem.ItemR1,
        "Swing":                   InputItem.ItemJoy1Left, # X AXIS by default from Joy1
        "IncliningINV":            InputItem.ItemJoy2Left, # X AXIS by default from Joy2
        "ZoomOut":                 InputItem.ItemDown,
        "ZoomIn":                  InputItem.ItemUp,
        "Select1":                 InputItem.ItemY, #blue
        "Select2":                 InputItem.ItemB, #red
        "Select3":                 InputItem.ItemA, #green
        "Coin":                    InputItem.ItemSelect,
        "Service":                 -1, #keyboard key F1 by deault
        "Test":                    -1, #keyboard key F2 by deault
        "ShowRenderStats":         -1, #keyboard key F8 by deault
        "ToggleTitlemapRender":    -1, #keyboard key F7 by deault
        "CyleTitlemapUpdateRate":  -1, #keyboard key F6 by deault
    }

    #################### Sega Water Ski ###############################
    # owner's manuam: https://arcarc.xmission.com/PDF_Arcade_Manuals_and_Schematics/Water%20Ski.pdf
    SegaWaterSkiInputs: Dict[str, int] = \
    {
        "SelectUp":                InputItem.ItemUp,
        "SelectDown":              InputItem.ItemDown,
        "SideLeft":                InputItem.ItemLeft,
        "SideRight":               InputItem.ItemRight,
        "SlideINV":                InputItem.ItemJoy1Left, # X AXIS by default from Joy1
        "Set":                     InputItem.ItemB,
        "PitchLeft":               InputItem.ItemL1,
        "PitchRight":              InputItem.ItemR1,
        "Start":                   InputItem.ItemStart,
        "Coin":                    InputItem.ItemSelect,
        "Service":                 -1, #keyboard key F1 by deault
        "Test":                    -1, #keyboard key F2 by deault
        "ShowRenderStats":         -1, #keyboard key F8 by deault
        "ToggleTitlemapRender":    -1, #keyboard key F7 by deault
        "CyleTitlemapUpdateRate":  -1, #keyboard key F6 by deault
    }

    #################### SkyTarget  ###############################
    SkyTargetInputs: Dict[str, int] = \
    {
        "Up":                      InputItem.ItemUp,
        "Down":                    InputItem.ItemDown,
        "Left":                    InputItem.ItemLeft,
        "Right":                   InputItem.ItemRight,
        "Horizontal":              InputItem.ItemJoy1Left, # X AXIS by default from Joy1
        "Vertical":                InputItem.ItemJoy1Up, # Y AXIS by default from Joy1
        "MachineGun":              InputItem.ItemB,
        "Missile":                 InputItem.ItemA,
        "ViewChange":              InputItem.ItemX,
        "Start":                   InputItem.ItemStart,
        "Coin1":                   InputItem.ItemSelect,
        "Coin2":                   -1, #keyboard key 6 by deault
        "Service":                 -1, #keyboard key F1 by deault
        "Test":                    -1, #keyboard key F2 by deault
        "ShowRenderStats":         -1, #keyboard key F8 by deault
        "ToggleTitlemapRender":    -1, #keyboard key F7 by deault
        "CyleTitlemapUpdateRate":  -1, #keyboard key F6 by deault
    }
    
    #################### Super GT 24h ###############################
    SuperGTInputs: Dict[str, int] = \
    {
        "Up":                      InputItem.ItemUp,
        "Down":                    InputItem.ItemDown,
        "Left":                    InputItem.ItemLeft,
        "Right":                   InputItem.ItemRight,
        "Steering":                InputItem.ItemJoy1Left, # X AXIS by default from Joy1
        "AccelerateINV":           InputItem.ItemR2, # R2 by default else Y AXIS inversed from Joy1 will be used but to invert for this game
        "BrakeINV":                InputItem.ItemL2, # L2 by default else Y AXIS from Joy1 will be used but to invert for this game
        "Button1":                 InputItem.ItemA,  # zoom in 
        "Button2":                 InputItem.ItemL1, # gears shift (decrease)
        "Button3":                 InputItem.ItemR1, # gears shift (increase)
        "Button4":                 InputItem.ItemX,  # zoom out
        "Start":                   InputItem.ItemStart,
        "Coin":                    InputItem.ItemSelect,
        "Service":                 -1, #keyboard key F1 by deault
        "Test":                    -1, #keyboard key F2 by deault
        "ShowRenderStats":         -1, #keyboard key F8 by deault
        "ToggleTitlemapRender":    -1, #keyboard key F7 by deault
        "CyleTitlemapUpdateRate":  -1, #keyboard key F6 by deault
    }

    #################### Top Skater (and clones) ###############################
    # owner's manual : https://segaretro.org/File:TopSkater_Model2_US_Manual.pdf
    TopSkaterInputs: Dict[str, int] = \
    {
        "Up":                      InputItem.ItemUp,
        "Down":                    InputItem.ItemDown,
        "Left":                    InputItem.ItemLeft,
        "Right":                   InputItem.ItemRight,
        "CurvingINV":              InputItem.ItemJoy1Left, # X AXIS by default from Joy1
        "SlideTS":                 InputItem.ItemJoy2Left, # X AXIS by default from Joy2
        "JumpFront":               InputItem.ItemB,
        "JumpTail":                InputItem.ItemA,
        "SelectLeft":              InputItem.ItemLeft,
        "SelectRight":             InputItem.ItemRight,
        "Start":                   InputItem.ItemStart,
        "Coin":                    InputItem.ItemSelect,
        "Service":                 -1, #keyboard key F1 by deault
        "Test":                    -1, #keyboard key F2 by deault
        "ShowRenderStats":         -1, #keyboard key F8 by deault
        "ToggleTitlemapRender":    -1, #keyboard key F7 by deault
        "CyleTitlemapUpdateRate":  -1, #keyboard key F6 by deault
    }

    #################### Pilot Kids  ###############################
    PilotKidsInputs: Dict[str, int] = \
    {
        "Up":                      InputItem.ItemUp,
        "Down":                    InputItem.ItemDown,
        "Left":                    InputItem.ItemLeft,
        "Right":                   InputItem.ItemRight,
        "Button1":                 InputItem.ItemB,
        "Button2":                 InputItem.ItemA,
        "Button3":                 InputItem.ItemY,
        "Button4":                 InputItem.ItemX,
        "Start":                   InputItem.ItemStart,
        "Coin":                    InputItem.ItemSelect,
        "Player2Up":               InputItem.ItemUp,
        "Player2Down":             InputItem.ItemDown,
        "Player2Left":             InputItem.ItemLeft,
        "Player2Right":            InputItem.ItemRight,
        "Player2Button1":          InputItem.ItemB,
        "Player2Button2":          InputItem.ItemA,
        "Player2Button3":          InputItem.ItemY,
        "Player2Button4":          InputItem.ItemX,
        "Player2Start":            InputItem.ItemStart,
        "Player2Coin":             InputItem.ItemSelect,
        "Service":                 -1, #keyboard key F1 by deault
        "Test":                    -1, #keyboard key F2 by deault
        "ShowRenderStats":         -1, #keyboard key F8 by deault
        "ToggleTitlemapRender":    -1, #keyboard key F7 by deault
        "CyleTitlemapUpdateRate":  -1, #keyboard key F6 by deault
    }
    
    #################### Virtua Fighter 2 / Dead Or Alive / Dynamite Cop (and clones)  ###############################
    VersusFighterInputs: Dict[str, int] = \
    {
        "Up":                      InputItem.ItemUp,
        "Down":                    InputItem.ItemDown,
        "Left":                    InputItem.ItemLeft,
        "Right":                   InputItem.ItemRight,
        "Button1":                 InputItem.ItemX,
        "Button2":                 InputItem.ItemB,
        "Button3":                 InputItem.ItemA,
        "Button4":                 InputItem.ItemY,
        "Start":                   InputItem.ItemStart,
        "Coin":                    InputItem.ItemSelect,
        "Player2Up":               InputItem.ItemUp,
        "Player2Down":             InputItem.ItemDown,
        "Player2Left":             InputItem.ItemLeft,
        "Player2Right":            InputItem.ItemRight,
        "Player2Button1":          InputItem.ItemX,
        "Player2Button2":          InputItem.ItemB,
        "Player2Button3":          InputItem.ItemA,
        "Player2Button4":          InputItem.ItemY,
        "Player2Start":            InputItem.ItemStart,
        "Player2Coin":             InputItem.ItemSelect,
        "Service":                 -1, #keyboard key F1 by deault
        "Test":                    -1, #keyboard key F2 by deault
        "ShowRenderStats":         -1, #keyboard key F8 by deault
        "ToggleTitlemapRender":    -1, #keyboard key F7 by deault
        "CyleTitlemapUpdateRate":  -1, #keyboard key F6 by deault
    }

    #################### Zero Gunner (and clones)  ###############################
    ZeroGunnerInputs: Dict[str, int] = \
    {
        "Up":                      InputItem.ItemUp,
        "Down":                    InputItem.ItemDown,
        "Left":                    InputItem.ItemLeft,
        "Right":                   InputItem.ItemRight,
        "Button1":                 InputItem.ItemA,
        "Button2":                 InputItem.ItemB,
        "Button3":                 InputItem.ItemX,
        "Button4":                 InputItem.ItemY,
        "Start":                   InputItem.ItemStart,
        "Coin":                    InputItem.ItemSelect,
        "Player2Up":               InputItem.ItemUp,
        "Player2Down":             InputItem.ItemDown,
        "Player2Left":             InputItem.ItemLeft,
        "Player2Right":            InputItem.ItemRight,
        "Player2Button1":          InputItem.ItemX,
        "Player2Button2":          InputItem.ItemB,
        "Player2Button3":          InputItem.ItemA,
        "Player2Button4":          InputItem.ItemY,
        "Player2Start":            InputItem.ItemStart,
        "Player2Coin":             InputItem.ItemSelect,
        "Service":                 -1, #keyboard key F1 by deault
        "Test":                    -1, #keyboard key F2 by deault
        "ShowRenderStats":         -1, #keyboard key F8 by deault
        "ToggleTitlemapRender":    -1, #keyboard key F7 by deault
        "CyleTitlemapUpdateRate":  -1, #keyboard key F6 by deault
    }
    
    #################### Virtual On Cybertroopers (and clones)  ###############################
    VirtualOnCybertroopersInputs: Dict[str, int] = \
    {
        "LeftStickUp":             InputItem.ItemJoy1Up,
        "LeftStickDown":           InputItem.ItemJoy1Down,
        "LeftStickLeft":           InputItem.ItemJoy1Left,
        "LeftStickRight":          InputItem.ItemJoy1Right,
        "LeftStickTrigger":        InputItem.ItemL2,
        "LeftStickButton":         InputItem.ItemL1,
        "Player1Start":            InputItem.ItemStart,
        "Player1Coin":             InputItem.ItemSelect,
        "RightStickUp":            InputItem.ItemJoy2Up,
        "RightStickDown":          InputItem.ItemJoy2Down,
        "RightStickLeft":          InputItem.ItemJoy2Left,
        "RightStickRight":         InputItem.ItemJoy2Right,
        "RightStickTrigger":       InputItem.ItemR2,
        "RightStickButton":        InputItem.ItemR1,
        "Service":                 -1, #keyboard key F1 by deault
        "Test":                    -1, #keyboard key F2 by deault
        "ShowRenderStats":         -1, #keyboard key F8 by deault
        "ToggleTitlemapRender":    -1, #keyboard key F7 by deault
        "CyleTitlemapUpdateRate":  -1, #keyboard key F6 by deault
    }

    #################### Wave Runner  ###############################
    # owner's manual : https://manualzz.com/doc/928456/sega-waverunner-owner-s-manual
    WaveRunnerInputs: Dict[str, int] = \
    {
        "RollLeft":                InputItem.ItemL1, # used only if Joy2 axis not available to roll
        "RollRight":               InputItem.ItemR1, # used only if Joy2 axis not available to roll
        "HandleLeft":              InputItem.ItemLeft, # used only if Joy1 axis not available to handle
        "HandleRight":             InputItem.ItemRight, # used only if Joy1 axis not available to handle
        "Throttle":                InputItem.ItemUp, # used only if R2 axis not available to accelerate
        "Handle":                  InputItem.ItemJoy1Left, # X AXIS by default from Joy1 else we will use Dpad left and right
        "Roll":                    InputItem.ItemJoy2Left, # X AXIS by default from Joy2 else we will use L1/R1
        "ThrottleAxis":            InputItem.ItemR2, # use R2 axis by default to accelerate else we will use Dpad Up
        "View":                    InputItem.ItemX,
        "Start":                   InputItem.ItemStart,
        "Coin":                    InputItem.ItemSelect,
        "Service":                 -1, #keyboard key F1 by deault
        "Test":                    -1, #keyboard key F2 by deault
        "ShowRenderStats":         -1, #keyboard key F8 by deault
        "ToggleTitlemapRender":    -1, #keyboard key F7 by deault
        "CyleTitlemapUpdateRate":  -1, #keyboard key F6 by deault
    }

    #################### Lightguns games (and clones)  ###############################
    LightgunsInputs: Dict[str, int] = \
    {
        "Up":                      InputItem.ItemUp,
        "Down":                    InputItem.ItemDown,
        "Left":                    InputItem.ItemLeft,
        "Right":                   InputItem.ItemRight,
        "Button1":                 InputItem.ItemA,
        "Button2":                 InputItem.ItemB,
        "Button3":                 InputItem.ItemX,
        "Button4":                 InputItem.ItemY,
        "Start":                   InputItem.ItemStart,
        "Coin":                    InputItem.ItemSelect,
        "Player2Up":               InputItem.ItemUp,
        "Player2Down":             InputItem.ItemDown,
        "Player2Left":             InputItem.ItemLeft,
        "Player2Right":            InputItem.ItemRight,
        "Player2Button1":          InputItem.ItemA,
        "Player2Button2":          InputItem.ItemB,
        "Player2Button3":          InputItem.ItemX,
        "Player2Button4":          InputItem.ItemY,
        "Player2Start":            InputItem.ItemStart,
        "Player2Coin":             InputItem.ItemSelect,
        "Service":                 -1, #keyboard key F1 by deault
        "Test":                    -1, #keyboard key F2 by deault
        "ShowRenderStats":         -1, #keyboard key F8 by deault
        "ToggleTitlemapRender":    -1, #keyboard key F7 by deault
        "CyleTitlemapUpdateRate":  -1, #keyboard key F6 by deault
    }

    #################### Default raw value if not used or not found from controller inputs ###############################
    DefaultInputs: Dict[str, bytes] = \
    {
        "Up":                      b'\xc8\x00\x00\x00',
        "Throttle":                b'\xc8\x00\x00\x00',
        "Down":                    b'\xd0\x00\x00\x00',
        "Left":                    b'\xcb\x00\x00\x00',
        "Right":                   b'\xcd\x00\x00\x00',
        "HandleLeft":              b'\xcb\x00\x00\x00',
        "HandleRight":             b'\xcd\x00\x00\x00',
        "LeftStickUp":             b'\xc8\x00\x00\x00',
        "LeftStickDown":           b'\xd0\x00\x00\x00',
        "LeftStickLeft":           b'\xcb\x00\x00\x00',
        "LeftStickRight":          b'\xcd\x00\x00\x00',
        "RightStickUp":            b'\xc8\x00\x00\x00',
        "RightStickDown":          b'\xd0\x00\x00\x00',
        "RightStickLeft":          b'\xcb\x00\x00\x00',
        "RightStickRight":         b'\xcd\x00\x00\x00',
        "SelectUp":                b'\xc8\x00\x00\x00',
        "SelectDown":              b'\xd0\x00\x00\x00',
        "SideLeft":                b'\xcb\x00\x00\x00',
        "SideRight":               b'\xcd\x00\x00\x00',
        "SwingLeft":               b'\xc8\x00\x00\x00',
        "SwingRight":              b'\xd0\x00\x00\x00',
        "InclLeft":                b'\xcb\x00\x00\x00',
        "InclRight":               b'\xcd\x00\x00\x00',
        "Player2Up":               b'\xc8\x00\x00\x00',
        "Player2Down":             b'\xd0\x00\x00\x00',
        "Player2Left":             b'\xcb\x00\x00\x00',
        "Player2Right":            b'\xcd\x00\x00\x00',
        "SteerLeft":               b'\xcb\x00\x00\x00',
        "SteerRight":              b'\xcd\x00\x00\x00',
        "TurretUp":                b'\x1e\x00\x00\x00',
        "TurretDown":              b'\x10\x00\x00\x00',
        "Accel+":                  b'\xc8\x00\x00\x00',
        "Accel-":                  b'\xd0\x00\x00\x00',
        "Cannon":                  b'\xd0\x00\x00\x00',
        "RollLeft":                b'\xc8\x00\x00\x00',
        "RollRight":               b'\xd0\x00\x00\x00',
        "Horizontal":              b'\x00\x00\x00\x00',
        "Slide":                   b'\x00\x00\x00\x00',
        "SlideINV":                b'\x00\x00\x00\x00',
        "SlideTS":                 b'\x00\x00\x00\x00',
        "Curving":                 b'\x00\x00\x00\x00',
        "CurvingINV":              b'\x00\x00\x00\x00',
        "Inclining":               b'\x00\x00\x00\x00',
        "IncliningINV":            b'\x00\x00\x00\x00',
        "Bat":                     b'\x00\x00\x00\x00',
        "Player2Bat":              b'\x00\x00\x00\x00',
        "Bank":                    b'\x00\x00\x00\x00',
        "Steering":                b'\x00\x00\x00\x00',
        "Accelerate":              b'\x00\x00\x00\x00',
        "AccelerateINV":           b'\x00\x00\x00\x00',
        "Turret":                  b'\x00\x00\x00\x00',
        "Vertical":                b'\x00\x00\x00\x00',
        "VerticalINV":             b'\x00\x00\x00\x00',
        "Swing":                   b'\x00\x00\x00\x00',
        "Brake":                   b'\x00\x00\x00\x00',
        "BrakeINV":                b'\x00\x00\x00\x00',
        "Handle":                  b'\x00\x00\x00\x00',
        "Roll":                    b'\x00\x00\x00\x00',
        "ThrottleAxis":            b'\x00\x00\x00\x00',
        "Accelerator":             b'\x00\x00\x00\x00',
        "ShiftUp":                 b'\x1e\x00\x00\x00',
        "ShiftDown":               b'\x10\x00\x00\x00',
        "Shift":                   b'\x2c\x00\x00\x00',
        "Shift1":                  b'\x1e\x00\x00\x00',
        "Shift2":                  b'\x1f\x00\x00\x00',
        "Shift3":                  b'\x20\x00\x00\x00',
        "Shift4":                  b'\x21\x00\x00\x00',
        "MachineGun":              b'\x12\x00\x00\x00',
        "Missile":                 b'\x2d\x00\x00\x00',
        "ViewChange":              b'\x2e\x00\x00\x00',
        "View":                    b'\x2e\x00\x00\x00',
        "ZoomOut":                 b'\x1e\x00\x00\x00',
        "ZoomIn":                  b'\x1f\x00\x00\x00',
        "Select1":                 b'\x2c\x00\x00\x00',
        "Select2":                 b'\x2d\x00\x00\x00',
        "Select3":                 b'\x2e\x00\x00\x00',
        "Set":                     b'\x2c\x00\x00\x00',
        "PitchLeft":               b'\x2d\x00\x00\x00',
        "PitchRight":              b'\x2e\x00\x00\x00',
        "Button1":                 b'\x2c\x00\x00\x00',
        "Button2":                 b'\x2d\x00\x00\x00',
        "Button3":                 b'\x2e\x00\x00\x00',
        "Button4":                 b'\x2f\x00\x00\x00',
        "LeftStickTrigger":        b'\x2c\x00\x00\x00',
        "LeftStickButton":         b'\x2d\x00\x00\x00',
        "RightStickTrigger":       b'\x2e\x00\x00\x00',
        "RightStickButton":        b'\x2f\x00\x00\x00',
        "JumpFront":               b'\x2c\x00\x00\x00',
        "JumpTail":                b'\x2d\x00\x00\x00',
        "SelectLeft":              b'\x2e\x00\x00\x00',
        "SelectRight":             b'\x2f\x00\x00\x00',
        "Bat+":                    b'\x2e\x00\x00\x00',
        "Bat-":                    b'\x2f\x00\x00\x00',
        "Player2Button1":          b'\x2c\x00\x00\x00',
        "Player2Button2":          b'\x2d\x00\x00\x00',
        "Player2Button3":          b'\x2e\x00\x00\x00',
        "Player2Button4":          b'\x2f\x00\x00\x00',
        "Player2Bat+":             b'\x2e\x00\x00\x00',
        "Player2Bat-":             b'\x2f\x00\x00\x00',
        "ShiftNeutral":            b'\x22\x00\x00\x00',
        "VR":                      b'\x2c\x00\x00\x00',
        "VR1":                     b'\x2c\x00\x00\x00',
        "VR2":                     b'\x2d\x00\x00\x00',
        "VR3":                     b'\x2e\x00\x00\x00',
        "VR4":                     b'\x2f\x00\x00\x00',
        "Kick":                    b'\x2c\x00\x00\x00',
        "Punch":                   b'\x2d\x00\x00\x00',
        "Start":                   b'\x02\x00\x00\x00',
        "Player1Start":            b'\x02\x00\x00\x00',
        "Player2Start":            b'\x03\x00\x00\x00',
        "Coin":                    b'\x06\x00\x00\x00',
        "Player1Coin":             b'\x06\x00\x00\x00',
        "Player2Coin":             b'\x07\x00\x00\x00',
        "Coin1":                   b'\x06\x00\x00\x00',
        "Coin2":                   b'\x07\x00\x00\x00',
        "Service":                 b'\x3b\x00\x00\x00',
        "Test":                    b'\x3c\x00\x00\x00',
        "ShowRenderStats":         b'\x42\x00\x00\x00',
        "ToggleTitlemapRender":    b'\x41\x00\x00\x00',
        "CyleTitlemapUpdateRate":  b'\x40\x00\x00\x00',
    }

    #################### "Analog" inputs ###############################
    AnalogInputs: Dict[str, int] = \
    {
        #input name                #axis position in configuration
        "Horizontal":              1,
        "Slide":                   1,
        "SlideINV":                1,
        "Curving":                 1,
        "CurvingINV":              1,
        "Inclining":               1,
        "IncliningINV":            1,
        "Bat":                     1,
        "Handle":                  1,
        "Player2Bat":              2,
        "Bank":                    1,
        "Steering":                1,
        "Vertical":                2,
        "VerticalINV":             2,
        "Accelerate":              2,
        "AccelerateINV":           2,
        "Turret":                  2,
        "SlideTS":                 2,
        "Roll":                    2,
        "Swing":                   2,
        "Brake":                   3,
        "BrakeINV":                3,
        "Accelerator":             3,
        "ThrottleAxis":            3,
    }

    #################### ROM links to specific INPUTS mapping defined #######################################
    ROM_INPUTS: Dict[str, Dict] = \
    {
        "bel": LightgunsInputs,
        "dayton93":   DaytonaInputs,
        "daytona":   DaytonaInputs,
        "daytonagtx":   DaytonaInputs,
        "daytonam":   DaytonaInputs,
        "daytonas":   DaytonaInputs,
        "daytonase":   DaytonaInputs,
        "daytonat":   DaytonaInputs,
        "daytonata":   DaytonaInputs,
        "desert": DesertTankInputs,
        "doa": VersusFighterInputs,
        "doaa": VersusFighterInputs,
        "dynabb97": DynamiteBaseballInputs,
        "dynamcop": VersusFighterInputs,
        "dyndek2b": VersusFighterInputs,
        "dyndeka2": VersusFighterInputs,
        "dynmcopb": VersusFighterInputs,
        "dynmcopc": VersusFighterInputs,
        "fvipers": VersusFighterInputs,
        "gunblade": LightgunsInputs,
        "hotd": LightgunsInputs,
        "indy500": IndianapolisInputs,
        "indy500d": IndianapolisInputs,
        "indy500to": IndianapolisInputs,
        "lastbrnx": VersusFighterInputs,
        "lastbrnxj": VersusFighterInputs,
        "lastbrnxu": VersusFighterInputs,
        "manxtt": ManxTTInputs,
        "manxttc": ManxTTInputs,
        "motoraid": MotorRaidInputs,
        "overrev" : OverRevInputs,
        "overrevb" : OverRevInputs,
        "pltkids": PilotKidsInputs,
        "pltkidsa": PilotKidsInputs,
        "rchase2": VersusFighterInputs,
        "schamp": VersusFighterInputs,
        "segawski": SegaWaterSkiInputs,
        "sfight": VersusFighterInputs,
        "sgt24h": SuperGTInputs,
        "skisuprg": SegaSkiSuperGInputs,
        "skytargt": SkyTargetInputs,
        "srallyc": SegaRallyInputs,
        "srallycb": SegaRallyInputs,
        "srallyp": SegaRallyInputs,
        "stcc": IndianapolisInputs,
        "stcca": IndianapolisInputs,
        "stccb": IndianapolisInputs,
        "topskatr": TopSkaterInputs,
        "topskatrj": TopSkaterInputs,
        "topskatru": TopSkaterInputs,
        "vcop": LightgunsInputs,
        "vcop2": LightgunsInputs,
        "vcopa": LightgunsInputs,
        "vf2": VersusFighterInputs,
        "vf2a": VersusFighterInputs,
        "vf2b": VersusFighterInputs,
        "vf2o": VersusFighterInputs,
        "von": VirtualOnCybertroopersInputs,
        "vonj": VirtualOnCybertroopersInputs,
        "vstriker": VersusFighterInputs,
        "vstrikro": VersusFighterInputs,
        "waverunr": WaveRunnerInputs,
        "zerogun": ZeroGunnerInputs,
        "zeroguna": ZeroGunnerInputs,
        "zerogunaj": ZeroGunnerInputs,
        "zerogunj": ZeroGunnerInputs,
    }

    ################################################# XInputs codes ##########################################
    XInputs: Dict[int,bytes] = \
    {
        InputItem.ItemUp:          '02', # from D-PAD
        InputItem.ItemDown:        '03', # from D-PAD
        InputItem.ItemLeft:        '00', # from D-PAD
        InputItem.ItemRight:       '01', # from D-PAD
        InputItem.ItemA:           '30',
        InputItem.ItemB:           '40',
        InputItem.ItemX:           '10',
        InputItem.ItemY:           '20',
        InputItem.ItemStart:       'b0',
        InputItem.ItemSelect:      'c0',
        InputItem.ItemJoy1Left     * AS_AXIS:       '02', # X AXIS by default from Joy1
        InputItem.ItemJoy1Right    * AS_AXIS:       '02',
        InputItem.ItemJoy1Up       * AS_AXIS:       '03', # Y AXIS by default from Joy1
        InputItem.ItemJoy1Down     * AS_AXIS:       '03',
        InputItem.ItemJoy1Left     * AS_BUTTON:     '04',
        InputItem.ItemJoy1Right    * AS_BUTTON:     '05',
        InputItem.ItemJoy1Up       * AS_BUTTON:     '06',
        InputItem.ItemJoy1Down     * AS_BUTTON:     '07',
        InputItem.ItemJoy2Left     * AS_AXIS:       '04', # X AXIS by default from Joy2
        InputItem.ItemJoy2Right    * AS_AXIS:       '04',
        InputItem.ItemJoy2Up       * AS_AXIS:       '05', # Y AXIS by default from Joy2
        InputItem.ItemJoy2Down     * AS_AXIS:       '05',
        InputItem.ItemJoy2Left     * AS_BUTTON:       '04', # X AXIS by default from Joy2
        InputItem.ItemJoy2Right    * AS_BUTTON:       '04',
        InputItem.ItemJoy2Up       * AS_BUTTON:       '05', # Y AXIS by default from Joy2
        InputItem.ItemJoy2Down     * AS_BUTTON:       '05',
        InputItem.ItemL1           * AS_BUTTON:     '50',
        InputItem.ItemR1           * AS_BUTTON:     '60',
        InputItem.ItemL2           * AS_BUTTON:     '70', #as button
        InputItem.ItemR2           * AS_BUTTON:     '80', #as button
        InputItem.ItemL2           * AS_AXIS:       '06', #as "axis" trigger left
        InputItem.ItemR2           * AS_AXIS:       '07', #as "axis" trigger right
        InputItem.ItemL3           * AS_BUTTON:     '90',
        InputItem.ItemR3           * AS_BUTTON:     'a0',
    }

    #init class object
    def __init__(self, system: Emulator, recalboxOptions: keyValueSettings, controllers: ControllerPerPlayer, rom: str, wine: wine):
        
        #init wine instance
        self.wine = wine
        #coef multiplicator to dicriminate usage as axis or button
        self.AS_AXIS = 100
        self.AS_BUTTON = 1
        
        self.system: Emulator = system
        self.recalboxOptions: keyValueSettings = recalboxOptions
        self.playersControllers: ControllerPerPlayer = controllers
        self.rom = rom
        self.AxisTableP1 = [] 
        self.AxisTableP2 = [] 
    
        if (self.recalboxOptions.getInt("model2emu.xinput", 0) == 1): 
            #For XInput
            self.controllerTypeEventToRemove = ""
            self.controllerTypeEventToKeep = ""
            self.controllerToOverride = False
        elif ("-8." in self.wine.wineversion):
            #For wine 8.X
            self.controllerTypeEventToRemove = ""
            self.controllerTypeEventToKeep = ""
            self.controllerToOverride = True
        else:
            #For DInput, to manage doublons of inputs (as js and as event) especially for wine 5.X
            self.controllerTypeEventToRemove = "event"
            self.controllerTypeEventToKeep = "js" #finally we keep js, because some bluetooth controllers doesn't have event :-(
            self.controllerToOverride = False
        
        #init lightgun config
        self.lightgunConfig = model2emuLightGun(system, recalboxOptions, rom)

    #add deadzone on Axis of any controller 
    def setAxisDeadzone(self):
        if self.recalboxOptions.getInt("model2emu.xinput", 0) == 0 :
            popup.Message("Set Dinput deadzone, please wait...", 5)
            #deadzone is set at 30% - set to 3000 - possible value: 0 to 10000
            self.wine.addStringValueInRegistry("HKEY_CURRENT_USER\\Software\\Wine\\DirectInput",
                                          "DefaultDeadZone",
                                          "3000")
            #if (not "-8." in self.wine.wineversion) :
            #to activate the use of evdev to discover and communicate with HID devices
            self.wine.addDwordValueInRegistry("HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Services\\WineBus",
                                 "DisableInput",
                                 "0")
        else:
            popup.Message("Set Xinput deadzone, please wait...", 5)
            if (not "-8." in self.wine.wineversion) :
                #to activate the use of evdev to discover and communicate with HID devices
                self.wine.addDwordValueInRegistry("HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Services\\WineBus",
                                     "DisableInput",
                                     "0")
            for index in self.playersControllers:
                #Log("Player {}".format(str(index)))
                controller = self.playersControllers[index]
                # we only care about player 1 and 2 for the moment
                if controller.PlayerIndex <= 2:
                    #force to 10000 as 30% for lot of controllers (max at 32767 usually) and on axis 0 in case of Xinput
                    os.system("evdev-joystick --evdev " + controller.DevicePath.rstrip() + " --axis 0 --deadzone 10000")
 
    #prepare Axist table for player 1/2 to avoid several call 
    def prepareAxisTable(self, pad_player: Controller): 
        #command to know /dev/input/js? from /dev/input/event? used
        #cat /proc/bus/input/devices | grep -ni event16 | awk -v FS='(js|)' '{print $2}'
        cmd = "cat /proc/bus/input/devices | grep -ni " + pad_player.DevicePath.replace("/dev/input/", "").rstrip() + " | awk -v FS='(js|)' '{print $2}'"
        Log(f"command: {str(cmd)}")
        p = subprocess.Popen(cmd, shell=True, 
            stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
        p.wait()
        output = p.stdout 
        jsIndex = ""
        for line in output:
            jsIndex = line.rstrip().split(' ')[0]
        #command to know axes available 
        # timeout 1 jstest /dev/input/js0 | grep -i joystick
        # Joystick (Xbox 360 Wireless Receiver) has 8 axes (X, Y, Z, Rx, Ry, Rz, Hat0X, Hat0Y) 
        cmd = "timeout 1 jstest /dev/input/js"+ jsIndex + " | grep -i joystick | awk -F'[()]' '{print $4}'"
        Log(f"command: {str(cmd)}")
        p = subprocess.Popen(cmd, shell=True, 
            stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
        p.wait()
        output = p.stdout 
        Log("pad_player.PlayerIndex : {}".format(str(pad_player.PlayerIndex)));
        controllerShortName = pad_player.DeviceName.split(' - ')[0]
        if pad_player.PlayerIndex == 1:
            popup.Message("Set " + controllerShortName + " for player " + str(pad_player.PlayerIndex) + ", please wait...", 5)
            for line in output: 
                Log("AxisTableP1 content : '" + line.rstrip() + "'")
                self.AxisTableP1 = line.rstrip().split(', ')
                dataForRegistry = line.rstrip()
                dataForRegistry = dataForRegistry.replace(" ", "") #remove space
                dataForRegistry = dataForRegistry.replace("Hat0X,Hat0Y", "POV1") #replace Hat by POV as requested by Dinput
                dataForRegistry = dataForRegistry.replace(",Hat1X,Hat1Y", "") #remove second Hat if exist (as for Steam Deck)
                Log("dataForRegistryP1 : '" + dataForRegistry + "'")
                if (self.controllerTypeEventToKeep != ''):
                    self.wine.addStringValueInRegistry("HKEY_CURRENT_USER\\Software\\Wine\\DirectInput",
                                                  controllerShortName + " (" + self.controllerTypeEventToKeep + ")",
                                                  dataForRegistry)
                else:
                    #new manner without (event)/(js)
                    self.wine.addStringValueInRegistry("HKEY_CURRENT_USER\\Software\\Wine\\DirectInput",
                                                  controllerShortName,
                                                  dataForRegistry)
            Log("AxisTableP1 lenght :" + str(len(self.AxisTableP1))) 
        if pad_player.PlayerIndex == 2:
            popup.Message("Set " + controllerShortName + " for player " + str(pad_player.PlayerIndex) + ", please wait...", 5)
            for line in output: 
                Log("AxisTableP2 content : '" + line.rstrip() + "'")
                self.AxisTableP2 = line.rstrip().split(', ')
                dataForRegistry = line.rstrip()
                dataForRegistry = dataForRegistry.replace(" ", "") #remove space
                dataForRegistry = dataForRegistry.replace("Hat0X,Hat0Y", "POV1") #replace Hat by POV as requested by Dinput
                dataForRegistry = dataForRegistry.replace(",Hat1X,Hat1Y", "") #remove second Hat if exist (as for Steam Deck)
                Log("dataForRegistryP2 : '" + dataForRegistry + "'")
                if self.controllerTypeEventToKeep != '' :
                    self.wine.addStringValueInRegistry("HKEY_CURRENT_USER\\Software\\Wine\\DirectInput",
                                                  controllerShortName + " (" + self.controllerTypeEventToKeep + ")",
                                                  dataForRegistry)
                else:
                    #new manner without (event)/(js)
                    self.wine.addStringValueInRegistry("HKEY_CURRENT_USER\\Software\\Wine\\DirectInput",
                                                  controllerShortName,
                                                  dataForRegistry)
            Log("AxisTableP2 lenght :" + str(len(self.AxisTableP2))) 
 
    #generate specific values for axis for player 1/2 (specific to model2emu)
    def getAxisHexaValue(self, code: int, table: list):
        Log("getAxisHexaValue - code:" + str(code))
        match code:
            #may be the mapping for all is : X,Y,Rz,Z,Rx,Ry,POV1
            case 0: #"X"
                return "00"
            case 1: #"Y"
                return "01"
            case 2: #"Z"
                return "03"
            case 3: #"Rx"
                return "04"
            case 4: #"Ry"
                return "05"
            case 5: #"Rz"
                return "02"
            case default:
                return "FF"
                
                #Examples for investigation
                #Joystick (Thrustmaster F430 Cockpit Wireless) has 5 axes 
                #(X, Y, Rz, Hat0X, Hat0Y)
                #need to fix as following in registry:
                #wine REG ADD HKEY_CURRENT_USER\\Software\\Wine\\DirectInput\\ /v 'Thrustmaster F430 Cockpit Wireless (js)' /d 'X,Y,Rz,POV1' '/f'
                #manual conf: 000100ff 021100ff 011100ff (seems to be X,Rz,Y as requested !!!)
                #auto conf:   000100ff 021100ff 011100ff
                
                #Joystick (Xbox 360 Wireless Receiver) has 8 axes 
                #(X, Y, Z, Rx, Ry, Rz, Hat0X, Hat0Y)
                #need to fix as following in registry:
                #wine REG ADD HKEY_CURRENT_USER\\Software\\Wine\\DirectInput\\ /v 'Xbox 360 Wireless Receiver (js)' /d 'X,Y,Z,Rx,Ry,Rz,POV1' '/f'
                #manual conf: 000100ff 020100ff 030100ff (seems to be X,Rz,Z)
                
                #the mapping manage by model2emu for all seems in the order finally : X,Y,Rz,Z,Rx,Ry,POV1

    #generate specific "invertion" flag for axis for player 1/2 (specific to model2emu)
    def getAxisInv(self, inputItem: InputItem, pad_player: Controller, model2input: str):
        # use this command to have current 'value': evdev-joystick --showcal /dev/input/event15 | grep '(X ' | awk -F'[:,]' '{print $2}'
        # use this command to have 'min' value: evdev-joystick --showcal /dev/input/event15 | grep '(X ' | awk -F'[:,]' '{print $4}'
        # use this command to have 'max' value: evdev-joystick --showcal /dev/input/event15 | grep '(X ' | awk -F'[:,]' '{print $6}'
        # - to improve sign detection for invertion (not liable in es_input.cfg some times)
        # - to improve also calibration for deadzone to manage in other function
        Log("getAxisInv - Code:" + str(inputItem.Code))
        
        match inputItem.Code:
            case 0: #"X"
                axisName = "X"
            case 1: #"Y"
                axisName = "Y"
            case 2: #"Z"
                axisName = "Z"
            case 3: #"Rx"
                axisName = "X Rate"
            case 4: #"Ry"
                axisName = "Y Rate"
            case 5: #"Rz"
                axisName = "Z Rate"
            case default:
                axisName = ""
        #get currentValue
        cmd = "evdev-joystick --showcal " + pad_player.DevicePath + " | grep '(" + axisName + " ' | awk -F'[:,]' '{print $2}'"
        Log(f"command: {str(cmd)}")
        p = subprocess.Popen(cmd, shell=True, 
            stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
        p.wait()
        output = p.stdout
        currentValue = ""
        for line in output:
            currentValue = line.rstrip().replace(" ","")
        Log("currentValue : " + currentValue)
        
        #get minValue
        cmd = "evdev-joystick --showcal " + pad_player.DevicePath + " | grep '(" + axisName + " ' | awk -F'[:,]' '{print $4}'"
        Log(f"command: {str(cmd)}")
        p = subprocess.Popen(cmd, shell=True, 
            stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
        p.wait()
        output = p.stdout
        minValue = ""
        for line in output:
            minValue = line.rstrip().replace(" ","")
        Log("minValue : " + minValue)
        
        #get maxValue
        cmd = "evdev-joystick --showcal " + pad_player.DevicePath + " | grep '(" + axisName + " ' | awk -F'[:,]' '{print $6}'"
        Log(f"command: {str(cmd)}")
        p = subprocess.Popen(cmd, shell=True, 
            stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
        p.wait()
        output = p.stdout
        maxValue = ""
        for line in output:
            maxValue = line.rstrip().replace(" ","")
        Log("maxValue : " + maxValue)
        
        #if current value is equal to max value, we detect that axis is inverted
        if int(currentValue) == int(maxValue):
            #if axes need to be inverted for this game
            if 'INV' in model2input:
                return "0"
            else:
                return "1"
        else:
            #if axes need to be inverted for this game
            if 'INV' in model2input:
                return "1"
            else:
                return "0"

    #generate specific values for axis direction for player 1/2 (specific to model2emu) - used for driving game using H shifter for example
    def getAxisDirectionHexaValue(self, inputItem: InputItem):
        Log("    def getAxisDirectionHexaValue(self, inputItem: InputItem)- Name:" + inputItem.Name)
        Log("                                                               Code:" + str(inputItem.Code))
        Log("                                                               Value:" + str(inputItem.Value))
        match inputItem.Code:
            #may be the mapping for all is : X,Y,Rz,Z,Rx,Ry,POV1
            case 0: #"X"
                if inputItem.Value == -1:
                    return "00"
                else:
                    return "01"
            case 1: #"Y"
                if inputItem.Value == -1:
                    return "02"
                else:
                    return "03"
            case 2: #"Z"
                if inputItem.Value == -1:
                    return "06"
                else:
                    return "07"
            case 3: #"Rx"
                if inputItem.Value == -1:
                    return "08"
                else:
                    return "09"
            case 4: #"Ry"
                if inputItem.Value == -1:
                    return "0a"
                else:
                    return "0b"
            case 5: #"Rz"
                if inputItem.Value == -1:
                    return "04"
                else:
                    return "05"
            case default:
                return "FF"
                
    #generate Binary DInput values for configuration file depending input type and controller index
    def getDInputBinaryValue(self, inputItem: InputItem, pad_player: Controller, model2input: str):
        #default value
        hexString = '00000000'
        padIndex = pad_player.JsOrder + 1
        # ************************* ********************** WARNING *******************************************
        # take care: if your device is in JsOrder 5 or more, it will be not detected and ignored by model2emu
        # only in position 0 to 3 (1 to 4 after +1) will be useable
        # ************************* **************************************************************************
        Log(" PlayerIndex of " + pad_player.DeviceName + " : " + str(pad_player.PlayerIndex))
        Log(" SdlIndex of " + pad_player.DeviceName + " : " + str(pad_player.SdlIndex))
        Log(" NaturalIndex of " + pad_player.DeviceName + " : " + str(pad_player.NaturalIndex))
        Log(" JsIndex of " + pad_player.DeviceName + " : " + str(pad_player.JsIndex))
        Log(" JsOrder of " + pad_player.DeviceName + " : " + str(pad_player.JsOrder))
        Log(" inputItem.Name :  " + inputItem.Name)
        Log(" inputItem.Type :  " + str(inputItem.Type))
        Log(" inputItem.Id :  " + str(inputItem.Id))
        if inputItem.IsButton:
            Log(" inputItem.IsButton ")
            Log(" padIndex : " + str(padIndex))
            Log(" inputItem.Id + 1: " + str(inputItem.Id + 1))
            if((inputItem.Id + 1) > 15):
                hexString ='{0:0x}01{1:x}0000'.format((inputItem.Id + 1)-16, padIndex)
            else:
                hexString ='{0:x}00{1:x}0000'.format(inputItem.Id + 1, padIndex)
        if inputItem.IsHat:
            if inputItem.Name == "up":
                hexString ='0e0{0:x}0000'.format(padIndex)
            if inputItem.Name == "down":
                hexString ='0f0{0:x}0000'.format(padIndex)
            if inputItem.Name == "left":
                hexString ='0c0{0:x}0000'.format(padIndex)
            if inputItem.Name == "right":
                hexString ='0d0{0:x}0000'.format(padIndex)
        if inputItem.IsAxis:
            if model2input in self.AnalogInputs:
                if pad_player.PlayerIndex == 1:
                    hexString ='{0}{1}{2:x}'.format(self.getAxisHexaValue(inputItem.Code, self.AxisTableP1),self.getAxisInv(inputItem, pad_player,model2input), padIndex)
                elif pad_player.PlayerIndex == 2:
                    hexString ='{0}{1}{2:x}'.format(self.getAxisHexaValue(inputItem.Code, self.AxisTableP2),self.getAxisInv(inputItem, pad_player,model2input), padIndex)
            else: #use only direction as button
                hexString ='{0}0{1:x}0000'.format(self.getAxisDirectionHexaValue(inputItem), padIndex)
            #to be complete with estimated calibration for the moment ;-)
        #return if type not identify
        Log("hexString : {}".format(hexString))
        return bytes.fromhex(hexString)

    #generate Binary XInput values for configuration file depending input type and controller index
    def getXInputBinaryValue(self, inputItem: InputItem, pad_player: Controller, model2input: str):
        #default value
        hexString = '00000000'
        padIndex = pad_player.SdlIndex + 1
        # ************************************************ WARNING *******************************************
        # take care: if your device is in SdlIndex 5 or more, it will be not detected and ignored by model2emu
        # only index 0 to 3 (1 to 4 after +1) will be useable
        # ****************************************************************************************************
        Log(" PlayerIndex of " + pad_player.DeviceName + " : " + str(pad_player.PlayerIndex))
        Log(" SdlIndex of " + pad_player.DeviceName + " : " + str(pad_player.SdlIndex))
        Log(" NaturalIndex of " + pad_player.DeviceName + " : " + str(pad_player.NaturalIndex))
        Log(" JsIndex of " + pad_player.DeviceName + " : " + str(pad_player.JsIndex))
        Log(" JsOrder of " + pad_player.DeviceName + " : " + str(pad_player.JsOrder))
        if inputItem.IsButton or inputItem.IsHat:
            hexString ='{0}0{1:x}0000'.format(self.XInputs[inputItem.Item], padIndex)
        if inputItem.IsAxis:
            if model2input in self.AnalogInputs:
                hexString ='{0}{1}{2:x}'.format(self.XInputs[inputItem.Item * AS_AXIS],self.getAxisInv(inputItem, pad_player,model2input), padIndex)
            else: #use only direction as button
                hexString ='{0}0{1:x}0000'.format(self.XInputs[inputItem.Item * AS_BUTTON], padIndex)
            #to be complete with estimated calibration for the moment ;-)
        #return if type not identify
        Log("hexString : {}".format(hexString))
        return bytes.fromhex(hexString)

    # Create the controller configuration file
    def generateControllerConfig(self) -> bool :

        AnalogActivationBytePosition1 = "00"
        AnalogActivationBytePosition2 = "00"
        AnalogActivationBytePosition3 = "00"

        # Load Configuration
        from configgen.settings.iniSettings import IniSettings
        model2emuSettings = IniSettings(recalboxFiles.model2emuConfigFile, False)
        model2emuSettings.loadFile(True) \
                       .defineBool('1', '0')
        
        popup.Message("Configure Xinput/Dinput, please wait...", 5)
        #activate/deactivate Xinput (to have vibration on xbox compatible pad - auto-mapping) or use Dinput (as input.cfg)
        model2emuSettings.setString("Input","XInput", str(self.recalboxOptions.getInt("model2emu.xinput", 0)))
        
        #for dinput/ wine appimage 5.11 : DisableHidraw = 0 & Enable SDL = 0
        #for xinput/ wine appimage 5.11 : DisableHidraw = 1 & Enable SDL = 1
        if (self.recalboxOptions.getInt("model2emu.xinput", 0) == 1):
            #XINPUT
            #set to have Hidraw deactivated in all cases for the moment
            self.wine.addDwordValueInRegistry("HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Services\\WineBus",
                                         "DisableHidraw",
                                         "1")
            #set to have sdl activate in all cases for the moment
            self.wine.addDwordValueInRegistry("HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Services\\WineBus",
                                                     "Enable SDL",
                                                     "1")
        else:
            #DINPUT
            #set to have Hidraw activated in all cases for the moment
            self.wine.addDwordValueInRegistry("HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Services\\WineBus",
                                         "DisableHidraw",
                                         "0")
            #set to have sdl deactivate in all cases for the moment
            self.wine.addDwordValueInRegistry("HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Services\\WineBus",
                                                     "Enable SDL",
                                                     "0")
 
        #set deadzone in commun function for Dinput and Xinput
        self.setAxisDeadzone()
        
        # Save configuration
        model2emuSettings.saveWindowsFile()
        #init boolean NeedPotentiallySindenBorder
        NeedPotentiallySindenBorder = False
        ## to check if Mayflash_Wiimote_PC_Adapter/Sinden Lightgun exists and get mouse indexes in a first time
        LightgunIndexByPlayer, NeedPotentiallySindenBorder = Controller.findLightguns("model2emu")
        #if game is not for lightgun, we need to configure the controller(s) as usual
        self.isLightgunGame = self.lightgunConfig.isLightGunGame(LightgunIndexByPlayer)
        if self.isLightgunGame:
			#for potential future used / deactivated or not really used for the moment
            #self.wine.addStringValueInRegistry("HKEY_CURRENT_USER\\Software\\Wine\\DirectInput",
            #                                          "MouseWarpOverride",
            #                                          "") # ""force)

            # Reload Configuration (because is isLightGunGame() could save also)
            model2emuSettings = IniSettings(recalboxFiles.model2emuConfigFile, False)
            model2emuSettings.loadFile(True) \
                           .defineBool('1', '0')
            model2emuSettings.setString("Input","UseRawInput", "0			;Read mouse through Rawinput, allowing 2 mice")
            model2emuSettings.setString("Input","RawDevP1", LightgunIndexByPlayer[0][0] + "			;Assign specific RawInput devices to players. If you have more than 2 mice")
            model2emuSettings.setString("Input","RawDevP2", LightgunIndexByPlayer[1][0] + "			;set which one is assigned to each player (0-based)")
            
            # Save configuration
            model2emuSettings.saveWindowsFile()
        else:
            #reset to "False" to avoid to diplay the border on the overlay in this case ;-)
            NeedPotentiallySindenBorder = False
            #search player 1 and player 2 controllers only
            for index in self.playersControllers:
                popup.Message("Prepare controller mapping for player {}".format(str(index)) + ", please wait...", 10)
                Log("Player {}".format(str(index)))
                controller = self.playersControllers[index]
                # we only care about player 1 and 2 for the moment
                if controller.PlayerIndex == 1:
                    #for testing only / to keep example of these command not used for the moment
                    # self.wine.addStringValueInRegistry("HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Services\\WineBus\\Map",
                                                  # "1",
                                                  # controller.generateSDLGameDBLine())
                    # self.wine.addDwordValueInRegistry("HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Services\\WineBus",
                                                 # "Enable SDL",
                                                 # "0")
                    # self.wine.addDwordValueInRegistry("HKEY_LOCAL_MACHINE\\System\\CurrentControlSet\\Services\\WineBus",
                                                 # "Map Controllers",
                                                 # "1")
                    pad_player1 = controller
                    #for DInput only, need to prepare mapping for Axis
                    if self.recalboxOptions.getInt("model2emu.xinput", 0) == 0:
                        self.prepareAxisTable(pad_player1)
                elif controller.PlayerIndex == 2:
                    pad_player2 = controller
                    #for DInput only, need to prepare mapping for Axis
                    if self.recalboxOptions.getInt("model2emu.xinput", 0) == 0:
                        self.prepareAxisTable(pad_player2)
                # remove Controller (event or js) to avoid potential dooblons with (js or event) ones
                # see example of wine command:
                # wine REG ADD HKEY_CURRENT_USER\\Software\\Wine\\DirectInput\\Joysticks /v 'Xbox 360 Wireless Receiver (event)' /d 'disabled' /f
                # remove additional naming as done for few bluetooth controllers as nintendo switch snes/nes/megadrive/genesis controllers
                # and we are lucky, it's the same to do for "Steam Deck", we have to remove the part after " - " ;-)
                controllerShortName = controller.DeviceName.split(' - ')[0]
                #for Dinput only and wine 5.X version (as set in init), we need to "disabled" some entry
                if (self.controllerTypeEventToRemove != ''):
                    self.wine.addStringValueInRegistry("HKEY_CURRENT_USER\\Software\\Wine\\DirectInput\\Joysticks",
                                                      controllerShortName + " (" + self.controllerTypeEventToRemove + ")",
                                                      "disabled")
                #for Xinput case only
                elif (self.controllerTypeEventToKeep == ""):
                    #remove values disable previously especially for js & event
                    self.wine.removeValueFromRegistry("HKEY_CURRENT_USER\\Software\\Wine\\DirectInput\\Joysticks",
                                          controllerShortName + " (js)")
                    self.wine.removeValueFromRegistry("HKEY_CURRENT_USER\\Software\\Wine\\DirectInput\\Joysticks",
                                          controllerShortName + " (event)")
                    self.wine.removeValueFromRegistry("HKEY_CURRENT_USER\\Software\\Wine\\DirectInput",
                                          controllerShortName + " (js)")
                    self.wine.removeValueFromRegistry("HKEY_CURRENT_USER\\Software\\Wine\\DirectInput",
                                          controllerShortName + " (event)")
                if (self.controllerToOverride == True):
                    self.wine.addStringValueInRegistry("HKEY_CURRENT_USER\\Software\\Wine\\DirectInput\\Joysticks",
                                                  controllerShortName,
                                                  "override")
                else :
                    self.wine.removeValueFromRegistry("HKEY_CURRENT_USER\\Software\\Wine\\DirectInput\\Joysticks",
                                          controllerShortName)
        
        #Force deactivation of some hardware by default
        popup.Message("Deactivate wrong controller device(s), please wait...", 3)
        #for macbook applesmc (internal accelerometer)
        self.wine.addStringValueInRegistry("HKEY_CURRENT_USER\\Software\\Wine\\DirectInput\\Joysticks",
                                  "applesmc (js)",
                                  "disabled")
                                  
        #Write input file in CFG diretory
        value_found = self.ROM_INPUTS.get(self.rom)
        if value_found != None:
            popup.Message("Create configuration for " + self.rom + " rom inputs, please wait...", 3)
            Log("Inputs configuration to create in {}.input".format(self.rom));
            #replace "test" by rom after testing
            file = open(recalboxFiles.model2emuCFG + "/" + self.rom + ".input", "wb")
            for model2input in self.ROM_INPUTS[self.rom]:
                Log("input Name : " + model2input)
                Log("input Value :" + str(self.ROM_INPUTS[self.rom][model2input]))
                if self.ROM_INPUTS[self.rom][model2input] != -1:
                    #initial value
                    selected_pad = None
                    if 'pad_player1' in vars():
                        selected_pad = pad_player1 #player1 selected by default
                    if "Player2" in model2input:
                        if 'pad_player2' in vars():
                            selected_pad = pad_player2
                        else: #get default value in this case
                            selected_pad = None

                    if selected_pad != None:
                        # manage case to reuse up/down for L2/R2 as button
                        # initial value
                        inputDetermined = self.ROM_INPUTS[self.rom][model2input]
                        #manage case for L2/R2 as buttons for racing games
                        if inputDetermined == InputItem.ItemUp:
                            #first we check if R2 is requested for this game
                            for input in self.ROM_INPUTS[self.rom].values():
                                if input == InputItem.ItemR2:
                                    if selected_pad.HasInput(InputItem.ItemR2):
                                        if not selected_pad.Input(InputItem.ItemR2).IsAxis:
                                            #if R2 is not axis, we map it on 'up' of model 2 emu
                                            inputDetermined = InputItem.ItemR2
                                            break
                                            #else no need to change anything
                        elif inputDetermined == InputItem.ItemDown:
                            #first we check if L2 is requested for this game
                            for input in self.ROM_INPUTS[self.rom].values():
                                if input == InputItem.ItemL2:
                                    if selected_pad.HasInput(InputItem.ItemL2):
                                        if not selected_pad.Input(InputItem.ItemL2).IsAxis:
                                            #if L2 is not axis, we map it on 'down' of model 2 emu
                                            inputDetermined = InputItem.ItemL2
                                            break
                                            #else no need to change anything
                        if selected_pad.HasInput(inputDetermined):
                            if self.recalboxOptions.getInt("model2emu.xinput", 0) == 0 :
                                #for DInput
                                binaryString = self.getDInputBinaryValue(selected_pad.Input(inputDetermined), selected_pad, model2input)
                            else: 
                                #for XInput
                                binaryString = self.getXInputBinaryValue(selected_pad.Input(inputDetermined), selected_pad, model2input)
                            if selected_pad.Input(inputDetermined).IsAxis:
                                if model2input in self.AnalogInputs:
                                    if self.AnalogInputs [model2input] == 1:
                                        AnalogActivationBytePosition1 = "01"
                                    elif self.AnalogInputs [model2input] == 2:
                                        AnalogActivationBytePosition2 = "01"
                                    elif self.AnalogInputs [model2input] == 3:
                                        AnalogActivationBytePosition3 = "01"
                                    binaryString = binaryString + b'\x00\xff'
                        else:
                            binaryString = self.DefaultInputs[model2input]
                    else:
                        if model2input in self.DefaultInputs:
                            binaryString = self.DefaultInputs[model2input]
                        else: #for robustness: if no default value identified, we have to set a value in all cases
                            binaryString = b'\x00\x00\x00\x00'
                else:
                    if model2input in self.DefaultInputs:
                        binaryString = self.DefaultInputs[model2input]
                    else: #for robustness: if no default value identified, we have to set a value in all cases
                        binaryString = b'\x00\x00\x00\x00'
                Log("binaryString :" + str(binaryString))
                file.write(binaryString)
            #write the trailer : 8 bytes to 0x00 and set analog if needed
            file.write(bytes.fromhex(AnalogActivationBytePosition1 + AnalogActivationBytePosition2 + AnalogActivationBytePosition3 + "00"))
            file.write(b'\x00\x00\x00\x00')
            # Save configuration
            file.close()
        else:
            Log("Inputs configuration not created for {}.zip".format(self.rom))
        return NeedPotentiallySindenBorder

