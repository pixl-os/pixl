#!/usr/bin/env python3

#Modules
import os
import subprocess
import sys
import shutil
import stat
import configgen.utils.popup as popup
import configgen.recalboxFiles as recalboxFiles
import datetime
#utils to manage advanced overlays features for all emulators
import configgen.utils.overlays as overlays

#Classes
from pathlib import Path
from configgen.utils.wine import wine
from configgen.Command import Command
from configgen.Emulator import Emulator
from configgen.controllers.controller import ControllerPerPlayer
from configgen.generators.Generator import Generator
from configgen.settings.keyValueSettings import keyValueSettings

def Log(txt):
    import datetime
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    #print(formatted_time[:-3] + " - " + txt)
    return 0

class Model2EmuGenerator(Generator):

    # Wide Ratio - 4/3 by default if in auto
    WIDE_RATIO = \
    {
       "auto": 0,
       "16/9": 1,
       "16/10": 2,
       "4/3": 0,
    }

    def generate(self, system: Emulator, playersControllers: ControllerPerPlayer, recalboxOptions: keyValueSettings, args) -> Command:
        
        popup.Message("Sega Model 2 initialization, please wait...", 60)
        
        # simplify the rom name (strip the directory & extension)
        rompath = "/recalbox/share/roms/model2"
        rom = args.rom;
        romname = rom.replace(rompath + "/", "")
        smplromname = romname.replace(".zip", "")
        rom = smplromname.split('/', 1)[-1]
        
        #check if some important files are missing
        #model2emuConfigFile = SAVES + '/model2/model2emu/EMULATOR.INI'
        #model2emuPWD = SAVES + '/model2/model2emu'
        #model2emuCFG = SAVES + '/model2/model2emu/CFG'
        #model2emuLUA = SAVES + '/model2/model2emu/scripts'
        #model2emuDAT = SAVES + '/model2/model2emu/NVDATA'
        #from
        #model2emuConfigFileInit = SAVES_INIT + '/model2/model2emu/EMULATOR.INI'
        #model2emuPWDInit = SAVES_INIT + '/model2/model2emu'
        #model2emuCFGInit = SAVES_INIT + '/model2/model2emu/CFG'
        #model2emuLUAInit = SAVES_INIT + '/model2/model2emu/scripts'
        #model2emuDATInit = SAVES_INIT + '/model2/model2emu/NVDATA'
        
        #if no model2/model2emu directory in saves, we restore everything
        if not os.path.exists(recalboxFiles.model2emuPWD):
            popup.Message("Saves directory missing. Restoring, please wait...", 5)
            #create directory
            #Log("Command: " + 'install -d' + recalboxFiles.model2emuPWD)
            os.system('install -d ' + recalboxFiles.model2emuPWD)
            #copy all files
            #Log("Command: " + 'cp -r ' + recalboxFiles.model2emuPWDInit + '/* -t ' + recalboxFiles.model2emuPWD + '/')
            os.system('cp -r ' + recalboxFiles.model2emuPWDInit + '/* -t ' + recalboxFiles.model2emuPWD + '/')

        #if no EMULATOR.ini
        if (not os.path.exists(recalboxFiles.model2emuConfigFile)):
            popup.Message("EMULATOR.INI file is missing. Restoring, please wait...", 5)
            os.system('cp ' + recalboxFiles.model2emuConfigFileInit + ' ' + recalboxFiles.model2emuConfigFile)

        #if no scripts/{rom}.lua
        if (not os.path.exists(recalboxFiles.model2emuLUA + "/" + rom + ".lua")):
            popup.Message(rom + ".lua file is missing. Restoring some lua files, please wait...", 5)
            os.system('install -d ' + recalboxFiles.model2emuLUA)
            os.system('cp ' + recalboxFiles.model2emuLUAInit + "/" + rom + ".lua " + recalboxFiles.model2emuLUA + "/" + rom + ".lua")
            os.system('cp ' + recalboxFiles.model2emuLUAInit + "/model2.lua " + recalboxFiles.model2emuLUA + "/model2.lua")
            os.system('cp ' + recalboxFiles.model2emuLUAInit + "/common.lua " + recalboxFiles.model2emuLUA + "/common.lua")
            #create/recreate directory for crosshairs
            os.system('install -d ' + recalboxFiles.model2emuLUA + "/crosshairs")
            #copy all crosshairs
            os.system('cp -r ' + recalboxFiles.model2emuLUAInit + "/crosshairs/* -t " + recalboxFiles.model2emuLUA + "/crosshairs/" )

        #if no CFG directory
        if (not os.path.exists(recalboxFiles.model2emuCFG)):
            popup.Message(rom + "CFG directory missing. Restoring, please wait...", 5)
            os.system('install -d ' + recalboxFiles.model2emuCFG)

        #if no NVDATA directory
        if (not os.path.exists(recalboxFiles.model2emuDAT)):
            popup.Message(rom + "DAT directory missing. Restoring, please wait...", 5)
            os.system('install -d ' + recalboxFiles.model2emuDAT)

        #init wine class
        #for dev/testing purpose we could let FS unlocked if we set last default parameter to True (please let to False in repo)
        #use relative path from /usr/wine/ for last parameter (call pixLWineBinPath) that contains all binary for a version/type of wine/fork
        
        #possible default choice for the moment
        #wineSelected = "/usr/wine/" + "proton" + "/bin/wine"
        #wineSelected = "/usr/wine/" + "lutris" + "/bin/wine"
        #wineSelected = "/usr/wine/" + "ge-custom" + "/bin/wine"
        wineSelected = "" #to use default appimage wine 5.11 staging
        self.wine = wine(system, recalboxOptions, True, wineSelected)
        
        popup.Message("Sega Model 2 initialization (" + self.wine.wineversion + "), please wait...", 60)

        #check and initialize wine
        self.wine.InitializeWine()

        # install additional library if missing (Set to True if you want to force quit in case of infinity loop risk else please let False)
        self.wine.InstallDlls("d3dx9",45, False) # timeout set to 45 seconds (set to False just to display message)
        self.wine.InstallDlls("d3dcompiler_42",45, False) # timeout set to 45 seconds (set to False just to display message)
        self.wine.InstallDlls("d3dx9_42",45, False) # timeout set to 45 seconds (set to False just to display message)
        self.wine.InstallDlls("xact",45, False) # timeout set to 45 seconds (set to False just to display message)
        if(self.wine.winearch != "win32"): #for win64/wow64 installation only
            self.wine.InstallDlls("xact_x64",45, False) # timeout set to 45 seconds (set to False just to display message)

        # Load Configuration
        #if needed ?!
        os.chmod(recalboxFiles.model2emuConfigFile, stat.S_IRWXO)
        from configgen.settings.iniSettings import IniSettings
        model2emuSettings = IniSettings(recalboxFiles.model2emuConfigFile, False)
        model2emuSettings.loadFile(True) \
                       .defineBool('1', '0')
        
        from configgen.utils.videoMode import getCurrentResolution
        screenWidth, screenHeight = getCurrentResolution()
        Log("the resolution detected is " + str(screenWidth) + "x" + str(screenHeight))
        # set ini to use custom resolution and automatically start in fullscreen
        model2emuSettings.setString("Renderer","FullScreenWidth", str(screenWidth))
        model2emuSettings.setString("Renderer","FullScreenHeight", str(screenHeight))

        #set a custom resolution
        model2emuSettings.setString("Renderer","FullMode", "4")
        
        #force auto full automatically
        model2emuSettings.setString("Renderer","AutoFull", "1")

        #force to enable/disbale sound from conf
        if(recalboxOptions.getString("audio.mode", "none") != "none"):
            model2emuSettings.setString("Renderer","Sound", "1")
        else:
            model2emuSettings.setString("Renderer","Sound", "0")
        
        # now set all other emulator features
        model2emuSettings.setString("Renderer","FakeGouraud", str(recalboxOptions.getInt("model2emu.fakeGouraud", 0)))
        model2emuSettings.setString("Renderer","Bilinear", str(recalboxOptions.getInt("model2emu.bilinearFiltering", 0)))
        model2emuSettings.setString("Renderer","Trilinear", str(recalboxOptions.getInt("model2emu.trilinearFiltering", 0)))
        model2emuSettings.setString("Renderer","FilterTilemaps", str(recalboxOptions.getInt("model2emu.filterTilemaps", 0)))
        model2emuSettings.setString("Renderer","ForceManaged", str(recalboxOptions.getInt("model2emu.forceManaged", 0)))
        model2emuSettings.setString("Renderer","AutoMip", str(recalboxOptions.getInt("model2emu.enableMIP", 0)))
        model2emuSettings.setString("Renderer","MeshTransparency", str(recalboxOptions.getInt("model2emu.meshTransparency", 0)))
        model2emuSettings.setString("Renderer","FSAA", str(recalboxOptions.getInt("model2emu.fullscreenAA", 0)))

        # Save configuration
        model2emuSettings.saveWindowsFile()

        # now run the emulator
        commandArray = ["wine", recalboxFiles.recalboxBins[system.Emulator]]
        # or
        # commandArray = ["wine", "start", "/d", "/recalbox/share/saves/model2/model2emu", "/unix", recalboxFiles.recalboxBins[system.Emulator]]
        commandArray.extend([rom])

        # add predefined scanlines proposed by .lua files fi activated in model2emu options
        if(recalboxOptions.getInt("model2emu.scanlines", 0) == 1):
            #set lua file to use scanlines=true
            Log("sed -i 's/scanlines=false/scanlines=true/' " + recalboxFiles.model2emuPWD + "/scripts/" + rom + ".lua")
            os.system("sed -i 's/scanlines=false/scanlines=true/' " + recalboxFiles.model2emuPWD + "/scripts/" + rom + ".lua")
            # To manage scanlines from screen sizes for model 2 using .lua script from a  template for model2emu
            if screenHeight <= 1080: #for 720p and 1080p screen
                os.system('cp ' + recalboxFiles.model2emuLUAInit + "/scanlines_default_2k.png " + recalboxFiles.model2emuLUA + "/scanlines_default.png")
            else: # for 1440p until 2160p (and upper ;-)
                os.system('cp ' + recalboxFiles.model2emuLUAInit + "/scanlines_default_4k.png " + recalboxFiles.model2emuLUA + "/scanlines_default.png")
        else:
            #set lua file to use scanlines=false
            Log("sed -i 's/scanlines=true/scanlines=false/' " + recalboxFiles.model2emuPWD + "/scripts/" + rom + ".lua")
            os.system("sed -i 's/scanlines=true/scanlines=false/' " + recalboxFiles.model2emuPWD + "/scripts/" + rom + ".lua")
        
        # Side wide display or not from ratio selected
        gameRatio = self.WIDE_RATIO[system.Ratio] if system.Ratio in self.WIDE_RATIO else 0
        Log("system.Ratio : " + system.Ratio + " - gameRatio : " + str(gameRatio))
        if(gameRatio != 0):
            #set lua file to use wide=true
            Log("sed -i 's/wide=false/wide=true/' " + recalboxFiles.model2emuPWD + "/scripts/" + rom + ".lua")
            os.system("sed -i 's/wide=false/wide=true/' " + recalboxFiles.model2emuPWD + "/scripts/" + rom + ".lua")
        else:
            #set lua file to use wide=false
            Log("sed -i 's/wide=true/wide=false/' " + recalboxFiles.model2emuPWD + "/scripts/" + rom + ".lua")
            os.system("sed -i 's/wide=true/wide=false/' " + recalboxFiles.model2emuPWD + "/scripts/" + rom + ".lua")

        #manage controller configuration by {rom}.input in CFG directory
        from configgen.generators.model2emu.model2emuControllers import model2emuControllers
        controllers = model2emuControllers(system, recalboxOptions, playersControllers, rom, self.wine)
        #init boolean NeedPotentiallySindenBorder
        NeedPotentiallySindenBorder = False
        NeedPotentiallySindenBorder = controllers.generateControllerConfig()

        # To manage overlay for model 2 using .lua script and using an overlay generated dynamically for model2emu
        overlayAvailable = overlays.processModel2emuOverlays(system, args.rom, recalboxOptions, NeedPotentiallySindenBorder)
        if(overlayAvailable == True):
            #set lua file to use overlay=true
            Log("sed -i 's/overlay=false/overlay=true/' " + recalboxFiles.model2emuPWD + "/scripts/" + rom + ".lua")
            os.system("sed -i 's/overlay=false/overlay=true/' " + recalboxFiles.model2emuPWD + "/scripts/" + rom + ".lua")
        else:
            #set lua file to use wide=false
            Log("sed -i 's/overlay=true/overlay=false/' " + recalboxFiles.model2emuPWD + "/scripts/" + rom + ".lua")
            os.system("sed -i 's/overlay=true/overlay=false/' " + recalboxFiles.model2emuPWD + "/scripts/" + rom + ".lua")
        
        
        #clean all popup before to launch game
        popup.Clean()
        #Add message to launch game
        popup.Message("Game is launching, please wait...", 5)
        
        # use 'command' working directory path (cwdPath) to ensure configs are well read/saved etc
        return Command(videomode=system.VideoMode, array=commandArray, env=self.wine.env, delay=0.5, cwdPath="/recalbox/share/saves/model2/model2emu", postExec=None)

