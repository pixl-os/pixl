#!/usr/bin/env python
## Released version: 1.0.0
##IMPORT STD---------------------------------------------------------------------
import os
import sys
import time
import logging
import subprocess
import linecache
import json

##IMPORT RECALBOX
import configgen.recalboxFiles as recalboxFiles
from configgen.settings.keyValueSettings import keyValueSettings
from configgen.Emulator import Emulator
import configgen.utils.popup as popup

##IMPORT for parsing/xml functions
##for regular expression
import re
##for XML parsing
import xml.etree.ElementTree as ET
from xml.dom import minidom

## remove "//" for robustness (if we move from HOME_INIT to HOME later)
pegasusLightGun = recalboxFiles.pegasusLightGun.replace("//", "/")
GameListFileName = recalboxFiles.GameListFileName

# Information game from ES
GAME_INFO_PATH = "/tmp/es_state.inf"
# Key in informatiojn files
KEY_GAME_NAME = "Game"

##Versions history:
##
## V1: initial version for model2emu
## V2: 03-01-2024 - new version for model2emu using wiimote with dolphin bar/sinden lightgun + new architecture

def Log(txt):
    import datetime
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    #print(formatted_time[:-3] + " - " + txt)
    return 0
    
class model2emuLightGun:
    
    # constructor
    def __init__(self, system: Emulator, recalboxOptions: keyValueSettings, rom: str):
        # initial settings
        self.system: Emulator = system
        self.recalboxOptions: keyValueSettings = recalboxOptions
        self.rom = rom
        ## init for all indexes
        self.MouseIndexByPlayer = MouseIndexByPlayer = [["nul","nul","nul"],["nul","nul","nul"],["nul","nul","nul"]]

    #find game name from /tmp/es_state.inf 
    def getGameNameFromESState(self):
        gameInfo = keyValueSettings(GAME_INFO_PATH, False)
        gameInfo.loadFile(True)
        game_name = gameInfo.getString(KEY_GAME_NAME, "Unknown")
        #for test purpose: 
        #game_name = gameInfo.getString(KEY_GAME_NAME, "gunblade")
        #game_name = gameInfo.getString(KEY_GAME_NAME, "virtuacop")
        del gameInfo
        if game_name != '':
            Log('Game name found, the game name is :' + game_name)
        else:
            Log('System/Game name are not set properly from gamelist/from ES or may be not scrapped ? or empty name ? the game name content is empty ')
        return game_name

    def manageModel2emuCrosshair(self, section, name, value) -> (str, str, str) :
        #if 'Drawcross' parameter is detected (that's the only one today ;-)
        if("drawcross" in name.lower()):
            # we need to udpate .lua file to set crosshair or not depending options/hardware used and lightgun.cfg file settings
            self.generateModel2CrosshairConfig(value)
            #for the moment - we force Drawcross to 0 in .ini file but we need to let it in .cfg file to know if we need crosshair or not on some games.
            value = "0			;Enable crossair in lightgun game only"
        return section, name, value

    def generateModel2CrosshairConfig(self, value):
        Log('system: ' + self.system.Name)
        Log('rom: ' + self.rom)
        Log('value: "' + value + '"')
                    
        rompath = recalboxFiles.ROMS + "/" + self.system.Name
        # simplify the rom name (strip the directory & extension)
        rom = self.rom
        romname = rom.replace(rompath + "/", "")
        smplromname = romname.replace(".zip", "")
        rom = smplromname.split('/', 1)[-1]
        for index, row in enumerate(self.MouseIndexByPlayer):
            NeedCrosshair = False
            playerIndex = index + 1
            if(playerIndex <= 2): #only for player 1 & 2
                if("mayflash" in row[1]):
                    #we force crosshair for wiimote/dolphin bar if request
                    if value.startswith("0") == True:
                        NeedCrosshair = False
                    else:
                        NeedCrosshair = True
                elif(("sinden" in row[1]) and (not self.recalboxOptions.getBool("lightgun.sinden.crossair.enabled", False))):
                    #we deactivate crosshair for sinden if crosshair is not enabled for sinden lightgun
                    NeedCrosshair = False
                elif(("sinden" in row[1]) and (self.recalboxOptions.getBool("lightgun.sinden.crossair.enabled", False))):
                    #we activate crosshair for sinden if crosshair is enabled and if request by cfg file
                    if value.startswith("0") == True:
                        NeedCrosshair = False
                    else:
                        NeedCrosshair = True
                elif(("nul" in row[1])): # if no controller connected
                    NeedCrosshair = False
                if(playerIndex == 2):
                    #we force to false to avoid to display crossair for player 2 for the moment because multi-mice doesn't work yet :-(
                    NeedCrosshair = False
                #we deactivate crosshair if nothing define
                if(NeedCrosshair == True):
                    #set lua file to use scanlines=true
                    Log("sed -i 's/P" + str(playerIndex) + "_Crosshair_Visibility=false/P" + str(playerIndex) + "_Crosshair_Visibility=true/' " + recalboxFiles.model2emuPWD + "/scripts/" + rom + ".lua")
                    os.system("sed -i 's/P" + str(playerIndex) + "_Crosshair_Visibility=false/P" + str(playerIndex) + "_Crosshair_Visibility=true/' " + recalboxFiles.model2emuPWD + "/scripts/" + rom + ".lua")
                else:
                    #set lua file to use scanlines=false
                    Log("sed -i 's/P" + str(playerIndex) + "_Crosshair_Visibility=true/P" + str(playerIndex) + "_Crosshair_Visibility=false/' " + recalboxFiles.model2emuPWD + "/scripts/" + rom + ".lua")
                    os.system("sed -i 's/P" + str(playerIndex) + "_Crosshair_Visibility=true/P" + str(playerIndex) + "_Crosshair_Visibility=false/' " + recalboxFiles.model2emuPWD + "/scripts/" + rom + ".lua")

    # Check if game is declared from the lightgun.cfg and configure emulator/core (for the moment ;-)
    def setLightGunConfig(self, system_name,game_name):
        if(game_name == ''):        
            Log('Game name empty, Not Configured !!!')
            return 'Not configured'
        systems = dict()
        tree = ET.parse(pegasusLightGun)
        root = tree.getroot()
        
        ##rom name cleaning (to be lower case and keep only alphanumeric characters):
        game_name=game_name.lower()
        game_name = re.sub(r'[^a-z0-9!]+', '', game_name)
        
        ##save lightgun dedicated emulator & core
        Log('selected emulator by user:' + self.system.Emulator)
        Log('selected core by user:' + self.system.Core)
                    
        ##1) first step check if system and game is supporting lightgun or not
        for child in root:
            if child.tag == 'system':
                for platform in child.iter('platform'):
                    if platform.text == system_name:
                        Log('System found: ' + platform.text)
                        emulator = child.find('emulator')
                        Log('Emulator found in system: ' + emulator.attrib["name"])
                        core = emulator.find('core')
                        Log('Core found in system: ' + core.text)
                        Log('Need to find this game: ' + game_name)
                        for games in child.iter('games'):
                            ##initial value for matching follow-up
                            best_matching_lenght = 0
                            best_matching_game_name = ""
                            for game in games.iter('game'):
                                game_pattern = game.find('name')
                                ##as optional we have to avoid error if doesn't exist or empty
                                tested = ""
                                if 'tested' in game.attrib:
                                    tested = game.attrib["tested"]
                                if re.search(game_pattern.text,game_name) and (tested != "nok") :
                                    #a matching found
                                    #keep matching to check if best one exist (to manage better case of a game and its versions ;-)
                                    Log('Pattern that match with game name: ' + game_pattern.text)
                                    if best_matching_lenght < len(game_pattern.text):
                                        best_matching_lenght = len(game_pattern.text)
                                        best_matching_game_name = game_pattern.text
                                        best_matching_game = game
                                        
                            if best_matching_lenght != 0:
                                popup.Message("Lightgun setting identified for '" + best_matching_game_name +  "' !", 3)
                                Log('Game name best match with pattern: ' + best_matching_game_name)
                                #load EMULATOR.INI from share/saves/model2/model2emu/
                                from configgen.settings.iniSettings import IniSettings
                                model2emuSettings = IniSettings(recalboxFiles.model2emuConfigFile, False)
                                model2emuSettings.loadFile(True) \
                                               .defineBool('1', '0')
                                
                                ## now that we are sure to have a matching, we could get the common part
                                ## Mandatory under the root
                                ## N/A for this system today
                                
                                ## Optional under the root
                                ## N/A for this system today
                                
                                ## we could get also the inputs from system
                                ## optional by sytem
                                ## N/A for this system today
                                
                                ## we could get also the options from system
                                ## optional by sytem
                                ## N/A for this system today
                                
                                ## now that we are sure to have a matching, we could get the emulator part first
                                ## optional by games
                                emulator_games = games.find('emulator')
                                if emulator_games is not None:
                                    Log('Games Emulator found: ' + emulator_games.attrib["name"])
                                    emulator = emulator_games
                                    core_games = emulator.find('core')
                                    if core_games is not None:
                                        Log('Games Core found: ' + core_games.text)
                                        core = core_games
                                
                                ## we could get also the inputs from games
                                ## optional by games
                                ## N/A for this system today
                                
                                ## we could get also the options from games
                                ## optional by games
                                Log('Games options to put in EMULATOR.INI from share/saves/model2/model2emu/ :')
                                options = games.find('options')
                                if options is not None:
                                    for string in options.iter('string'):
                                        Log('    [' + string.attrib["section"] + '] ' + string.attrib["name"]+ ' = ' + string.attrib["value"])
                                        section, name, value = self.manageModel2emuCrosshair(string.attrib["section"],string.attrib["name"],string.attrib["value"])
                                        Log('    after [' + section + '] ' + name + ' = ' + value)
                                        model2emuSettings.setString(section, name, value)
                                
                                ## Now for best matching game, we could get the inputs
                                ## optional by game
                                ## N/A for this system today
                                
                                ## and we could get also the options
                                ## optional by game
                                ## N/A for this system today
                                
                                ## and finally we could also have a specific emulator/core for a game
                                ## optional by game
                                emulator_game = best_matching_game.find('emulator')
                                if emulator_game is not None:
                                    Log('Game Emulator found: ' + emulator_game.attrib["name"])
                                    emulator = emulator_game
                                    core_game = emulator.find('core')
                                    if core_game is not None:
                                        Log('Game Core found: ' + core_game.text)
                                        core = core_game

                                ##save lightgun dedicated emulator & core
                                self.system.ChangeEmulatorAndCore(emulator.attrib["name"], core.text)
                                Log('Emulator value :' + self.system.Emulator)
                                Log('Core value :' + self.system.Core)
                                # Save model2emu configuration
                                model2emuSettings.saveWindowsFile()
                                Log('Configured')
                                return 'configured'
                            else:
                                Log('Not Yet Configured')
        Log('Not Configured !!!')                    
        return 'Not configured'

    # To confirm that lightgun game is detected and potentially configure partially at the same time
    def isLightGunGame(self, MouseIndexByPlayer):
        Log('system: ' + self.system.Name)
        Log('rom: ' + self.rom)
        ## finally, need mouse index to determine usage of crosshair or not
        self.MouseIndexByPlayer = MouseIndexByPlayer
        ## get file name from es_state.inf file
        game_name = self.getGameNameFromESState()

        ## return if game is declared as LightGun game
        result = self.setLightGunConfig(self.system.Name,game_name)
        if result == 'configured':
            return True
        else:
            return False

