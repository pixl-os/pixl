import os
import configgen.recalboxFiles as recalboxFiles
from configgen.controllers.inputItem import InputItem
from configgen.controllers.controller import ControllerPerPlayer
from os import path
import codecs
# from utils.logger import get_logger

# eslog = get_logger(__name__)
def Log(txt):
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    #print(txt)
    return 0

def generateControllerConfig(system, playersControllers: ControllerPerPlayer, rom):
    if not path.isdir("{}/rpcs3/input_configs/global".format(recalboxFiles.CONF)):
        os.makedirs("{}/rpcs3/input_configs/global".format(recalboxFiles.CONF))
    configFileName = "{}/{}/Default.yml".format(recalboxFiles.CONF, "rpcs3/input_configs/global")
    f = codecs.open(configFileName, "w", encoding="utf_8_sig")
    
    nplayer = 1
    for playersController, pad in sorted(playersControllers.items()):
        if nplayer <= 7:
            f.write(f"Player {nplayer} Input:\n")
            f.write("  Handler: SDL\n") # f.write("  Handler: Evdev\n") # 
            f.write(f"  Device: {pad.DeviceName} {nplayer}\n")
            f.write("  Config:\n")
            Log( "Device: " + str(pad.DeviceName) + " " + str(nplayer))
            # for input in pad.AvailableInput:
            #     print("input.Type is {}" .format (input.Type))
            #     key = rpcs3_mappingKey(input.Name)
            #     if key is not None:
            #         f.write(f"    {key}: {rpcs3_mappingValue(input.Name, input.type, input.code, int(input.value))}\n")
            #     else:
            #         Log(f"no rpcs3 mapping found for {input.Name}")

            #     # write the reverse
            #     if input.Name == InputItem.ItemJoy1Up or input.Name == InputItem.ItemJoy1Left or input.Name == InputItem.ItemJoy2Up or input.Name == InputItem.ItemJoy2Left:
            #         reversedName = rpcs3_reverseMapping(input.Name)
            #         key = rpcs3_mappingKey(reversedName)
            #         if key is not None:
            #             f.write(f"    {key}: {rpcs3_mappingValue(reversedName, input.type, input.code, int(input.value)*-1)}\n")
            #         else:
            #             Log(f"no rpcs3 mapping found for {input.Name}")

            # rpcs3_otherKeys(f, playersController)
        nplayer += 1
    f.close()

def generateControllerConfigUsio(args, playersControllers: ControllerPerPlayer):
    if args.rom.endswith(".sys357"):
        if not path.isdir(recalboxFiles.CONF + "/rpcs3/config"):
            os.makedirs(recalboxFiles.CONF + "/rpcs3/config")
        configFileName = "{}/{}/usio.yml".format(recalboxFiles.CONF, "/rpcs3/config")
        f = codecs.open(configFileName, "w", encoding="utf_8_sig")

        nplayer = 1
        for playersController, pad in sorted(playersControllers.items()):
            if nplayer <= 5:
                f.write(f"Player {nplayer}:\n")
                f.write("  Test: L3\n")
                f.write("  Coin: Select\n")
                f.write("  Service: R3\n")
                f.write("  Enter/Start: Start\n")
                f.write("  Up: D-Pad Up\n")
                f.write("  Down: D-Pad Down\n")
                f.write("  Left: D-Pad Left\n")
                f.write("  Right: D-Pad Right\n")
                f.write("  Taiko Hit Side Left: Square\n")
                f.write("  Taiko Hit Side Right: Circle\n")
                f.write("  Taiko Hit Center Left: Triangle\n")
                f.write("  Taiko Hit Center Right: Cross\n")
                f.write("  Tekken Button 1: Square\n")
                f.write("  Tekken Button 2: Triangle\n")
                f.write("  Tekken Button 3: Cross\n")
                f.write("  Tekken Button 4: Circle\n")
                f.write("  Tekken Button 5: R1\n")
            nplayer += 1
        f.close()

def rpcs3_reverseMapping(name):
    if name == InputItem.ItemJoy1Up:
        return InputItem.ItemJoy1Down
    if name == InputItem.ItemJoy1Left:
        return InputItem.ItemJoy1Right
    if name == InputItem.ItemJoy2Up:
        return InputItem.ItemJoy2Down
    if name == InputItem.ItemJoy2Left:
        return InputItem.ItemJoy2Right

def rpcs3_mappingKey(padInputs):
    keys = {
        InputItem.ItemStart:        "Start",
        InputItem.ItemSelect:       "Select",
        InputItem.ItemHotkey:       "PS Button",
        InputItem.ItemY:            "Square",
        InputItem.ItemB:            "Cross",
        InputItem.ItemA:            "Circle",
        InputItem.ItemX:            "Triangle",
        InputItem.ItemLeft:         "Left",
        InputItem.ItemDown:         "Down",
        InputItem.ItemRight:        "Right",
        InputItem.ItemUp:           "Up",
        InputItem.ItemL1:           "L1",
        InputItem.ItemR2:           "R2",
        InputItem.ItemR3:           "R3",
        InputItem.ItemR1:           "R1",
        InputItem.ItemL2:           "L2",
        InputItem.ItemL3:           "L3",
        InputItem.ItemJoy1Left:     "Left Stick Left",
        InputItem.ItemJoy1Down:     "Left Stick Down",
        InputItem.ItemJoy1Right:    "Left Stick Right",
        InputItem.ItemJoy1Up:       "Left Stick Up",
        InputItem.ItemJoy2Left:     "Right Stick Left",
        InputItem.ItemJoy2Down:     "Right Stick Down",
        InputItem.ItemJoy2Right:    "Right Stick Right",
        InputItem.ItemJoy2Up:       "Right Stick Up"
    }
    if input in keys:
        return keys[input]
    return None

def rpcs3_mappingValue(name, type, code, value):
    if   type == "button":
        return code
    elif type == "hat":
        if value == 1:
            return -1017
        if value == 2:
            return 1016
        if value == 4:
            return 1017
        if value == 8:
            return -1016
        raise Exception(f"invalid hat value {value}")
    elif type == "axis":
        if code is None:
            return ""
        res = int(code)+1000
        if value < 0:
            # eslog.debug(f"name = {name} and value = {value}")
            res = res * -1
        return res
    return None

def rpcs3_otherKeys(f, controller):
    f.write("    Left Stick Multiplier: 100\n")
    f.write("    Right Stick Multiplier: 100\n")
    f.write("    Left Stick Deadzone: 5000\n")
    f.write("    Right Stick Deadzone: 5000\n")
    f.write("    Left Trigger Threshold: 100\n")
    f.write("    Right Trigger Threshold: 100\n")
    f.write("    Left Pad Squircling Factor: 5000\n")
    f.write("    Right Pad Squircling Factor: 5000\n")
    f.write("    Color Value R: 0\n")
    f.write("    Color Value G: 0\n")
    f.write("    Color Value B: 20\n")
    f.write("    Blink LED when battery is below 20%: true\n")
    f.write("    Use LED as a battery indicator: false\n")
    f.write("    LED battery indicator brightness: 10\n")
    f.write("    Enable Large Vibration Motor: false\n")
    f.write("    Enable Small Vibration Motor: false\n")
    f.write("    Switch Vibration Motors: false\n")
    f.write("    Mouse Deadzone X Axis: 60\n")
    f.write("    Mouse Deadzone Y Axis: 60\n")
    f.write("    Mouse Acceleration X Axis: 200\n")
    f.write("    Mouse Acceleration Y Axis: 250\n")
    f.write("    Left Stick Lerp Factor: 100\n")
    f.write("    Right Stick Lerp Factor: 100\n")
    f.write("    Analog Button Lerp Factor: 100\n")
    f.write("    Trigger Lerp Factor: 100\n")
    f.write("    Device Class Type: 0\n")
    