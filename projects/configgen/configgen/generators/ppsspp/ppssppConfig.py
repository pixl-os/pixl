#!/usr/bin/env python
import configgen.recalboxFiles as recalboxFiles
from configgen.settings.keyValueSettings import keyValueSettings
from configgen.Emulator import Emulator
from configgen.utils.videoMode import GetHasVulkanSupport

def writePPSSPPConfig(system: Emulator):

    # https://github.com/hrydgard/ppsspp/blob/master/Core/HLE/sceUtility.h#L22
    PPSSPP_LANGUAGES = \
    {
        "jp": 0,
        "en": 1,
        "fr": 2,
        "es": 3,
        "de": 4,
        "it": 5,
        "du": 6,
        "pt": 7,
        "ru": 8,
        "kr": 9,
        "ch": 10,
    }

    conf = keyValueSettings(recalboxFiles.recalboxConf)
    conf.loadFile(True)

    # Nickname Netplay
    nickname = conf.getString("global.netplay.nickname", "pixL")
    # Get Video driver
    videoDriver = "3 (VULKAN)" if GetHasVulkanSupport() == '1' else "0 (OPENGL)"
    language = conf.getString("system.language", "en_US")
    pspLanguages = language[0:2].lower()
    internalResolution = conf.getInt("ppsspp.resolution", 1)
    multiSampleLevel = conf.getInt("ppsspp.msaa", 0)
    shaderChainRequires60FPS = conf.getInt("ppsspp.force.60fps", 0)
    texScalingLevel = conf.getInt("ppsspp.texture.scaling.level", 0)
    texScalingType = conf.getInt("ppsspp.texture.scaling.type", 0)
    anisotropyLevel = conf.getInt("ppsspp.anisotropy.level", 0)
    textureFiltering = conf.getInt("ppsspp.texture.filter", 0)
    textureShader = conf.getInt("ppsspp.texture.shader", 0)

    if pspLanguages in PPSSPP_LANGUAGES:
        pspLanguageCode = pspLanguages
    else:
        pspLanguageCode = "en"

    setPspLanuguage = PPSSPP_LANGUAGES[pspLanguageCode] if pspLanguageCode in PPSSPP_LANGUAGES else 1 # en
    
    SECTION_GRAPHICS = "Graphics"
    SECTION_GENERAL = "General"
    SECTION_NETWORK = "Network"
    SECTION_SYSTEM = "SystemParam"

    from configgen.settings.iniSettings import IniSettings
    settings = IniSettings(recalboxFiles.ppssppConfig, True)
    settings.loadFile(True) \
            .defineBool('True', 'False')

    # Display FPS
    # 1 for Speed%, 2 for FPS, 3 for both
    settings.setInt(SECTION_GRAPHICS, 'ShowFPSCounter', 3 if system.ShowFPS else 0) \
            .setString(SECTION_GRAPHICS, 'GraphicsBackend', videoDriver) \
            .setInt(SECTION_GRAPHICS, 'InternalResolution', internalResolution) \
            .setBool(SECTION_GRAPHICS, 'VSyncInterval', True) \
            .setBool(SECTION_GRAPHICS, 'ShaderChainRequires60FPS', shaderChainRequires60FPS) \
            .setInt(SECTION_GRAPHICS, 'MultiSampleLevel', multiSampleLevel) \
            .setInt(SECTION_GRAPHICS, 'TexScalingLevel', texScalingLevel) \
            .setInt(SECTION_GRAPHICS, 'TexScalingType', texScalingType) \
            .setInt(SECTION_GRAPHICS, 'AnisotropyLevel', anisotropyLevel) \
            .setInt(SECTION_GRAPHICS, 'TextureFiltering', textureFiltering) \
            .setInt(SECTION_GRAPHICS, 'TextureShader', textureShader)

    # Force cheat enable
    settings.setBool(SECTION_GENERAL, 'EnableCheats', True) \
            .setString(SECTION_GENERAL, 'Language', language)
    # Prepare Network configuratoin for futur support
    settings.setBool(SECTION_NETWORK, 'EnableAdhocServer', True) \
            .setBool(SECTION_NETWORK, 'EnableUPnP', True) \
            .setBool(SECTION_NETWORK, 'EnableWlan', True) \
            .setBool(SECTION_NETWORK, 'ForcedFirstConnect', False) \
            .setInt(SECTION_NETWORK, 'PortOffset', 10000) \
            .setBool(SECTION_NETWORK, 'UPnPUseOriginalPort', False) \
            .setString(SECTION_NETWORK, 'proAdhocServer', "socom.cc")

    settings.setString(SECTION_SYSTEM, 'NickName', nickname) \
            .setString(SECTION_SYSTEM, 'Language', setPspLanuguage)

    # Save Config
    settings.saveFile()
