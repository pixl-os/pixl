#!/usr/bin/env python
## Released version: 2.0.0
##IMPORT STD---------------------------------------------------------------------
import os
import sys
import time
import logging
import subprocess
import linecache

##IMPORT RECALBOX
import configgen.recalboxFiles as recalboxFiles
from configgen.settings.keyValueSettings import keyValueSettings

##IMPORT for parsing/xml functions
##for regular expression
import re
##for XML parsing
import xml.etree.ElementTree as ET
from xml.dom import minidom
import codecs

## remove "//" for robustness (if we move from HOME_INIT to HOME later)
pegasusLightGunFromShareInit = recalboxFiles.pegasusLightGun.replace("//", "/")
pegasusLightGunFromShare = recalboxFiles.pegasusLightGunFromShare.replace("//", "/")

# Information game from ES
GAME_INFO_PATH = "/tmp/es_state.inf"
# Key in informatiojn files
KEY_GAME_NAME = "Game"

##Versions history:
##
## V1: initial version
##
## V1.1: to manage several platform in same system: to regroup for example naomi/naomigd/atomiswave
## example : 
##    <system>
##        <platform>naomi</platform>
##        <platform>naomigd</platform>
##    </system>
##
## V1.2: to manage group of games with dedicated inputs/options/emulator/core - new tag <games> is mandatory but we could have several by system
## example : 
##    <games>
##      <emulator name="libretro">
##          <core>flycast</core> 
##      </emulator>
##      <inputs>
##      </inputs>
##      <options>
##      </options>
##    <game>
##            <name>exampleofgamename</name>
##        </game>
##        <game>
##            <name>exampleofgamename2</name>
##        </game>
##    </games>
##
## V1.3: For mame: to manage piority of core selected depending of systems availability
## example: (possible in system, games and game level)
## if mame, we search emulator by emulator already selected with the good romset.
## <system>
##      <platform>mame</platform>
##      <emulator name="libretro">
##          <core>mame</core> 
## </system>
## <system>
##      <platform>mame</platform>
##      <emulator name="libretro">
##          <core>mame2003_plus</core> 
##      </emulator>
## </system>
##
## V1.4: To find dynamically the event index to help to set mouse index in retroarch
## a) Creation of function "findDolphinBarIndexes"
## b) if no dolphin bar found, no override done
## c) if no lightgun.cfg exists, no override done
## d) bug corrected: replace of re.search by IN command for search in nodes of gamelist.xml and using encoding to UTF-8
## 
## V1.5:
## Usage of keyword value from CGF file to be replace dynamically by index (event value) of mouse found.
## a) KEYWORDS list to replace dynamically:
## GUNP1 or gunp1
## GUNP2 or gunp2
## GUNP3 or gunp3
## b) SetInputs function: created to manage replace of GUNP1/2/3 by MouseIndexes
## c) CancelDevicesNotUsed function: created to set to 0 input devices not used finally (to avoid too cursors on screen ;-)!
##
## V1.6.0 : To force mouse index to 0 for player 1 on PC X86_64 using Xorg, only 1 player possible on PC for the moment
## V1.6.1 : To force mouse index to 0 for player 1 on all platforms to avoid a bug of indexation when we have only one player.
## V1.6.2 : To manage system directories better as for "Ports"/"amstradcpc" and add robustnees/performance optimization.
## V1.6.3 : To get name of game from "/tmp/es_state.inf" and to avoid to use gamelist files (too slow) 
## V1.6.4 : To change way to recalculate the indexation of mouse for retroarch (using udev database)
## V1.6.5 : Cleaning + deactivate log by default + to support some keyboards including mouse and identified differently by retroarch (now, we count only header values as js + input + event to anticipate indexation)
## V1.6.6 : To support "!" characters in filename to search (remove of _ in regex also)
## V1.6.7: To patch index calculation to be compatible from retroarch 1.9.8
## V1.6.8: some fixs on emulator/core objects + get/setOption changed since recal 8.0
## V2.0.0: new architecture to manage several types of lightgun and mutualize between generators in Controller class

def Log(txt):
    import datetime
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    #print(formatted_time[:-3] + " - " + txt)
    return 0

#---------------------------------------------------------------------- xml functions --------------------------------------------------------
def getRoot(config, name):
    xml_section = config.getElementsByTagName(name)

    if len(xml_section) == 0:
        xml_section = config.createElement(name)
        config.appendChild(xml_section)
    else:
        xml_section = xml_section[0]

    return xml_section

def getSection(config, xml_root, name):
    xml_section = xml_root.getElementsByTagName(name)

    if len(xml_section) == 0:
        xml_section = config.createElement(name)
        xml_root.appendChild(xml_section)
    else:
        xml_section = xml_section[0]

    return xml_section

def removeSection(config, xml_root, name):
    xml_section = xml_root.getElementsByTagName(name)

    for i in range(0, len(xml_section)):
        old = xml_root.removeChild(xml_section[i])
        old.unlink()
#---------------------------------------------------------------------------------------------------------------------------------------------


class libretroLightGun:
    
    # constructor
    def __init__(self, system, rom, demo, retroarchConfig, coreConfig, MouseIndexByPlayer, recalboxOptions):
        # initial settings
        self.retroarchConfig = retroarchConfig
        self.recalboxOptions = recalboxOptions
        self.coreConfig = coreConfig
        self.system = system
        self.rom = rom
        self.demo = demo
        ## init for all indexes
        self.MouseIndexByPlayer = MouseIndexByPlayer
        self.pegasusLightGun = ""

    #find game name from /tmp/es_state.inf 
    def getGameNameFromESState(self):
        gameInfo = keyValueSettings(GAME_INFO_PATH, False)
        gameInfo.loadFile(True)
        game_name = gameInfo.getString(KEY_GAME_NAME, "Unknown")
        del gameInfo
        if game_name != '':
            Log('Game name found, the game name is :' + game_name)
        else:
            Log('System/Game name are not set properly from gamelist/from ES or may be not scrapped ? or empty name ? the game name content is empty ')
        return game_name

    #set inputs depending of keyword set or simple value
    def SetInputs(self,name,value):
        Log('    ' + name + ' = ' + value)
        if ("gunp" in value.lower()):
            index = int(value.lower().strip().split('gunp')[1])-1
            ##update value with mouse index of gun for this player
            value = self.MouseIndexByPlayer[index][0]
            if (value == "nul"):
                value = "99" ##to avoid to take nul value as "0" default value
                Log('       value set to "99" to avoid issue if we put "nul" !')
            Log('       updated with Mouse Index: ' + name + ' = ' + value)
        self.retroarchConfig.setString(name,value)

    #to cancel input device (parameter "input_libretro_device_pX") if the corresponding "input_playerX_mouse_index" is already set to 99 by SetInputs function
    def CancelDevicesNotUsed(self):
        for i in range(4):
            value = "0"
            value = self.retroarchConfig.getString("input_player" + str(i) + "_mouse_index",value)
            #check if we identified this index as not necessary
            if(value == "99"):
                Log('Mouse index cancelled: ' + "input_player" + str(i) + "_mouse_index" + ' = ' + value)
                Log('Device cancelled and options linked: ' + "input_libretro_device_p" + str(i) + ' = ' + '0')
                self.retroarchConfig.setString("input_libretro_device_p" + str(i),"0")
                self.retroarchConfig.setString("input_player" + str(i) + "_gun_start","nul")
                self.retroarchConfig.setString("input_player" + str(i) + "_gun_select","nul")
                self.retroarchConfig.setString("input_player" + str(i) + "_select","nul")
                self.retroarchConfig.setString("input_player" + str(i) + "_start","nul")
                self.retroarchConfig.setString("input_player" + str(i) + "_gun_dpad_up","nul")
                self.retroarchConfig.setString("input_player" + str(i) + "_gun_dpad_down","nul")
                self.retroarchConfig.setString("input_player" + str(i) + "_gun_dpad_right","nul")
                self.retroarchConfig.setString("input_player" + str(i) + "_gun_dpad_left","nul")

    def reverseCrossValue(self, name, value) -> str :
        Log("reverseCrossValue - name: " + name)
        Log("reverseCrossValue - initial value: " + value)
        #for mame 2003 plus or megadrive/master system
        if("enabled" in value):
            value = "disabled"
        #For mame 2003 plus or nes
        elif("disabled" in value):
            value = "enabled"
        #For flycast
        elif("Red" in value):
            value = "disabled"
        elif("Blue" in value):
            value = "disabled"
        #For beetle_saturn
        elif("beetle_saturn_virtuagun_crosshair" in name and "cross" in value):
            value = "Off"
        #For beetle_psx
        elif("beetle_psx_gun_cursor" in name and "cross" in value):
            value = "off"
        #for fbneo
        elif("0" in value):
            value = "1"
        elif("1" in value):
            value = "0"
        #For SNES/Justifier
        elif(value.isdigit()):
            if(int(value) > 0):
                value = "0"
            else:
                value = ""
        #For PSX or other case
        else: 
            value = ""
        Log("reverseCrossValue - new value: " + value)
        return value

    def manageRetroarchCrosshair(self, name, value) -> (str, str) :
        #if 'cross' or 'cursor' term in name/value to detect and manage crosshair
        if("cross" in name or "cross" in value or "cursor" in name):
            #if player 1 detected
            if("1" in name):
                if(("sinden" in self.MouseIndexByPlayer[0][1]) and (not self.recalboxOptions.getBool("lightgun.sinden.crossair.enabled", False))):
                    value = self.reverseCrossValue(name, value)
            #if player 2 detected
            elif("2" in name):
                if(("sinden" in self.MouseIndexByPlayer[1][1]) and (not self.recalboxOptions.getBool("lightgun.sinden.crossair.enabled", False))):
                    value = self.reverseCrossValue(name, value)
            #if player 3 detected - RFU
            elif("3" in name):
                if(("sinden" in self.MouseIndexByPlayer[2][1]) and (not self.ecalboxOptions.getBool("lightgun.sinden.crossair.enabled", False))):
                    value = self.reverseCrossValue(name, value)
            else:
                initialvalue = value
                for row in self.MouseIndexByPlayer:
                    if("mayflash" in row[1]):
                        #we restore initial value in this case to keep cross for wiimote with dolphinbar
                        value = initialvalue
                        break
                    elif(("sinden" in row[1]) and (not self.recalboxOptions.getBool("lightgun.sinden.crossair.enabled", False))):
                        value = self.reverseCrossValue(name, initialvalue)
        return name, value

    def generateMameCrosshairConfig(self):
        Log('system: ' + self.system.Name)
        Log('rom: ' + self.rom)
        cfgPath = recalboxFiles.libretroMameCFG
        rompath = recalboxFiles.ROMS + "/" + self.system.Name
        
        # simplify the rom name (strip the directory & extension)
        rom = self.rom
        romname = rom.replace(rompath + "/", "")
        smplromname = romname.replace(".zip", "")
        rom = smplromname.split('/', 1)[-1]
        # config file
        config = minidom.Document()
        configFile = cfgPath + "/" + rom + ".cfg"
        Log('configFile: ' + configFile)
        #load existing conf if exists
        if os.path.exists(configFile):
            try:
                config = minidom.parse(configFile)
            except:
                pass # reinit the file
        xml_mameconfig = getRoot(config, "mameconfig")
        xml_mameconfig.setAttribute("version", "10") # otherwise, config of pad won't work at first run
        xml_system     = getSection(config, xml_mameconfig, "system")
        xml_system.setAttribute("name", rom)
        #to remove crosshairs section if already exists before to recreate
        removeSection(config, xml_system, "crosshairs")
        xml_crosshairs = config.createElement("crosshairs")
        for index, row in enumerate(self.MouseIndexByPlayer):
            xml_crosshair = config.createElement("crosshair")
            xml_crosshair.setAttribute("player", str(index))
            if("mayflash" in row[1]):
                #we force crosshair for wiimote/dolphin bar
                xml_crosshair.setAttribute("mode", "1")
            elif(("sinden" in row[1]) and (not self.recalboxOptions.getBool("lightgun.sinden.crossair.enabled", False))):
                #we deactivate crosshair for sinden if crosshair is not enabled
                xml_crosshair.setAttribute("mode", "0")
            elif(("sinden" in row[1]) and (self.recalboxOptions.getBool("lightgun.sinden.crossair.enabled", False))):
                #we activate crosshair for sinden if crosshair is enabled
                xml_crosshair.setAttribute("mode", "1")
            else:
                #we deactivate crosshair if nothing define
                xml_crosshair.setAttribute("mode", "0")
            xml_crosshairs.appendChild(xml_crosshair)
        xml_system.appendChild(xml_crosshairs)
        # save the config file
        Log(f"Saving {configFile}")
        mameXml = codecs.open(configFile, "w", "utf-8")
        dom_string = os.linesep.join([s for s in config.toprettyxml().splitlines() if s.strip()]) # remove ugly empty lines while minicom adds them...
        mameXml.write(dom_string)

    # Load all configurations from the lightgun.cfg
    def SetLightGunConfig(self, system_name,game_name,game_rom):
        if(game_name == ''):        
            Log('Game name empty, Not Configured !!!')
            return 'Not configured'
        systems = dict()
        tree = ET.parse(self.pegasusLightGun)
        root = tree.getroot()
        
        ##rom name cleaning (to be lower case and keep only alphanumeric characters):
        game_name=game_name.lower()
        game_name = re.sub(r'[^a-z0-9!]+', '', game_name)
        
        ##save lightgun dedicated emulator & core
        Log('selected emulator by user:' + self.system.Emulator)
        Log('selected core by user:' + self.system.Core)
                    
        ##1) first step check if system and game is supporting lightgun or not
        core = None
        emulator = None
        for child in root:
            if child.tag == 'system':
                for platform in child.iter('platform'):
                    if platform.text == system_name:
                        Log('System found: ' + platform.text)
                        emulator = child.find('emulator')
                        Log('Emulator found in system: ' + emulator.attrib["name"])
                        core = emulator.find('core')
                        Log('Core found in system: ' + core.text)
                        Log('Need to find this game: ' + game_name)
                        if(emulator.attrib["name"] != "libretro"):
                            # emulator not compatible with Libretro (seems a standalone)
                            continue
                        for games in child.iter('games'):
                            ##initial value for matching follow-up
                            best_matching_lenght = 0
                            best_matching_game_name = ""
                            for game in games.iter('game'):
                                game_pattern = game.find('name')
                                rom_pattern = None
                                roms_pattern = game.find('roms')
                                ##as optional we have to avoid error if doesn't exist or empty
                                tested = ""
                                if 'tested' in game.attrib:
                                    tested = game.attrib["tested"]
                                if re.search(game_pattern.text,game_name):
                                    #a matching found
                                    #keep matching to check if best one exist (to manage better case of a game and its versions ;-)
                                    Log('Pattern that match with game name: ' + game_pattern.text)
                                    rom_matched = True
                                    # if <roms> tag exists in xml file for this game, we have to test if rom_name is matching
                                    if (roms_pattern != None):
                                        rom_matched = re.search(game_rom, roms_pattern.text)
                                    if (best_matching_lenght < len(game_pattern.text)) and rom_matched :
                                        best_matching_lenght = len(game_pattern.text)
                                        best_matching_game_name = game_pattern.text
                                        best_matching_game = game
                                        best_matching_game_tested = tested
                                        
                            if (best_matching_lenght != 0) and (best_matching_game_tested != "nok"):
                                Log('Game name best match with pattern: ' + best_matching_game_name)
                                ## now that we are sure to have a matching, we could get the common part
                                ## Mandatory under the root
                                common_inputs = root.find('common_inputs')
                                Log('Common inputs part found to put in retroarchcustom.cfg:')
                                for string in common_inputs.iter('string'):
                                    Log('    ' + string.attrib["name"]+ ' = ' + string.attrib["value"])
                                    self.SetInputs(string.attrib["name"], string.attrib["value"])
                                
                                ## Optional under the root
                                common_options = root.find('common_options')
                                Log('Common options part found to put in retroarch-core-options.cfg:')
                                for string in common_options.iter('string'):
                                    Log('    ' + string.attrib["name"]+ ' = ' + string.attrib["value"])
                                    name, value = self.manageRetroarchCrosshair(string.attrib["name"], string.attrib["value"])
                                    self.coreConfig.setString(name, value)
                                
                                ## we could get also the inputs from system
                                ## optional by sytem
                                Log('System inputs to put in retroarchcustom.cfg :')
                                inputs = child.find('inputs')
                                if inputs is not None:
                                    for string in inputs.iter('string'):
                                        Log('    ' + string.attrib["name"]+ ' = ' + string.attrib["value"])
                                        name, value = self.manageRetroarchCrosshair(string.attrib["name"], string.attrib["value"])
                                        self.SetInputs(name, value)
                                
                                ## we could get also the options from system
                                ## optional by sytem
                                Log('System options to put in retroarch-core-options.cfg :')
                                options = child.find('options')
                                if options is not None:
                                    for string in options.iter('string'):
                                        Log('    ' + string.attrib["name"]+ ' = ' + string.attrib["value"])
                                        name, value = self.manageRetroarchCrosshair(string.attrib["name"], string.attrib["value"])
                                        Log('  after: ' + name + ' = ' + value)
                                        self.coreConfig.setString(name,'"' + value + '"')
                                                                
                                ## now that we are sure to have a matching, we could get the emulator part first
                                ## optional by games
                                emulator_games = games.find('emulator')
                                if emulator_games is not None:
                                    Log('Games Emulator found: ' + emulator_games.attrib["name"])
                                    emulator = emulator_games
                                    core_games = emulator.find('core')
                                    if core_games is not None:
                                        Log('Games Core found: ' + core_games.text)
                                        core = core_games
                                
                                ## we could get also the inputs from games
                                ## optional by games
                                Log('Games inputs to put in retroarchcustom.cfg :')
                                inputs = games.find('inputs')
                                if inputs is not None:
                                    for string in inputs.iter('string'):
                                        Log('    ' + string.attrib["name"]+ ' = ' + string.attrib["value"])
                                        name, value = self.manageRetroarchCrosshair(string.attrib["name"], string.attrib["value"])
                                        self.SetInputs(name, value)
                                
                                ## we could get also the options from games
                                ## optional by games
                                Log('Games options to put in retroarch-core-options.cfg :')
                                options = games.find('options')
                                if options is not None:
                                    for string in options.iter('string'):
                                        Log('    ' + string.attrib["name"]+ ' = ' + string.attrib["value"])
                                        name, value = self.manageRetroarchCrosshair(string.attrib["name"], string.attrib["value"])
                                        Log('  after: ' + name + ' = ' + value)
                                        self.coreConfig.setString(name,'"' + value + '"')
                                
                                ## Now for best matching game, we could get the inputs
                                ## optional by game
                                inputs = best_matching_game.find('inputs')
                                Log('Game inputs to put in retroarchcustom.cfg :')
                                if inputs is not None:
                                    for string in inputs.iter('string'):
                                        Log('    ' + string.attrib["name"]+ ' = ' + string.attrib["value"])
                                        name, value = self.manageRetroarchCrosshair(string.attrib["name"], string.attrib["value"])
                                        self.SetInputs(name, value)
                                
                                ## and we could get also the options
                                ## optional by game
                                options = best_matching_game.find('options')
                                Log('Game options to put in retroarch-core-options.cfg :')
                                if options is not None:
                                    for string in options.iter('string'):
                                        Log('    ' + string.attrib["name"]+ ' = ' + string.attrib["value"])
                                        name, value = self.manageRetroarchCrosshair(string.attrib["name"], string.attrib["value"])
                                        self.coreConfig.setString(name,'"' + value + '"')
                                
                                ## and finally we could also have a specific emulator/core for a game
                                ## optional by game
                                emulator_game = best_matching_game.find('emulator')
                                if emulator_game is not None:
                                    Log('Game Emulator found: ' + emulator_game.attrib["name"])
                                    emulator = emulator_game
                                    core_game = emulator.find('core')
                                    if core_game is not None:
                                        Log('Game Core found: ' + core_game.text)
                                        core = core_game
                                ##Cancel Devices where mouse index is finally not set (no enough dolphinbar/lightgun available)
                                self.CancelDevicesNotUsed()
                                ##save both configurations
                                self.retroarchConfig.saveFile()
                                self.coreConfig.saveFile()
                                ##save lightgun dedicated emulator & core
                                self.system.ChangeEmulatorAndCore(emulator.attrib["name"], core.text)
                                Log('Emulator value :' + self.system.Emulator)
                                Log('Core value :' + self.system.Core)
                                if(self.system.Core == "mame"): self.generateMameCrosshairConfig()
                                Log('Configured')
                                return 'configured'
                            else:
                                Log('Not Yet Configured')
        Log('Not Configured !!!')                    
        return 'Not configured'

    def createLightGunConfiguration(self):
        Log('system: ' + self.system.Name)
        Log('rom: ' + self.rom)
        
        if self.MouseIndexByPlayer[0][0] == "nul":
            ##no dolphin bar or other lightgun found, no need to overload configuration in this case
            Log('No lightgun device found !!!')
            Log('Not Configured !!!')                    
            return 'Not configured'
        
        ## to check if lightgun.cfg exists from share or share_init
        if os.path.exists(pegasusLightGunFromShare) == False:
            ##no xml configuration file found for lightgun
            Log('Info: No lightgun.cfg xml file found from share (optional)')
            ## to check if lightgun.cfg exists or not
            if os.path.exists(pegasusLightGunFromShareInit) == False:
                ##no xml configuration file found for lightgun
                Log('No lightgun.cfg xml file found from share_init !!! (mandatory)')
                Log('Not Configured !!!')                    
                return 'Not configured'
            else:
                #Use one from the share_init in this case
                self.pegasusLightGun = pegasusLightGunFromShareInit
        else:
            #Use custom one from share and not share_init in this case
            self.pegasusLightGun = pegasusLightGunFromShare
            
        ## get file name from es_state.inf file
        game_name = self.getGameNameFromESState()
        #to uncomment for test purpose only when configgen is launched in command line
        #game_name = "Duck Hunt (World)"
        #game_name = "Alien3: The Gun (World)"
        #game_name = "Time Crisis (Europe)"
        #game_name = "The House Of The Dead 2(USA)"
        #game_name = "Space Gun(Europe)"
        #game_name = "The House Of The Dead [EU)"
        
        # simplify the rom name (strip the directory & extension)
        rom = self.rom
        rompath = recalboxFiles.ROMS + "/" + self.system.Name
        romname = rom.replace(rompath + "/", "")
        smplromname = romname.replace(".zip", "")
        rom_name = smplromname.split('/', 1)[-1]
        
        ## set LightGun Configuration for the game selected
        result = self.SetLightGunConfig(self.system.Name,game_name, rom_name)
        return result
