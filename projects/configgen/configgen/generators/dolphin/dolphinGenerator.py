#!/usr/bin/env python
from configgen.Command import Command
import configgen.recalboxFiles as recalboxFiles
from configgen.Emulator import Emulator
from configgen.controllers.controller import ControllerPerPlayer
from configgen.generators.Generator import Generator
from configgen.settings.keyValueSettings import keyValueSettings
import configgen.generators.dolphin.dolphinControllers as dolphinControllers
from configgen.utils.videoMode import GetHasVulkanSupport
import configgen.generators.dolphin.dolphinSYSCONF as dolphinSYSCONF
#utils to manage advanced overlays features for all emulators
import configgen.utils.overlays as overlays

def Log(txt):
    import datetime
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    print(formatted_time[:-3] + " - " + txt)
    return 0

class DolphinGenerator(Generator):

    # Game Ratio
    GAME_RATIO = \
    {
       "auto": 0,
       "16/9": 1, 
       "4/3": 2,
       "squarepixel": 3,
    }

    # Gamecube languages
    #0 = English/Japanese, depending on the region of the console/game.
    #1 = German
    #2 = French
    #3 = Spanish
    #4 = Italian
    #5 = Dutch
    GAMECUBE_LANGUAGES = \
    {
        "en": 0,
        "de": 1,
        "fr": 2,
        "es": 3,
        "it": 4,
        "du": 5,
    }

    # Wii languages
    #0 = Japanese
    #1 = English
    #2 = German
    #3 = French
    #4 = Spanish
    #5 = Italian
    #6 = Dutch
    #7 = Simplified Chinese
    #8 = Traditional Chinese
    #9 = Korean
    WII_LANGUAGES = \
    {
        "jp": 0,
        "en": 1,
        "de": 2,
        "fr": 3,
        "es": 4,
        "it": 5,
        "du": 6,
        "zh": 7, # traditional chinese 8, ignored
        "kr": 9,
    }

    # Neplay servers
    NETPLAY_SERVERS = \
    {
        # Chine
        "CN": "CH",
        "TW": "CH",
        # Est Asia
        "KR": "EA",
        "RU": "EA",
        "JP": "EA",
        # Europe
        "GB": "EU",
        "DE": "EU",
        "FR": "EU",
        "ES": "EU",
        "IT": "EU",
        "PT": "EU",
        "TR": "EU",
        "SU": "EU",
        "NO": "EU",
        "NL": "EU",
        "PL": "EU",
        "HU": "EU",
        "CZ": "EU",
        "GR": "EU",
        "LU": "EU",
        "LV": "EU",
        "SE": "EU",
        # South America
        "BR": "SA",
        # North America
        "US": "NA",
    }
    ## Setting on dolphin.ini
    SECTION_ANALYTICS = "Analytics"
    SECTION_BTPASSTHROUGH = "BluetoothPassthrough"
    SECTION_CONTROLS = "Controls"
    SECTION_CORE = "Core"
    SECTION_GAMELIST = "GameList"
    SECTION_GENERAL = "General"
    SECTION_INTERFACE = "Interface"
    SECTION_NETPLAY = "NetPlay"
    SECTION_DISPLAY = "Display"
    SECTION_DSP = "DSP"
    ## Setting on GFX.ini
    SECTION_SETTINGS = "Settings"
    SECTION_HARDWARE = "Hardware"
    SECTION_ACHIEVEMENTS = "Achievements"

    # Check if Gamecube bios IPL.bin in folders
    @staticmethod
    def CheckGamecubeIpl() -> str:
        ipl0 = "/recalbox/share/bios/gamecube/EUR/IPL.bin"
        ipl1 = "/recalbox/share/bios/gamecube/JP/IPL.bin"
        ipl2 = "/recalbox/share/bios/gamecube/USA/IPL.bin"
        # if os.path? return "True" set "False" for disable "SkipIpl=Video boot intro"
        import os.path
        if os.path.exists(ipl0) or os.path.exists(ipl1) or os.path.exists(ipl2):
            return "False"
        else:
            return "True"

    # Set Wii sensor bar position in SYSCONF if wii.sensorbar.position=0 (bottom) or =1 (top) set in recalbox.conf
    # (source : https://wiibrew.org/wiki//shared2/sys/SYSCONF#BT_Settings)
    @staticmethod
    def SetSensorBarPosition():
        conf = keyValueSettings(recalboxFiles.recalboxConf)
        conf.loadFile(True)
        sensorBarPosition = conf.getString("wii.sensorbar.position", "$")
        keyValue = '\x00'.encode('utf-8') if sensorBarPosition == '0' else '\x01'.encode('utf-8')
        keyString = "BT.BAR".encode('utf-8')
        import os.path
        if os.path.exists(recalboxFiles.dolphinSYSCONF):
            with open(recalboxFiles.dolphinSYSCONF, 'rb+') as sysconf:
                buf = sysconf.read()
                keyAddr = buf.find(keyString) + len(keyString)
                sysconf.seek(keyAddr)
                sysconf.write(keyValue)

    def mainConfiguration(self, system: Emulator, recalboxOptions: keyValueSettings, args):
        # read recalbox.conf for advanced configuration
        language =  recalboxOptions.getString("system.language", "en")[0:2].lower()
        systemLanguage = recalboxOptions.getString("system.language", "US")[2:2].upper()
        # GameCube Controller Adapter for Wii U in Dolphin controllers if realgamecubepads=1 set in recalbox.conf
        padsgamecube = recalboxOptions.getString("gamecube.realgamecubepads", "$")
        cheats = recalboxOptions.getString("dolphin.cheats", "0") # Disable on default
        nickname = recalboxOptions.getString("global.netplay.nickname", "pixL")
        disc_change = recalboxOptions.getString("dolphin.disc.change", "0")

        # Set video renderer OpenGl / vulkan		
        videoDriver = "Vulkan" if GetHasVulkanSupport() == '1' else "OGL"

        # set languageCode variable
        Log("language:{}".format(language));
        if language in self.GAMECUBE_LANGUAGES:
            languageCode = language
        else:
            languageCode = "en"

        Log("languageCode:{}".format(languageCode));
        
        # set systemLanguage variable
        Log("systemLanguage:{}".format(systemLanguage));
        gamecubeLanguage = self.GAMECUBE_LANGUAGES[languageCode] if languageCode in self.GAMECUBE_LANGUAGES else 0 #en
        
        # set gamecubeLanguage variable
        Log("gamecubeLanguage:{}".format(gamecubeLanguage));
        wiiLanguage = self.WII_LANGUAGES[languageCode] if languageCode in self.WII_LANGUAGES else 1 #en
        Log("wiiLanguage:{}".format(wiiLanguage));
        # Get Netplay configs
        lobbyServer = self.NETPLAY_SERVERS[systemLanguage] if systemLanguage in self.NETPLAY_SERVERS else None
        ## Get if have all IPL.bin in folders Gc bios
        biosGamecube = self.CheckGamecubeIpl()
        
        # Load Configuration
        from configgen.settings.iniSettings import IniSettings
        dolphinSettings = IniSettings(recalboxFiles.dolphinIni, True)
        dolphinSettings.loadFile(True) \
                       .defineBool('True', 'False')

        # Resolution
        from configgen.utils.resolutions import ResolutionParser
        resolution = ResolutionParser(system.VideoMode)
        if resolution.isSet and resolution.selfProcess:
            dolphinSettings.setString(self.SECTION_DISPLAY, "FullscreenResolution", resolution.string)
            dolphinSettings.setBool(self.SECTION_ANALYTICS, "Fullscreen", True)
        # Logging
        dolphinSettings.setBool(self.SECTION_DSP, "CaptureLog", args.verbose)
        # Force audio backend on Pulse Audio
        dolphinSettings.setString(self.SECTION_DSP, "Backend", "Pulse")
        # Analytics
        dolphinSettings.setBool(self.SECTION_ANALYTICS, "Enabled", True)
        dolphinSettings.setBool(self.SECTION_ANALYTICS, "PermissionAsked", True)
        # BluetoothPasstrough
        dolphinSettings.setBool(self.SECTION_BTPASSTHROUGH, "Enabled", False)
        # Core
        dolphinSettings.setInt(self.SECTION_CORE, "SelectedLanguage", gamecubeLanguage) #Gamecube
        dolphinSettings.setInt(self.SECTION_CORE, "AutoDiscChange", disc_change)
        # for Wii, the language is setable in sysconf file
        dolphinSettings.setBool(self.SECTION_CORE, "WiimoteContinuousScanning", True)
        dolphinSettings.setBool(self.SECTION_CORE, "WiiKeyboard", False)
        dolphinSettings.setString(self.SECTION_CORE, "SkipIpl", biosGamecube)
        dolphinSettings.setInt(self.SECTION_CORE, "SIDevice0", 12 if padsgamecube == '1' else 6)
        dolphinSettings.setInt(self.SECTION_CORE, "SIDevice1", 12 if padsgamecube == '1' else 6)
        dolphinSettings.setInt(self.SECTION_CORE, "SIDevice2", 12 if padsgamecube == '1' else 6)
        dolphinSettings.setInt(self.SECTION_CORE, "SIDevice3", 12 if padsgamecube == '1' else 6)
        dolphinSettings.setInt(self.SECTION_CORE, "SlotA", 8)
        dolphinSettings.setInt(self.SECTION_CORE, "SlotB", 8)
        # Set video renderer OpenGl / vulkan
        dolphinSettings.setInt(self.SECTION_CORE, "GFXBackend", videoDriver)
        # disable/enable cheats
        dolphinSettings.setInt(self.SECTION_CORE, "EnableCheats", cheats)
        # GameList
        dolphinSettings.setBool(self.SECTION_GAMELIST, "ColumnID", True)
        # General
        dolphinSettings.setInt(self.SECTION_GENERAL, "ISOPaths", 2)
        dolphinSettings.setString(self.SECTION_GENERAL, "ISOPath0", "/recalbox/share/roms/gamecube")
        dolphinSettings.setString(self.SECTION_GENERAL, "ISOPath1", "/recalbox/share/roms/wii")
        dolphinSettings.setBool(self.SECTION_GENERAL, "RecursiveISOPaths", True)
        # Interface
        dolphinSettings.setString(self.SECTION_INTERFACE, "LanguageCode", languageCode)
        dolphinSettings.setBool(self.SECTION_INTERFACE, "AutoHideCursor", True)
        dolphinSettings.setBool(self.SECTION_INTERFACE, "ConfirmStop", False)
        dolphinSettings.setInt(self.SECTION_INTERFACE, "Language", wiiLanguage)
        dolphinSettings.setInt(self.SECTION_INTERFACE, "PauseOnFocusLost", True)
        dolphinSettings.setInt(self.SECTION_INTERFACE, "CursorVisibility", 0)
        # Netplay
        dolphinSettings.setString(self.SECTION_NETPLAY, "Nickname", nickname)
        dolphinSettings.setBool(self.SECTION_NETPLAY, "UseUPNP", True)
        dolphinSettings.setString(self.SECTION_NETPLAY, "IndexName", nickname)
        dolphinSettings.setString(self.SECTION_NETPLAY, "TraversalChoice", "traversal")
        dolphinSettings.setBool(self.SECTION_NETPLAY, "UseIndex", True)
        dolphinSettings.setString(self.SECTION_NETPLAY, "IndexRegion", lobbyServer)

        # Save configuration
        dolphinSettings.saveFile()
        
        # Set Wii sensor bar position (in SYSCONF)
        self.SetSensorBarPosition()

        # Update SYSCONF also for language and ratio
        try:
            # update SYSCONF file
            dolphinSYSCONF.update(recalboxFiles.dolphinSYSCONF, system.Ratio, wiiLanguage)
        except Exception:
            pass # don't fail in case of SYSCONF update

    def gfxConfiguration(self, system: Emulator, recalboxOptions: keyValueSettings):
        if (recalboxOptions.getBool("dolphin.widescreenhack", False)):
            # Get Ratio / Forced to 16/9 for wide screen hack
            gameRatio = self.GAME_RATIO["16/9"]
        else:
            # Get Ratio from system configuration
            gameRatio = self.GAME_RATIO[system.Ratio] if system.Ratio in self.GAME_RATIO else 0 # "auto" is the default mode and to avoid issue

        # Load Configuration
        from configgen.settings.iniSettings import IniSettings
        gfxSettings = IniSettings(recalboxFiles.dolphinGFX, True)
        gfxSettings.loadFile(True)

        resolution = recalboxOptions.getString("dolphin.resolution", "1") # Native by default
        vsync = recalboxOptions.getBool("dolphin.vsync", False)
        antialiasing = recalboxOptions.getString("dolphin.antialiasing", "0x00000001")  # None by default
        wideScreenHack = recalboxOptions.getBool("dolphin.widescreenhack", False)
        
        # Hardware
        gfxSettings.setBool(self.SECTION_HARDWARE, "VSync", vsync)
        # Settings
        gfxSettings.setBool(self.SECTION_SETTINGS, "ShowFPS", system.ShowFPS)
        gfxSettings.setInt(self.SECTION_SETTINGS, "AspectRatio", gameRatio)
        gfxSettings.setBool(self.SECTION_SETTINGS, "HiresTextures", True)
        gfxSettings.setBool(self.SECTION_SETTINGS, "CacheHiresTextures", True)
        gfxSettings.setInt(self.SECTION_SETTINGS, "InternalResolution", resolution)
        gfxSettings.setInt(self.SECTION_SETTINGS, "MSAA", antialiasing)
        gfxSettings.setBool(self.SECTION_SETTINGS, "wideScreenHack", wideScreenHack)

        # Save configuration
        gfxSettings.saveFile()

    def cheevosConfiguration(self, system: Emulator, recalboxOptions: keyValueSettings):

        from configgen.settings.iniSettings import IniSettings
        retroConfigFile = IniSettings(recalboxFiles.RetroAchievementsIni, True)
        retroConfigFile.defineBool("true", "false") \
                  .loadFile(True)

        retroachievements = recalboxOptions.getBool("global.retroachievements", "0")
        hardcoreMode = recalboxOptions.getBool("global.retroachievements.hardcore", "0")
        cheevosUsername = recalboxOptions.getString("global.retroachievements.username", "pixl")
        cheevosToken = recalboxOptions.getString("global.retroachievements.token", "")

        retroConfigFile.setBool(self.SECTION_ACHIEVEMENTS, "Enabled", retroachievements)
        retroConfigFile.setBool(self.SECTION_ACHIEVEMENTS, "HardcoreEnabled", hardcoreMode)
        retroConfigFile.setString(self.SECTION_ACHIEVEMENTS, "Username", cheevosUsername)
        retroConfigFile.setString(self.SECTION_ACHIEVEMENTS, "ApiToken", cheevosToken)

        # Save configuration
        retroConfigFile.saveFile()

    def generate(self, system: Emulator, playersControllers: ControllerPerPlayer, recalboxOptions: keyValueSettings, args) -> Command:
        if not system.HasConfigFile:
            # Controllers
            dolphinControllers.generateControllerConfig(system, playersControllers, recalboxOptions)
            #main configuration
            self.mainConfiguration(system, recalboxOptions, args)
            #graphics configuration
            self.gfxConfiguration(system, recalboxOptions)
			#retroachievements configuration
            self.cheevosConfiguration(system , recalboxOptions)

        commandArray = [recalboxFiles.recalboxBins[system.Emulator], "-e", args.rom]

        env = {}
        env["XDG_CONFIG_HOME"] = recalboxFiles.CONF
        env["XDG_DATA_HOME"] = recalboxFiles.CONF
        env["XDG_CACHE_HOME"] = recalboxFiles.CACHE

        if system.HasArgs: commandArray.extend(system.Args)
        
        # To manage Vulkan/OpenGL overlay activated for monitoring or bezel display
        if not (recalboxOptions.getBool("dolphin.widescreenhack", False)):
            overlays.processOverlays(system, args.rom, recalboxOptions, env, commandArray)

        return Command(videomode=system.VideoMode, array=commandArray, env=env)
