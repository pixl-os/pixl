#!/usr/bin/env python
from typing import Dict, IO

import configgen.recalboxFiles as recalboxFiles
from configgen.Emulator import Emulator
from configgen.controllers.inputItem import InputItem
from configgen.controllers.controller import ControllerPerPlayer
from configgen.settings.keyValueSettings import keyValueSettings

##to run subprocess as shell command
import subprocess

def Log(txt):
    import datetime
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    print(formatted_time[:-3] + " - " + txt)
    return 0

hotkeysCombo: Dict[int, str] =\
{
    #this 2 hotkeys combinaisons have been deactivated due to usage of evmapy now
    #to use kill app to exit + to use tab and don't reset for that
    #InputItem.ItemB:      "Stop", #stop game
    InputItem.ItemL1:     "Take Screenshot",
    #InputItem.ItemStart:  "Exit", #exit dolphin
    InputItem.ItemA:      "Reset",
    InputItem.ItemY:      "Save to selected slot",
    InputItem.ItemX:      "Load from selected slot",
    InputItem.ItemR2:     "Toggle Pause",
    InputItem.ItemUp:     "Select State Slot 1",
    InputItem.ItemDown:   "Select State Slot 2",
    InputItem.ItemLeft:   "Decrease Emulation Speed",
    InputItem.ItemRight:  "Increase Emulation Speed"
}

hotkeysXboxCombo: Dict[int,str] =\
{
    InputItem.ItemB:      "General/Stop", ## to unify the unique combination of Recalbox ;)
    #    "b":      "Toggle Pause",
    InputItem.ItemL1:     "General/Take Screenshot",
    InputItem.ItemStart:  "General/Exit",
    InputItem.ItemA:      "General/Reset",
    InputItem.ItemY:      "Save State/Save to selected slot",
    InputItem.ItemX:      "Load State/Load from selected slot",
    #    "r2":     "Stop",
    InputItem.ItemR2:     "General/Toggle Pause",
    InputItem.ItemUp:     "Select State/Select State Slot 1",
    InputItem.ItemDown:   "Select State/Select State Slot 2",
    InputItem.ItemLeft:   "Emulation Speed/Decrease Emulation Speed",
    InputItem.ItemRight:  "Emulation Speed/Increase Emulation Speed"
}

hotkeysXboxComboValues: Dict[int,str] =\
{
    InputItem.ItemB:      "SOUTH", ## to unify the unique combination of Recalbox ;)
    #    "b":      "Toggle Pause",
    InputItem.ItemL1:     "TL",
    InputItem.ItemStart:  "START",
    InputItem.ItemA:      "EAST",
    InputItem.ItemY:      "NORTH",
    InputItem.ItemX:      "WEST",
    #    "r2":     "Stop",
    InputItem.ItemR2:     "`Full Axis 5+`",
    InputItem.ItemUp:     "`Axis 7+`",
    InputItem.ItemDown:   "`Axis 7-`",
    InputItem.ItemLeft:   "`Axis 6-`",
    InputItem.ItemRight:  "`Axis 6+`"
}

# Create the controller configuration file
def generateControllerConfig(system: Emulator, playersControllers: ControllerPerPlayer, recalboxOptions: keyValueSettings, gameID: str):

    generateHotkeys(playersControllers)
    generateControllerConfigTriforce(playersControllers, system, recalboxOptions, gameID)

def generateControllerConfigTriforce(playersControllers: ControllerPerPlayer, system: Emulator, recalboxOptions: keyValueSettings, gameID: str):


    ############################################ F-ZERO AX  #################################################
    # mapping from emulator = GOAL -> mapping from pixL
    # L Trigger = Brake -> L2
    # R TRigger = Gas -> R2
    # B Button  = left Side paddle -> L1
    # A Button  = Right Side paddle -> R1
    # Y Button = Boost -> A
    # X Button = Credit/Service -> Select
    # Z Button = Test -> R3
    # Start Button = Start -> Start
    # D-Pad = View Change 1 to 4 -> Up/Down/Left/Right
    # Main Stick X = Steering X -> Joy1 Horizontal axis
    # Main Stick Y = Steering Y -> Joy1 Vertical axis
    # info on controller from owner's manual page 42 from http://backup.segakore.fr/sauservice/Manuals/FZeroAX/FZeroSTD.pdf
    #########################################################################################################
    
    FZERO_AX_Mapping: Dict[int, str] = \
    {
        InputItem.ItemL2:       'Triggers/L', # - Analog will be added if axis detected
        InputItem.ItemR2:       'Triggers/R', # - Analog will be added if axis detected
        InputItem.ItemL1:       'Buttons/A',
        InputItem.ItemR1:       'Buttons/B',
        InputItem.ItemB:        'Buttons/Y',
        InputItem.ItemSelect:   'Buttons/X', # - coin/credit
        InputItem.ItemR3:       'Buttons/Z',
        InputItem.ItemStart:    'Buttons/Start',
        InputItem.ItemUp:       'D-Pad/Up',
        InputItem.ItemDown:     'D-Pad/Down',
        InputItem.ItemLeft:     'D-Pad/Left',
        InputItem.ItemRight:    'D-Pad/Right',
        InputItem.ItemJoy1Up:   'Main Stick/Up',
        InputItem.ItemJoy1Left: 'Main Stick/Left',
        InputItem.ItemJoy1Down: 'Main Stick/Down',
        InputItem.ItemJoy1Right:'Main Stick/Right'
    }

    ############################################ Mario Kart Arcade GP 1 & 2 #################################
    # mapping from emulator = GOAL -> mapping from pixL
    # L Trigger = Brake -> L2 or L1 : A Rocket Start can be performed by hitting accelerate on the green light.
    # R Trigger = Gas -> R2 or R1 : If pressed quickly, the brake pedal can also make karts jump and power slide if pressed twice. 
    # A Button  = ITEM BUTTON (Throw)
    # B Button  = CANCEL BUTTON (TBC)
    # Z Button = Credit/Service -> Select (TBC)
    # X Button = Test -> R3 (TBC)
    # Start Button = Start -> Start (TBC)
    # Main Stick X = Steering X -> Joy1 Horizontal axis
    # info on controller for MK Arcade GP 1 from operators manual page 51 from https://www.progettosnaps.net/manuals/pdf/mkartagp.pdf
    # info on controller for MK Arcade GP 1 from operators manual page 54 from https://wiki.arcadeotaku.com/images/d/d4/MarioKart_2_Manual.pdf
    #########################################################################################################
    
    MARIO_KART_ARCADE_GP_Mapping: Dict[int, str] = \
    {
        InputItem.ItemL1:       'Triggers/L',
        InputItem.ItemR1:       'Triggers/R',
        InputItem.ItemL2:       'Triggers/L-Analog', # - Analog will be removed if axis not detected
        InputItem.ItemR2:       'Triggers/R-Analog', # - Analog will be removed if axis not detected
        InputItem.ItemB:        'Buttons/A',
        InputItem.ItemA:        'Buttons/B',
        InputItem.ItemSelect:   'Buttons/X', # - coin/credit
        InputItem.ItemR3:       'Buttons/Z', # - menu service (not work some times :-( )
        InputItem.ItemStart:    'Buttons/Start',
        InputItem.ItemJoy1Left: 'Main Stick/Left',
        InputItem.ItemJoy1Right:'Main Stick/Right'
    }

    ############################################ Virtua Stricker 3 ver.2002 / 4 / 4 ver.2006 #################################
    # info on controller for Sega Virtua Stricker 4 from service manual page 28 from https://www.manualslib.com/manual/2098666/Sega-Virtua-Striker-4.html?page=31#manual
    #########################################################################################################
    
    VIRTUA_STRIKER_Mapping: Dict[int, str] = \
    {
        InputItem.ItemA:        'Buttons/A', # - long pass (VS3) - shoot (VS4)
        InputItem.ItemB:        'Triggers/R', # - short pass
        InputItem.ItemY:        'Triggers/L', # - shoot (VS3) - long pass (VS4)
        InputItem.ItemX:        'Buttons/B', # - dash (VS4 only)
        InputItem.ItemSelect:   'Buttons/X', # - coin/credit
        InputItem.ItemR3:       'Buttons/Z', # - menu service (not work some times :-()
        InputItem.ItemStart:    'Buttons/Start',
        InputItem.ItemUp:       'D-Pad/Up', # - direction (VS3) - Tactics (M) (VS4)
        InputItem.ItemDown:     'D-Pad/Down', # - direction (VS3) - not assigned (VS4)
        InputItem.ItemLeft:     'D-Pad/Left', # - direction (VS3) - Tactics (U) (VS4)
        InputItem.ItemRight:    'D-Pad/Right', # - direction (VS3) - Tactics (D) (VS4)
        InputItem.ItemJoy1Up:   'Main Stick/Left', # - direction (VS4)
        InputItem.ItemJoy1Left: 'Main Stick/Down', # - direction (VS4)
        InputItem.ItemJoy1Down: 'Main Stick/Right', # - direction (VS4)
        InputItem.ItemJoy1Right:'Main Stick/Up', # - direction (VS4)
    }


    ##################### Gekitou Pro Yakyuu Mizushima Shinji All Stars vs. Pro Yakyuu #####################
    #
    ########################################################################################################
    
    GETIKOU_Mapping: Dict[int, str] = \
    {
        InputItem.ItemA:        'Buttons/A', # - A button
        InputItem.ItemB:        'Buttons/B', # - B button
        InputItem.ItemY:        'Triggers/L',# - GETIKOU
        InputItem.ItemR3:       'Buttons/Z', # - Test button
        InputItem.ItemStart:    'Buttons/Start', # - Start button
        InputItem.ItemSelect:   'Buttons/X', # - coin/credit or service button
        InputItem.ItemUp:       'D-Pad/Up', # - dpad direction
        InputItem.ItemDown:     'D-Pad/Down', # - dpad direction
        InputItem.ItemLeft:     'D-Pad/Left', # - dpad direction
        InputItem.ItemRight:    'D-Pad/Right', # - dpad direction
    }
    
    ############################################ DEFAULT  #################################################
    
    default_Mapping: Dict[int, str] = \
    {
        InputItem.ItemA:        'Buttons/A',
        InputItem.ItemB:        'Buttons/B',
        InputItem.ItemX:        'Buttons/Y',
        InputItem.ItemL1:       'Triggers/L',
        InputItem.ItemR1:       'Triggers/R',
        InputItem.ItemL2:       'Triggers/L-Analog',
        InputItem.ItemR2:       'Triggers/R-Analog',
        InputItem.ItemR3:       'Buttons/Z',
        InputItem.ItemStart:    'Buttons/Start',
        InputItem.ItemSelect:   'Buttons/X', # - coin/credit
        InputItem.ItemUp:       'D-Pad/Up',
        InputItem.ItemDown:     'D-Pad/Down',
        InputItem.ItemLeft:     'D-Pad/Left',
        InputItem.ItemRight:    'D-Pad/Right',
        InputItem.ItemJoy1Up:   'Main Stick/Up',
        InputItem.ItemJoy1Left: 'Main Stick/Left',
        InputItem.ItemJoy1Down: 'Main Stick/Down',
        InputItem.ItemJoy1Right:'Main Stick/Right',
        InputItem.ItemJoy2Up:   'C-Stick/Up',
        InputItem.ItemJoy2Left: 'C-Stick/Left',
        InputItem.ItemJoy2Down: 'C-Stick/Down',
        InputItem.ItemJoy2Right:'C-Stick/Right'
    }

    #################### ROM links to specific inputs Mapping defined and using gameID #######################################
    ROM_MAPPINGS: Dict[str, Dict] = \
    {
        "GFZJ8P": FZERO_AX_Mapping,
        "GGPE01": MARIO_KART_ARCADE_GP_Mapping,
        "GGPE02": MARIO_KART_ARCADE_GP_Mapping,
        "GGPJ02": MARIO_KART_ARCADE_GP_Mapping,
        "GVS45E": VIRTUA_STRIKER_Mapping,
        "GVSJ8P": VIRTUA_STRIKER_Mapping,
        "GVS32E": VIRTUA_STRIKER_Mapping,
        "GVS32J": VIRTUA_STRIKER_Mapping,
        "GVS46E": VIRTUA_STRIKER_Mapping,
        "GVS46J": VIRTUA_STRIKER_Mapping,
        "GPBJ8P": GETIKOU_Mapping
    }
    
    #check if inputs mapping exists for this game using gameID or select default one
    found_Mapping = ROM_MAPPINGS.get(gameID)
    if found_Mapping != None:
        generateControllerConfigAny(playersControllers, "GCPadNew.ini", "GCPad", found_Mapping, system, recalboxOptions)
    else:
        generateControllerConfigAny(playersControllers, "GCPadNew.ini", "GCPad", default_Mapping, system, recalboxOptions)

def EvdevGetJoystickName(path: str) -> str:
    import fcntl
    with open(path) as fd:
        return fcntl.ioctl(fd, 0x80804506, bytes(128)).decode('utf-8').rstrip().replace('\x00', '')

# Type def for convenience
ControllerEventCollection = Dict[int, str]

def getControllerEventButtonName(path: str) -> ControllerEventCollection:
     p = subprocess.Popen('timeout 1 evtest {} | grep "Event code" | grep "BTN_"'.format(path), shell=True,
     stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
     p.wait()
     output = p.stdout
     events: ControllerEventCollection = {}
     for line in output:
        Log('   Event code line:' + line.strip())
        code = int(line.strip().split(' ')[2])
        Log('   Event code value:' + str(code))
        name = line.strip().split(' ')[3].split('BTN_')[1].split(')')[0]
        Log('   Event code name:' + name)
        events[code] = name
     return events

def getControllerName(device: str):
    import os
    sysFile = "/sys/class/input/{}/device/name".format(os.path.basename(device))
    if os.path.isfile(sysFile):
        with open(sysFile, "r") as f:
            return f.readline().rstrip("\n")
    return ""

def isXINPUTController(path: str) -> bool:
     driver = ""
     #check first the  type of connection USB or Bluetooth
     Log('command: udevadm info {} | grep -i BUS'.format(path))
     p = subprocess.Popen(' udevadm info {} | grep -i BUS'.format(path), shell=True,
     stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
     p.wait()
     output = p.stdout
     for line in output:
        Log('   BUS line:' + line.strip())
        bus = line.strip().split('=')[1]
        Log('   BUS value:' + bus)
     if(bus == "usb"):
         #check the driver for USB connection
         Log('command: udevadm info {} | grep -i DRIVER'.format(path))
         p = subprocess.Popen(' udevadm info {} | grep -i DRIVER'.format(path), shell=True,
         stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
         p.wait()
         output = p.stdout
         for line in output:
            Log('   DRIVER line:' + line.strip())
            driver = line.strip().split('=')[1]
            Log('   DRIVER value:' + driver)
     if ("hid" in driver): # DINPUT controler / could work in XINPUT but we are not sure
        return False
     elif ("xpad" in driver) or ("xone" in driver): # XINPUT controler / could work in DINPUT but we are not sure
        return True
     else: #if we don't know we prefer to stay in DINPUT (it's a choice ;-)
        return False

def generateControllerConfigAny(playersControllers: ControllerPerPlayer, filename: str, anyDefKey: str, anyMapping: Dict[int, str], system: Emulator, recalboxOptions: keyValueSettings):
    configFileName = "{}/{}".format(recalboxFiles.dolphinConfig, filename)
    f = open(configFileName, "w")
    nplayer = 1

    # in case of two pads having the same name, dolphin wants a number to handle this
    double_pads = dict()

    for playercontroller in playersControllers:
        # handle x pads having the same name
        pad = playersControllers[playercontroller]
        if pad.DeviceName in double_pads:
            nsamepad = double_pads[pad.DeviceName]
        else:
            nsamepad = 0
        double_pads[pad.DeviceName] = nsamepad + 1

        f.write("[" + anyDefKey + str(nplayer) + "]" + "\n")
        f.write('Device = "evdev/' + str(nsamepad) + '/' + EvdevGetJoystickName(pad.DevicePath) + '"\n')

        #get Controller Event Name for XINPUT controllers
        events: ControllerEventCollection = {}
        if(isXINPUTController(pad.DevicePath)): #get/use events name only for XINPUT controllers
            events = getControllerEventButtonName(pad.DevicePath)
        #configuration deadzone for all sticks
        f.write("Main Stick/Dead Zone = 25.000000000000000" + "\n")
        f.write("C-Stick/Dead Zone = 25.000000000000000" + "\n")

        for inp in pad.AvailableInput:

            if inp.Item in anyMapping:
                keyname = anyMapping[inp.Item]
                #take care about L2/R2
                #we set Triggers/L-Analog or Triggers/R-Analog only if L2 or R2 are axis
                #else we let as Trigger/L and Trigger/R
                if (inp.Item == InputItem.ItemL2) or (inp.Item == InputItem.ItemR2):
                    if inp.IsAxis:
                        if not '-Analog' in keyname:
                            keyname = keyname + '-Analog'
                    else:
                        if '-Analog' in keyname:
                            keyname = keyname.replace('-Analog','')
                # write the configuration for this key
                writeKey(f, keyname, inp, pad.AxisCount, events)

        nplayer += 1
        if nplayer > 2: break #exit for because we can't have more than 2 players on Triforce

    f.close()

def writeKey(f: IO, keyname: str, inputItem: InputItem, inputGlobalId: int, events: ControllerEventCollection={}):
    f.write(keyname + " = `")
    if inputItem.IsButton:
        if events=={}: #if events are empty
            Log("Button " + str(inputItem.Id))
            f.write("Button " + str(inputItem.Id))
        else: #events are completed, we could write EV_KEY name using event code
            Log("events[inputItem.Code] : " + events[inputItem.Code])
            f.write(events[inputItem.Code])
    elif inputItem.IsHat:
        f.write("Axis " + str(inputGlobalId + (1 if inputItem.Value in (1, 4) else 0)) + ('-' if inputItem.Value in (1, 8) else '+'))
    elif inputItem.IsAxis:
        f.write("Axis " + str(inputItem.Id) + ('+' if inputItem.Value > 0 else '-'))
    f.write("`\n")


def generateHotkeys(playersControllers: ControllerPerPlayer):
    player1 = None
    iniValues: Dict[str, str] = {}

    # Find player 1
    for idx, playerController in playersControllers.items():
        if playerController.PlayerIndex == 1:
            Log("P1 found")
            player1 = playerController
            break

    if player1 is None:
        Log("no P1")
        raise ValueError("Couldn't find Player 1 input config")

    # Read its inputs, get the hotkey
    # depreacted way deactivated:
    # if player1.DeviceName not in getControllerName(player1.DevicePath):
    isXinput = isXINPUTController(player1.DevicePath)
    if (isXinput):
        # Mostly sure it's an Xbox-recognized controller
        HK: str = "MODE" if player1.HasHotkey else -1
    else:
        # Non-Xbox controller
        HK: int = player1.Hotkey.Id if player1.HasHotkey else -1

        if HK < 0:
            Log("no HK")
            raise ValueError("Couldn't find Player 1 hotkey")

    # Now generate the hotkeys
    # depreacted way deactivated:
    # if player1.DeviceName not in getControllerName(player1.DevicePath):
    if (isXinput):
        for inputItem in player1.AvailableInput:
            if inputItem.Item in hotkeysXboxCombo:
                propertyName = "{}".format(hotkeysXboxCombo[inputItem.Item])
                Log(propertyName)
                propertyValue = "@({}+{})".format(HK, hotkeysXboxComboValues[inputItem.Item])
                iniValues[propertyName] = propertyValue

        iniValues["Device"] = '"evdev/0/{}"'.format(getControllerName(player1.DevicePath))
        # Prepare the ini write
        iniSections = { "Hotkeys": iniValues }
    else:
        for inputItem in player1.AvailableInput:
            if inputItem.Item in hotkeysCombo:
                propertyName = "Keys/{}".format(hotkeysCombo[inputItem.Item])
                Log(propertyName)
                propertyValue = "`Button {}` & `Button {}`".format(HK, inputItem.Id)
                iniValues[propertyName] = propertyValue
        iniValues["Device"] = '"evdev/0/{}"'.format(player1.DeviceName)
        # Prepare the ini write
        iniSections = { "Hotkeys1": iniValues }
    writeIniFile(recalboxFiles.dolphinHKeys, iniSections)


def writeIniFile(filename: str, sectionsAndValues: Dict[str, Dict[str, str]]):
    # filename: file to write
    # sectionsAndValues: a dict indexed on sections on the ini. Each section has a dict of propertyName: propertyValue
    from configgen.settings.iniSettings import IniSettings
    config = IniSettings(filename, True)

    # Write dynamic config
    for section, values in sectionsAndValues.items():
        for propertyName, propertyValue in values.items():
            config.setString(section, propertyName, propertyValue)

    # Open file
    config.saveFile()
