#!/usr/bin/env python
from typing import List, Dict

from configgen.controllers.controller import Controller, InputItem
from configgen.controllers.controller import ControllerPerPlayer
from configgen.Emulator import Emulator
from configgen.generators.pcsx2.pcsx2LightGuns import pcsx2LightGun
from configgen.settings.keyValueSettings import keyValueSettings
from configgen.settings.iniSettings import IniSettings
import configgen.recalboxFiles as recalboxFiles
import os
##for regular expression
import re

def Log(txt):

    import datetime
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    #print(formatted_time[:-3] + " - " + txt)
    return 0

def generateControllerConfig(self, playersControllers: ControllerPerPlayer, system: Emulator, recalboxOptions: keyValueSettings) -> (bool, bool):

    SECTION_HOTKEYS = "Hotkeys"
    SECTION_INPUT_SOURCES = "InputSources"
    SECTION_EMUCORE_GS = "EmuCore/GS"

    inis = IniSettings(recalboxFiles.pcsx2ConfigFile)
    inis.loadFile(True) \
        .defineBool('true', 'false')
    
    # set default input sources available
    inis.setString(SECTION_INPUT_SOURCES, "Keyboard", "true")
    inis.setString(SECTION_INPUT_SOURCES, "Mouse", "true")
    inis.setString(SECTION_INPUT_SOURCES, "SDL", "true")
    inis.setString(SECTION_INPUT_SOURCES, "SDLControllerEnhancedMode", "true")

    # clean USB sections
    if inis.hasRegion("USB1"):
        inis.clearRegion("USB1")
    if inis.hasRegion("USB2"):
        inis.clearRegion("USB2")

    # remove the previous [Padx] sections to avoid phantom controllers
    existingPadX= ["Pad1", "Pad2", "Pad3", "Pad4", "Pad5", "Pad6", "Pad7", "Pad8", "Hotkeys"]
    for padX in existingPadX:
        inis.clearRegion(padX)
        
    for index in playersControllers:
        controller = playersControllers[index]
        Log(" PlayerIndex of " + controller.DeviceName + " : {}".format(controller.PlayerIndex))
        Log(" SdlIndex of " + controller.DeviceName + " : {}".format(controller.SdlIndex))
        Log(" NaturalIndex of " + controller.DeviceName + " : {}".format(controller.NaturalIndex))
        Log(" JsIndex of " + controller.DeviceName + " : {}".format(controller.JsIndex))
        Log(" JsOrder of " + controller.DeviceName + " : {}".format(controller.JsOrder))
        # dynamic Pad and sdl int
        padIndex = str = "Pad{}".format(controller.PlayerIndex)
        sdlIndex = str = "SDL-{}".format(controller.SdlIndex)

        # Define type of controller DualShock2/Guitare and other default parameters
        inis.setString(padIndex, "Type", "DualShock2")
        inis.setString(padIndex, "InvertL", "0")
        inis.setString(padIndex, "InvertR", "0")
        inis.setString(padIndex, "Deadzone", "0.2")
        inis.setString(padIndex, "AxisScale", "1.33")
        inis.setString(padIndex, "TriggerDeadzone", "0.2")
        inis.setString(padIndex, "TriggerScale", "1")
        inis.setString(padIndex, "LargeMotorScale", "1")
        inis.setString(padIndex, "SmallMotorScale", "1")
        inis.setString(padIndex, "ButtonDeadzone", "0.2")
        inis.setString(padIndex, "PressureModifier", "0")
        
        # Mapping controllers
        if controller.HasUp :           inis.setString(padIndex, "Up", "{}/DPadUp".format(sdlIndex))
        if controller.HasRight :        inis.setString(padIndex, "Right", "{}/DPadRight".format(sdlIndex))
        if controller.HasDown :         inis.setString(padIndex, "Down", "{}/DPadDown".format(sdlIndex))
        if controller.HasLeft :         inis.setString(padIndex, "Left", "{}/DPadLeft".format(sdlIndex))
        if controller.HasY :            inis.setString(padIndex, "Triangle", "{}/Y".format(sdlIndex))
        if controller.HasB :            inis.setString(padIndex, "Circle", "{}/B".format(sdlIndex))
        if controller.HasA :            inis.setString(padIndex, "Cross", "{}/A".format(sdlIndex))
        if controller.HasX :            inis.setString(padIndex, "Square", "{}/X".format(sdlIndex))
        if controller.HasSelect :       inis.setString(padIndex, "Select", "{}/Back".format(sdlIndex))
        if controller.HasStart :        inis.setString(padIndex, "Start", "{}/Start".format(sdlIndex))
        if controller.HasL1 :           inis.setString(padIndex, "L1", "{}/LeftShoulder".format(sdlIndex))
        if controller.HasL2 :           inis.setString(padIndex, "L2", "{}/+LeftTrigger".format(sdlIndex))
        if controller.HasR1 :           inis.setString(padIndex, "R1", "{}/RightShoulder".format(sdlIndex))
        if controller.HasR2 :           inis.setString(padIndex, "R2", "{}/+RightTrigger".format(sdlIndex))
        if controller.HasL3 :           inis.setString(padIndex, "L3", "{}/LeftStick".format(sdlIndex))
        if controller.HasR3 :           inis.setString(padIndex, "R3", "{}/RightStick".format(sdlIndex))
        if controller.HasJoy1Up :       inis.setString(padIndex, "LUp", "{}/-LeftY".format(sdlIndex))
        if controller.HasJoy1Right :    inis.setString(padIndex, "LRight", "{}/+LeftX".format(sdlIndex))
        if controller.HasJoy1Down :     inis.setString(padIndex, "LDown", "{}/+LeftY".format(sdlIndex))
        if controller.HasJoy1Left :     inis.setString(padIndex, "LLeft", "{}/-LeftX".format(sdlIndex))
        if controller.HasJoy2Up :       inis.setString(padIndex, "RUp", "{}/-RightY".format(sdlIndex))
        if controller.HasJoy2Right :    inis.setString(padIndex, "RRight", "{}/+RightX".format(sdlIndex))
        if controller.HasJoy2Down :     inis.setString(padIndex, "RDown", "{}/+RightY".format(sdlIndex))
        if controller.HasJoy2Left :     inis.setString(padIndex, "RLeft", "{}/-RightX".format(sdlIndex))

        if controller.PlayerIndex == 1:
            print("P1 found")
            player1 = "SDL-{}".format(controller.SdlIndex)
            Log("Hotkey player1 Index SDL: {}".format(player1))
            inis.setString(SECTION_HOTKEYS, "OpenPauseMenu", "{}/Guide & {}/A".format(player1, player1))
            inis.setString(SECTION_HOTKEYS, "OpenAchievementsList", "{}/Guide & {}/RightShoulder".format(player1, player1))
            inis.setString(SECTION_HOTKEYS, "Screenshot", "{}/Guide & {}/LeftShoulder".format(player1, player1))
            inis.setString(SECTION_HOTKEYS, "ToggleSlowMotion", "{}/Guide & {}/DPadLeft".format(player1, player1))
            inis.setString(SECTION_HOTKEYS, "HoldTurbo", "{}/Guide & {}/DPadRight".format(player1, player1))
            inis.setString(SECTION_HOTKEYS, "LoadStateFromSlot", "{}/Guide & {}/X".format(player1, player1))
            inis.setString(SECTION_HOTKEYS, "NextSaveStateSlot", "{}/Guide & {}/DPadUp".format(player1, player1))
            inis.setString(SECTION_HOTKEYS, "PreviousSaveStateSlot", "{}/Guide & {}/DPadDown".format(player1, player1))
            inis.setString(SECTION_HOTKEYS, "SaveStateToSlot", "{}/Guide & {}/Y".format(player1, player1))
        else:
            print("P1 Not Found")

    # Save configuration
    inis.saveFile()
    
    #-----------------------------------------------------------------------------------------------------------------------------
    #------------------------------- part to manage mouse/lightgun controls-------------------------------------------------------
    #-----------------------------------------------------------------------------------------------------------------------------
    
    #init lightgun config (configure wiimote or a default mouse)
    self.lightgunConfig = pcsx2LightGun(system, recalboxOptions)
    #check first if it's a lightgun game
    #init boolean NeedPotentiallySindenBorder/SplitScreenGame
    NeedPotentiallySindenBorder = False
    SplitScreenGameOverlay = False
    if self.lightgunConfig.isLightGunGame():
        ## Parts to configure wiimote(s)/mouse(s)
        ## to check if Mayflash_Wiimote_PC_Adapter/Sinden exists and get indexes in a first time
        LightgunIndexByPlayer, NeedPotentiallySindenBorder = Controller.findLightguns()
        if(LightgunIndexByPlayer[0][0] != "nul"):
            self.lightgunFound = True
        else:
            self.lightgunFound = False
        if not self.lightgunFound:
            #Search if mouse(s) exist(s) and get indexes in a first time
            LightgunIndexByPlayer = Controller.findMice()
            if(LightgunIndexByPlayer[0][0] != "nul"):
                self.mouseFound = True
            else:
                self.mouseFound = False
        if self.lightgunFound or self.mouseFound:
            # Configure MOUSE(S) from lightgun(s) (in priority) or mouse/mice
            Log("Configure MOUSE(S) from lightgun(s) (in priority) or mouse/mice")
            self.lightgunConfig.configureLightGunGame(LightgunIndexByPlayer)
            # Activate 16/9 split screen hack for lightgun games if requested
            if recalboxOptions.getBool("pcsx2.splitscreen.hack", True):
                # Check if 2 lightguns are available at minimum to activate the split screen
                if(LightgunIndexByPlayer[1][0] != "nul"):
                    game_name = self.lightgunConfig.getGameNameFromESState()
                    ## rom name cleaning (to be lower case and keep only alphanumeric characters)
                    game_name=game_name.lower()
                    game_name = re.sub(r'[^a-z0-9!]+', '', game_name)
                    # Check if game is compatible as: Time Crisis 2/3 & Crisis Zone
                    splitscreen_lightgun_games = ["timecrisis2","timecrisisii","timecrisis3","timecrisisiii"]
                    Log("game_name :" + game_name)
                    for game_filter in splitscreen_lightgun_games:
                        if game_filter in game_name:
                            Log(game_name + " found !!!")
                            #reload PCSX2.ini file
                            inis = IniSettings(recalboxFiles.pcsx2ConfigFile)
                            inis.loadFile(True) \
                                .defineBool('true', 'false')
                            if recalboxOptions.getBool("pcsx2.splitscreen.fullstretch", False):
                                inis.setString("USB1", "guncon2_split_screen_full_stretch", "true")
                                inis.setString("USB2", "guncon2_split_screen_full_stretch", "true")
                                SplitScreenGameOverlay = False # no need overlay usually because full screen game view in this case
                            else:
                                inis.setString("USB1", "guncon2_split_screen_full_stretch", "false")
                                inis.setString("USB2", "guncon2_split_screen_full_stretch", "false")
                                SplitScreenGameOverlay = True # could use specific overlay for split screen that why we have this info to return
                            
                            #activate the 'famous' feature called "pixL splitscreen hack" to know that we have to resize/adapt during game ;-)
                            inis.setString("USB1", "guncon2_split_screen_hack", "true")
                            inis.setString("USB2", "guncon2_split_screen_hack", "true")
                            
                            inis.saveFile()
                            break #exit for

    return NeedPotentiallySindenBorder, SplitScreenGameOverlay