#!/usr/bin/env python
from typing import Dict

from configgen.Command import Command
from configgen.Emulator import Emulator
from configgen.controllers.controller import ControllerPerPlayer
from configgen.controllers.controller import Controller
from configgen.emulatorlauncher import recalboxFiles
from configgen.generators.Generator import Generator
from configgen.settings.iniSettings import IniSettings
from configgen.settings.keyValueSettings import keyValueSettings
from configgen.utils.videoMode import GetHasVulkanSupport
import os
import shutil

# to manage controllers for PCSX2
import configgen.generators.pcsx2.pcsx2Controllers as pcsx2Controllers

#utils to manage advanced overlays features for all emulators
import configgen.utils.overlays as overlays

def Log(txt):
    import datetime
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    #print(formatted_time[:-3] + " - " + txt)
    return 0

#for logs from emulator PCSX, you can manage using this section from PCXS2.ini : 
# [Logging]
# EnableControllerLogs = false
# EnableEEConsole = false
# EnableFileLogging = true
# EnableIOPConsole = false
# EnableInputRecordingLogs = true
# EnableSystemConsole = true
# EnableTimestamps = true
# EnableVerbose = true

class Pcsx2Generator(Generator):
    
    SECTION_EMUCORE = "EmuCore"
    SECTION_EMUCORE_GS = "EmuCore/GS"
    SECTION_EMUCORE_SPEEDHACKS = "EmuCore/Speedhacks"
    SECTION_FILENAMES = "Filenames"
    SECTION_UI = "UI"
    SECTION_ACHIEVEMENTS = "Achievements"
    SECTION_GAMELIST = "GameList"
    SECTION_FOLDERS = "Folders"

    # Bios names
    BIOS_USA = "ps2-0230a-20080220.bin"
    BIOS_EU  = "ps2-0230e-20080220.bin"
    BIOS_JP  = "ps2-0230j-20080220.bin"
    BIOS_HK  = "ps2-0230h-20080220.bin"

    # To select good language from country code (essentially for PAL iso for the moment)
    PS2_LANGUAGES = \
    {
        "jp": "Japanese",
        "en": "English",
        "fr": "French",
        "es": "Spanish",
        "de": "German",
        "it": "Italian",
        "nl": "Dutch",
        "pt": "Portuguese",
        "zh": "Chinese",
    }

    # Make dirs
    memcards  : str = os.path.join(recalboxFiles.SAVES, "ps2/pcsx2/memorycards")
    sstates: str = os.path.join(recalboxFiles.SAVES, "ps2/pcsx2/savestates")
    cache: str = os.path.join(recalboxFiles.SAVES, "ps2/pcsx2/cache")
    gamesettings: str = os.path.join(recalboxFiles.SAVES, "ps2/pcsx2/gamesettings")
    configFolders = str = os.path.join(recalboxFiles.pcsx2RootFolder, "PCSX2/inis")
    biosFolders = str = os.path.join(recalboxFiles.BIOS, "ps2/pcsx2")
    if not os.path.exists(memcards): os.makedirs(name=memcards, exist_ok=True)
    if not os.path.exists(sstates): os.makedirs(name=sstates, exist_ok=True)
    if not os.path.exists(cache): os.makedirs(name=cache, exist_ok=True)
    if not os.path.exists(gamesettings): os.makedirs(name=gamesettings, exist_ok=True)
    if not os.path.exists(configFolders): os.makedirs(name=configFolders, exist_ok=True)
    if not os.path.exists(biosFolders): os.makedirs(name=biosFolders, exist_ok=True)

    @staticmethod
    def __CheckPs2Bios(rom: str) -> str:
        import os.path
        def Exists(romPath: str) -> bool:
            return os.path.exists( '/recalbox/share/bios/ps2/pcsx2/' + romPath)

        # Try to get Redump region
        lrom = rom.lower()
        # 12/02/2025: add first paranthesis to well catch "primary" region/country in the game (need Redump decoration)
        # EU region
        if   "(europe" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(france" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(ireland" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(germany" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(belgium" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(italy" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(greece" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(croatia" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(denmark" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(spain" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(portugal" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(poland" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(finland" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(russia" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(austria" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(netherlands" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(switzerland" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(norway" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(sweden" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(scandinavia" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(australia" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(india" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        elif "(south africa" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU
        # USA region
        elif "(canada" in lrom and Exists(Pcsx2Generator.BIOS_USA): return Pcsx2Generator.BIOS_USA
        elif "(brazil" in lrom and Exists(Pcsx2Generator.BIOS_USA): return Pcsx2Generator.BIOS_USA
        elif "(latin america" in lrom and Exists(Pcsx2Generator.BIOS_USA): return Pcsx2Generator.BIOS_USA
        # JP region
        elif "(japan" in lrom and Exists(Pcsx2Generator.BIOS_JP): return Pcsx2Generator.BIOS_JP
        elif "(korea" in lrom and Exists(Pcsx2Generator.BIOS_JP): return Pcsx2Generator.BIOS_JP
        # ASIA region
        elif "(china" in lrom and Exists(Pcsx2Generator.BIOS_HK): return Pcsx2Generator.BIOS_HK
        elif "(taiwan" in lrom and Exists(Pcsx2Generator.BIOS_HK): return Pcsx2Generator.BIOS_HK
        elif "(asia" in lrom and Exists(Pcsx2Generator.BIOS_HK): return Pcsx2Generator.BIOS_HK
        # Shortest name at the end
        elif "(uk" in lrom and Exists(Pcsx2Generator.BIOS_EU): return Pcsx2Generator.BIOS_EU 
        elif "(usa" in lrom and Exists(Pcsx2Generator.BIOS_USA): return Pcsx2Generator.BIOS_USA

        # Check if PS2 bios bin in folders
        for bios in (Pcsx2Generator.BIOS_USA, Pcsx2Generator.BIOS_EU, Pcsx2Generator.BIOS_JP, Pcsx2Generator.BIOS_HK):
            if Exists(bios):
                return bios
        return ""

    def mainConfiguration(self, system: Emulator, conf: keyValueSettings, args):
        
        #set rom variable
        rom = args.rom
        
        # Game Ratio AspectRatio=Auto 4:3/3:2
        GAME_RATIO: Dict[str, str] = \
        {
            "auto": "Auto 4:3/3:2",
            "16/9": "16:9",
            "4/3": "4:3",
        }
        gameRatio = GAME_RATIO[system.Ratio] if system.Ratio in GAME_RATIO else "4:3"

        ## Language parameter
        language =  conf.getString("system.language", "en-US")
        
        ## Parameters from "advanced emulator settings" pegasus menu (exclude lightgun parameters one)
        resolution = conf.getInt("pcsx2.resolution", 1) # Native resolution by default
        vsync = conf.getBool("pcsx2.vsync", False)
        anisotropy = conf.getInt("pcsx2.anisotropy", 0) # None
        tvShaders = conf.getInt("pcsx2.tvshaders", 0) # None
        cheats = conf.getBool("pcsx2.cheats", False)
        fastBoot = conf.getBool("pcsx2.fastboot", False)
        injectSystemLanguage = conf.getBool("pcsx2.injectsystemlanguage", True)
        
        ## Enable retroarchievements
        retroachievements = conf.getBool("global.retroachievements", False)
        soundEffects = conf.getBool("global.retroachievements.unlock.sound", False)
        hardcoreMode = conf.getBool("global.retroachievements.hardcore", False)
        cheevosUsername = conf.getString("global.retroachievements.username", "pixl")
        cheevosToken = conf.getString("global.retroachievements.token", "")
        videoDriver = "14" if GetHasVulkanSupport() == '1' else "12" # OpenGl(12)/vulkan(14)
        
        # test if file is not deleted for create a default basic conf stocked in share_init
        if not os.path.isfile(recalboxFiles.pcsx2ConfigFile):
            shutil.copyfile(recalboxFiles.pcsx2DefaultConfigFile, recalboxFiles.pcsx2ConfigFile)

        # Load PCSX2 Configuration
        inis = IniSettings(recalboxFiles.pcsx2ConfigFile)
        inis.loadFile(True) \
            .defineBool('true', 'false')
        
        # Override UI Configuration with Default values for pixL
        inis.setString("UI", "SettingsVersion", "1")
        inis.setBool(Pcsx2Generator.SECTION_UI, "SetupWizardIncomplete", False)
        inis.setBool(Pcsx2Generator.SECTION_UI, "ConfirmShutdown", False)
        inis.setBool(Pcsx2Generator.SECTION_UI, "InhibitScreensaver", True)
        inis.setBool(Pcsx2Generator.SECTION_UI, "StartFullscreen", False)
        inis.setBool(Pcsx2Generator.SECTION_UI, "RenderToSeparateWindow", False)
        inis.setBool(Pcsx2Generator.SECTION_UI, "DisableWindowResize", False)
        inis.setBool(Pcsx2Generator.SECTION_UI, "HideMouseCursor", True)
        inis.setBool(Pcsx2Generator.SECTION_UI, "ShowStatusBar", False)
        inis.setBool(Pcsx2Generator.SECTION_UI, "HideMainWindowWhenRunning", True)
        inis.setBool(Pcsx2Generator.SECTION_UI, "DoubleClickTogglesFullscreen", False)

        ## Language parameter for UI
        inis.setString(Pcsx2Generator.SECTION_UI, "Language", language.replace("_", "-"))

        inis.setString(Pcsx2Generator.SECTION_GAMELIST,"RecursivePaths", "/recalbox/share/roms/ps2")

        inis.setString(Pcsx2Generator.SECTION_FOLDERS, "Bios", "/recalbox/share/bios/ps2/pcsx2")
        inis.setString(Pcsx2Generator.SECTION_FOLDERS, "Cache", "/recalbox/share/saves/ps2/pcsx2/cache")
        inis.setString(Pcsx2Generator.SECTION_FOLDERS, "MemoryCards", "/recalbox/share/saves/ps2/pcsx2/memorycards")
        inis.setString(Pcsx2Generator.SECTION_FOLDERS, "Savestates", "/recalbox/share/saves/ps2/pcsx2/savestates")
        inis.setString(Pcsx2Generator.SECTION_FOLDERS, "Covers", "/recalbox/share/roms/ps2/media/box2dfront")
        inis.setString(Pcsx2Generator.SECTION_FOLDERS, "Snapshots", "/recalbox/share/screenshots")

        # choose bios files 
        bios = Pcsx2Generator.__CheckPs2Bios(rom);
        Log("BIOS selected: " + bios)
        inis.setString(Pcsx2Generator.SECTION_FILENAMES, "BIOS", bios)

        # Emucore paremters
        inis.setBool(Pcsx2Generator.SECTION_EMUCORE, "EnableCheats", cheats)
        inis.setBool(Pcsx2Generator.SECTION_EMUCORE, "WarnAboutUnsafeSettings", False)
        inis.setBool(Pcsx2Generator.SECTION_EMUCORE, "EnableFastBoot", fastBoot)
        inis.setInt(Pcsx2Generator.SECTION_EMUCORE, "NotificationsDuration", 7)
        
        ## Language parameter for BIOS
        if injectSystemLanguage:
            language = language[0:2].lower()
            Log("language from conf (country code): " + language)
            if not language in Pcsx2Generator.PS2_LANGUAGES:
                language ="en" #to force to english if unknown
            Log("language selected (country code): " + language)
            Log("language selected (string): " + Pcsx2Generator.PS2_LANGUAGES[language])
            inis.setString(Pcsx2Generator.SECTION_EMUCORE, "PreferedSystemLanguage", Pcsx2Generator.PS2_LANGUAGES[language])

        inis.setBool(Pcsx2Generator.SECTION_EMUCORE_GS, "VsyncEnable", vsync)
        inis.setInt(Pcsx2Generator.SECTION_EMUCORE_GS, "VsyncQueueSize", 2)
        inis.setString(Pcsx2Generator.SECTION_EMUCORE_GS, "AspectRatio", gameRatio)

        # set default StretchY to 100 %
        inis.setString(Pcsx2Generator.SECTION_EMUCORE_GS, "StretchY", "100")
    
        # overide user hack to default state
        inis.setInt(Pcsx2Generator.SECTION_EMUCORE_GS, "UserHacks_SkipDraw_Start", 0)
        inis.setInt(Pcsx2Generator.SECTION_EMUCORE_GS, "UserHacks_SkipDraw_End", 0)
        inis.setBool(Pcsx2Generator.SECTION_EMUCORE_GS, "OsdShowFPS", system.ShowFPS)
        inis.setInt(Pcsx2Generator.SECTION_EMUCORE_GS, "Renderer", videoDriver)
        inis.setInt(Pcsx2Generator.SECTION_EMUCORE_GS, "upscale_multiplier", resolution)
        inis.setInt(Pcsx2Generator.SECTION_EMUCORE_GS, "MaxAnisotropy", anisotropy)
        inis.setInt(Pcsx2Generator.SECTION_EMUCORE_GS, "TVShader", tvShaders)

        inis.setBool(Pcsx2Generator.SECTION_ACHIEVEMENTS, "Enabled", retroachievements)
        inis.setBool(Pcsx2Generator.SECTION_ACHIEVEMENTS, "SoundEffects", soundEffects)
        inis.setBool(Pcsx2Generator.SECTION_ACHIEVEMENTS, "ChallengeMode", hardcoreMode)
        inis.setString(Pcsx2Generator.SECTION_ACHIEVEMENTS, "Username", cheevosUsername)
        inis.setString(Pcsx2Generator.SECTION_ACHIEVEMENTS, "Token", cheevosToken)

        # Log level
        inis.setBool("Logging", "EnableVerbose", True if args.verbose else False)

        # Save configuration
        inis.saveFile()

    def generate(self, system: Emulator, playersControllers: ControllerPerPlayer, recalboxOptions: keyValueSettings, args) -> Command:

        #init boolean NeedPotentiallySindenBorder/SplitScreenGameOverlay
        NeedPotentiallySindenBorder = False
        SplitScreenGameOverlay = False
        if not system.HasConfigFile:
            # Systems and other Configurations
            self.mainConfiguration(system, recalboxOptions, args)
            # Controllers
            NeedPotentiallySindenBorder, SplitScreenGameOverlay = pcsx2Controllers.generateControllerConfig(self, playersControllers, system, recalboxOptions)

        env = {}
        env["XDG_CONFIG_HOME"] = recalboxFiles.pcsx2RootFolder
        env["QT_QPA_PLATFORM"] = "xcb"
        env["SDL_JOYSTICK_HIDAPI"] = "0"
        #finally for some cases it's better to regenerate also the SDL GAME CONTROLLER CONFIG(S) DYNAMICALLY
        #the file provided by default from pegasus-frontend could have missing values finally
        SDLGameDBLines = ""
        for index in playersControllers:
            controller = playersControllers[index]
            SDLGameDBLines = SDLGameDBLines + controller.generateSDLGameDBLine() + "\n"
        env["SDL_GAMECONTROLLERCONFIG"] = SDLGameDBLines
   
        commandArray = [recalboxFiles.recalboxBins[system.Emulator], args.rom]

        if system.HasArgs: commandArray.extend(system.Args)
        #activate/deactivate GUI
        gui = recalboxOptions.getBool("pcsx2.gui", False)
        if not gui:
            commandArray.insert(1, "-nogui")
        
        #to detect if just a bios is requested as rom to configure it - thanks singleplay feature ;-)
        if("/bios(" in args.rom):
            commandArray.insert(1, "-bios")
        
        # To manage Vulkan/OpenGL overlay activated for monitoring or bezel display + "Time crisis 2/3" split screen games
        suffix = ""
        if SplitScreenGameOverlay:
            suffix = "_splitscreen" #String to add to the additional filename to search and find any second overlay for split screen games
        # with 2% of cropping of overlay to match better with standalone using libretro core overlay
        overlays.processOverlays(system, args.rom, recalboxOptions, env, commandArray, NeedPotentiallySindenBorder, suffix, 2)

        return Command(videomode=system.VideoMode, array=commandArray, env=env)
