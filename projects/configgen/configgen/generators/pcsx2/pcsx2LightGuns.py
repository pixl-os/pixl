#!/usr/bin/env python
## Released version: 1.0.0
##IMPORT STD---------------------------------------------------------------------
import os
import sys
import time
import logging
import subprocess
import linecache
import json
import datetime

##IMPORT RECALBOX
import configgen.recalboxFiles as recalboxFiles
from configgen.settings.keyValueSettings import keyValueSettings
from configgen.Emulator import Emulator
import configgen.utils.popup as popup

##IMPORT for parsing/xml functions
##for regular expression
import re
##for XML parsing
import xml.etree.ElementTree as ET
from xml.dom import minidom

## remove "//" for robustness (if we move from HOME_INIT to HOME later)
pegasusLightGunFromShareInit = recalboxFiles.pegasusLightGun.replace("//", "/")
pegasusLightGunFromShare = recalboxFiles.pegasusLightGunFromShare.replace("//", "/")

# Information game from ES
GAME_INFO_PATH = "/tmp/es_state.inf"
# Key in informatiojn files
KEY_GAME_NAME = "Game"

##Versions history:
##
## V1: initial version for pcsx2

def Log(txt):
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    #print(formatted_time[:-3] + " - " + txt)
    return 0

class pcsx2LightGun:
    
    nbMouseIndexesFound = 0
    
    # constructor
    def __init__(self, system: Emulator, recalboxOptions: keyValueSettings):
        # initial settings
        self.system: Emulator = system
        self.recalboxOptions: keyValueSettings = recalboxOptions
        ## init for all indexes
        self.MouseIndexByPlayer = [["nul","nul","nul","nul"],["nul","nul","nul","nul"],["nul","nul","nul","nul"]]
        self.pegasusLightGun = ""
        self.crosshairs = recalboxOptions.getBool("pcsx2.crosshairs", True)

    #find game name from /tmp/es_state.inf 
    def getGameNameFromESState(self):
        gameInfo = keyValueSettings(GAME_INFO_PATH, False)
        gameInfo.loadFile(True)
        game_name = gameInfo.getString(KEY_GAME_NAME, "Unknown")
        #for test purpose: 
        #game_name = gameInfo.getString(KEY_GAME_NAME, "lostworldjurassicpark")
        del gameInfo
        if game_name != '':
            Log('Game name found, the game name is :' + game_name)
        else:
            Log('System/Game name are not set properly from gamelist/from ES or may be not scrapped ? or empty name ? the game name content is empty ')
        return game_name

    #set options/inputs depedening of keyword set or simple value
    def setString(self,section,name,value):
        Log('[' + section + ']')
        Log('    ' + name + ' = ' + value)
        #initial values
        index=0
        indexValue="nul"
        
        if ("{gunp" in section.lower()):
            Log("section.lower().strip().split('{gunp')[1] : " + section.lower().strip().split('{gunp')[1][0])
            index = int(section.lower().strip().split('{gunp')[1][0])-1
            ##update section with player index of gun for this player
            indexValue = self.MouseIndexByPlayer[index][2]
            Log('       Player Index: ' + str(index))
            Log('       Mouse Index: ' + indexValue)
            section = section.replace("{GUNP"+str(index+1)+"}",str(index+1))
            Log('       updated with Player Index + 1: ' + name + ' = ' + value)
            if (indexValue == "nul"):
                Log('       No GUN for Player Index + 1 - need to remove option if exists !')
                if self.pcsx2Settings.hasRegion(section) and self.pcsx2Settings.hasOption(section, name):
                   self.pcsx2Settings.removeOption(section, name)
                return 0

        if ("cursor" in name.lower()):
            if (("sinden" in self.MouseIndexByPlayer[index][1]) and self.recalboxOptions.getBool("lightgun.sinden.crossair.enabled", False) and self.crosshairs):
                #need cursor
                Log('       Need cursor !')
            elif ((self.MouseIndexByPlayer[index][1] != "nul") and not ("sinden" in self.MouseIndexByPlayer[index][1]) and self.crosshairs):
                #need cursor
                Log('       Need cursor !')
            else:
                #no need cursor
                Log('       No need cursor - need to remove option if exists !')
                if self.pcsx2Settings.hasRegion(section) and self.pcsx2Settings.hasOption(section, name):
                   self.pcsx2Settings.removeOption(section, name)
                return 0

        if ("{gunp" in value.lower()):
            #Log("value.lower().strip().split('{gunp')[1] : " + value.lower().strip().split('{gunp')[1][0])
            index = int(value.lower().strip().split('{gunp')[1][0])-1
            ##update value with mouse index of gun for this player
            indexValue = self.MouseIndexByPlayer[index][2]
            if (indexValue != "nul"):
                # 2 player gun support in Linux from pcsx2 to DO / need to patch emulator
                Log('       Player Index: ' + str(index))
                Log('       Mouse Index: ' + indexValue)
                value = value.replace("{GUNP"+str(index+1)+"}",str(int(index)))
                Log('       updated with Mouse Index: ' + name + ' = ' + value)
                self.pcsx2Settings.setString(section,name,value)
            else:
                Log('       No GUN for Player Index + 1 - need to remove option if exists !')
                if self.pcsx2Settings.hasRegion(section) and self.pcsx2Settings.hasOption(section, name):
                   self.pcsx2Settings.removeOption(section, name)
                return 0
        elif ("{input_event_gunp" in value.lower()):
            Log("value.lower().strip().split('{input_event_gunp')[1] : " + value.lower().strip().split('{input_event_gunp')[1][0])
            index = int(value.lower().strip().split('{input_event_gunp')[1][0])-1
            Log('       Player Index: ' + str(index))
            ##update value input event path of gun for this player
            input_event = self.MouseIndexByPlayer[index][3]
            Log('       Input Event path: ' + input_event)
            if (input_event != "nul"):
                value = value.replace("{INPUT_EVENT_GUNP"+str(index+1)+"}", input_event)
                Log('       updated with Mouse Index: ' + name + ' = ' + value)
                self.pcsx2Settings.setString(section,name,value)
            else:
                Log('       No GUN Input Event path for Player Index + 1 - need to remove option if exists !')
                if self.pcsx2Settings.hasRegion(section) and self.pcsx2Settings.hasOption(section, name):
                   self.pcsx2Settings.removeOption(section, name)
                return 0
        else:
            self.pcsx2Settings.setString(section,name,value)

    # Check if game is declared from the lightgun.cfg and optionaly (if check_only = false) configure emulator/core (for the moment ;-)
    def setLightGunConfig(self, system_name, game_name, check_only):
        if(game_name == ''):        
            Log('Game name empty, Not Configured !!!')
            return 'Not configured'

        ## to check if lightgun.cfg exists from share or share_init
        if os.path.exists(pegasusLightGunFromShare) == False:
            ##no xml configuration file found for lightgun
            Log('No lightgun.cfg xml file found from share !!!')
            ## to check if lightgun.cfg exists or not
            if os.path.exists(pegasusLightGunFromShareInit) == False:
                ##no xml configuration file found for lightgun
                Log('No lightgun.cfg xml file found from share_init !!!')
                Log('Not Configured !!!')                    
                return 'Not configured'
            else:
                #Use one from the share_init in this case
                self.pegasusLightGun = pegasusLightGunFromShareInit
        else:
            #Use custom one from share and not share_init in this case
            self.pegasusLightGun = pegasusLightGunFromShare

        systems = dict()
        tree = ET.parse(self.pegasusLightGun)
        root = tree.getroot()
        
        ##rom name cleaning (to be lower case and keep only alphanumeric characters):
        game_name=game_name.lower()
        game_name = re.sub(r'[^a-z0-9!]+', '', game_name)
        
        ##save lightgun dedicated emulator & core
        Log('selected emulator by user:' + self.system.Emulator)
        Log('selected core by user:' + self.system.Core)
                    
        ##1) first step check if system and game is supporting lightgun or not
        for child in root:
            if child.tag == 'system':
                for platform in child.iter('platform'):
                    if platform.text == system_name:
                        Log('System found: ' + platform.text)
                        emulator = child.find('emulator')
                        Log('Emulator found in system: ' + emulator.attrib["name"])
                        core = emulator.find('core')
                        Log('Core found in system: ' + core.text)
                        Log('Need to find this game: ' + game_name)
                        for games in child.iter('games'):
                            ##initial value for matching follow-up
                            best_matching_lenght = 0
                            best_matching_game_name = ""
                            for game in games.iter('game'):
                                game_pattern = game.find('name')
                                ##as optional we have to avoid error if doesn't exist or empty
                                tested = ""
                                if 'tested' in game.attrib:
                                    tested = game.attrib["tested"]
                                if re.search(game_pattern.text,game_name) and (tested != "nok") :
                                    #a matching found
                                    #keep matching to check if best one exist (to manage better case of a game and its versions ;-)
                                    Log('Pattern that match with game name: ' + game_pattern.text)
                                    if best_matching_lenght < len(game_pattern.text):
                                        best_matching_lenght = len(game_pattern.text)
                                        best_matching_game_name = game_pattern.text
                                        best_matching_game = game
                                        
                            if best_matching_lenght != 0:
                                #popup.Message("Lightgun setting identified for '" + best_matching_game_name +  "' !", 3)
                                Log('Game name best match with pattern: ' + best_matching_game_name)
                                if check_only == True:
                                    return 'Game found'
                                #load PCSX2.ini from /recalbox/share/system/configs/PCSX2/inis/
                                from configgen.settings.iniSettings import IniSettings
                                self.pcsx2Settings = IniSettings(recalboxFiles.pcsx2ConfigFile, True)
                                self.pcsx2Settings.loadFile(True).defineBool('true', 'false')
                                ## now that we are sure to have a matching, we could get the common part
                                ## Mandatory under the root
                                ## N/A for this system today
                                
                                ## Optional under the root
                                ## N/A for this system today
                                
                                ## we could get also the inputs from system
                                ## optional by sytem
                                Log('System inputs in PCSX2.ini from /recalbox/share/system/configs/PCSX2/inis/ :')
                                inputs = child.find('inputs')
                                if inputs is not None:
                                    for string in inputs.iter('string'):
                                        Log('    [' + string.attrib["section"] + '] ' + string.attrib["name"]+ ' = ' + string.attrib["value"])
                                        self.setString(string.attrib["section"],string.attrib["name"],string.attrib["value"])
                                
                                ## we could get also the options from system
                                ## optional by sytem
                                Log('System options to put in PCSX2.ini from /recalbox/share/system/configs/PCSX2/inis/ :')
                                options = child.find('options')
                                if options is not None:
                                    for string in options.iter('string'):
                                        Log('    [' + string.attrib["section"] + '] ' + string.attrib["name"]+ ' = ' + string.attrib["value"])
                                        self.setString(string.attrib["section"],string.attrib["name"],string.attrib["value"])
                                
                                ## now that we are sure to have a matching, we could get the emulator part first
                                ## optional by games
                                emulator_games = games.find('emulator')
                                if emulator_games is not None:
                                    Log('Games Emulator found: ' + emulator_games.attrib["name"])
                                    emulator = emulator_games
                                    core_games = emulator.find('core')
                                    if core_games is not None:
                                        Log('Games Core found: ' + core_games.text)
                                        core = core_games
                                
                                ## we could get also the inputs from games
                                ## optional by games
                                Log('Games inputs in PCSX2.ini from /recalbox/share/system/configs/PCSX2/inis/ :')
                                inputs = games.find('inputs')
                                if inputs is not None:
                                    for string in inputs.iter('string'):
                                        Log('    [' + string.attrib["section"] + '] ' + string.attrib["name"]+ ' = ' + string.attrib["value"])
                                        self.setString(string.attrib["section"],string.attrib["name"],string.attrib["value"])
                                
                                ## we could get also the options from games
                                ## optional by games
                                Log('Games options to put in PCSX2.ini from /recalbox/share/system/configs/PCSX2/inis/ :')
                                options = games.find('options')
                                if options is not None:
                                    for string in options.iter('string'):
                                        Log('    [' + string.attrib["section"] + '] ' + string.attrib["name"]+ ' = ' + string.attrib["value"])
                                        self.setString(string.attrib["section"],string.attrib["name"],string.attrib["value"])
                                
                                ## Now for best matching game, we could get the inputs
                                ## optional by game
                                inputs = best_matching_game.find('inputs')
                                Log('Game inputs in PCSX2.ini from /recalbox/share/system/configs/PCSX2/inis/ :')
                                if inputs is not None:
                                    for string in inputs.iter('string'):
                                        Log('    [' + string.attrib["section"] + '] ' + string.attrib["name"]+ ' = ' + string.attrib["value"])
                                        self.setString(string.attrib["section"],string.attrib["name"],string.attrib["value"])
                                
                                ## and we could get also the options
                                ## optional by game
                                ## N/A for this system today
                                
                                ## and finally we could also have a specific emulator/core for a game
                                ## optional by game
                                emulator_game = best_matching_game.find('emulator')
                                if emulator_game is not None:
                                    Log('Game Emulator found: ' + emulator_game.attrib["name"])
                                    emulator = emulator_game
                                    core_game = emulator.find('core')
                                    if core_game is not None:
                                        Log('Game Core found: ' + core_game.text)
                                        core = core_game

                                ##save lightgun dedicated emulator & core
                                self.system.ChangeEmulatorAndCore(emulator.attrib["name"], core.text)
                                Log('Emulator value :' + self.system.Emulator)
                                Log('Core value :' + self.system.Core)
                                # Save pcsx2 configuration
                                self.pcsx2Settings.saveFile()
                                Log('Configured')
                                return 'Configured'
                            else:
                                Log('Not found as lightgun game')
        Log('Not found as lightgun game')                    
        return 'Not found as lightgun game'

    # To configure lightgun game
    def configureLightGunGame(self, LightgunIndexByPlayer):
        Log('system: ' + self.system.Name)
        #set indexes before to configure games
        self.MouseIndexByPlayer = LightgunIndexByPlayer
        ## get file name from es_state.inf file
        game_name = self.getGameNameFromESState()

        ## return 'Configured' if game is configured as LightGun game
        result = self.setLightGunConfig(self.system.Name,game_name, False)
        if result == 'Configured':
            return True
        else:
            return False

    # To check if lightgun game
    def isLightGunGame(self):
        Log('system: ' + self.system.Name)

        ## get file name from es_state.inf file
        game_name = self.getGameNameFromESState()

        ## return 'Game found' if game is declared as LightGun game
        result = self.setLightGunConfig(self.system.Name,game_name, True)
        if result == 'Game found':
            return True
        else:
            return False
