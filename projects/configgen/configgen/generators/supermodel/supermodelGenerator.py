#!/usr/bin/env python
from typing import List

from configgen.Command import Command
import configgen.recalboxFiles as recalboxFiles
from configgen.Emulator import Emulator
from configgen.controllers.controller import ControllerPerPlayer
from configgen.generators.Generator import Generator
from configgen.settings.keyValueSettings import keyValueSettings
from configgen.settings.iniSettings import IniSettings
from configgen.utils.videoMode import getCurrentResolution
import shutil
import os
import datetime
from os import path
from os import environ
import configgen.generators.supermodel.supermodelControllers as supermodelControllers

#utils to manage advanced overlays features for all emulators
import configgen.utils.overlays as overlays

def Log(txt):
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    print(formatted_time[:-3] + " - " + txt)
    return 0

class SupermodelGenerator(Generator):

    # Global configuration
    SECTION_GLOBAL = "Global"

    def mainConfiguration(self, system: Emulator, args):
        # Get recalbox.conf Options
        conf = keyValueSettings(recalboxFiles.recalboxConf)
        conf.loadFile(True)

        # Per game configuration
        # Many game need to change the power PC Frequency
        # 4 frequency conf as know :
        # https://github.com/mamedev/mame/blob/master/src/mame/sega/model3.cpp#L10
        # Step 1.0: 66 MHz PPC
        # Step 1.5: 100 MHz PPC, faster 3D engine
        # Step 2.0: 166 MHz PPC, even faster 3D engine
        # Step 2.1: 166 MHz PPC, same 3D engine as 2.0, differences unknown
        # For testing have forced configuration in supermodel.ini for games in 66 Mhz and 100Mhz over games in 166 Mhz on default 

        MODEL3_66MHZ = \
        {
        "vf3",
        "vf3c",
        "vf3a",
        "vf3tb",
        "bassdx",
        "getbassdx",
        "getbassur",
        "getbass"
        }

        MODEL3_100MHZ = \
        {
        "scud",
        "scuddx",
        "scudau",
        "scudplus",
        "scudplusa",
        "lostwsga",
        "lostwsgp",
        "vs215",
        "vs215",
        "vs215o",
        "lemans24",
        "vs29815",
        "vs29915",
        "vs29915a",
        "vs29915j",
        }

        # Force sound parameter by games
        # Model 3 sound and MPEG music are generated separately and then mixed by an
        # amplifier.  The relative signal levels are not known, so Supermodel simply
        # outputs all audio at full volume.  This causes the MPEG music to be too quiet
        # in some games ('Scud Race', 'Daytona USA 2') and too loud in others ('Star Wars
        # Trilogy').
        MPEG_MIX_QUIET_FIX = \
        {
        "scud",
        "scuddx",
        "scudau",
        "scudplus",
        "scudplusa",
        "dayto2pe",
        }

        MPEG_MIX_LOUD_FIX = \
        {
        "swtrilgy",
        "swtrilgyp",
        "swtrilgya",
        }

        # remove path and name extension
        romName = args.rom.split('/')[5].split('.')[0]

        #Video Options:
        getResolution = conf.getString("supermodel.resolution", "auto") # [Default: 640,480]
        refreshRate = conf.getString("system.primary.screen.frequency", 60.00)
        new3DEngine = conf.getBool("supermodel.new3d.engine", 1) # [Default]
        multiTexture = conf.getBool("supermodel.multi.texture", 0) # [Default] (legacy engine)
        quadRendering = conf.getBool("supermodel.quad.rendering", 0) # [Default]
        soundType = conf.getBool("supermodel.legacy.sound.engine", 0) # [Default]
        crosshairs = conf.getBool("supermodel.crosshairs", 1)
        SuperSampling = conf.getInt("supermodel.supersampling", 1) # [Default]
        UpscaleMode = conf.getInt("supermodel.upscalemode", 0) # [Default]
        CrtColors = conf.getInt("supermodel.crtcolors", 0) # [Default]
        # Audio Options:
        flipStereo = conf.getBool("supermodel.flip.stereo", 0)
        # Core Options:
        # powerPcFrequency = conf.getInt("supermodel.powerpc.frequency", 50) # [Default: 50]
        GpuThreaded = conf.getBool("supermodel.gpu.threaded", 1) # [Default]
        # Net Options:
        addressOut = conf.getString("supermodel.address.out", "127.0.0.1")
        network = conf.getInt("supermodel.network", 0)
        portIn = conf.getInt("supermodel.port.in", 1970)
        portOut = conf.getInt("supermodel.port.out", 1971)

        if not os.path.exists(recalboxFiles.supermodelRooFolder):
            os.makedirs(recalboxFiles.supermodelRooFolder)
            shutil(recalboxFiles.supermodelConfigInit, recalboxFiles.supermodelConfig)

        supermodelSettingsIni = IniSettings(recalboxFiles.supermodelConfig, True)
        supermodelSettingsIni.loadFile(True).defineBool('true', 'false')

        #Video Options:
        print("launch with resolution: "+ getResolution)
        if getResolution == "auto" or None:
            width, height = getCurrentResolution()
            supermodelSettingsIni.setInt(self.SECTION_GLOBAL, "XResolution", width) \
                                 .setInt(self.SECTION_GLOBAL, "YResolution", height)
        else:
            splitResolution = getResolution.split(',')
            supermodelSettingsIni.setInt(self.SECTION_GLOBAL, "XResolution", splitResolution[0]) \
                                 .setInt(self.SECTION_GLOBAL, "YResolution", splitResolution[1])

        # VSync forced to false to preserved perf
        supermodelSettingsIni.setBool(self.SECTION_GLOBAL, "LegacySoundDSP", soundType) \
                             .setBool(self.SECTION_GLOBAL, "New3DEngine", new3DEngine) \
                             .setBool(self.SECTION_GLOBAL, "QuadRendering", quadRendering) \
                             .setBool(self.SECTION_GLOBAL, "FullScreen", True) \
                             .setBool(self.SECTION_GLOBAL, "VSync", False) \
                             .setBool(self.SECTION_GLOBAL, "Throttle", True) \
                             .setString(self.SECTION_GLOBAL, "RefreshRate", refreshRate) \
                             .setBool(self.SECTION_GLOBAL, "MultiTexture", multiTexture) \
                             .setBool(self.SECTION_GLOBAL, "ShowFrameRate", True) \
                             .setInt(self.SECTION_GLOBAL, "CRTcolors", CrtColors) \
                             .setInt(self.SECTION_GLOBAL, "Supersampling", SuperSampling) \
                             .setInt(self.SECTION_GLOBAL, "UpscaleMode", UpscaleMode)

        if system.Ratio == "16/9":
            supermodelSettingsIni.setBool(self.SECTION_GLOBAL, "Stretch", False) \
                                 .setBool(self.SECTION_GLOBAL, "WideBackground", True) \
                                 .setBool(self.SECTION_GLOBAL, "WideScreen", True)
        elif system.Ratio == "squarepixel":
            supermodelSettingsIni.setBool(self.SECTION_GLOBAL, "Stretch", True) \
                                 .setBool(self.SECTION_GLOBAL, "WideBackground", False) \
                                 .setBool(self.SECTION_GLOBAL, "WideScreen", False)
        elif system.Ratio == "4/3" or "auto":
            supermodelSettingsIni.setBool(self.SECTION_GLOBAL, "Stretch", False) \
                                 .setBool(self.SECTION_GLOBAL, "WideBackground", False) \
                                 .setBool(self.SECTION_GLOBAL, "WideScreen", False)
        
        supermodelSettingsIni.setBool(self.SECTION_GLOBAL, "BorderlessWindow", True)
        if crosshairs == True : 
            # crosshairs off as default value because will be calculated from controllers part
            supermodelSettingsIni.setInt(self.SECTION_GLOBAL, "Crosshairs", 0)
            supermodelSettingsIni.setInt(self.SECTION_GLOBAL, "CrosshairStyle", "bmp") # to take into account new crosshair
        else: 
            supermodelSettingsIni.setInt(self.SECTION_GLOBAL, "Crosshairs", 0) # stay off for this time 
        # Core Options:
        supermodelSettingsIni.setBool(self.SECTION_GLOBAL, "GPUMultiThreaded", GpuThreaded)

        if romName in MODEL3_66MHZ:
            supermodelSettingsIni.setInt(romName, "PowerPCFrequency", 66)
        elif romName in MODEL3_100MHZ:
            supermodelSettingsIni.setInt(romName, "PowerPCFrequency", 100)
        else:
            supermodelSettingsIni.setInt(self.SECTION_GLOBAL, "PowerPCFrequency", 166)

        # Audio Options:
        # supermodelSettingsIni.setString(SECTION_GLOBAL, "EmulateSound", 1)
        # supermodelSettingsIni.setString(SECTION_GLOBAL, "EmulateDSB", 1)
        supermodelSettingsIni.setBool(self.SECTION_GLOBAL, "FlipStereo", flipStereo)
        supermodelSettingsIni.setInt(self.SECTION_GLOBAL, "SoundVolume", 100) \
                             .setInt(self.SECTION_GLOBAL, "MusicVolume", 100)

        if romName in MPEG_MIX_QUIET_FIX:
            supermodelSettingsIni.setInt(romName, "SoundVolume", 80) \
                                 .setInt(romName, "MusicVolume", 170)
        elif romName in MPEG_MIX_LOUD_FIX:
            supermodelSettingsIni.setInt(romName, "SoundVolume", 170) \
                                 .setInt(romName, "MusicVolume", 80)

        # Net Options:
        supermodelSettingsIni.setString(self.SECTION_GLOBAL, "AddressOut", '"' + addressOut + '"') \
                             .setBool(self.SECTION_GLOBAL, "Network", network) \
                             .setBool(self.SECTION_GLOBAL, "SimulateNet", network) \
                             .setInt(self.SECTION_GLOBAL, "PortIn", portIn) \
                             .setInt(self.SECTION_GLOBAL, "PortOut", portOut)

        # Save configuration
        supermodelSettingsIni.saveFile()

    def generate(self, system: Emulator, playersControllers: ControllerPerPlayer, recalboxOptions: keyValueSettings, args) -> Command:

        #if no /recalbox/share/saves/model3/savestate directory, we generate it
        if not os.path.exists(recalboxFiles.supermodelSavestates):
            #create directory
            #Log("Command: " + 'install -d' + recalboxFiles.supermodelSavestates)
            os.system('install -d ' + recalboxFiles.supermodelSavestates)
        #init boolean NeedPotentiallySindenBorder
        NeedPotentiallySindenBorder = False
        if not system.HasConfigFile:
            # Configuration
            self.mainConfiguration(system, args)
            # Controllers
            NeedPotentiallySindenBorder = supermodelControllers.generateControllerConfig(self, playersControllers, system)

        commandArray = [recalboxFiles.recalboxBins[system.Emulator], args.rom]

        env = {}
        env["XDG_CONFIG_HOME"] = recalboxFiles.CONF
        env["XDG_DATA_HOME"] = recalboxFiles.CONF
        env["XDG_CACHE_HOME"] = recalboxFiles.CACHE

        if system.HasArgs: commandArray.extend(system.Args)
        
        # To manage Vulkan/OpenGL overlay activated for monitoring or bezel display
        overlays.processOverlays(system, args.rom, recalboxOptions, env, commandArray, NeedPotentiallySindenBorder)
        
        #launch unclutter in background to remove display of cursor for this emulator
        os.system("(sleep 10.0; unclutter -noevents)&") #we wait 10s tu be sure that we activate it after game launching
        #with postExec to remove unclutter at the end of the game sesson ;-)
        return Command(videomode=system.VideoMode, array=commandArray, env=env, postExec="killall unclutter; sleep 1.0; killall unclutter")
