#!/usr/bin/env python
from typing import List, Dict

from configgen.controllers.controller import Controller, InputItem
from configgen.controllers.controller import ControllerPerPlayer
from configgen.Emulator import Emulator
from configgen.generators.supermodel.supermodelLightGuns import supermodelLightGun
from configgen.settings.keyValueSettings import keyValueSettings
from configgen.settings.iniSettings import IniSettings
import configgen.recalboxFiles as recalboxFiles
import datetime
import os
import subprocess
import re

def Log(txt):
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    print(formatted_time[:-3] + " - " + txt)
    return 0

def generateControllerConfig(self, playersControllers: ControllerPerPlayer, system: Emulator) -> bool:

    ################ Mapping Conversion Supermodel to Recalbox ################

    # set recalbox Hotkey
    HOTKEY_BUTTONS: Dict[str, int] = \
    {
        "InputUIExit":              InputItem.ItemStart, #confirmed
        "InputUISaveState":         InputItem.ItemY, #confirmed, need absolutely the 'savestate' directory, created now by configgen itself if needed
        "InputUIChangeSlot":        InputItem.ItemUp, #working partially, because we could only increase slot and never decrease, not usefull
        "InputUILoadState":         InputItem.ItemX, #confirmed, need absolutely the 'savestate' directory, created now by configgen itself if needed
        "InputUIScreenShot":        InputItem.ItemL1, #confirmed but generate .bmp file, not visible in web manager for the moment
        "InputUIReset":             InputItem.ItemA, #confirmed
        "InputUIPause":             InputItem.ItemB, #confirmed
    }

    SERVICE_TEST_BUTTON: Dict[str, int] = \
    {    
        "InputServiceA":            InputItem.ItemR3,
        "InputTestA":               InputItem.ItemL3,
    }

##################### Player 1 controllers ###############################

    BUTTONS_P1: Dict[str, int] = \
    {
        "InputCoin1":               InputItem.ItemSelect,
        "InputStart1":              InputItem.ItemStart,
        # Fighting game buttons
        "InputEscape":              InputItem.ItemY,
        "InputGuard":               InputItem.ItemX,
        "InputKick":                InputItem.ItemB,
        "InputPunch":               InputItem.ItemA,
        # Spikeout buttons
        "InputBeat":                InputItem.ItemB,
        "InputCharge":              InputItem.ItemX,
        "InputJump":                InputItem.ItemA,
        "InputShift":               InputItem.ItemY,
        # Virtua Striker buttons
        "InputLongPass":            InputItem.ItemB,
        "InputShoot":               InputItem.ItemY,
        "InputShortPass":           InputItem.ItemA,
        # Ski Champ controls
        "InputSkiPollLeft":         InputItem.ItemL1,
        "InputSkiPollRight":        InputItem.ItemR1,
        "InputSkiSelect1":          InputItem.ItemY,
        "InputSkiSelect2":          InputItem.ItemB,
        "InputSkiSelect3":          InputItem.ItemA,
        # Magical Truck Adventure controls
        "InputMagicalPedal1":       InputItem.ItemB,
        # Handbrake (Dirt Devils, Sega Rally 2)
        "InputHandBrake":           InputItem.ItemY,
        # Harley-Davidson controls
        "InputMusicSelect":         InputItem.ItemB,
        "InputRearBrake":           InputItem.ItemA,
        # Virtual On buttons
        "InputTwinJoyShot1":        InputItem.ItemX,
        # "InputTwinJoyShot2":        InputItem.ItemY,
        "InputTwinJoyTurbo1":       InputItem.ItemL1,
        # "InputTwinJoyTurbo2":       InputItem.ItemR1,
        # Virtual On macros
        "InputTwinJoyCrouch":       InputItem.ItemA,
        "InputTwinJoyJump":         InputItem.ItemB,
        # Up/down shifter manual transmission (all racers)
        "InputGearShiftDown":       InputItem.ItemL1,
        "InputGearShiftUp":         InputItem.ItemR1,
        # 4-Speed manual transmission (Daytona 2, Sega Rally 2, Scud Race)
        # unsettings N
        "InputGearShiftN":          -1,
        # VR4 view change buttons (Daytona 2, Le Mans 24, Scud Race)
        "InputVR1":                 InputItem.ItemY,
        "InputVR2":                 InputItem.ItemX,
        "InputVR3":                 InputItem.ItemA,
        "InputVR4":                 InputItem.ItemB,
        # Single view change button (Dirt Devils, ECA, Harley-Davidson, Sega Rally 2)
        "InputViewChange":          InputItem.ItemX,
        # Sega Bass Fishing / Get Bass controls
        "InputFishingCast":         InputItem.ItemY,
        "InputFishingSelect":       InputItem.ItemA,
    }

    DIGITAL_JOYSTICK_P1: Dict[str, int] = \
    {
        "InputJoyUp":               InputItem.ItemUp,
        "InputJoyDown":             InputItem.ItemDown,
        "InputJoyLeft":             InputItem.ItemLeft,
        "InputJoyRight":            InputItem.ItemRight,
        # Ski Champ controls
        "InputSkiLeft":             InputItem.ItemLeft,
        "InputSkiRight":            InputItem.ItemRight,
        "InputSkiUp":               InputItem.ItemUp,
        "InputSkiDown":             InputItem.ItemDown,
        # Steering wheel
        # Set digital, turn wheel
        "InputSteeringLeft":        InputItem.ItemLeft,
        "InputSteeringRight":       InputItem.ItemRight,
        # Magical Truck Adventure controls
        "InputMagicalLeverUp1":     InputItem.ItemUp,
        "InputMagicalLeverDown1":   InputItem.ItemDown,
        # Sega Bass Fishing / Get Bass controls
        "InputFishingRodLeft":      InputItem.ItemLeft,
        "InputFishingRodRight":     InputItem.ItemRight,
        "InputFishingRodUp":        InputItem.ItemUp,
        "InputFishingRodDown":      InputItem.ItemDown,
        # need setting only AXIS don't need a position (POS/NEG)
        # Magical Truck Adventure controls
        "InputMagicalLever1":       InputItem.ItemJoy1Up,
        # Steering wheel
        "InputSteering":            InputItem.ItemJoy1Left,
        # Ski Champ controls
        "InputSkiX":                InputItem.ItemJoy1Left,
        "InputSkiY":                InputItem.ItemJoy1Up,
        # Sega Bass Fishing / Get Bass controls
        "InputFishingRodX":         InputItem.ItemJoy2Left,
        "InputFishingRodY":         InputItem.ItemJoy2Up,
        "InputFishingStickX":       InputItem.ItemJoy1Left,
        "InputFishingStickY":       InputItem.ItemJoy1Up,
        # Analog joystick (Star Wars Trilogy)
        "InputAnalogJoyX":          InputItem.ItemJoy1Right,
        "InputAnalogJoyY":          InputItem.ItemJoy1Down,
    }

    BUTTONS_AXIS: Dict[str, int] = \
    {
        #### NEED A BYPASS BUTTON OR AXIS ####
        # Pedals
        "InputAccelerator":         InputItem.ItemR2,
        "InputBrake":               InputItem.ItemL2,
        #### NEED A BYPASS BUTTON OR AXIS ####
        "InputFishingReel":         InputItem.ItemL2,
        "InputFishingTension":      InputItem.ItemR2,
    }

    ANALOG_JOYSTICK_P1: Dict[str, int] = \
    {
        # Need setting all axis position (POS/NEG)
        # Virtual On individual joystick mapping
        "InputTwinJoyDown1":        InputItem.ItemJoy1Down,
        "InputTwinJoyDown2":        InputItem.ItemJoy2Down,
        "InputTwinJoyLeft1":        InputItem.ItemJoy1Left,
        "InputTwinJoyLeft2":        InputItem.ItemJoy2Left,
        "InputTwinJoyRight1":       InputItem.ItemJoy1Right,
        "InputTwinJoyRight2":       InputItem.ItemJoy2Right,
        "InputTwinJoyUp1":          InputItem.ItemJoy1Up,
        "InputTwinJoyUp2":          InputItem.ItemJoy2Up,
        # 4-Speed manual transmission (Daytona 2, Sega Rally 2, Scud Race)
        # Setting on joystick2
        "InputGearShift1":          InputItem.ItemJoy2Up,
        "InputGearShift2":          InputItem.ItemJoy2Down,
        "InputGearShift3":          InputItem.ItemJoy2Left,
        "InputGearShift4":          InputItem.ItemJoy2Right,
    }

    MOUSE_INPUTS_TO_PAD_P1: Dict[str, int] = \
    {
        # Light guns (Lost World)
        "InputGunX":                InputItem.ItemJoy1Left,
        "InputGunY":                InputItem.ItemJoy1Up,
        "InputTrigger":             InputItem.ItemB,
        "InputOffscreen":           InputItem.ItemA,
        # Analog guns (Ocean Hunter, LA Machineguns)
        "InputAnalogGunX":          InputItem.ItemJoy1Left,
        "InputAnalogGunY":          InputItem.ItemJoy1Up,
        "InputAnalogTriggerLeft":   InputItem.ItemB,
        "InputAnalogTriggerRight":  InputItem.ItemA,
        # Analog joystick (Star Wars Trilogy)
        "InputAnalogJoyTrigger":    InputItem.ItemB,
        "InputAnalogJoyEvent":      InputItem.ItemA,
        
    }

    ####################### Players 2 controllers ############################

    BUTTONS_P2: Dict[str, int] = \
    {
        # Commons buttons
        "InputCoin2":               InputItem.ItemSelect,
        "InputStart2":              InputItem.ItemStart,
        # Fighting game buttons
        "InputEscape2":             InputItem.ItemY,
        "InputGuard2":              InputItem.ItemX,
        "InputKick2":               InputItem.ItemB,
        "InputPunch2":              InputItem.ItemA,
        # Virtua Striker buttons
        "InputLongPass2":           InputItem.ItemA,
        "InputShoot2":              InputItem.ItemX,
        "InputShortPass2":          InputItem.ItemB,
        # Magical Truck Adventure controls
        "InputMagicalPedal2":       InputItem.ItemB,
    }

    DIGITAL_JOYSTICK_P2: Dict[str, int] = \
    {
        "InputJoyDown2":            InputItem.ItemDown,
        "InputJoyLeft2":            InputItem.ItemLeft,
        "InputJoyRight2":           InputItem.ItemRight,
        "InputJoyUp2":              InputItem.ItemUp,
        "InputMagicalLeverDown2":   InputItem.ItemDown,
        "InputMagicalLeverUp2":     InputItem.ItemUp,
        # Magical Truck Adventure controls
        "InputMagicalLever2":       InputItem.ItemJoy1Left, #change to joyleft
    }

    MOUSE_INPUTS_TO_PAD_P2: Dict[str, int] = \
    {
        # Light guns (Lost World)
        "InputGunX2":                InputItem.ItemJoy1Left,
        "InputGunY2":                InputItem.ItemJoy1Up,
        "InputTrigger2":             InputItem.ItemB,
        "InputOffscreen2":           InputItem.ItemA,
        # Analog guns (Ocean Hunter, LA Machineguns)
        "InputAnalogGunX2":          InputItem.ItemJoy1Left,
        "InputAnalogGunY2":          InputItem.ItemJoy1Up,
        "InputAnalogTriggerLeft2":   InputItem.ItemB,
        "InputAnalogTriggerRight2":  InputItem.ItemA,
    }

    ###### Map an Recalbox direction to the corresponding Supermodel ######

    TYPE_TO_NAME: Dict[int, str] = \
    {
        InputItem.TypeAxis:   'AXIS',
        InputItem.TypeButton: 'BUTTON',
        InputItem.TypeHat:    'POV',
    }

    HATS_TO_NAME: Dict[int, str] = \
    {
        1:                        'UP',
        2:                        'RIGHT',
        4:                        'DOWN',
        8:                        'LEFT',
    }

    
    DIGITAL_INPUT_NEED_DIRECTION_IF_AXIS = \
    [
        InputItem.ItemUp,
        InputItem.ItemDown,
        InputItem.ItemLeft,
        InputItem.ItemRight
    ]

    #prepare Axis table for player 1/2 to avoid several call 
    def prepareAxisTable(self, pad_player: Controller): 
        #command to know /dev/input/js? from /dev/input/event? used
        #cat /proc/bus/input/devices | grep -ni event16 | awk -v FS='(js|)' '{print $2}'
        cmd = "cat /proc/bus/input/devices | grep -ni " + pad_player.DevicePath.replace("/dev/input/", "").rstrip() + " | awk -v FS='(js|)' '{print $2}'"
        Log(f"command: {str(cmd)}")
        p = subprocess.Popen(cmd, shell=True, 
            stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
        p.wait()
        output = p.stdout 
        jsIndex = ""
        for line in output:
            jsIndex = line.rstrip().split(' ')[0]
        #command to know axes available 
        # timeout 1 jstest /dev/input/js0 | grep -i joystick | sed -e 's/(null)//g' | awk -F'[()]' '{print $(NF-1)}'
        # Joystick (Xbox 360 Wireless Receiver) has 8 axes (X, Y, Z, Rx, Ry, Rz, Hat0X, Hat0Y) 
        # 18-09-23: (Robustness)
        #   - add "| sed -e 's/(null)//g'" to remove "(null)" axis
        #   - replace ", " by new delimiter " " and after "," by nothing to remove last empty occurence to help parsing by split command
        cmd = "timeout 1 jstest /dev/input/js"+ jsIndex + " | grep -i joystick | sed -e 's/(null)//g' | awk -F'[()]' '{print $(NF-1)}'"
        Log(f"command: {str(cmd)}")
        p = subprocess.Popen(cmd, shell=True, 
            stdout=subprocess.PIPE, close_fds=True, encoding='utf8')
        p.wait()
        output = p.stdout 
        Log("pad_player.PlayerIndex : {}".format(str(pad_player.PlayerIndex)));
        controllerShortName = pad_player.DeviceName.split(' - ')[0]
        if pad_player.PlayerIndex == 1:
            Log("Set " + controllerShortName + " for player " + str(pad_player.PlayerIndex))
            for line in output: 
                #Log("AxisTableP1 content : '" + line.rstrip() + "'")
                self.AxisTableP1 = line.rstrip().replace(', ', ' ').replace(',', '').split(' ')
                Log("AxisTableP1 content : " + str(self.AxisTableP1))
            Log("AxisTableP1 lenght :" + str(len(self.AxisTableP1))) 
        if pad_player.PlayerIndex == 2:
            Log("Set " + controllerShortName + " for player " + str(pad_player.PlayerIndex))
            for line in output: 
                #Log("AxisTableP2 content : '" + line.rstrip() + "'")
                self.AxisTableP2 = line.rstrip().replace(', ', ' ').replace(',', '').split(' ')
                Log("AxisTableP2 content : " + str(self.AxisTableP2))
            Log("AxisTableP2 lenght :" + str(len(self.AxisTableP2))) 

    #generate specific values for axis for player 1/2
    def getAxisValue(self, id: int, table: list):
        Log("getAxisValue - id:" + str(id))
        axis = table[id]
        match axis:
            #the mapping could be with all this values: X,Y,Z,Rx,Ry,Rz,Hat0X,Hat0Y
            case "X":
                return "XAXIS"
            case "Y":
                return "YAXIS"
            case "Z":
                return "ZAXIS"
            case "Rx":
                return "RXAXIS"
            case "Ry":
                return "RYAXIS"
            case "Rz":
                if "Z" in table:
                    return "RZAXIS"
                else:
                    return "ZAXIS" #we return as ZAXIS if no other axis is exist as Z axis (specific to Supermodel)
            case default:
                return "NONE"
                
    def getConfigValue(inputItem: InputItem, axisTable: list):
        # Output format BUTTONX
        if inputItem.IsButton:
            Log("'{}{}'.format(TYPE_TO_NAME[inputItem.Type], inputItem.Id + 1) : " + '{}{}'.format(TYPE_TO_NAME[inputItem.Type], inputItem.Id + 1))
            return '{}{}'.format(TYPE_TO_NAME[inputItem.Type], inputItem.Id + 1)
        # Ouput format XAXIS, YAXIS, ZAXIS, RXAXIS, RYAXIS, RZAXIS, or ?AXIS_NEG/?AXIS_POS
        if inputItem.IsAxis:
            axisValue = getAxisValue(self, inputItem.Id, axisTable)
            if axisValue != "NONE":
                #new method calculated from inputItem.Id
                if inputItem.Item in DIGITAL_INPUT_NEED_DIRECTION_IF_AXIS:
                    if inputItem.Value == -1:
                        Log("AXIS VALUE : " + '{}_NEG'.format(axisValue))
                        return '{}_NEG'.format(axisValue)
                    else:
                        Log("AXIS VALUE : " + '{}_POS'.format(axisValue))
                        return '{}_POS'.format(axisValue)
                else:
                    Log("AXIS VALUE : " + '{}'.format(axisValue))
                    return '{}'.format(axisValue)
            else: 
                Log("AXIS VALUE : " + axisValue)
                return "NONE"
        # Output format POV1_DOWN
        if inputItem.IsHat:
            Log("'{}{}_{}'.format(TYPE_TO_NAME[inputItem.Type], inputItem.Id + 1, HATS_TO_NAME[inputItem.Value]) : " + '{}{}_{}'.format(TYPE_TO_NAME[inputItem.Type], inputItem.Id + 1, HATS_TO_NAME[inputItem.Value]))
            return '{}{}_{}'.format(TYPE_TO_NAME[inputItem.Type], inputItem.Id + 1, HATS_TO_NAME[inputItem.Value])
        raise TypeError

    # More Games need a Position value of Axis 
    def getPositionConfigValue(inputItem: InputItem, axisTable: list):
        # Output format BUTTONX
        if inputItem.IsButton:
            Log("'{}{}'.format(TYPE_TO_NAME[inputItem.Type], inputItem.Id + 1) : " + '{}{}'.format(TYPE_TO_NAME[inputItem.Type], inputItem.Id + 1))
            return '{}{}'.format(TYPE_TO_NAME[inputItem.Type], inputItem.Id + 1)
        # Ouput format XAXIS, YAXIS, ZAXIS, RXAXIS, RYAXIS, RZAXIS, or ?AXIS_NEG/?AXIS_POS
        elif inputItem.IsAxis:
            axisValue = getAxisValue(self, inputItem.Id, axisTable)
            if axisValue != "NONE":
                if inputItem.Value == -1:
                    Log("AXIS VALUE : " + '{}_NEG'.format(axisValue))
                    return '{}_NEG'.format(axisValue)
                else:
                    Log("AXIS VALUE : " + '{}_POS'.format(axisValue))
                    return '{}_POS'.format(axisValue)
            else: 
                Log("AXIS VALUE : " + axisValue)
                return "NONE"
        raise TypeError

    #to manage and calculate the good Crosshairs supermodel value
    def manageSupermodelCrosshairsValue(MouseIndexByPlayer: List, supermodelControllersSettings: keyValueSettings, recalboxOptions: keyValueSettings) -> int :
        #value expected from supermodel source code ;-)
        #  switch (crosshairs)
        #  {
        #  case 0: puts("Crosshairs disabled.");             break;
        #  case 3: puts("Crosshairs enabled.");              break;
        #  case 1: puts("Showing Player 1 crosshair only."); break;
        #  case 2: puts("Showing Player 2 crosshair only."); break;
        #  }
        #get value from existing value from PAD configuration
        CrosshairsValue = supermodelControllersSettings.getInt(self.SECTION_GLOBAL, "Crosshairs","0")
        #we check in this case if lightgun request crosshair
        for index, row in enumerate(MouseIndexByPlayer):
            if((("sinden" in row[1]) and (not recalboxOptions.getBool("lightgun.sinden.crossair.enabled", False))) and index == 0): #for player 1
                if CrosshairsValue == 3: 
                    CrosshairsValue = 2 #Only Player 2 crosshair potentially
                elif CrosshairsValue == 1: 
                    CrosshairsValue = 0 #Crosshairs disabled for all players potentially
            elif (((("sinden" in row[1]) and recalboxOptions.getBool("lightgun.sinden.crossair.enabled", False)) or ("mayflash" in row[1])) and index == 0): #for player 1
                if CrosshairsValue == 2: 
                    CrosshairsValue = 3 #Crosshairs enabled for all players potentially
                elif CrosshairsValue == 0: 
                    CrosshairsValue = 1 #Crosshairs enabled for player 1 only potentially
            if((("sinden" in row[1]) and (not recalboxOptions.getBool("lightgun.sinden.crossair.enabled", False))) and index == 1): #for player 2
                if CrosshairsValue == 3: 
                    CrosshairsValue = 1 #Showing Player 1 crosshair only
                elif CrosshairsValue == 2:
                    CrosshairsValue = 0 #Crosshairs disabled for all players
            elif (((("sinden" in row[1]) and recalboxOptions.getBool("lightgun.sinden.crossair.enabled", False)) or ("mayflash" in row[1])) and index == 1): #for player 2
                if CrosshairsValue == 1: 
                    CrosshairsValue = 3 #Crosshairs enabled for all players
                elif CrosshairsValue == 0: 
                    CrosshairsValue = 2 #Crosshairs enabled for player 2 only
            
        return CrosshairsValue

    # Load Configuration
    supermodelSettings = keyValueSettings(recalboxFiles.recalboxConf)
    supermodelSettings.loadFile(True)

    sensitivity = supermodelSettings.getInt("supermodel.sensitivity", 25)
    deadzone = supermodelSettings.getInt("supermodel.deadzone", 2)
    saturation = supermodelSettings.getInt("supermodel.saturation", 100)
    crosshairs = supermodelSettings.getBool("supermodel.crosshairs", True)

    supermodelControllersSettings = IniSettings(recalboxFiles.supermodelConfig, True)
    supermodelControllersSettings.loadFile(True)

    ## Set default configuration
    ## Set auto triggers activate on default
    ## automatic reload when off-screen
    supermodelControllersSettings.setInt(self.SECTION_GLOBAL, "InputAutoTrigger", 1) \
                                 .setInt(self.SECTION_GLOBAL, "InputAutoTrigger2", 1)

    supermodelControllersSettings.setString(self.SECTION_GLOBAL, "InputKeySensitivity", sensitivity)

    # joystick1 Player 1
    supermodelControllersSettings.setString(self.SECTION_GLOBAL, "InputJoy1XDeadZone", deadzone) \
                                 .setString(self.SECTION_GLOBAL, "InputJoy1YDeadZone", deadzone)
    # joystick2 player 1
    supermodelControllersSettings.setString(self.SECTION_GLOBAL, "InputJoy1RXDeadZone", deadzone) \
                                 .setString(self.SECTION_GLOBAL, "InputJoy1RYDeadZone", deadzone)
    # triggers R2/L2 player 1
    # supermodelControllersSettings.setOption(self.SECTION_GLOBAL, "InputJoy1RZDeadZone", deadzone)
    # supermodelControllersSettings.setOption(self.SECTION_GLOBAL, "InputJoy1ZDeadZone", deadzone)

    # joystick1 Player 2
    supermodelControllersSettings.setString(self.SECTION_GLOBAL, "InputJoy2YDeadZone", deadzone) \
                                 .setString(self.SECTION_GLOBAL, "InputJoy2ZDeadZone", deadzone)
    # joystick2 Player 2
    supermodelControllersSettings.setString(self.SECTION_GLOBAL, "InputJoy2RXDeadZone", deadzone) \
                                 .setString(self.SECTION_GLOBAL, "InputJoy2RYDeadZone", deadzone)
    # triggers R2/L2 player 2
    # supermodelControllersSettings.setString(self.SECTION_GLOBAL, "InputJoy2RZDeadZone", deadzone) \
    # supermodelControllersSettings..setString(self.SECTION_GLOBAL, "InputJoy2XDeadZone", deadzone)

    # joystick1 Players 1
    supermodelControllersSettings.setString(self.SECTION_GLOBAL, "InputJoy1RXSaturation", saturation) \
                                 .setString(self.SECTION_GLOBAL, "InputJoy1RYSaturation", saturation) \
                                 .setString(self.SECTION_GLOBAL, "InputJoy1RZSaturation", saturation) \
                                 .setString(self.SECTION_GLOBAL, "InputJoy1XSaturation", saturation) \
                                 .setString(self.SECTION_GLOBAL, "InputJoy1YSaturation", saturation) \
                                 .setString(self.SECTION_GLOBAL, "InputJoy1ZSaturation", saturation)
    # joystick2 Players 2
    supermodelControllersSettings.setString(self.SECTION_GLOBAL, "InputJoy2RXSaturation", saturation) \
                                 .setString(self.SECTION_GLOBAL, "InputJoy2RYSaturation", saturation) \
                                 .setString(self.SECTION_GLOBAL, "InputJoy2RZSaturation", saturation) \
                                 .setString(self.SECTION_GLOBAL, "InputJoy2XSaturation", saturation) \
                                 .setString(self.SECTION_GLOBAL, "InputJoy2YSaturation", saturation) \
                                 .setString(self.SECTION_GLOBAL, "InputJoy2ZSaturation", saturation)

    # set SDL as default inputs system
    supermodelControllersSettings.setString(self.SECTION_GLOBAL, "InputSystem", "sdl") \

    #reset specific commnds
    supermodelControllersSettings.setString(self.SECTION_GLOBAL, "InputStart1", "NONE")
    supermodelControllersSettings.setString(self.SECTION_GLOBAL, "InputStart2", "NONE")
    supermodelControllersSettings.setString(self.SECTION_GLOBAL, "InputCoin1", "NONE")
    supermodelControllersSettings.setString(self.SECTION_GLOBAL, "InputCoin2", "NONE")
    supermodelControllersSettings.setString(self.SECTION_GLOBAL, "InputUIExit", "NONE")
    
    #to determine axis value
    self.AxisTableP1 = []
    self.AxisTableP2 = []

    #-----------------------------------------------------------------------------------------------------------------------------
    #---------------- part now dedicated only to PAD linked to each players (mouse and lightgun are separated now) ---------------
    #-----------------------------------------------------------------------------------------------------------------------------
    for playercontroller in playersControllers:
        pad: Controller = playersControllers[playercontroller]
        padIndex = 'JOY{}'.format(pad.NaturalIndex + 1)
        Log(" playerIndex of " + pad.DeviceName + " : " + str(pad.PlayerIndex))
        Log(" SdlIndex of " + pad.DeviceName + " : " + str(pad.SdlIndex))
        Log(" naturalIndex of " + pad.DeviceName + " : " + str(pad.NaturalIndex))
        
        #for player 1 only
        if pad.PlayerIndex == 1:
            
            #to prepare axis table for player 1
            prepareAxisTable(self,pad)
            Log("padIndex - player 1 : " + padIndex)
            # Add hotkey buttons
            for x in HOTKEY_BUTTONS:
                if pad.HasInput(HOTKEY_BUTTONS[x]):
                    inp = pad.Input(HOTKEY_BUTTONS[x])
                    supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}_{}+{}_{}"'.format(padIndex, getConfigValue(pad.Input(InputItem.ItemHotkey), self.AxisTableP1), padIndex, getConfigValue(inp, self.AxisTableP1)))

            # Service and Test menu
            # Set on 0 or 1 in ConfigModel3.ini
            for x in SERVICE_TEST_BUTTON:
                if pad.HasInput(SERVICE_TEST_BUTTON[x]):
                    inp = pad.Input(SERVICE_TEST_BUTTON[x])
                    ServiceBtn = supermodelSettings.getInt("supermodel.service.button", 0)
                    if ServiceBtn == 1:
                        configValue = getConfigValue(inp, self.AxisTableP1)
                        if configValue == "NONE":
                            supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}"'.format(configValue))
                        else:
                            supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}_{}"'.format(padIndex, configValue))
                    else:
                        supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"NONE"')
                else:
                    supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}"'.format("NONE"))

            for x in BUTTONS_P1:
                if pad.HasInput(BUTTONS_P1[x]):
                    inp = pad.Input(BUTTONS_P1[x])
                    configValue = getConfigValue(inp, self.AxisTableP1)
                    if configValue == "NONE":
                        supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}"'.format(configValue))
                    else:
                        supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}_{}"'.format(padIndex, configValue))
                else:
                    supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}"'.format("NONE"))

            for x in DIGITAL_JOYSTICK_P1:
                if pad.HasInput(DIGITAL_JOYSTICK_P1[x]):
                    inp = pad.Input(DIGITAL_JOYSTICK_P1[x])
                    configValue = getConfigValue(inp, self.AxisTableP1)
                    if configValue == "NONE":
                        supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}"'.format(configValue))
                    else:
                        supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}_{}"'.format(padIndex, configValue))
                else:
                    supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}"'.format("NONE"))

            for x in ANALOG_JOYSTICK_P1:
                if pad.HasInput(ANALOG_JOYSTICK_P1[x]): # for up/left direction
                    inp = pad.Input(ANALOG_JOYSTICK_P1[x])
                    configValue = getPositionConfigValue(inp, self.AxisTableP1)
                    if configValue == "NONE":
                        supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}"'.format(configValue))
                    else:
                        supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}_{}"'.format(padIndex, configValue))
                else:
                    supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}"'.format("NONE"))

            for x in BUTTONS_AXIS:
                if pad.HasInput(BUTTONS_AXIS[x]):
                    inp = pad.Input(BUTTONS_AXIS[x])
                    configValue = getPositionConfigValue(inp, self.AxisTableP1)
                    if configValue == "NONE":
                        supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}"'.format(configValue))
                    else:
                        supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}_{}"'.format(padIndex, configValue))
                else:
                    supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}"'.format("NONE"))

            for x in MOUSE_INPUTS_TO_PAD_P1:
                if pad.HasInput(MOUSE_INPUTS_TO_PAD_P1[x]):
                    # Select crosshairs for 1 playersonly if any input is compatible
                    if crosshairs == True:
                        if ("X" in x) or ("Y" in x): #to detect if it's an axis conmpatible to control the mouse
                            supermodelControllersSettings.setString(self.SECTION_GLOBAL, "Crosshairs", "1")
                    inp = pad.Input(MOUSE_INPUTS_TO_PAD_P1[x])
                    configValue = getConfigValue(inp, self.AxisTableP1)
                    if configValue == "NONE":
                        supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}"'.format(configValue))
                    else:
                        supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}_{}"'.format(padIndex, configValue))
                else:
                    supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}"'.format("NONE"))
        
        #for player 2 only
        if pad.PlayerIndex == 2:
            
            #to prepare axis table for player 2
            prepareAxisTable(self,pad)
            Log("padIndex - player 2 : " + padIndex)
            
            for x in BUTTONS_P2:
                if pad.HasInput(BUTTONS_P2[x]):
                    inp = pad.Input(BUTTONS_P2[x])
                    configValue = getConfigValue(inp, self.AxisTableP2)
                    if configValue == "NONE":
                        supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}"'.format(configValue))
                    else:
                        supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}_{}"'.format(padIndex, configValue))
                else:
                    supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}"'.format("NONE"))

            for x in DIGITAL_JOYSTICK_P2:
                if pad.HasInput(DIGITAL_JOYSTICK_P2[x]):
                    inp = pad.Input(DIGITAL_JOYSTICK_P2[x])
                    configValue = getConfigValue(inp, self.AxisTableP2)
                    if configValue == "NONE":
                        supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}"'.format(configValue))
                    else:
                        supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}_{}"'.format(padIndex, configValue))
                else:
                    supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}"'.format("NONE"))

            for x in MOUSE_INPUTS_TO_PAD_P2:
                if pad.HasInput(MOUSE_INPUTS_TO_PAD_P2[x]):
                    # Select crosshairs for 2 players only if any input is compatible
                    if crosshairs == True:
                        if ("X" in x) or ("Y" in x): #to detect if it's an axis conmpatible to control the mouse
                            supermodelControllersSettings.setString(self.SECTION_GLOBAL, "Crosshairs", "3")
                    inp = pad.Input(MOUSE_INPUTS_TO_PAD_P2[x])
                    configValue = getConfigValue(inp, self.AxisTableP2)
                    if configValue == "NONE":
                        supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}"'.format(configValue))
                    else:
                        supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}_{}"'.format(padIndex, configValue))
                else:
                    supermodelControllersSettings.setString(self.SECTION_GLOBAL, x, '"{}"'.format("NONE"))

        #for more pad than 2 we should stop
        if pad.PlayerIndex >= 2:
            break

    # Save configuration for PADs
    supermodelControllersSettings.saveFile()
    
    #-----------------------------------------------------------------------------------------------------------------------------
    #------------------------------- part to manage mouse/lightgun controls-------------------------------------------------------
    #-----------------------------------------------------------------------------------------------------------------------------
    
    #init lightgun config (configure wiimote or a default mouse)
    self.lightgunConfig = supermodelLightGun(system, supermodelSettings)
    #check first if it's a lightgun game
    #init boolean NeedPotentiallySindenBorder
    NeedPotentiallySindenBorder = False
    if self.lightgunConfig.isLightGunGame():
        ## Parts to configure wiimote(s)/mouse(s)
        ## to check if Mayflash_Wiimote_PC_Adapter/Sinden exists and get indexes in a first time
        LightgunIndexByPlayer, NeedPotentiallySindenBorder = Controller.findLightguns()
        if(LightgunIndexByPlayer[0][0] != "nul"):
            self.lightgunFound = True
        else:
            self.lightgunFound = False
        if not self.lightgunFound:
            #Search if mouse(s) exist(s) and get indexes in a first time
            LightgunIndexByPlayer = Controller.findMice()
            if(LightgunIndexByPlayer[0][0] != "nul"):
                self.mouseFound = True
            else:
                self.mouseFound = False
        if self.lightgunFound or self.mouseFound: 
            # Configure MOUSE(S) from lightgun(s) (in priority) or mouse/mice
            self.lightgunConfig.configureLightGunGame(LightgunIndexByPlayer)
            #activate evdev to use mouse/wiimote only
            supermodelControllersSettings = IniSettings(recalboxFiles.supermodelConfig, True)
            supermodelControllersSettings.loadFile(True)
            supermodelControllersSettings.setString(self.SECTION_GLOBAL, "InputSystem", "evdev")
            #activate cursor depending number of gamepad/wiimote/mouse and sinden option
            CrosshairsValue = manageSupermodelCrosshairsValue(LightgunIndexByPlayer, supermodelControllersSettings, supermodelSettings)
            supermodelControllersSettings.setString(self.SECTION_GLOBAL, "Crosshairs",str(CrosshairsValue))  
            supermodelControllersSettings.saveFile()
    return NeedPotentiallySindenBorder

