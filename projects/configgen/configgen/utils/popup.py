#!/usr/bin/env python3

import os
import subprocess
import sys

import datetime
def Log(txt):
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    #print(formatted_time[:-3] + " - " + txt)
    return 0

#Display a popup message during game loading/configuration 
# if delay = 0, no timeout activated, a popuClean will be necessary in this case
def Message(message: str, delay: float):
    #calculate screen width to well display popup message at the bottom of the screen
    from configgen.utils.videoMode import getCurrentResolution
    screenWidth, screenHeight = getCurrentResolution()
    fontSize = int((screenWidth / 100) * 1.77) # for 1.77% of the screen for the font size
    shiftFromBorder = int((screenWidth/100)*2) # for 2% of shift as margin
    popupWidth = screenWidth - (shiftFromBorder*2)
    popupHeight = int(fontSize*1.8) # 1,8 x size of the font
    Log('echo -e "'+ message + '" | xmessage -timeout ' + str(delay)+ ' -buttons "" -font "-*-*-*-*-*-*-' + str(fontSize) + '-*-*-*-*-*-iso8859-1" -bw 0 -fg white -bg black -bd black -geometry ' + str(popupWidth) + 'x'+ str(popupHeight) + '+' + str(shiftFromBorder) + '-' + str(shiftFromBorder) + ' -file - &')
    os.system('echo -e "'+ message + '" | xmessage -timeout ' + str(delay)+ ' -buttons "" -font "-*-*-*-*-*-*-' + str(fontSize) + '-*-*-*-*-*-iso8859-1" -bw 0 -fg white -bg black -bd black -geometry ' + str(popupWidth) + 'x'+ str(popupHeight) + '+' + str(shiftFromBorder) + '-' + str(shiftFromBorder) + ' -file - &')
    # examples
    # echo -e "ceci est un test de police, veuillez patienter svp..." |xmessage -timeout 10 -buttons "" -font "-*-*-*-*-*-*-34-*-*-*-*-*-iso8859-1" -bw 0 -fg white -bg black -bd black -geometry 1200x50+40-40 -file - &
    # echo -e "ceci est un test de police, veuillez patienter svp..." |xmessage -timeout 10 -buttons "" -font -*-*-*-*-*-*-34-*-*-*-*-*-iso8859-1 -bw 0 -fg white -bg black -bd black -geometry 1200x50+40-40 -file - &    
    # echo -e "Hello,\nThis is\na multiple lines\nmessage" | xmessage -timeout 1 -buttons "" -bw 0 -fg white -bg black -bd black -geometry 1920x70+0-0 -file -        

def Clean():
    os.system('killall -s SIGKILL xmessage &')
    #example
    #killall -s SIGKILL xmessage &

