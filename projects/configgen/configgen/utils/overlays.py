#!/usr/bin/env python3
from typing import List

import os
import subprocess
import sys
import shutil

import configgen.recalboxFiles as recalboxFiles
from configgen.Emulator import Emulator
from configgen.settings.keyValueSettings import keyValueSettings
import struct

from PIL import Image
from PIL import ImageDraw
from PIL import ImageOps

def Log(txt):
    import datetime
    current_time = datetime.datetime.now()
    formatted_time = current_time.strftime('%Y-%m-%d %H:%M:%S.%f')
    #uncomment/comment the following line to activate/desactivate additional logs on this script
    print(formatted_time[:-3] + " - " + txt)
    return 0

# Much faster than PIL Image.size
def fast_image_size(image_file):
    if not os.path.exists(image_file):
        return -1, -1
    with open(image_file, 'rb') as fhandle:
        head = fhandle.read(32)
        if len(head) != 32:
           # corrupted header, or not a PNG
           return -1, -1
        check = struct.unpack('>i', head[4:8])[0]
        if check != 0x0d0a1a0a:
           # Not a PNG
           return -1, -1
        return struct.unpack('>ii', head[16:24]) #image width, height

def resizeImage(input_png: str, output_png: str, screen_width: int, screen_height: int, bezel_stretch: bool = False, horizontalCropping: int = 0):
    #Log(f"Resizing bezel - initial screen_width: {screen_width}")
    #Log(f"Resizing bezel - horizontalCropping: {horizontalCropping}")
    imgin = Image.open(input_png)
    Log(f"Resizing bezel - image mode: {imgin.mode}")
    if imgin.mode != "RGBA":
        fillcolor = 'black'
        alphaPaste(input_png, output_png, fillcolor, (screen_width, screen_height), bezel_stretch)
    else:
        if(bezel_stretch):
            imgin = imgin.resize((screen_width, screen_height), Image.BICUBIC)
            imgin.save(output_png, mode="RGBA", format="PNG")
        else:
            #new method to have a best color convertion
            width, height = imgin.size
            i_ratio = (float(width) / float(height))
            s_ratio = (float(screen_width) / float(screen_height))
            crop = 0
            if (i_ratio - s_ratio > 0.01):
                # cut off bezel sides for 16:10 screens
                new_width = int(width*(s_ratio/i_ratio))
                delta = int(width-new_width)
                if delta != 0:
                    crop = int(delta/2)
                    #Log(f"Resizing bezel - ratio - crop (for each side): {crop}")
                    imgin = imgin.crop((crop, 0, new_width + crop, height))
                    #Log(f"Resizing bezel - ratio - new image width: {outwidth}")
                    #Log(f"Resizing bezel - ratio - new image outheight: {outheight}")
                    imgin = imgin.resize((screen_width, screen_height), Image.BICUBIC)
                    width, height = imgin.size
            
            if horizontalCropping >= 1 and horizontalCropping <= 100:
                crop = int(((width * horizontalCropping)/100)/2)
                imgin = imgin.crop((crop, 0, width - crop, height))
                imgin = imgin.resize((screen_width, screen_height), Image.BICUBIC)
            
            imgin.save(output_png, mode="RGBA", format="PNG")        

def getTargetedResolution(videoMode) -> (str, str):
    # Screen resolution
    from configgen.utils.resolutions import ResolutionParser
    resolution = ResolutionParser(videoMode)
    if resolution.isSet and resolution.selfProcess:
        width = str(resolution.width)
        height = str(resolution.height)
    else: #to propose default value
        width = "1920"
        height = "1080"
    resolution = (width, height)
    Log("The Targeted resolution is : "+ width + ":" + height)
    return resolution
    
def getPictureResolution(picturePath) -> (int, int):
    ##get picture resolution
    Log("The picture path is :  " + picturePath)
    im = Image.open(picturePath)
    if os.path.isfile(picturePath):
        resolution = im.size
        Log("The picture resolution is :  " + str(resolution[0]) + " x " + str(resolution[1]))
        return resolution[0], resolution[1]
    else:
        return 0,0

# function to improve way to manage overlay combinaisons
def overlayIsActivated(recalboxOptions: keyValueSettings, systemName: str) -> bool:
        #Principles: 
        # true -> activated
        # false or missing -> not activated
        # global parameter is in priority if system parameter is not yet set from user.
        # In Pegasus-frontend, system option is as global one if never set from menu by user.
        # but if user sets from system options menu himself or from recalbox.conf, the priority become the system one.
        if recalboxOptions.getBool(systemName + ".recalboxoverlays",recalboxOptions.getBool("global.recalboxoverlays",True)):
            Log("Overlay is activated !")
            return True
        else:
            Log("Overlay is not activated !")
            return False

# function to manage shaderset with overlay combinaisons
def shadersetIsUsingOverlay(recalboxOptions: keyValueSettings, systemName: str) -> bool:
        #Principles: 
        # not "none" and contains "_above_overlay" -> activated
        # "none" or missing or didn't conatains "_above_overlay" -> not activated
        # global parameter is in priority if system parameter is not yet set from user.
        # In Pegasus-frontend, system option is as global one if never set from menu by user.
        # but if user sets from system options menu himself or from recalbox.conf, the priority become the system one.
        shaderset = recalboxOptions.getString(systemName + ".shaderset",recalboxOptions.getString("global.shaderset","none"))
        if "_above_overlay" in shaderset:
            Log("Shaderset " + shaderset + " is using overlay !")
            return True
        else:
            Log("Shaderset " + shaderset + " is not using overlay !")
            return False

# function to increase shader size in purcentage to well cover system/custom overlay
def shaderBorderCoverage(recalboxOptions: keyValueSettings, systemName: str) -> int:
        #Principles: 
        # missing -> return 5% (as average of needed coverage)
        # global parameter is in priority if system parameter is not yet set from user.
        # In Pegasus-frontend, system option is as global one if never set from menu by user.
        # but if user sets from system options menu himself or from recalbox.conf, the priority become the system one.
        return recalboxOptions.getInt(systemName + ".shaderbordercoverage",recalboxOptions.getInt("global.shaderbordercoverage",5))

# function to get shader file from shaderset selected
def getShaderFileFromShaderSetSelected(recalboxOptions: keyValueSettings, systemName: str) -> str:
        #Principles: 
        # not "none" and contains "_above_overlay" -> activated
        # "none" or missing or didn't conatains "_above_overlay" -> not activated
        # global parameter is in priority if system parameter is not yet set from user.
        # In Pegasus-frontend, system option is as global one if never set from menu by user.
        # but if user sets from system options menu himself or from recalbox.conf, the priority become the system one.
        shaderset = recalboxOptions.getString(systemName + ".shaderset",recalboxOptions.getString("global.shaderset","none"))
        # Shaders
        if len(shaderset) != 0 and shaderset != 'none':
            import configgen.recalboxFiles as recalboxFiles
            # For choose good shaders (vulkan / opengl)
            from configgen.utils.videoMode import GetHasVulkanSupport
            shaderFileExtension = "slang" if GetHasVulkanSupport() == '1' else "glsl"
            shadersCfgFile = recalboxFiles.shaderPresetRoot + '/' + shaderset + '.cfg'
            shadersCfgContent = keyValueSettings(shadersCfgFile, False)
            shadersCfgContent.loadFile(True)
            shaderFile = shadersCfgContent.getString(systemName + "_" + shaderFileExtension, "")
            if len(shaderFile) != 0:
                return shaderFile
            else:
                return ""
        else:
            return ""

def getOverlayPictureFileAndData(userOverlayApplied: str) -> (str, int, int, int, int, int, int):
    #if overlay identified
    if userOverlayApplied != "":
        #get screen resolution to see if overlay if matching with it
        from configgen.utils.videoMode import getCurrentResolution
        screenWidth, screenHeight = getCurrentResolution()
        Log("processOverlays - the resolution detected is " + str(screenWidth) + "x" + str(screenHeight))
        Log("processOverlays - the ratio detected is " + str(screenWidth / screenHeight))
    
        #read and copy existing overlay in tmp
        root_overlay_path = userOverlayApplied
        
        ##loading root overlay (usually with "system" or "rom" .cfg as file name)...
        Log("root_overlay_path : " + root_overlay_path)
        root_overlay_file = keyValueSettings(root_overlay_path, True)
        root_overlay_file.loadFile(True)

        ##load overlay existing parameter or take default values for later...
        custom_viewport_height = int(root_overlay_file.getString("custom_viewport_height", "0").replace('"',''))
        custom_viewport_width = int(root_overlay_file.getString("custom_viewport_width", "0").replace('"',''))
        custom_viewport_x = int(root_overlay_file.getString("custom_viewport_x", "0").replace('"',''))
        custom_viewport_y = int(root_overlay_file.getString("custom_viewport_y", "0").replace('"',''))
        video_fullscreen_x = int(root_overlay_file.getString("video_fullscreen_x", "0").replace('"',''))
        video_fullscreen_y = int(root_overlay_file.getString("video_fullscreen_y", "0").replace('"',''))

        
        ##loading input overlay (usually with "system" or "rom" _overlay.cfg as file name)...
        input_overlay_path = root_overlay_file.getString("input_overlay","").replace('//','/').replace('"','')
        Log("input_overlay_path : " + input_overlay_path)
        if input_overlay_path != "":
            input_overlay_file = keyValueSettings(input_overlay_path, True)
            input_overlay_file.loadFile(True)
            
            ##load input overlay existing parameter or take default values for later...
            nb_overlays = input_overlay_file.getString("overlays", "0")
            Log("...nb overlay : " + nb_overlays)
            if(nb_overlays == "0"):
                #issue detected missing overlay configuration / need to return empty one to avoid crash
                overlay_picture_file = ""
                return overlay_picture_file, 0, 0, 0, 0, 0, 0
            Log("...overlay0_overlay : " + input_overlay_file.getString("overlay0_overlay", ""))
            overlay_picture_file = os.path.dirname(input_overlay_path.replace('//','/').replace('"','')) + "/" + input_overlay_file.getString("overlay0_overlay", "").replace('"','')
            Log("overlay_picture_file : " + str(overlay_picture_file))
            return overlay_picture_file, custom_viewport_x, custom_viewport_y, custom_viewport_width, custom_viewport_height, video_fullscreen_x, video_fullscreen_y
    else:
        #issue detected missing overlay configuration / need to return empty one to avoid crash
        overlay_picture_file = ""
        return overlay_picture_file, 0, 0, 0, 0, 0, 0
def matchOverlaySizeToScreen(userOverlayApplied: str, horizontalCropping : int = 0) -> str:
    #Log(f"matchOverlaySizeToScreen - initial horizontalCropping: {horizontalCropping}")
    #if overlay identified
    if userOverlayApplied != "":
        #get screen resolution to see if overlay if matching with it
        from configgen.utils.videoMode import getCurrentResolution
        screenWidth, screenHeight = getCurrentResolution()
        Log("processOverlays - the resolution detected is " + str(screenWidth) + "x" + str(screenHeight))
        Log("processOverlays - the ratio detected is " + str(screenWidth / screenHeight))
    
        #read and copy existing overlay in tmp
        root_overlay_path = userOverlayApplied
        
        ##loading root overlay (usually with "system" or "rom" .cfg as file name)...
        Log("root_overlay_path : " + root_overlay_path)
        root_overlay_file = keyValueSettings(root_overlay_path, True)
        root_overlay_file.loadFile(True)

        ##load overlay existing parameter or take default values for later...
        custom_viewport_height = int(root_overlay_file.getString("custom_viewport_height", "0").replace('"',''))
        custom_viewport_width = int(root_overlay_file.getString("custom_viewport_width", "0").replace('"',''))
        custom_viewport_x = int(root_overlay_file.getString("custom_viewport_x", "0").replace('"',''))
        custom_viewport_y = int(root_overlay_file.getString("custom_viewport_y", "0").replace('"',''))

        
        ##loading input overlay (usually with "system" or "rom" _overlay.cfg as file name)...
        input_overlay_path = root_overlay_file.getString("input_overlay","").replace('//','/').replace('"','')
        Log("input_overlay_path : " + input_overlay_path)
        if input_overlay_path != "":
            input_overlay_file = keyValueSettings(input_overlay_path, True)
            input_overlay_file.loadFile(True)
            
            ##load input overlay existing parameter or take default values for later...
            nb_overlays = input_overlay_file.getString("overlays", "0")
            Log("...nb overlay : " + nb_overlays)
            if(nb_overlays == "0"):
                #issue detected missing overlay configuration / need to return empty one to avoid crash
                userOverlayApplied = ""
                return userOverlayApplied
            Log("...overlay0_overlay : " + input_overlay_file.getString("overlay0_overlay", ""))
            overlay_picture_file = os.path.dirname(input_overlay_path.replace('//','/').replace('"','')) + "/" + input_overlay_file.getString("overlay0_overlay", "").replace('"','')
            Log("overlay_picture_file : " + str(overlay_picture_file))
            #get size of overlay picture
            pictureWidth, pictureHeight = getPictureResolution(overlay_picture_file)
            #check if screen resolution is different of picture resolution
            if((pictureWidth != screenWidth) or (pictureHeight != screenHeight) or (horizontalCropping != 0)):
                #Save overlay files (2 .cfg + .png) in tmp before updates
                root_resized_overlay_path = "/tmp/resized_overlay.cfg"
                Log("new tmp root_resized_overlay_path : " + root_resized_overlay_path)
                input_resized_overlay_path = "/tmp/input_resized_overlay.cfg"
                Log("new tmp input_resized_overlay_path : " + input_resized_overlay_path)
                output_resized_png_file = "/tmp/resized_overlay.png"
                Log("new tmp output_resized_png_file : " + output_resized_png_file)
                #remove previous ones to be sure about update ;-)
                os.system("rm " + root_resized_overlay_path)
                os.system("rm " + input_resized_overlay_path)
                os.system("rm " + output_resized_png_file)
                #save in new .cfg files
                input_overlay_file.changeSettingsFile(input_resized_overlay_path)
                #set overlay 0 overlay
                input_overlay_file.setString("overlay0_overlay", "resized_overlay.png")
                input_overlay_file.saveFile()
                #set also the input overlay path with new value
                root_overlay_file.setString("input_overlay", '"' + input_resized_overlay_path + '"')
                root_overlay_file.changeSettingsFile(root_resized_overlay_path)
                #update custom_viewport if needed
                if(custom_viewport_height != "0") or (custom_viewport_width != "0"):
                    #calculate vertical ratio between existing overlay and targeted screen 
                    # because resize of overlay is always done to keep the maximum of vertical display on 16/9 - 16/10 screen
                    heightRatio = pictureHeight/screenHeight # this ratio is to recalculate the size of viewport and y
                    xDelta = int(((pictureWidth/heightRatio) - screenWidth)/2)
                    # this delta is used if we from 16/9 to 16/10
                    viewport_width = int(int(custom_viewport_width)/heightRatio)
                    if( viewport_width > screenWidth ) : 
                        viewport_width = screenWidth
                    root_overlay_file.setString("custom_viewport_width", '"' + str(viewport_width) + '"')
                    viewport_height = int(int(custom_viewport_height)/heightRatio)
                    if( viewport_height > screenHeight ) : 
                        viewport_height = screenHeight
                    root_overlay_file.setString("custom_viewport_height", '"' + str(viewport_height) + '"')
                    viewport_x = int(int(custom_viewport_x)/heightRatio) - xDelta
                    if( viewport_x < 0 ) : 
                        viewport_x = 0
                    root_overlay_file.setString("custom_viewport_x", '"' + str(viewport_x) + '"')
                    viewport_y = int(int(custom_viewport_y)/heightRatio)
                    if( viewport_y < 0 ) : 
                        viewport_y = 0
                    root_overlay_file.setString("custom_viewport_y", '"' + str(viewport_y) + '"')
                root_overlay_file.setString("video_fullscreen_x", '"' + str(screenWidth) + '"')
                root_overlay_file.setString("video_fullscreen_y", '"' + str(screenHeight) + '"')
                #save changes
                root_overlay_file.saveFile()
                #resize overlay png file first
                #Log(f"matchOverlaySizeToScreen - horizontalCropping: {horizontalCropping}")
                if horizontalCropping != 0:
                    resizeImage(overlay_picture_file, output_resized_png_file, screenWidth, screenHeight, False, horizontalCropping)
                else:
                    resizeImage(overlay_picture_file, output_resized_png_file, screenWidth, screenHeight, False)
                #select overlay from tmp
                userOverlayApplied = root_resized_overlay_path

        else:
            Log("Warning: No input overlay !")
    #return overlay udpated or not
    return userOverlayApplied

# Retroarch Overlay management
def processRetroarchOverlays(system: Emulator, romName: str, configs: List[str], recalboxOptions: keyValueSettings, needSindenBorder: bool):
    # User overlays
    userOverlayApplied = ""
    # Shader File Using Overlay from Shader Set
    shaderFileUsingOverlay = ""

    # If we are in crt mode, we only allow recalbox default 240p overlays
    if recalboxOptions.hasOption("system.crt"):
        if system.RecalboxOverlays:
            crtOverlayFile = "{}/{}/{}.cfg".format(recalboxFiles.RECALBOX_240P_OVERLAYS, system.Name,
                                                       system.Name)
            if os.path.isfile(crtOverlayFile):
                userOverlayApplied = crtOverlayFile

    # Overlays are applied only when we are not in wide core
    else:
        # ------------------ part to get overlays available by overload, by rom, by system, by ratio ------------------
        if system.Core not in ["genesisplusgxwide"] and overlayIsActivated(recalboxOptions, system.Name):
            Log("part to get overlays available by overload, by rom, by system, by ratio")
            overlayFile = "{}/{}/.overlay.cfg".format(recalboxFiles.OVERLAYS, system.Name)
            if os.path.isfile(overlayFile):
                # System global configuration
                userOverlayApplied = overlayFile
            else:
                overlayFile = "{}/.overlay.cfg".format(recalboxFiles.OVERLAYS)
                if os.path.isfile(overlayFile):
                    # All system global configuration
                    userOverlayApplied = overlayFile
            overlayFile = "{}/{}/{}.cfg".format(recalboxFiles.OVERLAYS, system.Name, romName)
            if os.path.isfile(overlayFile):
                # Rom file overlay
                userOverlayApplied = overlayFile
            else:
                overlayFile = "{}/{}/{}.cfg".format(recalboxFiles.OVERLAYS, system.Name, system.Name)
                if os.path.isfile(overlayFile):
                    # System overlay
                    userOverlayApplied = overlayFile
            if userOverlayApplied == "":
                # The recalbox overlays should be added only if
                # global.recalboxoverlays=1 or system.recalboxoverlays activated 
                # and no user overlays available from /recalbox/share/overlays
                # ratio = we can activate when ratio is not 16/9 and 16/10
                if system.Ratio not in ["16/9", "16/10"]:
                    # screen resolution that can support overlays are over 1.5 ratio (as it is float > 1.51)
                    from configgen.utils.videoMode import getCurrentResolution
                    screenWidth, screenHeight = getCurrentResolution()
                    Log("processOverlays - the resolution detected is " + str(screenWidth) + "x" + str(screenHeight))
                    Log("processOverlays - the ratio detected is " + str(screenWidth / screenHeight))
                    if ( screenWidth / screenHeight) > 1.51:
                        defaultOverlayFile = "{}/{}/{}.cfg".format(recalboxFiles.RECALBOX_OVERLAYS, system.Name,
                                                                   system.Name)
                        if os.path.isfile(defaultOverlayFile):
                            userOverlayApplied = defaultOverlayFile

        # ------------------ part to manage resize of overlay if necessary due to different size of screen ------------
        if userOverlayApplied != "":
            Log("part to manage resize of overlay if necessary due to different size of screen")
            userOverlayApplied = matchOverlaySizeToScreen(userOverlayApplied)
            Log("userOverlayApplied :" + userOverlayApplied)

        # ------------------ part to get shader file from shader set selected -----------------------------------------
        if shadersetIsUsingOverlay(recalboxOptions, system.Name):
            Log("part to get shader file from shader set selected")
            shaderFileUsingOverlay = getShaderFileFromShaderSetSelected(recalboxOptions, system.Name)
            Log("shaderFileUsingOverlay :" + shaderFileUsingOverlay)
            
        # ------------------ part to configure overlay settings (if shader using overlay) -----------------------
        #to manage new case using overlay with mega bezel shader "above overlay"
        if (userOverlayApplied != "") and (shaderFileUsingOverlay != ""):
            Log("part to configure overlay settings (if shader using overlay)")
            if os.path.isfile(shaderFileUsingOverlay):
                #copy existing overlay image to be used with shader
                output_png_file, custom_viewport_x, custom_viewport_y, custom_viewport_width, custom_viewport_height, video_fullscreen_x, video_fullscreen_y = getOverlayPictureFileAndData(userOverlayApplied)
                if os.path.isfile(output_png_file):
                    #save file used as background
                    #depreacted (we will update soon the  .slang file directly now)
                    shutil.copyfile(output_png_file, "/tmp/megabezel_backgroundimage.png")
                    
                    #enable mega bezel using "full" ratio and block rotation
                    enable_megabezel = keyValueSettings("{}/enable_megabezel.cfg".format(recalboxFiles.retroarchRoot), True)
                    
                    #aspect_ratio_index = "24" -> full size for mega bezel
                    #video_rotation = "0" -> to block rotation !!!
                    #video_allow_rotate = "false" -> important for mame/fbneo cores
                    enable_megabezel.setString('aspect_ratio_index', '"24"')
                    enable_megabezel.setString('video_rotation', '"0"')
                    enable_megabezel.setString('video_allow_rotate', '"false"')
                    #fbneo-vertical-mode = "disabled" -> to avoid problem of rotation with fbneo
                    #mame_rotation_mode = "internal" -> to avoid problem of rotation with mame
                    enable_megabezel.setString('fbneo-vertical-mode', '"disabled"')
                    enable_megabezel.setString('mame_rotation_mode', '"internal"')
                    
                    enable_megabezel.saveFile()
                    configs.append("{}/enable_megabezel.cfg".format(recalboxFiles.retroarchRoot))
                    
                    #write scale/offset to shader
                    megabezel_settings = keyValueSettings(shaderFileUsingOverlay, True)
                    megabezel_settings.loadFile(False)
                    
                    # Search base of overlay used (Is it 1080p or 720p ?)
                    if (custom_viewport_height <= 720) and (video_fullscreen_y == 720):
                        base_height = 720
                        base_width = 1280
                        bordersize = 60 #50 #70 #50 #36
                    else:
                        base_height = 1080
                        base_width = 1920
                        bordersize = 120 #100 #140 #100 #72
                    # border coverage in purcent to manage little error/shift from overlay details
                    # usually: 10% for naomi / 4% for nes / 10% for fbneo / 3% for gameboy advance / 2% for gameboy / 0% for gameboy color
                    bordercoverage = shaderBorderCoverage(recalboxOptions, system.Name) 
                    #[ NON-INTEGER SCALE PERCENT ]:
                    # Non-Integer Scale %
                    # If integer scale isn't used, this sets the vertical size of the vertical percentage of the full viewport. 
                    # The default is 82.97 which corresponds to an exact integer scale of 224p content
                    if custom_viewport_height == 0 :
                        non_integer_scale = float((100/base_height) * (base_height - bordersize) * (1 + float(bordercoverage/100)))
                    else:
                        non_integer_scale = float((100/base_height) * (custom_viewport_height - bordersize) * (1 + float(bordercoverage/100)))
                    megabezel_settings.setString('HSM_NON_INTEGER_SCALE','"' + str(non_integer_scale) + '"')
                    
                    #[ POSITION OFFSET ]:
                    # Position X - Moves the entire screen and frame left and right
                    # Position Y - Moves the entire screen and frame up and down
                    # in case of mega bezel shaders, we are already centralized by default
                    # we have to calculate the delta necessary versus the center
                    # and not the borders as in overlays
                    if (custom_viewport_y == 0) or (custom_viewport_height == 0) :
                        #if missing information, we can't calculate teh delta
                        position_y = 0
                    else:
                        #calculate center in Y from overlay information
                        overlay_center_y = int((custom_viewport_height)/2) + custom_viewport_y
                        position_y = base_height/2 - overlay_center_y 
                    megabezel_settings.setString('HSM_SCREEN_POSITION_Y','"' + str(position_y) + '"')
                    if (custom_viewport_x == 0) or (custom_viewport_width == 0):
                        #if missing information, we can't calculate teh delta
                        position_x = 0
                    else:
                        overlay_center_x = int((custom_viewport_width)/2) + custom_viewport_x
                        position_x = - (base_width/2 - overlay_center_x)
                    megabezel_settings.setString('HSM_SCREEN_POSITION_X','"' + str(position_x) + '"')
                    #save in selected shader
                    megabezel_settings.saveFile()
                    #reset overlay to avoid to use it after
                    userOverlayApplied = ""
        
        # ------------------ part to manage sinden lightgun border ----------------------------------------------
        if (needSindenBorder):
            Log("part to manage sinden lightgun border")
            #for the moment we use the system/rom name in /tmp directory to test it
            output_png_file = "" #init this file name
            if userOverlayApplied != "":
                #need to reuse overlay to add border
                #read and copy existing overlay in tmp
                root_overlay_path = userOverlayApplied
                
                ##loading root overlay (usually with "system" or "rom" .cfg as file name)...
                Log("root_overlay_path : " + root_overlay_path)
                root_overlay_file = keyValueSettings(root_overlay_path, True)
                root_overlay_file.loadFile(True)
                
                ##load overlay existing parameter or take default values for later...
                custom_viewport_height = int(root_overlay_file.getString("custom_viewport_height", "0").replace('"',''))
                custom_viewport_width = int(root_overlay_file.getString("custom_viewport_width", "0").replace('"',''))
                custom_viewport_x = int(root_overlay_file.getString("custom_viewport_x", "0").replace('"',''))
                custom_viewport_y = int(root_overlay_file.getString("custom_viewport_y", "0").replace('"',''))
                
                ##loading input overlay (usually with "system" or "rom" _overlay.cfg as file name)...
                input_overlay_path = root_overlay_file.getString("input_overlay","").replace('//','/').replace('"','')
                Log("input_overlay_path : " + input_overlay_path)
                if input_overlay_path != "":
                    input_overlay_file = keyValueSettings(input_overlay_path, True)
                    input_overlay_file.loadFile(True)
                    
                    ##load input overlay existing parameter or take default values for later...
                    Log("...nb overlay : " + input_overlay_file.getString("overlays", "0"))
                    nb_overlays = input_overlay_file.getString("overlays", "0")
                    Log("...overlay0_overlay : " + input_overlay_file.getString("overlay0_overlay", ""))
                    overlay_picture_file = os.path.dirname(input_overlay_path.replace('//','/').replace('"','')) + "/" + input_overlay_file.getString("overlay0_overlay", "").replace('"','')
                    Log("overlay_picture_file : " + str(overlay_picture_file))
                    
                    #Save overlay files (2 .cfg + .png) in tmp before updates
                    root_overlay_path = "/tmp/sinden.cfg"
                    Log("new tmp root_overlay_path : " + root_overlay_path)
                    input_overlay_path = "/tmp/sinden_overlay.cfg"
                    Log("new tmp input_overlay_path : " + input_overlay_path)
                    output_png_file = "/tmp/sinden.png"
                    Log("new tmp output_png_file : " + output_png_file)
                    #remove previous ones to be sure about update ;-)
                    os.system("rm " + root_overlay_path)
                    os.system("rm " + input_overlay_path)
                    os.system("rm " + output_png_file)
                    #save in new .cfg files
                    input_overlay_file.changeSettingsFile(input_overlay_path)
                    #set overlay 0 overlay
                    input_overlay_file.setString("overlay0_overlay", "sinden.png")
                    input_overlay_file.saveFile()
                    #set also the input overlay path with new value
                    root_overlay_file.setString("input_overlay", '"' + input_overlay_path + '"')
                    root_overlay_file.changeSettingsFile(root_overlay_path)
                    root_overlay_file.saveFile()
                else:
                    Log("Warning: No input overlay !")
            else:
                Log("Info: No overlay available / or used with Mega Bezel shaderset.")
                #create empty overlay from scratch
                root_overlay_path = "/tmp/sinden_border.cfg"
                Log("empty root_overlay_path : " + root_overlay_path)
                input_overlay_path = "/tmp/sinden_border_overlay.cfg"
                Log("empty input_overlay_path : " + input_overlay_path)
                output_png_file = "/tmp/sinden_border.png"
                Log("empty output_png_file : " + output_png_file)
                #remove previous ones to be sure about update ;-)
                os.system("rm " + root_overlay_path)
                os.system("rm " + input_overlay_path)
                os.system("rm " + output_png_file)
                
                #loading empty root overlay
                root_overlay_file = keyValueSettings(root_overlay_path, True)
                #set input overlay
                root_overlay_file.setString("input_overlay", '"' + input_overlay_path + '"')
                #set input overlay opacity
                root_overlay_file.setString("input_overlay_opacity", '"' + "1.0" + '"')
                #set video message position
                root_overlay_file.setString("video_message_pos_x", '"' + "0.133333333333333" + '"')
                root_overlay_file.setString("video_message_pos_y", '"' + "0.0638888888888889" + '"')
                root_overlay_file.saveFile()
                
                #loading empty root overlay
                input_overlay_file = keyValueSettings(input_overlay_path, True)
                #set overlay 0 descs
                input_overlay_file.setString("overlay0_descs", '0')
                #set overlay 0 full screen
                input_overlay_file.setString("overlay0_full_screen", 'true')
                #set overlay 0 overlay
                input_overlay_file.setString("overlay0_overlay", "sinden_border.png")
                #set overlays
                input_overlay_file.setString("overlays", '1')
                input_overlay_file.saveFile()
                #get targeted resolution
                targetedWidth, targetedHeight = getTargetedResolution(system.VideoMode)
                #create border only
                createTransparentOverlay(output_png_file, int(targetedWidth), int(targetedHeight))
                #in this case we will reuse the same file for input and output
                overlay_picture_file = output_png_file
            
            Log("output_png_file : " + output_png_file)
            if(output_png_file != ""):
                #generate border from overlay
                innerSize, outerSize = sindenBordersSize(recalboxOptions)
                borderSize = sindenBorderImage(overlay_picture_file, output_png_file, innerSize, outerSize, sindenBordersColor(recalboxOptions))
                # Border "tmp" file overlay
                userOverlayApplied = root_overlay_path

    # ----------- part to configure any overlay settings (if no shader using overlay / or specific overlay as for sinden) --------------------
    if (userOverlayApplied != ""):
        Log("part to configure any overlay settings (if no shader using overlay / or specific overlay as for sinden)")
        #if no sinden border necessary we could configure the overlay now in config
        configs.append(userOverlayApplied)
        #since retroarch v1.19.1 and v1.20.0 added in pixL, need to add parameter for overlays enabling: 'input_overlay_enable = "true"'
        overlay_file = keyValueSettings(userOverlayApplied, True)
        overlay_file.loadFile(True)
        enable_overlays = keyValueSettings("{}/enable_overlays.cfg".format(recalboxFiles.retroarchRoot), True)
        enable_overlays.setBool("input_overlay_enable", True)
        #check if aspect_ratio_index == 23 to cancel all default offsets if custom ratio is requested
        if(overlay_file.getInt("aspect_ratio_index", 0) == 23):
            #if yes, we have to set these offest to 0.0
            enable_overlays.setString("video_viewport_bias_x", "0.0")
            from configgen.utils.videoMode import GetHasVulkanSupport
            #strangly... we have to change video_viewport_bias_y in scaling options if we are in openGL or Vulkan
            if GetHasVulkanSupport() == '1':
                enable_overlays.setString("video_viewport_bias_y", "0.0")
            else:
                enable_overlays.setString("video_viewport_bias_y", "1.0")
        enable_overlays.saveFile()
        configs.append("{}/enable_overlays.cfg".format(recalboxFiles.retroarchRoot))

# HUD Overlay management
def processOverlays(system: Emulator, rom: str, recalboxOptions: keyValueSettings, env: dict, commandArray: List[str], needSindenBorder: bool = False, suffix: str = "", horizontalCropping: int = 0):
    # ----------------------------------------- for "usual" overlay --------------------------------------------------------------
    overlayFile = ""
    # User overlays
    userOverlayApplied = ""
    Log("processOverlays - rom : " + rom)
    romName = os.path.basename(rom)
    Log("processOverlays - romName : " + romName)
    root_ext = os.path.splitext(rom)
    romExtension = root_ext[1]
    Log("processOverlays - romExtension : " + romExtension)
    
    # If we are in crt mode, we only allow recalbox default 240p overlays
    if recalboxOptions.hasOption("system.crt"):
        if system.RecalboxOverlays:
            crtOverlayFile = "{}/{}/{}.cfg".format(recalboxFiles.RECALBOX_240P_OVERLAYS, system.Name,
                                                       system.Name)
            if os.path.isfile(crtOverlayFile):
                overlayFile = crtOverlayFile
    # Overlays are applied only when we are not in wide core
    else:
        if system.Core not in ["genesisplusgxwide"] and overlayIsActivated(recalboxOptions, system.Name):
            # User overlays
            overlayFile = "{}/{}/.overlay.cfg".format(recalboxFiles.OVERLAYS, system.Name)
            if os.path.isfile(overlayFile):
                # System global configuration
                userOverlayApplied = overlayFile
            else:
                overlayFile = "{}/.overlay.cfg".format(recalboxFiles.OVERLAYS)
                if os.path.isfile(overlayFile):
                    # All system global configuration
                    userOverlayApplied = overlayFile
                else:
                    overlayFile = "{}/{}/{}.cfg".format(recalboxFiles.OVERLAYS, system.Name, romName)
                    Log("processOverlays - overlayFile (from rom name) : " + overlayFile)
                    if os.path.isfile(overlayFile):
                        # Rom file overlay
                        userOverlayApplied = overlayFile
                    else:
                        #if not found, we retry if exist any without decoration using () or []
                        result = romName.split("(") # we search first ( to remove decoration using it   
                        result2 = result[0].split("[") # we search first [ to remove decoration using it
                        romNameWithoutDeco = result2[0].strip() # we use strip() to remove space at the begin and end of the string
                        romNameWithoutDeco = romNameWithoutDeco + romExtension # we add extension in this case also as done usually
                        overlayFile = "{}/{}/{}.cfg".format(recalboxFiles.OVERLAYS, system.Name, romNameWithoutDeco)
                        Log("processOverlays - overlayFile (from rom name without deco) : " + overlayFile)
                        if os.path.isfile(overlayFile):
                            # System overlay
                            userOverlayApplied = overlayFile
                        else:
                            overlayFile = "{}/{}/{}.cfg".format(recalboxFiles.OVERLAYS, system.Name, system.Name)
                            if os.path.isfile(overlayFile):
                                # System overlay
                                userOverlayApplied = overlayFile
            if userOverlayApplied == "":
                # The recalbox overlays should be added only if
                # global.recalboxoverlays=1 or system.recalboxoverlays activated 
                # and no user overlays available from /recalbox/share/overlays
                # ratio = we can activate when ratio is not 16/9 and 16/10
                # suffix = we can activate "alternatibe" overlay when suffix is not empty to manage specific cases
                if system.Ratio not in ["16/9", "16/10"] :
                    # screen resolution that can support overlays are over 1.5 ratio (as it is float > 1.51)
                    
                    from configgen.utils.videoMode import getCurrentResolution
                    screenWidth, screenHeight = getCurrentResolution()
                    Log("processOverlays - the resolution detected is " + str(screenWidth) + "x" + str(screenHeight))
                    Log("processOverlays - the ratio detected is " + str(screenWidth / screenHeight))
                    if ( (screenWidth / screenHeight) > 1.51):
                        defaultOverlayFile = "{}/{}/{}.cfg".format(recalboxFiles.RECALBOX_OVERLAYS, system.Name,
                                                                   system.Name)
                        if os.path.isfile(defaultOverlayFile):
                            userOverlayApplied = defaultOverlayFile
    Log("processOverlays - userOverlayApplied : " + userOverlayApplied)
    #to manage resize of overlay if necessary due to different size of screen or cropping (especially for standalone specific game screen ratio)
    if userOverlayApplied != "":
        Log(f"processOverlays - horizontalCropping: {horizontalCropping}")
        userOverlayApplied = matchOverlaySizeToScreen(userOverlayApplied, horizontalCropping)
    #init overlay Picture File string
    overlayPictureFile = ""
    if (userOverlayApplied != "") and (system.Ratio not in ["16/9", "16/10"]):
        # Load configuration
        configOverlay = keyValueSettings(userOverlayApplied, False)
        configOverlay.loadFile(True)
        overlayInputFile = configOverlay.getString("input_overlay", "").strip('"')
        Log("processOverlays - overlayInputFile : " + overlayInputFile)
        if os.path.isfile(overlayInputFile):
            Log("processOverlays - overlayInputFile exists !")
            # Load configuration
            configInputOverlay = keyValueSettings(overlayInputFile, False)
            configInputOverlay.loadFile(True)
            overlay0File = configInputOverlay.getString("overlay0_overlay", "").strip('"')
            Log("processOverlays - overlay0File : " + overlay0File)
            # to search directory of .cfg input overlay file to have it for picture to use as overlay
            cfgName = os.path.basename(overlayInputFile)
            Log("processOverlays - cfgName : " + cfgName)
            overlayPictureFile = overlayInputFile.replace(cfgName,'') + overlay0File
            Log("processOverlays - overlayPictureFile : " + overlayPictureFile)
    if needSindenBorder == True :
        #for the moment we use the system/rom name in /tmp directory to test it
        output_png_file = "" #init this file name
        if overlayPictureFile != "":
            #need to reuse overlay to add border
            #just overlay picture file .png in tmp to manage
            output_png_file = "/tmp/sinden.png"
            Log("new tmp output_png_file : " + output_png_file)
            #remove previous ones to be sure about update (including libretro ones ;-)
            os.system("rm " + output_png_file)
        else:
            Log("Info: No overlay available.")
            #create overlay from scratch
            output_png_file = "/tmp/sinden_border.png"
            Log("empty output_png_file : " + output_png_file)
            #remove previous ones to be sure about update ;-)
            os.system("rm " + output_png_file)
            #get targeted resolution
            targetedWidth, targetedHeight = getTargetedResolution(system.VideoMode)
            #create border only
            createTransparentOverlay(output_png_file, int(targetedWidth), int(targetedHeight))
            #in this case we will reuse the same file for input and output
            overlayPictureFile = output_png_file
        from configgen.utils.videoMode import getCurrentResolution
        screenWidth, screenHeight = getCurrentResolution()
        Log("processOverlays - the resolution detected is " + str(screenWidth) + "x" + str(screenHeight))
        Log("processOverlays - the ratio detected is " + str(screenWidth / screenHeight))
        Log("output_png_file : " + output_png_file)
        if(output_png_file != ""):
            #generate border from overlay
            innerSize, outerSize = sindenBordersSize(recalboxOptions)
            borderSize = sindenBorderImage(overlayPictureFile, output_png_file, innerSize, outerSize, sindenBordersColor(recalboxOptions))
            overlayPictureFile = output_png_file

    # ----------------------------------------- for "alternative" overlay using suffix--------------------------------------------------------------
    #init overlay Picture File string
    alternativeOverlayPictureFile = ""
    if suffix != "":
        overlayFile = ""
        # User overlays
        userOverlayApplied = ""
        Log("processOverlays - rom : " + rom)
        romName = os.path.basename(rom)
        Log("processOverlays - romName : " + romName)
        root_ext = os.path.splitext(rom)
        romExtension = root_ext[1]
        Log("processOverlays - romExtension : " + romExtension)
        
        # If we are in crt mode, we only allow recalbox default 240p overlays
        if recalboxOptions.hasOption("system.crt"):
            if system.RecalboxOverlays:
                crtOverlayFile = "{}/{}/{}{}.cfg".format(recalboxFiles.RECALBOX_240P_OVERLAYS, system.Name,
                                                           system.Name, suffix)
                if os.path.isfile(crtOverlayFile):
                    overlayFile = crtOverlayFile
        # Overlays are applied only when we are not in wide core
        else:
            if system.Core not in ["genesisplusgxwide"] and overlayIsActivated(recalboxOptions, system.Name):
                # User overlays
                overlayFile = "{}/{}/.overlay.cfg".format(recalboxFiles.OVERLAYS, system.Name)
                if os.path.isfile(overlayFile):
                    # System global configuration
                    userOverlayApplied = overlayFile
                else:
                    overlayFile = "{}/.overlay.cfg".format(recalboxFiles.OVERLAYS)
                    if os.path.isfile(overlayFile):
                        # All system global configuration
                        userOverlayApplied = overlayFile
                    else:
                        overlayFile = "{}/{}/{}{}.cfg".format(recalboxFiles.OVERLAYS, system.Name, romName, suffix)
                        Log("processOverlays - overlayFile (from rom name) : " + overlayFile)
                        if os.path.isfile(overlayFile):
                            # Rom file overlay
                            userOverlayApplied = overlayFile
                        else:
                            #if not found, we retry if exist any without decoration using () or []
                            result = romName.split("(") # we search first ( to remove decoration using it   
                            result2 = result[0].split("[") # we search first [ to remove decoration using it
                            romNameWithoutDeco = result2[0].strip() # we use strip() to remove space at the begin and end of the string
                            romNameWithoutDeco = romNameWithoutDeco + romExtension # we add extension in this case also as done usually
                            overlayFile = "{}/{}/{}{}.cfg".format(recalboxFiles.OVERLAYS, system.Name, romNameWithoutDeco, suffix)
                            Log("processOverlays - overlayFile (from rom name without deco) : " + overlayFile)
                            if os.path.isfile(overlayFile):
                                # System overlay
                                userOverlayApplied = overlayFile
                            else:
                                overlayFile = "{}/{}/{}{}.cfg".format(recalboxFiles.OVERLAYS, system.Name, system.Name, suffix)
                                if os.path.isfile(overlayFile):
                                    # System overlay
                                    userOverlayApplied = overlayFile
                if userOverlayApplied == "":
                    # The recalbox overlays should be added only if
                    # global.recalboxoverlays=1 or system.recalboxoverlays activated 
                    # and no user overlays available from /recalbox/share/overlays
                    # ratio = we can activate when ratio is not 16/9 and 16/10
                    # suffix = we can activate overlay when suffix is not empty to manage specific cases
                    if system.Ratio not in ["16/9", "16/10"] or suffix != "":
                        # screen resolution that can support overlays are over 1.5 ratio (as it is float > 1.51)
                        
                        from configgen.utils.videoMode import getCurrentResolution
                        screenWidth, screenHeight = getCurrentResolution()
                        Log("processOverlays - the resolution detected is " + str(screenWidth) + "x" + str(screenHeight))
                        Log("processOverlays - the ratio detected is " + str(screenWidth / screenHeight))
                        if ( (screenWidth / screenHeight) > 1.51) or suffix != "":
                            defaultOverlayFile = "{}/{}/{}{}.cfg".format(recalboxFiles.RECALBOX_OVERLAYS, system.Name,
                                                                       system.Name, suffix)
                            if os.path.isfile(defaultOverlayFile):
                                userOverlayApplied = defaultOverlayFile
        Log("processOverlays - userOverlayApplied : " + userOverlayApplied)
        #to manage resize of overlay if necessary due to different size of screen
        if userOverlayApplied != "":
            userOverlayApplied = matchOverlaySizeToScreen(userOverlayApplied)
        if (userOverlayApplied != "") and ((system.Ratio not in ["16/9", "16/10"]) or (suffix != "")):
            # Load configuration
            configOverlay = keyValueSettings(userOverlayApplied, False)
            configOverlay.loadFile(True)
            overlayInputFile = configOverlay.getString("input_overlay", "").strip('"')
            Log("processOverlays - overlayInputFile : " + overlayInputFile)
            if os.path.isfile(overlayInputFile):
                Log("processOverlays - overlayInputFile exists !")
                # Load configuration
                configInputOverlay = keyValueSettings(overlayInputFile, False)
                configInputOverlay.loadFile(True)
                overlay0File = configInputOverlay.getString("overlay0_overlay", "").strip('"')
                Log("processOverlays - overlay0File : " + overlay0File)
                # to search directory of .cfg input overlay file to have it for picture to use as overlay
                cfgName = os.path.basename(overlayInputFile)
                Log("processOverlays - cfgName : " + cfgName)
                alternativeOverlayPictureFile = overlayInputFile.replace(cfgName,'') + overlay0File
                Log("processOverlays - alternativeOverlayPictureFile : " + alternativeOverlayPictureFile)

        if needSindenBorder == True :
            #for the moment we use the system/rom name in /tmp directory to test it
            output_png_file = "" #init this file name
            if alternativeOverlayPictureFile != "":
                #need to reuse overlay to add border
                #just overlay picture file .png in tmp to manage
                output_png_file = "/tmp/alternative_sinden.png"
                Log("new tmp output_png_file : " + output_png_file)
                #remove previous ones to be sure about update (including libretro ones ;-)
                os.system("rm " + output_png_file)
            else:
                Log("Info: No overlay available.")
                #create overlay from scratch
                output_png_file = "/tmp/alternative_sinden_border.png"
                Log("empty output_png_file : " + output_png_file)
                #remove previous ones to be sure about update ;-)
                os.system("rm " + output_png_file)
                #get targeted resolution
                targetedWidth, targetedHeight = getTargetedResolution(system.VideoMode)
                #create border only
                createTransparentOverlay(output_png_file, int(targetedWidth), int(targetedHeight))
                #in this case we will reuse the same file for input and output
                alternativeOverlayPictureFile = output_png_file
            from configgen.utils.videoMode import getCurrentResolution
            screenWidth, screenHeight = getCurrentResolution()
            Log("processOverlays - the resolution detected is " + str(screenWidth) + "x" + str(screenHeight))
            Log("processOverlays - the ratio detected is " + str(screenWidth / screenHeight))
            Log("output_png_file : " + output_png_file)
            if(output_png_file != ""):
                #generate border from overlay
                innerSize, outerSize = sindenBordersSize(recalboxOptions)
                borderSize = sindenBorderImage(alternativeOverlayPictureFile, output_png_file, innerSize, outerSize, sindenBordersColor(recalboxOptions))
                alternativeOverlayPictureFile = output_png_file

    #init configuration string for mangoHUD
    configstr = ""
    nb_overlay = 0
    if overlayPictureFile != "":
        # configure string to store in mangohud config file if any picture overlay found
        configstr += f"background_image={overlayPictureFile}\n"
        nb_overlay += 1

    if alternativeOverlayPictureFile != "":
        # configure string to store in mangohud config file if any picture overlay found
        configstr += f"alternative_background_image={alternativeOverlayPictureFile}\n"
        nb_overlay += 1
        
    if nb_overlay != 0:
        # configure string to store in mangohud config file if any picture overlay found
        configstr += f"legacy_layout=false\n"
    
    if recalboxOptions.getBool("system.monitoring.enabled", False): # monitoring to display stats and monitor GPU/CPU
        if recalboxOptions.getString("system.monitoring.custom", "") != "":
            configstr += recalboxOptions.getString("system.monitoring.custom","").replace("\\n", "\n")
        else:
            #positions
            monitoring_position = recalboxOptions.getString("system.monitoring.position", "top-left")
            # possible values:
            #   "bottom-left"
            #   "top-left"
            #   "top-right"
            #   "bottom-right"
            configstr += "position=" + monitoring_position + "\nbackground_alpha=0.5\nlegacy_layout=false\nround_corners=10"
            configstr += "\nfps\nfps_color_change\nfps_value=30,60\nfps_color=B22222,FDFD09,39F900"
            configstr += "\nengine_version\nvulkan_driver\narch"
            configstr += "\ncpu_stats\ncpu_temp\ncpu_power\ncpu_mhz\ncpu_load_change\ncpu_load_color=39F900,FDFD09,B22222\nram"
            
            #this following line is really significant for nvidia native drivers for the moment
            configstr += "\ngpu_stats\ngpu_temp\ngpu_core_clock\ngpu_mem_temp\ngpu_mem_clock\ngpu_power\ngpu_load_change\ngpu_load_color=39F900,FDFD09,B22222\nvram"
            
            configstr += "\nresolution"
            configstr += "\nfont_size=22\ncustom_text=%GAMENAME%\ncustom_text=%SYSTEMNAME%\ncustom_text=%EMULATORCORE%"
            #RFU
            #configstr += "\nimage_max_width=200\nimage=%THUMBNAIL%"
            
        #for custom or standard monitoring information
        configstr = configstr.replace("%SYSTEMNAME%", "System: " + system.Name)
        configstr = configstr.replace("%GAMENAME%", "Rom: " + romName)
        configstr = configstr.replace("%EMULATORCORE%", "Emulator: " + system.Emulator)
        #RFU
        #configstr = configstr.replace("%THUMBNAIL%", "")
    elif configstr != "": #default mode using overlay only
        configstr += "background_alpha=0\n" # hide the background
        #if 2 overlays to manage and switch
        
    #if only 1 overlay to manage 
    if nb_overlay == 1:         
        configstr += "toggle_hud=F10\n" # hide/unhide the background (to be able to deactivate overlay temporary if needed)
        if overlayPictureFile != "": 
          configstr += "no_display=0\n" # to display "usual" overlay at start (could be hide by F10)
          configstr += "switch_overlay=0\n" # to display "usual" overlay at start (could be switch by F10)
        else:
          configstr += "no_display=1\n" # to hide "alternative" overlay at start (as for split screen in games)
          configstr += "switch_overlay=1\n" # to display "alternative" overlay at first (because unique ;-)

    #if 2 overlays to manage and switch
    if nb_overlay == 2:
        configstr += "toggle_switch_overlay=F10\n" # hide/unhide the background (to be able to deactivate overlay temporary if needed)
        configstr += "switch_overlay=0\n" # to display "usual" overlay at start (could be switch by F10)

    if configstr != "": #only if any configuration string has been created, we could add mangohud command
        hudconfig = configstr
        with open('/var/run/hud.config', 'w') as f:
            f.write(hudconfig)
        env["MANGOHUD_CONFIGFILE"] = "/var/run/hud.config"
        
        env["MANGOHUD_DLSYM"] = "1"
        commandArray.insert(0, "mangohud")

# Model2emu Overlay management
def processModel2emuOverlays(system: Emulator, rom: str, recalboxOptions: keyValueSettings, needSindenBorder: bool = False)-> bool:
    overlayFile = ""
    # User overlays
    userOverlayApplied = ""
    Log("processOverlays - rom : " + rom)
    romName = os.path.basename(rom)
    Log("processOverlays - romName : " + romName)
    root_ext = os.path.splitext(rom)
    romExtension = root_ext[1]
    Log("processOverlays - romExtension : " + romExtension)
    from configgen.utils.videoMode import getCurrentResolution
    screenWidth, screenHeight = getCurrentResolution()
    
    # If we are in crt mode, we only allow recalbox default 240p overlays
    if recalboxOptions.hasOption("system.crt"):
        if system.RecalboxOverlays:
            crtOverlayFile = "{}/{}/{}.cfg".format(recalboxFiles.RECALBOX_240P_OVERLAYS, system.Name,
                                                       system.Name)
            if os.path.isfile(crtOverlayFile):
                overlayFile = crtOverlayFile
    # Overlays are applied only when we are not in wide core
    else:
        if system.Core not in ["genesisplusgxwide"] and overlayIsActivated(recalboxOptions, system.Name):
            # User overlays
            overlayFile = "{}/{}/.overlay.cfg".format(recalboxFiles.OVERLAYS, system.Name)
            if os.path.isfile(overlayFile):
                # System global configuration
                userOverlayApplied = overlayFile
            else:
                overlayFile = "{}/.overlay.cfg".format(recalboxFiles.OVERLAYS)
                if os.path.isfile(overlayFile):
                    # All system global configuration
                    userOverlayApplied = overlayFile
                else:
                    overlayFile = "{}/{}/{}.cfg".format(recalboxFiles.OVERLAYS, system.Name, romName)
                    Log("processOverlays - overlayFile (from rom name) : " + overlayFile)
                    if os.path.isfile(overlayFile):
                        # Rom file overlay
                        userOverlayApplied = overlayFile
                    else:
                        #if not found, we retry if exist any without decoration using () or []
                        result = romName.split("(") # we search first ( to remove decoration using it   
                        result2 = result[0].split("[") # we search first [ to remove decoration using it
                        romNameWithoutDeco = result2[0].strip() # we use strip() to remove space at the begin and end of the string
                        romNameWithoutDeco = romNameWithoutDeco + romExtension # we add extension in this case also as done usually
                        overlayFile = "{}/{}/{}.cfg".format(recalboxFiles.OVERLAYS, system.Name, romNameWithoutDeco)
                        Log("processOverlays - overlayFile (from rom name without deco) : " + overlayFile)
                        if os.path.isfile(overlayFile):
                            # System overlay
                            userOverlayApplied = overlayFile
                        else:
                            overlayFile = "{}/{}/{}.cfg".format(recalboxFiles.OVERLAYS, system.Name, system.Name)
                            if os.path.isfile(overlayFile):
                                # System overlay
                                userOverlayApplied = overlayFile
            if userOverlayApplied == "":
                # The recalbox overlays should be added only if
                # global.recalboxoverlays=1 or system.recalboxoverlays activated 
                # and no user overlays available from /recalbox/share/overlays
                # ratio = we can activate when ratio is not 16/9 and 16/10
                overlayFile = ""
                # ratio = we can activate when ratio is not 16/9 and 16/10
                if system.Ratio not in ["16/9", "16/10"]:
                    # screen resolution that can support overlays are over 1.5 ratio (as it is float > 1.51)
                    Log("processOverlays - the resolution detected is " + str(screenWidth) + "x" + str(screenHeight))
                    Log("processOverlays - the ratio detected is " + str(screenWidth / screenHeight))
                    if ( screenWidth / screenHeight) > 1.51:
                        defaultOverlayFile = "{}/{}/{}.cfg".format(recalboxFiles.RECALBOX_OVERLAYS, system.Name,
                                                                   system.Name)
                        if os.path.isfile(defaultOverlayFile):
                            overlayFile = defaultOverlayFile
    Log("processOverlays - overlayFile : " + overlayFile)
    #init overlay Picture File string
    overlayPictureFile = ""
    if (overlayFile != "") and (system.Ratio not in ["16/9", "16/10"]):
        #to manage resize of overlay if necessary due to different size of screen
        if overlayFile != "":
            overlayFile = matchOverlaySizeToScreen(overlayFile)
        # Load configuration
        configOverlay = keyValueSettings(overlayFile, False)
        configOverlay.loadFile(True)
        overlayInputFile = configOverlay.getString("input_overlay", "").strip('"')
        Log("processOverlays - overlayInputFile : " + overlayInputFile)
        if os.path.isfile(overlayInputFile):
            Log("processOverlays - overlayInputFile exists !")
            # Load configuration
            configInputOverlay = keyValueSettings(overlayInputFile, False)
            configInputOverlay.loadFile(True)
            overlay0File = configInputOverlay.getString("overlay0_overlay", "").strip('"')
            Log("processOverlays - overlay0File : " + overlay0File)
            # to search directory of .cfg input overlay file to have it for picture to use as overlay
            cfgName = os.path.basename(overlayInputFile)
            Log("processOverlays - cfgName : " + cfgName)
            overlayPictureFile = overlayInputFile.replace(cfgName,'') + overlay0File
            Log("processOverlays - overlayPictureFile : " + overlayPictureFile)
            
    if needSindenBorder == True :
        #for the moment we use the system/rom name in /tmp directory to test it
        output_png_file = "" #init this file name
        if overlayPictureFile != "":
            #need to reuse overlay to add border
            #just overlay picture file .png in tmp to manage
            output_png_file = "/tmp/sinden.png"
            Log("new tmp output_png_file : " + output_png_file)
            #remove previous ones to be sure about update (including libretro ones ;-)
            os.system("rm " + output_png_file)
        else:
            Log("Info: No overlay available.")
            #create overlay from scratch
            output_png_file = "/tmp/sinden_border.png"
            Log("empty output_png_file : " + output_png_file)
            #remove previous ones to be sure about update ;-)
            os.system("rm " + output_png_file)
            #get targeted resolution
            targetedWidth, targetedHeight = getTargetedResolution(system.VideoMode)
            #create border only
            createTransparentOverlay(output_png_file, int(targetedWidth), int(targetedHeight))
            #in this case we will reuse the same file for input and output
            overlayPictureFile = output_png_file
        Log("processOverlays - the resolution detected is " + str(screenWidth) + "x" + str(screenHeight))
        Log("processOverlays - the ratio detected is " + str(screenWidth / screenHeight))
        Log("output_png_file : " + output_png_file)
        if(output_png_file != ""):
            #generate border from overlay
            innerSize, outerSize = sindenBordersSize(recalboxOptions)
            borderSize = sindenBorderImage(overlayPictureFile, output_png_file, innerSize, outerSize, sindenBordersColor(recalboxOptions))
            overlayPictureFile = output_png_file

    #prepare overlays now for .lua script for model2emu
    output_png = "/tmp/model2emu_overlay.png"
    if overlayPictureFile != "":
        #create specific "surface iamge" for model2emu overlay
        model2emuSurfaceImage(overlayPictureFile, output_png, screenWidth, screenHeight)
        return True
    else:
        # Delete overlay generated previously in /tmp directory to avoid to be found by Model2emu .lua script
        os.system("rm /tmp/model2emu_overlay.png")
        return False

def model2emuSurfaceImage(input_png, output_png, w, h):
    # we need to do an image with the overlay inside but reduced in a specific size in an empty transparent image 
    # but don't ask me why ;-), I just did reverse engineeting of overlay prodivded by sinden lightgun wiki.
    
    # At the beginning, we created a transparent .png file with 1920x1080
    # and we will include the input overlay (with or without sinden border calculated)
    # but with a resize in 1800 x 571
    
    # now we could manage several resolutions as after:
    # 16/9 RESOLUTIONS	TOTAL PIXELS	DESIGNATION
    # 1280 x 720	    921,600  	    HD (720p)
    # 1920 x 1080	    2,073,600	    FHD / Full HD (1080p)
    # 2560 x 1440	    3,686,400	    WQHD
    # 3200 x 1800	    5,760,000	    QHD+
    # 3840 x 2160	    8,294,400	    4K UHD

    #size tested for the moment in model2emu that could be used to calculate the ratio
    rw = 1920
    rh = 1080
    #overlay targeted size
    rtw = 1800
    rth= 571
    #new size caculated from resolution w,h and from ratio identified in case of 1920*1080
    tw = int(w * (rtw/rw)) # ratio seems 0,9375
    th = int(h * (rth/rh)) # ratio seems 0,5287
    
    # resize existing overlay fist using tmp file TO DO: do it in memory if needed
    output_tmp_png = "/tmp/overlay_resized.png"
    resizeImage(input_png, output_tmp_png, tw, th, True) #for full HD: 1920x1080 as fixed for this emulator
    #open tmp file just resized
    output_tmp_image = Image.open(output_tmp_png)
    #create empty image in full full hd or ultra hd
    if(h <= 1080):
        #use image of 2K to be compatible with resolutions from 720p to 1080p
        imgnew = Image.new("RGBA", (1920,1080), (0,0,0,255))
    else:
        #use image of 4K to be compatible with resolutions from 1800 to 4k
        imgnew = Image.new("RGBA", (3840,2160), (0,0,0,255))
    
    #copy overlay resized in empty image in full hd
    imgnew.paste(output_tmp_image, (0,0,tw,th))
    #save it to finish
    imgnew.save(output_png, mode="RGBA", format="PNG")

def alphaPaste(input_png, output_png, fillcolor, screensize, bezel_stretch):
  imgin = Image.open(input_png)
  # TheBezelProject have Palette + alpha, not RGBA. PIL can't convert from P+A to RGBA.
  # Even if it can load P+A, it can't save P+A as PNG. So we have to recreate a new image to adapt it.
  if not 'transparency' in imgin.info:
      raise Exception("no transparent pixels in the image, abort")
  alpha = imgin.split()[-1]  # alpha from original palette + alpha
  ix,iy = fast_image_size(input_png)
  sx,sy = screensize
  i_ratio = (float(ix) / float(iy))
  s_ratio = (float(sx) / float(sy))

  if (i_ratio - s_ratio > 0.01):
      # cut off bezel sides for 16:10 screens
      new_x = int(ix*s_ratio/i_ratio)
      delta = int(ix-new_x)
      borderx = delta//2
      ix = new_x
      alpha_new = alpha.crop((borderx, 0, new_x+borderx, iy))
      alpha = alpha_new

  imgnew = Image.new("RGBA", (ix,iy), (0,0,0,255))
  imgnew.paste(alpha, (0,0,ix,iy))
  if bezel_stretch:
      imgout = ImageOps.fit(imgnew, screensize)
  else:
      imgout = ImageOps.pad(imgnew, screensize, color=fillcolor, centering=(0.5,0.5))
  imgout.save(output_png, mode="RGBA", format="PNG")

def sindenBordersSize(recalboxOptions: keyValueSettings) -> (float, float):
    bordersSize = recalboxOptions.getString("lightgun.sinden.bordersize","superthin")
    if bordersSize == "crt":
        return -1, -1
    if bordersSize == "superthin":
        return 0.5, 0
    if bordersSize == "thin":
        return 1, 0
    if bordersSize == "medium":
        return 2, 0
    if bordersSize == "big":
        return 2, 1
    return 0.5, 0 # as superthin by default

def sindenBorderImage(input_png, output_png, innerBorderSizePer = 2, outerBorderSizePer = 3, innerBorderColor = "#ffffff", outerBorderColor = "#000000"):
    # good default border that works in most circumstances is:
    # 
    # 2% of the screen width in white.  Surrounded by 3% screen width of
    # black.  I have attached an example.  The black helps the lightgun detect
    # the border against a bright background behind the tv.
    # 
    # The ideal solution is to draw the games inside the border rather than
    # overlap.  Then you can see the whole game.  The lightgun thinks that the
    # outer edge of the border is the edge of the game screen.  So you have to
    # make some adjustments in the lightgun settings to keep it aligned.  This
    # is why normally the border overlaps as it means that people do not need
    # to calculate an adjustment and is therefore easier.
    # 
    # If all the games are drawn with the border this way then the settings
    # are static and the adjustment only needs to be calculated once.

    w,h = fast_image_size(input_png)

    # outer border
    outerBorderSize = h * outerBorderSizePer // 100 # use only h to have homogen border size
    if outerBorderSize < 1: # minimal size
        outerBorderSize = 1
    outerShapes = [ [(0, 0), (w, outerBorderSize)], [(w-outerBorderSize, 0), (w, h)], [(0, h-outerBorderSize), (w, h)], [(0, 0), (outerBorderSize, h)] ]

    # inner border
    innerBorderSize = w * innerBorderSizePer // 100 # use only h to have homogen border size
    if innerBorderSize < 1: # minimal size
        innerBorderSize = 1
    innerShapes = [ [(outerBorderSize, outerBorderSize), (w-outerBorderSize, outerBorderSize+innerBorderSize)],
                    [(w-outerBorderSize-innerBorderSize, outerBorderSize), (w-outerBorderSize, h-outerBorderSize)],
                    [(outerBorderSize, h-outerBorderSize-innerBorderSize), (w-outerBorderSize, h-outerBorderSize)],
                    [(outerBorderSize, outerBorderSize), (outerBorderSize+innerBorderSize, h-outerBorderSize)] ]
    
    back = Image.open(input_png)
    imgnew = Image.new("RGBA", (w,h), (0,0,0,255))
    imgnew.paste(back, (0,0,w,h))
    imgnewdraw = ImageDraw.Draw(imgnew)
    for shape in outerShapes:
        imgnewdraw.rectangle(shape, fill=outerBorderColor)
    for shape in innerShapes:
        imgnewdraw.rectangle(shape, fill=innerBorderColor)
    imgnew.save(output_png, mode="RGBA", format="PNG")

    return outerBorderSize + innerBorderSize

def sindenBorderSize(w, h, innerBorderSizePer = 2, outerBorderSizePer = 3):
    return (h * (innerBorderSizePer + outerBorderSizePer)) // 100

def sindenBordersColor(recalboxOptions: keyValueSettings) -> str:
    color = recalboxOptions.getString("lightgun.sinden.bordercolor","white")
    if color == "red":
        return "#ff0000"
    if color == "green":
        return "#00ff00"
    if color == "blue":
        return "#0000ff"
    if color == "white":
        return "#ffffff"
    return "#ffffff"

def createTransparentOverlay(output_png, width, height):
    imgnew = Image.new("RGBA", (width,height), (0,0,0,0))
    imgnewdraw = ImageDraw.Draw(imgnew)
    imgnew.save(output_png, mode="RGBA", format="PNG")
