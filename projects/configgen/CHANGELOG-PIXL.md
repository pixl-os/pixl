# Change Log
All notable changes to this project will be documented in this file (focus on change done on PIXL branch).

## [pixL] - Beta 27 - 2022-07-09
- Introduction "Evmapy.py" and added in "emulatorlauncher.py" :
	- change logguer,  way to import Evmapy and start to update pad functions
	- updates to be compatible with recal inputs and change Log function
	- fix issue with some axis id not well manage
	- add capacity to have file .keys by emulator and/or core as for dolphin
	- fix bad controls in dolphin-triforce

- Dolphin-triforce :
	- activation and rework of controllers part
	- fixes on controllers input types
	- force widescreen hack on ratio to 16/9
	- add load save state automatically if needed using xdotool (wait 7 s before to load save state by F1)
	- add cleaning of "card" in case of one game

- Dolphin :
	- change to manage better languages and also for SYSCONF
	- for Wii now, we update the SYSCONF for video ratio and language
	- add comments and logs function
	- remove 2 hotkeys to be managed by evmapy
	- add PauseOnFocusLost set to true to prepare windows switching
	- add F10 game pause key and using automation using new table to manage keyboard keys linked to player1 hotkeys
	- fix buttons position on gamecube for Z/L/R
	- fix to correct way to select language for wii & gamecube
	- fix buttons mapping for B/2 buttons on wii
	- add F8/F9 as key to change direction of emulated wiimote (that are 2 toggles in fact)
	- fix for gamecube to replicate L/R to L-Analog & R-Analog

- Xemu:
    - move initial hdd file only for selected and used system
    - add languages management for xbox

- Pcsx2:
    - new pad order management and remove usage of PAD.ini

## [pixL] - Beta 26 - 2022-05-30
- "Libretro" Lightgun:
    - recover original lightgun project (due to bug from 1.7 updates)
    - creation of 1.6.7 version to be compatible 'simply' with retroarch 1.9.8 indexing
    - compatible python 3
    - restoring of original xml configuration file
    - some fixes on emulator/core objects + get/setOption changed since recal 8.0 (updated in 1.6.8)
- Pcsx2:
   - new bump
   - fix on options
   - fix .config settings for PCSX2_reg.ini only for skip wizzard
- hypseus: 
   - rework hypseus after bump
- xbox/chihiro:
   - introduction new configgen for microsoft xbox
   - rework for latest python3
   - fix bios path
   - fix after new bump
   - Add sega chihiro system
- triforce(alpha version):
   - introduction new configgen for triforce arcade
   - rework many options and generator

## [pixL] - Beta 22 - 2022-04-21
- revert flycast fix: to forced left analog on dpad mode for Atomiswave, Naomi and NaomiGD

## [pixL] - Beta 19 - 2021-12-30
- citra: add files to rework it for new configgen objects since python 3.9 new architecture
- citra: update setup.py to add this generator
- fix flycast: to forced left analog on dpad mode for Atomiswave, Naomi and NaomiGD
- creation of new CHANGELOG-PIXL.md

## [RETRO-X] - Beta 12 - 2021-05-25 (extract from CHANGELOG-RETRO-X.md)
- citra-emu introduction:
    - init configgen for citra-emu
    - add auto language in NAND Config By Littlebalup
    - fix typo in emulateur launcher and and citra in setup
    - rework generator for many changes in configgen
    - fix controllers L1 and R1

